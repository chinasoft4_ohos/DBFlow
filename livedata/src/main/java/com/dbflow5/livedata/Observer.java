package com.dbflow5.livedata;

public interface Observer<T>  {
    void onChanged(T t);
}
