package com.dbflow5.contentprovider.annotation;

public enum NotifyMethod {
    INSERT,
    UPDATE,
    DELETE
}
