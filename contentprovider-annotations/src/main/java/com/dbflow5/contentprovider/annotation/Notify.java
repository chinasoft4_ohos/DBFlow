package com.dbflow5.contentprovider.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: Annotates a method part of [com.dbflow5.annotation.provider.TableEndpoint]
 * that gets called back when changed. The method must return a Uri or an array of Uri[] to notify changed on
 * the content provider.
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface Notify{
    /**
     * @return The [com.dbflow5.annotation.provider.Notify.Method] notify
     */
    NotifyMethod notifyMethod();
    /**
     * @return Registers itself for the following paths. If a specific path is called for the specified
     * method, the method this annotation corresponds to will be called.
     */
    String[] paths() default  {};
}
