package com.dbflow5.contentprovider.annotation;

public @interface PathSegment {
    /**
     * @return The number segment this corresponds to.
     */
    int segment() default 0;
    /**
     * @return The column name that this segment will use.
     */
    String column() default "";
}
