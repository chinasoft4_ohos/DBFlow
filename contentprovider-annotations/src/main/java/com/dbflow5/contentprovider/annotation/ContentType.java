package com.dbflow5.contentprovider.annotation;

public class ContentType {
    public static final String VND_MULTIPLE = "vnd.ohos.cursor.dir/";

    public static final String VND_SINGLE = "vnd.ohos.cursor.item/";
}
