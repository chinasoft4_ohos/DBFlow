package com.dbflow5.contentprovider.annotation;

/**
 * Description: Defines the URI for a content provider.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.SOURCE)
public @interface ContentUri{
    /**
     * @return the path of this ContentUri. ex: notes/#, notes/1, etc. Must be unique within a [TableEndpoint]
     */
    String path();
    /**
     * @return The type of content that this uri is associated with. Ex: [ContentType.VND_SINGLE]
     */
    String type();
    /**
     * @return If the path defines "#", then we use these numbers to find them in the same order as
     * where column.
     */
    PathSegment[] segments() default  {};
    /**
     * @return false if you wish to not allow queries from the specified URI.
     */
    boolean queryEnabled() default true;
    /**
     * @return false if you wish to prevent inserts.
     */
    boolean insertEnabled() default true;
    /**
     * @return false if you wish to prevent deletion.
     */
    boolean deleteEnabled() default true;
    /**
     * @return false if you wish to prevent updates.
     */
    boolean updateEnabled() default true;
}

