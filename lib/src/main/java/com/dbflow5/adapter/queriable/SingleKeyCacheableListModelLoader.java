package com.dbflow5.adapter.queriable;

import com.dbflow5.adapter.CacheAdapter;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.cache.ModelCache;

import java.util.ArrayList;

/**
 * Description:
 */
public class SingleKeyCacheableListModelLoader<T> extends CacheableListModelLoader<T>{

    public SingleKeyCacheableListModelLoader(Class<T> tModelClass, CacheAdapter<T> cacheAdapter){
        super(tModelClass, cacheAdapter);
    }

    @Override
    public ArrayList<T> convertToData(FlowCursor cursor, DatabaseWrapper databaseWrapper) {
        ArrayList<T> data = new ArrayList<>();
        Object cacheValue;
        // Ensure that we aren't iterating over this cursor concurrently from different threads
        if (cursor.goToFirstRow()) {
            do {
                cacheValue = cacheAdapter.getCachingColumnValueFromCursor(cursor);
                T model = ModelCache.addOrReload(modelCache, cacheValue, cacheAdapter, modelAdapter, cursor, databaseWrapper);
                data.add(model);
            } while (cursor.goToNextRow());
        }
        return data;
    }
}
