package com.dbflow5.adapter.queriable;

import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;

import java.util.ArrayList;

/**
 * Description: Loads a [List] of [T].
 */
public class ListModelLoader<T> extends ModelLoader<T, ArrayList<T>>{

    public ListModelLoader(Class<T> modelClass){
        super(modelClass);
    }

    @Override
    public ArrayList<T> convertToData(FlowCursor cursor, DatabaseWrapper databaseWrapper) {
        ArrayList<T> retData = new ArrayList<>();
        if (cursor.goToFirstRow()) {
            do {
                retData.add(getInstanceAdapter().loadFromCursor(cursor, databaseWrapper));
            } while (cursor.goToNextRow());
        }
        return retData;
    }
}
