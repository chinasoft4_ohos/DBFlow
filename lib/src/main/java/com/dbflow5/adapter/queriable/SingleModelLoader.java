package com.dbflow5.adapter.queriable;

import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;

/**
 * Description: Responsible for loading data into a single object.
 */
public class SingleModelLoader<T> extends ModelLoader<T, T>{

    public SingleModelLoader(Class<T> modelClass){
        super(modelClass);
    }

    @Override
    public T convertToData(FlowCursor cursor, DatabaseWrapper databaseWrapper) {
        return convertToData(cursor, true, databaseWrapper);
    }

    public T convertToData(FlowCursor cursor, boolean moveToFirst, DatabaseWrapper databaseWrapper){
        if (!moveToFirst || cursor.goToFirstRow()) {
            return getInstanceAdapter().loadFromCursor(cursor, databaseWrapper);
        } else {
            return null;
        }
    }
}
