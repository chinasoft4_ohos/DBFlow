package com.dbflow5.adapter;

import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;

/**
 * Description: Provides a set of methods for creating a DB object.
 */
public interface CreationAdapter {
    /**
     * The query used to create this table.
     *
     * @return The query used to create this table.
     */
    String getCreationQuery();

    String getName();

    ObjectType getType();

    /**
     * When false, this table gets generated and associated with database
     *
     * @return When false, this table gets generated and associated with database, however it will not immediately
     * get created upon startup. This is useful for keeping around legacy tables for migrations.
     */
    default boolean createWithDatabase() {
        return true;
    }

    /**
     * Runs the creation query on the DB.
     * @param wrapper wrapper
     */
    default void createIfNotExists(DatabaseWrapper wrapper) {
        DatabaseStatement statement = wrapper.compileStatement(getCreationQuery());
        statement.execute();
    }

    /**
     * Drops the table by running a drop query.
     * @param wrapper wrapper
     * @param ifExists ifExists
     */
    default void drop(DatabaseWrapper wrapper, boolean ifExists) {
        String exist = "";
        if (ifExists) {
            exist = "IF EXISTS";
        }
        String sql = "DROP " + getType().value + " " + getName() + " " + exist + ";";
        DatabaseStatement statement = wrapper.compileStatement(sql);
        statement.execute();
    }

}


