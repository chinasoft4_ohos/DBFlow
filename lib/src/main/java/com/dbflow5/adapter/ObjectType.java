package com.dbflow5.adapter;

public enum ObjectType {
    View("VIEW"),
    Table("TABLE");

    String value;

    ObjectType(String value) {
        this.value = value;
    }
}