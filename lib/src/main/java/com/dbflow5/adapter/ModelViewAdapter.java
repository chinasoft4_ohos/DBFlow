package com.dbflow5.adapter;

import com.dbflow5.config.DBFlowDatabase;

/**
 * Description: The base class for a [T] adapter that defines how it interacts with the DB.
 */
public abstract class ModelViewAdapter<T> extends RetrievalAdapter<T> implements CreationAdapter {
    public ModelViewAdapter(DBFlowDatabase databaseDefinition) {
        super(databaseDefinition);
    }
}

