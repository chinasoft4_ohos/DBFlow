package com.dbflow5.transaction;

import com.dbflow5.database.DatabaseWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Description: Wraps multiple transactions together.
 */
public class TransactionWrapper implements ITransaction<Object> {

    private List<ITransaction<Object>> transactions = new ArrayList<>();

    public TransactionWrapper(ITransaction<Object>... transactions){
        this.transactions.addAll(Arrays.asList(transactions));
    }

    public TransactionWrapper(Collection<ITransaction<Object>> transactions){
        this.transactions.addAll(transactions);
    }

    @Override
    public Object execute(DatabaseWrapper databaseWrapper) {
        transactions.forEach(it -> {
            it.execute(databaseWrapper);
        });
        return null;
    }
}
