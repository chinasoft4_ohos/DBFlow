package com.dbflow5.transaction;

import com.dbflow5.config.DBFlowDatabase;

/**
 * Description: This class manages batch database interactions. Places DB operations onto the same Thread.
 */
public class DefaultTransactionManager extends BaseTransactionManager {

    public DefaultTransactionManager(DBFlowDatabase databaseDefinition){
        super(new DefaultTransactionQueue("DBFlow Transaction Queue"), databaseDefinition);
    }

    public DefaultTransactionManager(ITransactionQueue transactionQueue, DBFlowDatabase databaseDefinition){
        super(transactionQueue, databaseDefinition);
    }

}
