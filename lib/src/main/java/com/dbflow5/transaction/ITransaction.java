package com.dbflow5.transaction;

import com.dbflow5.database.DatabaseWrapper;

/**
 * Description: Simplest form of a transaction. It represents an interface by which code is executed
 * inside a database transaction.
 */
public interface ITransaction<R> {

    /**
     * Called within a database transaction.
     *
     * @param databaseWrapper The database to save data into. Use this access to operate on the DB
     * without causing an Ohos SQLiteDatabaseLockedException or other problems due to locking.
     * @return <R>
     */
    R execute(DatabaseWrapper databaseWrapper);
}
