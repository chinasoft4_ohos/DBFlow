package com.dbflow5.structure;

import com.dbflow5.config.FlowManager;
import com.dbflow5.query.ModelQueriable;

import java.util.List;
import java.util.function.Function;

/**
 * Description: Wraps a [OneToMany] annotation getter into a concise property setter.
 */
public class OneToMany<T>{
//private val query: () -> ModelQueriable<T>

    private final Function<Void, ModelQueriable<T>> query;

    private List<T> list = null;

    public OneToMany(Function<Void, ModelQueriable<T>> query){
        this.query = query;
    }

    public List<T> getValue(Class<T> clazz){
        if (list != null && list.isEmpty()) {
            list = query.apply(null).queryList(FlowManager.getDatabaseForTable(clazz));
        }
        return list;
    }

    public void setValue(List<T> value) {
        list = value;
    }

    public static <T> OneToMany<T> oneToMany(Function<Void, ModelQueriable<T>> query){
        return new OneToMany<T>(query);
    }
}