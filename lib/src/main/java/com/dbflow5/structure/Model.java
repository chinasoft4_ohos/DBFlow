package com.dbflow5.structure;

import com.dbflow5.config.FlowManager;
import com.dbflow5.database.DatabaseWrapper;
import ohos.aafwk.ability.DataAbilityRemoteException;

public interface Model extends ReadOnlyModel {

    /**
     * Returned when [.insert] occurs in an async state or some kind of issue occurs.
     */
     long INVALID_ROW_ID = -1;

    /**
     * Saves the object in the DB.
     *
     * @param wrapper wrapper
     * @return true if successful
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    boolean save(DatabaseWrapper wrapper) throws DataAbilityRemoteException;

    /**
     * Deletes the object in the DB
     *
     * @param wrapper wrapper
     * @return true if successful
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    boolean delete(DatabaseWrapper wrapper) throws DataAbilityRemoteException;

    /**
     * Updates an object in the DB. Does not insert on failure.
     *
     * @param wrapper wrapper
     * @return true if successful
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    boolean update(DatabaseWrapper wrapper) throws DataAbilityRemoteException;

    /**
     * Inserts the object into the DB
     *
     * @param wrapper wrapper
     * @return the count of the rows affected, should only be 1 here, or -1 if failed.
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    long insert(DatabaseWrapper wrapper) throws DataAbilityRemoteException;


    static <T> boolean save(Class<T> clazz, T model, DatabaseWrapper databaseWrapper){
        return FlowManager.modelAdapter(clazz).save(model, databaseWrapper);
    }

    static <T> long insert(Class<T> clazz, T model, DatabaseWrapper databaseWrapper){
        return FlowManager.modelAdapter(clazz).insert(model, databaseWrapper);
    }

    static <T> boolean update(Class<T> clazz, T model, DatabaseWrapper databaseWrapper){
        return FlowManager.modelAdapter(clazz).update(model, databaseWrapper);
    }

    static <T> boolean delete(Class<T> clazz, T model, DatabaseWrapper databaseWrapper){
        return FlowManager.modelAdapter(clazz).delete(model, databaseWrapper);
    }

    static <T> boolean exists(Class<T> clazz, T model, DatabaseWrapper databaseWrapper){
        return FlowManager.modelAdapter(clazz).exists(model, databaseWrapper);
    }

    static <T> T load(Class<T> clazz, T model, DatabaseWrapper databaseWrapper) {
        return FlowManager.modelAdapter(clazz).load(model, databaseWrapper);
    }

}



