package com.dbflow5.structure;

/**
 * Specifies the Action that was taken when data changes
 */
public enum ChangeAction {
    INSERT,
    UPDATE,
    DELETE,
    CHANGE
}