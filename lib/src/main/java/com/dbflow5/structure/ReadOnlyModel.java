package com.dbflow5.structure;

import com.dbflow5.database.DatabaseWrapper;
import ohos.aafwk.ability.DataAbilityRemoteException;

public interface ReadOnlyModel {

    /**
     * Loads from the database the most recent version of the model based on it's primary keys.
     * @param wrapper wrapper
     * @param <T> T
     * @return T
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    <T> T load(DatabaseWrapper wrapper) throws DataAbilityRemoteException;

    /**
     * if this object exists in the DB
     * @param wrapper wrapper
     * @return true if this object exists in the DB. It combines all of it's primary key fields
     * into a SELECT query and checks to see if any results occur.
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    boolean exists(DatabaseWrapper wrapper) throws DataAbilityRemoteException;

}
