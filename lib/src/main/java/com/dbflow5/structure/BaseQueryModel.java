package com.dbflow5.structure;

import com.dbflow5.annotation.QueryModel;
import com.dbflow5.database.DatabaseWrapper;

/**
 * Description: Provides a base class for objects that represent [QueryModel].
 */
public class BaseQueryModel extends NoModificationModel {
    @Override
    public boolean exists(DatabaseWrapper wrapper) {
        throw new InvalidSqlViewOperationException("Query ${wrapper.javaClass.name} does not exist as a table." +
                "It's a convenient representation of a complex SQLite cursor.");
    }
}
