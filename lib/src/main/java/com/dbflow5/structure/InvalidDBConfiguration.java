package com.dbflow5.structure;

/**
 * Description: Thrown when a DB is incorrectly configured.
 */
public class InvalidDBConfiguration extends RuntimeException{
    public InvalidDBConfiguration(String message) {
        super(message);
    }
}
