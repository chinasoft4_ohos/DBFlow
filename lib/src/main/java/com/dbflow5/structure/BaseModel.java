package com.dbflow5.structure;

import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.annotation.ColumnIgnore;
import com.dbflow5.config.FlowManager;
import com.dbflow5.database.DatabaseWrapper;
import ohos.aafwk.ability.DataAbilityRemoteException;

/**
 * Description: The base implementation of [Model]. It is recommended to use this class as
 * the base for your [Model], but it is not required.
 */
public class BaseModel implements Model {

    /**
     * @return The associated [ModelAdapter]. The [FlowManager]
     * may throw a [InvalidDBConfiguration] for this call if this class
     * is not associated with a table, so be careful when using this method.
     */
    @ColumnIgnore
    public transient ModelAdapter<BaseModel> modelAdapter = FlowManager.modelAdapter((Class<BaseModel>) getClass());

    @Override
    public boolean save(DatabaseWrapper wrapper) throws DataAbilityRemoteException {
        return modelAdapter.save(this, wrapper);
    }

    @Override
    public boolean delete(DatabaseWrapper wrapper) throws DataAbilityRemoteException {
        return modelAdapter.delete(this, wrapper);
    }

    @Override
    public boolean update(DatabaseWrapper wrapper) throws DataAbilityRemoteException {
        return modelAdapter.update(this, wrapper);
    }

    @Override
    public long insert(DatabaseWrapper wrapper) throws DataAbilityRemoteException {
        return modelAdapter.insert(this, wrapper);
    }

    @Override
    public boolean exists(DatabaseWrapper wrapper) throws DataAbilityRemoteException {
        return modelAdapter.exists(this, wrapper);
    }

    @Override
    public <T> T load(DatabaseWrapper databaseWrapper) throws DataAbilityRemoteException {
        return (T)modelAdapter.load(this, databaseWrapper);
    }
}
