package com.dbflow5.structure;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.FlowManager;
import com.dbflow5.database.DatabaseWrapper;

/**
 * Description: A convenience class for [ReadOnlyModel].
 */
public abstract class NoModificationModel implements ReadOnlyModel {

    private final transient RetrievalAdapter<NoModificationModel> retrievalAdapter = FlowManager.getRetrievalAdapter(NoModificationModel.class);

    @Override
    public <T> T load(DatabaseWrapper wrapper) {
        return (T)retrievalAdapter.load(this, wrapper);
    }

    @Override
    public boolean exists(DatabaseWrapper wrapper) {
        return retrievalAdapter.exists(this, wrapper);
    }

    /**
     * Gets thrown when an operation is not valid for the SQL View
     */
    public static class InvalidSqlViewOperationException extends RuntimeException{
        public InvalidSqlViewOperationException(String message) {
            super(message);
        }
    }
}
