package com.dbflow5;

import ohos.data.rdb.RdbStore;

public class ConflictActionUtils {
    public static RdbStore.ConflictResolution getConflictResolution(int index) {
        switch (index) {
            case 1:
                return RdbStore.ConflictResolution.ON_CONFLICT_ROLLBACK;
            case 2:
                return RdbStore.ConflictResolution.ON_CONFLICT_ABORT;
            case 3:
                return RdbStore.ConflictResolution.ON_CONFLICT_FAIL;
            case 4:
                return RdbStore.ConflictResolution.ON_CONFLICT_IGNORE;
            case 5:
                return RdbStore.ConflictResolution.ON_CONFLICT_REPLACE;
            default:
                return RdbStore.ConflictResolution.ON_CONFLICT_NONE;
        }
    }
}
