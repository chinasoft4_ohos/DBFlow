package com.dbflow5;

import ohos.app.Context;

import java.io.File;

public class DatabaseFileUtils {

    public static File getDatabasePath(Context context, String databaseName){
        // /data/data/com.dbflow5/databases/db/prepackaged.db
        return new File(new File("/data/data/com.dbflow5/databases/db/"), databaseName);
    }

    public static boolean deleteDatabase(Context context, String databaseName) {
        File dbFile = getDatabasePath(context, databaseName);
        return dbFile.delete();
    }

}
