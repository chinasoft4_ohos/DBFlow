package com.dbflow5.query;

import com.dbflow5.SqlUtils;
import com.dbflow5.sql.Query;
import com.dbflow5.structure.ChangeAction;
import ohos.data.rdb.ValuesBucket;

/**
 * Description: Used to specify the SET part of an [com.dbflow5.query.Update] query.
 */
public class Set<T> extends BaseTransformable<T> implements WhereBase<T> {

    private final Query queryBuilderBase;

    private final OperatorGroup operatorGroup = OperatorGroup.nonGroupingClause().setAllCommaSeparated(true);

    public Set(Query queryBuilderBase, Class<T> table) {
        super(table);
        this.queryBuilderBase = queryBuilderBase;
    }

    @Override
    public Query queryBuilderBase() {
        return queryBuilderBase;
    }

    @Override
    public String getQuery() {
        return " "+ queryBuilderBase.getQuery() +"SET "+ operatorGroup.getQuery() +" ";
    }

    @Override
    public ChangeAction primaryAction() {
        return ChangeAction.UPDATE;
    }

    /**
     * Specifies a varg of conditions to append to this SET
     *
     * @param conditions The varg of conditions
     * @return This instance.
     */
    public Set<T> conditions(SQLOperator... conditions) {
        operatorGroup.andAll(conditions);
        return this;
    }

    /**
     * Specifies a varg of conditions to append to this SET
     *
     * @param condition The varg of conditions
     * @return This instance.
     */
    public Set<T> and(SQLOperator condition) {
        operatorGroup.and(condition);
        return this;
    }

    public Set<T> conditionValues(ValuesBucket contentValues) {
        SqlUtils.addContentValues(contentValues, operatorGroup);
        return this;
    }

    @Override
    public Set<T> cloneSelf() {
        Query query;
        if(queryBuilderBase instanceof Update<?>){
            query = ((Update<?>) queryBuilderBase).cloneSelf();
        }else {
            query = queryBuilderBase;
        }
        Set<T> set = new Set<>(query, table);
        set.operatorGroup.andAll(operatorGroup.conditions());
        return set;
    }

    @Override
    public Class<T> table() {
        return table;
    }
}
