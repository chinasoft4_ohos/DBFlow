package com.dbflow5.query.list;

import com.dbflow5.database.FlowCursor;
import java.io.Closeable;
import java.io.IOException;

/**
 * Description: Simple interface that allows you to iterate a [FlowCursor].
 */
public interface IFlowCursorIterator<T> extends Closeable, Iterable<T> {

    /**
     * Count of the [FlowCursor].
     * @return Count of the [FlowCursor].
     */
    long count();

    /**
     * The index within the [FlowCursor] to retrieve and convert into a [T]
     * @param index The index within the [FlowCursor] to retrieve and convert into a [T]
     * @return T
     */
    T get(long index);

    /**
     * The index within the [FlowCursor] to retrieve and convert into a [T]
     * @param index The index within the [FlowCursor] to retrieve and convert into a [T]
     * @return T
     */
    default T get(int index) {
        return get((long)index);
    }

    /**
     * The cursor.
     * @return The cursor.
     */
    FlowCursor cursor();

    /**
     * If true, we are tracking a passed cursor. If not, we are using new cursor constructed within this class.
     * @return trackingCursor
     */
    boolean trackingCursor();

    /**
     * If true, [FlowCursor] is closed and this object should be discarded.
     * @return If true, [FlowCursor] is closed and this object should be discarded.
     */
    boolean isClosed();

    FlowCursorIterator<T> iterator();

    /**
     * Can iterate the [FlowCursor]
     * @param startingLocation startingLocation
     * @param limit limit
     * @return Can iterate the [FlowCursor]. Specifies a starting location + count limit of results.
     */
    public FlowCursorIterator<T> iterator(long startingLocation, long limit);

    @Override
    void close();
}