package com.dbflow5.query;

import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.sql.Query;
import com.dbflow5.structure.ChangeAction;
import com.dbflow5.structure.Model;

/**
 * Description: The most basic interface that some of the classes such as [Insert], [ModelQueriable],
 * [Set], and more implement for convenience.
 */
public interface Queriable extends Query {

    /**
     * ChangeAction
     *
     * @return ChangeAction
     */
    ChangeAction primaryAction();

    /**
     * cursor
     *
     * @param databaseWrapper databaseWrapper
     * @return A cursor from the DB based on this query
     */
    FlowCursor cursor(DatabaseWrapper databaseWrapper);

    /**
     * compileStatement
     *
     * @param databaseWrapper databaseWrapper
     * @return A new [DatabaseStatement] from this query.
     */
    DatabaseStatement compileStatement(DatabaseWrapper databaseWrapper);

    /**
     * longValue
     *
     * @param databaseWrapper databaseWrapper
     * @return the long value of the results of a query or single-column result.
     */
    long longValue(DatabaseWrapper databaseWrapper);

    /**
     * stringValue
     *
     * @param databaseWrapper databaseWrapper
     * @return the string value for results of a query or single-column result.
     */
    String stringValue(DatabaseWrapper databaseWrapper);

    /**
     * executeUpdateDelete
     *
     * @param databaseWrapper databaseWrapper
     * @return This may return the number of rows affected from a [Set] or [Delete] statement.
     * If not, returns [Model.INVALID_ROW_ID]
     */
    long executeUpdateDelete(DatabaseWrapper databaseWrapper);

    /**
     * executeInsert
     *
     * @param databaseWrapper databaseWrapper
     * @return This may return the number of rows affected from a [Insert]  statement.
     * If not, returns [Model.INVALID_ROW_ID]
     */
    long executeInsert(DatabaseWrapper databaseWrapper);

    /**
     * hasData
     *
     * @param databaseWrapper databaseWrapper
     * @return True if this query has data. It will run a [.count] greater than 0.
     */
    boolean hasData(DatabaseWrapper databaseWrapper);

    /**
     * Will not return a result, rather simply will execute a SQL statement. Use this for non-SELECT statements or when
     * you're not interested in the result.
     * @param databaseWrapper databaseWrapper
     */
    void execute(DatabaseWrapper databaseWrapper);

}