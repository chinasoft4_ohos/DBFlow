package com.dbflow5.query;

import ohos.data.rdb.ValuesBucket;

/**
 * Description: Called after the declared [ContentValues] are binded. It enables
 * us to listen and add custom behavior to the [ContentValues]. These must be
 * defined in a [Model] class to register properly.
 *
 *
 * This class will no longer get called during updates unless explicit call to
 * [ModelAdapter.bindToContentValues]
 * or [ModelAdapter.bindToInsertValues] with setting [Table.generateContentValues] to true.
 *
 */
@Deprecated
public interface ContentValuesListener {

    /**
     * Called during an [Model.update] and at the end of
     * [ModelAdapter.bindToContentValues]
     * . It enables you to customly change the values as necessary during update to the database.
     *
     * @param contentValues The content values to bind to for an update.
     */
    void onBindToContentValues(ValuesBucket contentValues);

    /**
     * Called during an [Model.update] and at the end of
     * [ModelAdapter.bindToInsertValues].
     * It enables you to customly change the values as necessary during insert
     * to the database for a [ContentProvider].
     *
     * @param contentValues The content values to insert into DB for a ContentProvider
     */
    void onBindToInsertValues(ValuesBucket contentValues);
}
