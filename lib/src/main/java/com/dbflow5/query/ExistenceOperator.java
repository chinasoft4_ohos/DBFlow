package com.dbflow5.query;

import com.dbflow5.StringUtils;
import com.dbflow5.sql.Query;

/**
 * Description: The condition that represents EXISTS in a SQL statement.
 */
public class ExistenceOperator implements SQLOperator, Query {
    private final Where<?> innerWhere;

    public ExistenceOperator(Where<?> innerWhere){
        this.innerWhere = innerWhere;
    }

    @Override
    public void appendConditionToQuery(StringBuilder queryBuilder) {
        StringUtils.appendQualifier(queryBuilder,"EXISTS", innerWhere.enclosedQuery());
    }

    @Override
    public String columnName() {
        throw new RuntimeException("Method not valid for ExistenceOperator");
    }

    @Override
    public String separator() {
        throw new RuntimeException("Method not valid for ExistenceOperator");
    }

    @Override
    public SQLOperator separator(String separator) {
        throw new RuntimeException("Method not valid for ExistenceOperator");
    }

    @Override
    public boolean hasSeparator() {
        return false;
    }

    @Override
    public String operation() {
        return "";
    }

    @Override
    public Object value() {
        return innerWhere;
    }

    @Override
    public String getQuery() {
        return appendToQuery();
    }
}
