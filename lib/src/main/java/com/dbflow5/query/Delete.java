package com.dbflow5.query;

import com.dbflow5.sql.Query;

/**
 * Description: Constructs the beginning of a SQL DELETE query
 */
public class Delete implements Query {

    public Delete() {
    }

    @Override
    public String getQuery() {
        return "DELETE ";
    }

    /**
     * Returns the new SQL FROM statement wrapper
     *
     * @param table    The table we want to run this query from
     * @return [T]
     **/
    public <T> From<T> from(Class<T> table) {
        return new From<>(this, table, null);
    }

}

