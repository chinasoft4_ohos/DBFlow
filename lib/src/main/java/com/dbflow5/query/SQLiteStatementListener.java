package com.dbflow5.query;

import com.dbflow5.database.DatabaseStatement;

/**
 * Description: Marks a [Model] as subscribing to the [DatabaseStatement]
 * that is used to [Model.insert] a model into the DB.
 */
public interface SQLiteStatementListener {

    /**
     * Called at the end of [InternalAdapter.bindToInsertStatement]
     * Perform a custom manipulation of the statement as willed.
     *
     * @param databaseStatement The insert statement from the [ModelAdapter]
     */
    void onBindToInsertStatement(DatabaseStatement databaseStatement);

    /**
     * Called at the end of [InternalAdapter.bindToUpdateStatement]
     * Perform a custom manipulation of the statement as willed.
     *
     * @param databaseStatement The insert statement from the [ModelAdapter]
     */
    void onBindToUpdateStatement(DatabaseStatement databaseStatement);

    void onBindToDeleteStatement(DatabaseStatement databaseStatement);
}
