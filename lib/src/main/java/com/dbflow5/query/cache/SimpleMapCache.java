package com.dbflow5.query.cache;

import com.dbflow5.config.FlowLog;
import java.util.HashMap;
import java.util.Map;

/**
 * Description: A simple implementation that keeps [Model] you interact with in memory.
 */
public class SimpleMapCache<TModel> extends ModelCache<TModel, Map<Object, TModel>> {

    /**
     * Constructs new instance with a [HashMap] with the specified capacity.
     *
     * @param capacity The capacity to use on the hashmap.
     */
    public SimpleMapCache(int capacity) {
        super(new HashMap<>(capacity));
    }

    /**
     * Constructs new instance with a cache
     *
     * @param cache The arbitrary underlying cache class.
     */
    public SimpleMapCache(Map<Object, TModel> cache) {
        super(cache);
    }

    @Override
    public void addModel(Object id, TModel model) {
        cache.put(id, model);
    }

    @Override
    public TModel removeModel(Object id) {
        return cache.remove(id);
    }

    @Override
    public void clear() {
        cache.clear();
    }

    @Override
    public TModel get(Object id) {
        return cache.get(id);
    }

    @Override
    public void setCacheSize(int size) {
        FlowLog.log(FlowLog.Level.W, "The cache size for SimpleMapCache is not re-configurable.");
    }
}
