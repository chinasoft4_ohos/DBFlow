package com.dbflow5.query.cache;

import com.dbflow5.annotation.Table;

import java.util.function.Function;

/**
 * Description: Provides an [LruCache] under its hood
 * and provides synchronization mechanisms.
 */
public class ModelLruCache<TModel> extends ModelCache<TModel, LruCache<Long, TModel>> {

    public ModelLruCache(int size){
        super(new LruCache<Long, TModel>(size));
    }

    @Override
    public void addModel(Object id, TModel model) {
        throwIfNotNumber(id, number -> {
            synchronized(cache) {
                cache.put(number.longValue(), model);
                return null;
            }
        });
    }

    @Override
    public TModel removeModel(Object id) {
        return throwIfNotNumber(id, number -> {
            synchronized(cache) {
                return cache.remove(number.longValue());
            }
        });
    }

    @Override
    public void clear() {
        synchronized(cache) {
            cache.evictAll();
        }
    }

    @Override
    public void setCacheSize(int size) {
        cache.resize(size);
    }

    @Override
    public TModel get(Object id) {
        return throwIfNotNumber(id, number -> {
            cache.get(number.longValue());
            return null;
        });
    }

    private static <R> R throwIfNotNumber(Object id, Function<Number, R> fn) {
        if (id instanceof Number) {
            return fn.apply((Number) id);
        } else {
            throw new IllegalArgumentException("A ModelLruCache must use an id that can cast to"
                    + "a Number to convert it into a long");
        }
    }

    /**
     * ModelLruCache
     *
     * @param size The size, if less than or equal to 0 we set it to [DEFAULT_CACHE_SIZE].
     * @param <TModel> TModel
     * @return new ModelLruCache<>(locSize)
     */
    public static <TModel> ModelLruCache<TModel> newInstance(int size) {
        int locSize = size;
        if (locSize <= 0) {
            locSize = Table.DEFAULT_CACHE_SIZE;
        }
        return new ModelLruCache<>(locSize);
    }
}
