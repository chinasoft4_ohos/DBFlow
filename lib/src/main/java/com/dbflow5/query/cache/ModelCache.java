package com.dbflow5.query.cache;

import com.dbflow5.adapter.CacheAdapter;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;

/**
 * Description: A generic cache for models that is implemented or can be implemented to your liking.
 */
public abstract class ModelCache<TModel, CacheClass> {
    public CacheClass cache;

    public ModelCache(CacheClass cache) {
        this.cache = cache;
    }

    /**
     * Adds a model to this cache.
     *
     * @param id    The id of the model to use.
     * @param model The model to add
     */
    public abstract void addModel(Object id, TModel model);

    /**
     * Removes a model from this cache.
     *
     * @param id The id of the model to remove.
     * @return model
     */
    public abstract TModel removeModel(Object id);

    /**
     * Clears out all models from this cache.
     */
    public abstract void clear();

    /**
     * TModel
     *
     * @param id The id of the model to retrieve.
     * @return a model for the specified id. May be null.
     */
    public abstract TModel get(Object id);

    /**
     * Sets a new size for the underlying cache (if applicable) and may destroy the cache.
     *
     * @param size The size of cache to set to
     */
    public abstract void setCacheSize(int size);

    public static <T, C> T addOrReload(ModelCache<T, C> modelCache, Object cacheValue, CacheAdapter<T> cacheAdapter, ModelAdapter<T> modelAdapter,
                                       FlowCursor cursor, DatabaseWrapper databaseWrapper) {
        T model = modelCache.get(cacheValue);
        if (model != null) {
            cacheAdapter.reloadRelationships(model, cursor, databaseWrapper);
        } else {
            model = modelAdapter.loadFromCursor(cursor, databaseWrapper);
            modelCache.addModel(cacheValue, model);
        }
        return model;
    }
}
