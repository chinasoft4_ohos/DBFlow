package com.dbflow5.query.cache;

import com.dbflow5.config.FlowLog;
import ohos.utils.PlainArray;

/**
 * Description: A cache backed by a [PlainArray]
 */
public class SparseArrayBasedCache<TModel> extends ModelCache<TModel, PlainArray<TModel>> {

    /**
     * Constructs new instance with a [SparseArray] cache
     */
    public SparseArrayBasedCache() {
        super(new PlainArray<>());
    }

    /**
     * Constructs new instance with a [ohos.utils.PlainArray] cache
     *
     * @param initialCapacity The initial capacity of the sparse array
     */
    public SparseArrayBasedCache(int initialCapacity) {
        super(new PlainArray<>(initialCapacity));
    }

    public SparseArrayBasedCache(PlainArray<TModel> sparseArray) {
        super(sparseArray);
    }

    @Override
    public void addModel(Object id, TModel model) {
        if (id instanceof Number) {
            synchronized(cache) {
                cache.put(((Number) id).intValue(), model);
            }
        } else {
            throw new IllegalArgumentException("A SparseArrayBasedCache must use an id that can cast to " + "a Number to convert it into a int");
        }
    }

    @Override
    public TModel removeModel(Object id) {
        TModel model = get(id);
        synchronized(cache) {
            cache.remove(((Number) id).intValue());
        }
        return model;
    }

    @Override
    public void clear() {
        synchronized(cache) {
            cache.clear();
        }
    }

    @Override
    public TModel get(Object id) {
        if (id instanceof Number && cache.get(((Number) id).intValue()).isPresent()) {
            return cache.get(((Number) id).intValue()).get();
        } else {
            throw new IllegalArgumentException("A SparseArrayBasedCache uses an id that can cast to "
                    + "a Number to convert it into a int");
        }
    }

    @Override
    public void setCacheSize(int size) {
        FlowLog.log(FlowLog.Level.I, "The cache size for SparseArrayBasedCache is not re-configurable.");
    }
}
