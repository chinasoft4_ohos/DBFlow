package com.dbflow5.query;

/**
 * Description: Basic interface for all of the Operator classes.
 */
public interface SQLOperator {

    /**
     * Appends itself to the [StringBuilder]
     *
     * @param queryBuilder The builder to append to.
     */
    void appendConditionToQuery(StringBuilder queryBuilder);

    /**
     * The name of the column.
     *
     * @return The column name.
     */
    String columnName();

    /**
     * The separator for this condition when paired with a [OperatorGroup]
     *
     * @return The separator, an AND, OR, or other kinds.
     */
    String separator();

    /**
     * Sets the separator for this condition
     *
     * @param separator The string AND, OR, or something else.
     * @return This instance.
     */
    SQLOperator separator(String separator);

    /**
     * If has a separator
     * @return true if it has a separator, false if not.
     */
    boolean hasSeparator();

    /**
     * the operation that is used.
     * @return the operation that is used.
     */
    String operation();

    /**
     * The raw value of the condition.
     * @return The raw value of the condition.
     */
    Object value();

    default String appendToQuery() {
        StringBuilder queryBuilder = new StringBuilder();
        appendConditionToQuery(queryBuilder);
        return queryBuilder.toString();
    }

    default OperatorGroup clause2() {
        return OperatorGroup.clause(this);
    }

}

