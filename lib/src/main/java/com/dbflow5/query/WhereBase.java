package com.dbflow5.query;

import com.dbflow5.sql.Query;
import com.dbflow5.sql.QueryCloneable;

/**
 * Description: The base for a [Where] statement.
 */
public interface WhereBase<TModel> extends Query, Actionable, QueryCloneable<WhereBase<TModel>> {

    /**
     * The table of this query.
     * @return The table of this query.
     */
    Class<TModel> table();

    /**
     * The base Query object.
     * @return The base Query object.
     */
    Query queryBuilderBase();

}
