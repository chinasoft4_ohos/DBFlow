package com.dbflow5.query;

import com.dbflow5.sql.Query;

import java.util.Collection;

/**
 * Description: Interface for objects that can be used as [Operator] that have a type parameter.
 */
public interface IOperator<T> extends Query, IConditional {

    /**
     * Assigns the operation to "="
     *
     * @param value The [T] that we express equality to.
     * @return A [Operator] that represents equality between this and the parameter.
     */
    Operator<T> is(T value);

    /**
     * Assigns the operation to "=". Identical to [.is]
     *
     * @param value The [T] that we express equality to.
     * @return A [Operator] that represents equality between this and the parameter.
     * @see .is
     */
    Operator<T> eq(T value);

    /**
     * Generates a [Operator] that concatenates this [IOperator] with the [T] via "||"
     * by columnName=columnName || value
     *
     * @param value The value to concatenate.
     * @return A [<] that represents concatenation.
     */
    Operator<T> concatenate(Object value);

    /**
     * Assigns the operation to "!="
     *
     * @param value The [T] that we express inequality to.
     * @return A [<] that represents inequality between this and the parameter.
     */
    Operator<T> isNot(T value);

    /**
     * Assigns the operation to "!="
     *
     * @param value The [T] that we express inequality to.
     * @return A [<] that represents inequality between this and the parameter.
     * @see .notEq
     */
    Operator<T> notEq(T value);

    /**
     * Assigns operation to "&gt;"
     *
     * @param value The [T] that this [IOperator] is greater than.
     * @return A [<] that represents greater than between this and the parameter.
     */
    Operator<T> greaterThan(T value);

    /**
     * Assigns operation to "&gt;="
     *
     * @param value The [T] that this [IOperator] is greater than or equal to.
     * @return A [<] that represents greater than or equal between this and the parameter.
     */
    Operator<T> greaterThanOrEq(T value);


    /**
     * Assigns operation to "&lt;"
     *
     * @param value The [T] that this [IOperator] is less than.
     * @return A [<] that represents less than between this and the parameter.
     */
    Operator<T> lessThan(T value);


    /**
     * Assigns operation to "&lt;="
     *
     * @param value The [T] that this [IOperator] is less than or equal to.
     * @return A [<] that represents less than or equal to between this and the parameter.
     */
    Operator<T> lessThanOrEq(T value);

    Operator.Between<T> between(T value);

    /**
     * Turns this [IOperator] into an [.In][<]. It means that this object should
     * be represented by the set of [T] provided to follow.
     *
     * @param firstValue The first value (required to enforce >= 1)
     * @param values     The rest of the values to pass optionally.
     * @return A new [.In][<] built from this [IOperator].
     */
    Operator.In<T> in(T firstValue, T... values);

    /**
     * Turns this [IOperator] into an [.In][<] (not). It means that this object should NOT
     * be represented by the set of [T] provided to follow.
     *
     * @param firstValue The first value (required to enforce >= 1)
     * @param values     The rest of the values to pass optionally.
     * @return A new [.In][<] (not) built from this [IOperator].
     */
    Operator.In<T> notIn(T firstValue, T... values);

    /**
     * Turns this [IOperator] into an [.In][<]. It means that this object should
     * be represented by the set of [T] provided to follow.
     *
     * @param values The rest of the values to pass optionally.
     * @return A new [.In][<] built from this [IOperator].
     */
    Operator.In<T> in(Collection<T> values);

    /**
     * Turns this [IOperator] into an [.In][<] (not). It means that this object should NOT
     * be represented by the set of [T] provided to follow.
     *
     * @param values The rest of the values to pass optionally.
     * @return A new [.In][<] (not) built from this [IOperator].
     */
    Operator.In<T> notIn(Collection<T> values);

    /**
     * Adds another value and returns the operator. i.e p1 + p2
     *
     * @param value the value to add.
     * @return A new instance.
     */
    Operator<T> plus(T value);

    /**
     * Subtracts another value and returns the operator. i.e p1 - p2
     *
     * @param value the value to subtract.
     * @return A new instance.
     */
    Operator<T> minus(T value);

    /**
     * Divides another value and returns as the operator. i.e p1 / p2
     *
     * @param value the value to divide.
     * @return A new instance.
     */
    Operator<T> div(T value);

    /**
     * Multiplies another value and returns as the operator. i.e p1 * p2
     *
     * @param value the value to multiply.
     * @return A new instance.
     */
    Operator<T> times(T value);

    /**
     * Modulous another value and returns as the operator. i.e p1 % p2
     *
     * @param value the value to calculate remainder of.
     * @return A new instance.
     */
    Operator<T> rem(T value);
}
