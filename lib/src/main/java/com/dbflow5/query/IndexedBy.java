package com.dbflow5.query;

import com.dbflow5.StringUtils;
import com.dbflow5.query.property.IndexProperty;
import com.dbflow5.sql.Query;
import com.dbflow5.structure.ChangeAction;

/**
 * Description: The INDEXED BY part of a SELECT/UPDATE/DELETE
 */
public class IndexedBy<TModel> extends BaseTransformable<TModel> {
    private final IndexProperty<TModel> indexProperty;
    private final WhereBase<TModel> whereBase;

    public IndexedBy(IndexProperty<TModel> indexProperty, WhereBase<TModel> whereBase) {
        super(whereBase.table());
        this.indexProperty = indexProperty;
        this.whereBase = whereBase;
    }

    @Override
    public Query queryBuilderBase() {
        return whereBase.queryBuilderBase();
    }

    @Override
    public String getQuery() {
        StringBuilder builder = new StringBuilder();
        builder.append(whereBase.getQuery());
        builder.append(" INDEXED BY ");
        builder.append(StringUtils.quoteIfNeeded(indexProperty.indexName));
        builder.append(" ");
        return builder.toString();
    }

    @Override
    public ChangeAction primaryAction() {
        return whereBase.primaryAction();
    }

    @Override
    public IndexedBy<TModel> cloneSelf() {
        return new IndexedBy<>(indexProperty, whereBase.cloneSelf());
    }

    @Override
    public Class<TModel> table() {
        return table;
    }
}
