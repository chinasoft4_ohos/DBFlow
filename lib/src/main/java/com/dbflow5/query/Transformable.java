package com.dbflow5.query;

import com.dbflow5.query.property.IProperty;
import com.dbflow5.sql.QueryCloneable;

import java.util.List;

/**
 * Description: Provides a standard set of methods for ending a SQLite query method. These include
 * groupby, orderby, having, limit and offset.
 */
public interface Transformable<T> {

    Where<T> groupBy(NameAlias... nameAliases);

    Where<T> groupBy(IProperty<?>... properties);

    Where<T> orderBy(NameAlias nameAlias, boolean ascending);

    Where<T> orderBy(IProperty<?> property, boolean ascending);

    Where<T> orderBy(OrderBy orderBy);

    Where<T> limit(long count);

    Where<T> offset(long offset);

    Where<T> having(SQLOperator... conditions);

    Where<T> orderByAll(List<OrderBy> orderByList);

    /**
     * Constrains the given [Transformable] by the [offset] and [limit] specified. It copies over itself
     *  into a new instance to not preserve changes.
     * @param offset offset
     * @param limit limit
     * @return ModelQueriable
     */
    default ModelQueriable<T> constrain(long offset, long limit) {
        Transformable<T> tr = this;
        if (tr instanceof QueryCloneable<?>) {
            tr = (Transformable<T>)((QueryCloneable<?>)tr).cloneSelf();
        }
        return tr.offset(offset).limit(limit);
    }
}
