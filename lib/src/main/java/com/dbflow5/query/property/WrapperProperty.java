package com.dbflow5.query.property;

import com.dbflow5.config.FlowManager;
import com.dbflow5.query.NameAlias;

/**
 * Description: Provides convenience for types that are represented in different ways in the DB.
 *
 * @author Andrew Grosner (fuzz)
 */
public class WrapperProperty<T, V> extends Property<V> {

    private WrapperProperty<V, T> databaseProperty = null;

    Class<?> table = super.table;

    public WrapperProperty(Class<?> table, NameAlias nameAlias) {
        super(table, nameAlias);
    }

    public WrapperProperty(Class<?> table, String columnName) {
        super(table, columnName);
    }

    @Override
    public WrapperProperty<T, V> withTable() {
        NameAlias nameAlias = this.nameAlias()
                .newBuilder()
                .withTable(FlowManager.getTableName(table))
                .build();
        return new WrapperProperty<>(this.table, nameAlias);
    }

    @Override
    public WrapperProperty<T, V> withTable(NameAlias tableNameAlias) {
        NameAlias nameAlias = this.nameAlias()
                .newBuilder()
                .withTable(tableNameAlias.tableName)
                .build();
        return new WrapperProperty<>(this.table, nameAlias);
    }

    /**
     * A new [Property]
     * @return A new [Property] that corresponds to the inverted type of the [WrapperProperty]. Convenience
     * for types that have different DB representations.
     */
    public WrapperProperty<V, T> invertProperty() {
        if(databaseProperty == null){
            databaseProperty = new WrapperProperty<>(table, nameAlias());
        }
        return databaseProperty;
    }
}
