package com.dbflow5.query.property;

import com.dbflow5.StringUtils;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.query.Index;

import java.util.Arrays;
import java.util.List;

/**
 * Description: Defines an INDEX in Sqlite. It basically speeds up data retrieval over large datasets.
 * It gets generated from [Table.indexGroups], but also can be manually constructed. These are activated
 * and deactivated manually.
 */
public class IndexProperty<T> {

    List<IProperty<?>> properties;

    public Index<T> index;
    public String indexName;

    public IndexProperty(String _indexName, boolean unique, Class<T> table, IProperty<?>... properties){
        this.properties = Arrays.asList(properties);
        indexName  =  StringUtils.quoteIfNeeded(_indexName);
        if(indexName == null){
            indexName = "";
        }
        index = new Index<>(indexName, table).on(properties).unique(unique);
    }

    private IProperty<?>[] properties() {
        return (IProperty<?>[])properties.toArray();
    }

    public void createIfNotExists(DatabaseWrapper wrapper) {
        index.createIfNotExists(wrapper);
    }

    public void drop(DatabaseWrapper wrapper) {
        index.drop(wrapper);
    }
}
