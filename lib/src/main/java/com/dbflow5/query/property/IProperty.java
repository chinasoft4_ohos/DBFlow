package com.dbflow5.query.property;

import com.dbflow5.query.NameAlias;
import com.dbflow5.query.OrderBy;
import com.dbflow5.sql.Query;
import com.dbflow5.structure.Model;

/**
 * Description: Defines the base interface all property classes implement.
 */
public interface IProperty<P extends IProperty<P>> extends Query {

    /**
     * The underlying [NameAlias] that represents the name of this property.
     * @return The underlying [NameAlias] that represents the name of this property.
     */
    NameAlias nameAlias();

    /**
     * The table this property belongs to.
     * @return the table this property belongs to.
     */
    Class<?> table();

    /**
     * As another property and returns as a new propert
     * @param aliasName The name of the alias.
     * @return A new [P] that expresses the current column name with the specified Alias name.
     */
    P as(String aliasName);

    /**
     * Adds another property and returns as a new property. i.e p1 + p2
     *
     * @param property the property to add.
     * @return A new instance.
     */
    P plus(IProperty<?> property);

    /**
     * Subtracts another property and returns as a new property. i.e p1 - p2
     *
     * @param property the property to subtract.
     * @return A new instance.
     */
    P minus(IProperty<?> property);

    /**
     * Divides another property and returns as a new property. i.e p1 / p2
     *
     * @param property the property to divide.
     * @return A new instance.
     */
    P div(IProperty<?> property);

    /**
     * Multiplies another property and returns as a new property. i.e p1 * p2
     *
     * @param property the property to multiply.
     * @return A new instance.
     */
     P times(IProperty<?> property);

    /**
     * Modulous another property and returns as a new property. i.e p1 % p2
     *
     * @param property the property to calculate remainder of.
     * @return A new instance.
     */
    P rem(IProperty<?> property);

    /**
     * Concats another property and returns as a new propert.y i.e. p1 || p2
     *
     * @param property The property to concatenate.
     * @return A new instance.
     */
    P concatenate(IProperty<?> property);

    /**
     * Distinct another property and returns as a new propert
     * @return Appends DISTINCT to the property name. This is handy in [Method] queries.
     * This distinct [P] can only be used with one column within a [Method].
     */
    P distinct();

    /**
     * WithTable another property and returns as a new propert
     * @return A property that represents the [Model] from which it belongs to. This is useful
     * in [Join] queries to represent this property.
     *
     *
     * The resulting [P] becomes `tableName`.`columnName`.
     */
    P withTable();

    /**
     * WithTable another property and returns as a new propert
     * @param tableNameAlias The name of the table to append. This may be different because of complex queries
     * that use a [NameAlias] for the table name.
     * @return A property that represents the [Model] from which it belongs to. This is useful
     * in [Join] queries to represent this property.
     *
     *
     * The resulting column name becomes `tableName`.`columnName`.
     */
    P withTable(NameAlias tableNameAlias);

    OrderBy asc();

    OrderBy desc();
}
