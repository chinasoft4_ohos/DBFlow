package com.dbflow5.migration;

import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.query.Index;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: Defines and enables an Index structurally through a migration.
 */
public abstract class IndexMigration<T> extends BaseMigration {
    private Class<T> onTable;

    private boolean unique = false;
    private final List<IProperty<?>> columns = new ArrayList<IProperty<?>>();

    abstract String name();

    @Override
    public void migrate(DatabaseWrapper database) {
        Index<T> index = SQLite.index(name(), onTable).unique(unique);
        for(IProperty<?> property:columns){
            index.and(property);
        }
        database.execSQL(index.getQuery());
    }

    /**
     * Adds a column to the underlying INDEX
     *
     * @param property The name of the column to add to the Index
     * @return This migration
     */
    public IndexMigration<T> addColumn(IProperty<?> property) {
        columns.add(property);
        return this;
    }

    /**
     * Sets the INDEX to UNIQUE
     *
     * @return This migration.
     */
    public IndexMigration<T> unique() {
        unique = true;
        return this;
    }

}
