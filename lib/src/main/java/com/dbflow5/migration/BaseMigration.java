package com.dbflow5.migration;

import com.dbflow5.database.DatabaseWrapper;
import ohos.aafwk.ability.DataAbilityRemoteException;

/**
 * Description: Provides the base implementation of [Migration] with
 * only [Migration.migrate] needing to be implemented.
 */
public abstract class BaseMigration implements Migration {

    @Override
    public void onPreMigrate() {

    }

    @Override
    public abstract void migrate(DatabaseWrapper database) throws DataAbilityRemoteException;

    @Override
    public void onPostMigrate() {

    }

}
