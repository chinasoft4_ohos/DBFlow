package com.dbflow5.migration;

import com.dbflow5.database.DatabaseWrapper;
import ohos.aafwk.ability.DataAbilityRemoteException;

/**
 * Description: Called when the Database is migrating. We can perform custom migrations here. A [com.dbflow5.annotation.Migration]
 * is required for registering this class to automatically be called in an upgrade of the DB.
 */
public interface Migration {

    /**
     * Called before we migrate data. Instantiate migration data before releasing it in [.onPostMigrate]
     */
    void onPreMigrate();

    /**
     * Perform your migrations here
     *
     * @param database The database to operate on
     * @throws DataAbilityRemoteException Exception
     */
    void migrate(DatabaseWrapper database) throws DataAbilityRemoteException;

    /**
     * Called after the migration completes. Release migration data here.
     */
    void onPostMigrate();
}
