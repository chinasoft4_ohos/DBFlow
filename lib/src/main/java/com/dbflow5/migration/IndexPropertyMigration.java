package com.dbflow5.migration;

import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.query.property.IndexProperty;

/**
 * Description: Allows you to specify if and when an [IndexProperty] gets used or created.
 */
public abstract class IndexPropertyMigration extends BaseMigration {
    protected boolean shouldCreate = true;

    abstract IndexProperty<?> indexProperty();

    @Override
    public void migrate(DatabaseWrapper database) {
        if (shouldCreate) {
            indexProperty().createIfNotExists(database);
        } else {
            indexProperty().drop(database);
        }
    }
}
