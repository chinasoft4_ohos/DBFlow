package com.dbflow5.config;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Description: Mirrors [Log] with its own [Level] flag.
 */
public class FlowLog {
    private static final HiLogLabel LABEL =
            new HiLogLabel(HiLog.LOG_APP, 0x00111, FlowLog.class.getSimpleName());

    private FlowLog(){
    }

    private static Level level = Level.E;

    /**
     * Sets the minimum level that we wish to print out log statements with.
     * The default is [Level.E].
     *
     * @param level level
     */
    public static void setMinimumLoggingLevel(Level level) {
        FlowLog.level = level;
    }

    public static void log(Level level, HiLogLabel tag, String message) {
        if (isEnabled(level)) {
            level.call(tag, message, null);
        }
    }

    /**
     * Logs information to the [Log] class. It wraps around the standard implementation.
     *
     * @param level     The log level to use
     * @param tag       The tag of the log
     * @param message   The message to print out
     * @param throwable The optional stack trace to print
     */
    public static void log(Level level, HiLogLabel tag, String message, Throwable throwable) {
        if (isEnabled(level)) {
            level.call(tag, message, throwable);
        }
    }

    public static void log(Level level, String message){
        log(level, LABEL, message, null);
    }

    /**
     * Logs information to the [Log] class. It wraps around the standard implementation.
     *
     * @param level     The log level to use
     * @param message   The message to print out
     * @param throwable The optional stack trace to print
     */
    public static void log(Level level, String message, Throwable throwable){
        log(level, LABEL, message, throwable);
    }

    /**
     * Returns true if the logging level is lower than the specified [Level]
     *
     * @param level level
     * @return isEnable
     */
    public static boolean isEnabled(Level level){
        return level.ordinal() >= FlowLog.level.ordinal();
    }

    /**
     * Logs a [Throwable] as an error.
     *
     * @param throwable The stack trace to print
     */
    public static void logError(Throwable throwable) {
        log(Level.E, LABEL, "", throwable);
    }

    /**
     * Logs a [Throwable] as a warning.
     *
     * @param throwable The stack trace to print
     */
    public static void logWarning(Throwable throwable) {
        log(Level.W, LABEL, "",throwable);
    }

    /**
     * Defines a log level that will execute
     */
    public enum Level {
        V {
            @Override
            void call(HiLogLabel tag, String message, Throwable throwable) {
                HiLog.info(tag, message);
            }
        },
        D {
            @Override
            void call(HiLogLabel tag, String message, Throwable throwable) {
                HiLog.debug(tag, message);
            }

        },
        I {
            @Override
            void call(HiLogLabel tag, String message, Throwable throwable) {
                HiLog.info(tag, message);
            }
        },
        W {
            @Override
            void call(HiLogLabel tag, String message, Throwable throwable) {
                HiLog.warn(tag, message);
            }
        },
        E {
            @Override
            void call(HiLogLabel tag, String message, Throwable throwable) {
                HiLog.error(tag, message);
            }
        },
        WTF {
            @Override
            void call(HiLogLabel tag, String message, Throwable throwable) {
                HiLog.fatal(tag, message);
            }
        };

        abstract void call(HiLogLabel tag, String message, Throwable throwable);
    }

}
/**
 * Logs information to the [Log] class. It wraps around the standard implementation.
 * It uses the [.TAG] for messages and sends a null throwable.
 *
 * @param level   The log level to use
 * @param message The message to print out
 */
