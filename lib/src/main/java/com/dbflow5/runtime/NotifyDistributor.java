package com.dbflow5.runtime;

import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.config.FlowManager;
import com.dbflow5.structure.ChangeAction;

/**
 * Description: Distributes notifications to the [ModelNotifier].
 */
public class NotifyDistributor implements ModelNotifier {

    private static final NotifyDistributor distributor = new NotifyDistributor();

    public static NotifyDistributor get(){
        return distributor;
    }

    @Override
    public TableNotifierRegister newRegister(){
        throw new RuntimeException("Cannot create a register from the distributor class");
    }

    @Override
    public <T> void notifyModelChanged(T model, ModelAdapter<T> adapter, ChangeAction action) {
        ModelNotifier modelNotifier = FlowManager.getModelNotifierForTable(adapter.table());
        if(modelNotifier != null) {
            modelNotifier.notifyModelChanged(model, adapter, action);
        }
    }

    @Override
    public <T> void notifyTableChanged(Class<T> table, ChangeAction action) {
        ModelNotifier modelNotifier = FlowManager.getModelNotifierForTable(table);
        if(modelNotifier != null) {
            modelNotifier.notifyTableChanged(table, action);
        }
    }
}
