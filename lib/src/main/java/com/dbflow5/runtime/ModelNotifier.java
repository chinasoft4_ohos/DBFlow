package com.dbflow5.runtime;

import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.structure.ChangeAction;

/**
 * Interface for defining how we notify model changes.
 */
public interface ModelNotifier {

    <T> void notifyModelChanged(T model, ModelAdapter<T> adapter, ChangeAction action);

    <T> void notifyTableChanged(Class<T> table, ChangeAction action);

    TableNotifierRegister newRegister();
}
