package com.dbflow5.runtime;

import com.dbflow5.structure.ChangeAction;

import java.util.function.BiFunction;

/**
 * Description: Defines how [ModelNotifier] registers listeners. Abstracts that away.
 */
public interface TableNotifierRegister {

    boolean isSubscribed();

    <T> void register(Class<T> tClass);

    <T> void unregister(Class<T> tClass);

    void unregisterAll();

    void setListener(OnTableChangedListener listener);

    default void setListener(BiFunction<Class<?>, ChangeAction, Void> listener){
        setListener(new OnTableChangedListener() {
            @Override
            public void onTableChanged(Class<?> table, ChangeAction action) {
                listener.apply(table, action);
            }
        });
    }

}

