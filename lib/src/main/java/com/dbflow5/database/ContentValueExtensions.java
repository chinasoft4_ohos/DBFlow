package com.dbflow5.database;

import ohos.data.rdb.ValuesBucket;

class ContentValueExtension{
    public static void set(ValuesBucket valuesBucket, String key, String value) {
        valuesBucket.putString(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, byte value) {
        valuesBucket.putByte(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, short value) {
        valuesBucket.putShort(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, int value) {
        valuesBucket.putInteger(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, long value) {
        valuesBucket.putLong(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, float value) {
        valuesBucket.putFloat(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, double value) {
        valuesBucket.putDouble(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, boolean value) {
        valuesBucket.putBoolean(key, value);
    }

    public static void set(ValuesBucket valuesBucket, String key, byte[] value) {
        valuesBucket.putByteArray(key, value);
    }
}



