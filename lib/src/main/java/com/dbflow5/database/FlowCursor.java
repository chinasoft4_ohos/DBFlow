package com.dbflow5.database;

import ohos.data.resultset.ResultSet;
import ohos.data.resultset.ResultSetWrapper;

/**
 * Common [Cursor] class that wraps cursors we use in this library with convenience loading methods.
 * This is used to help cut down on generated code size and potentially decrease method count.
 */
public class FlowCursor extends ResultSetWrapper {
    public static final int COLUMN_NOT_FOUND = -1;

    private final ResultSet cursor;

    private FlowCursor(ResultSet cursor) {
        super(cursor);
        this.cursor = cursor;
    }

    @Override
    public ResultSet getResultSet() {
        return cursor;
    }

    public String getStringOrDefault(int index, String defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getString(index);
        } else {
            return defValue;
        }
    }

    public String getStringOrDefault(String columnName) {
        return getStringOrDefault(cursor.getColumnIndexForName(columnName));
    }


    public String getStringOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getString(index);
        } else {
            return null;
        }
    }

    public String getStringOrDefault(String columnName, String defValue) {
        return getStringOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }

    public int getIntOrDefault(String columnName) {
        return getIntOrDefault(cursor.getColumnIndexForName(columnName));
    }


    public int getIntOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getInt(index);
        } else {
            return 0;
        }
    }

    public int getIntOrDefault(int index, int defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getInt(index);
        } else {
            return defValue;
        }
    }

    public int getIntOrDefault(String columnName, int defValue) {
        return getIntOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }

    public double getDoubleOrDefault(int index, double defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getDouble(index);
        } else {
            return defValue;
        }
    }

    public double getDoubleOrDefault(String columnName) {
        return getDoubleOrDefault(cursor.getColumnIndexForName(columnName));
    }


    public double getDoubleOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getDouble(index);
        } else {
            return 0.0;
        }
    }

    public double getDoubleOrDefault(String columnName, double defValue) {
        return getDoubleOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }

    public float getFloatOrDefault(int index, float defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getFloat(index);
        } else {
            return defValue;
        }
    }

    public float getFloatOrDefault(String columnName) {
        return getFloatOrDefault(cursor.getColumnIndexForName(columnName));
    }


    public float getFloatOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getFloat(index);
        } else {
            return 0f;
        }
    }

    public float getFloatOrDefault(String columnName, float defValue) {
        return getFloatOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }

    public long getLongOrDefault(int index, long defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getLong(index);
        } else {
            return defValue;
        }
    }

    public long getLongOrDefault(String columnName) {
        return getLongOrDefault(cursor.getColumnIndexForName(columnName));
    }

    public long getLongOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getLong(index);
        } else {
            return 0L;
        }
    }

    public long getLongOrDefault(String columnName, long defValue) {
        return getLongOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }

    public short getShortOrDefault(int index, short defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getShort(index);
        } else {
            return defValue;
        }
    }

    public short getShortOrDefault(String columnName) {
        return getShortOrDefault(cursor.getColumnIndexForName(columnName));
    }


    public short getShortOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getShort(index);
        } else {
            return 0;
        }
    }

    public short getShortOrDefault(String columnName, short defValue) {
        return getShortOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }

    public byte[] getBlobOrDefault(String columnName) {
        return getBlobOrDefault(cursor.getColumnIndexForName(columnName));
    }


    public byte[] getBlobOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getBlob(index);
        } else {
            return null;
        }
    }

    public byte[] getBlobOrDefault(int index, byte[] defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return cursor.getBlob(index);
        } else {
            return defValue;
        }
    }

    public byte[] getBlobOrDefault(String columnName, byte[] defValue) {
        return getBlobOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }

    public boolean getBooleanOrDefault(int index, boolean defValue) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return getBoolean(index);
        } else {
            return defValue;
        }
    }

    public boolean getBooleanOrDefault(String columnName) {
        return getBooleanOrDefault(cursor.getColumnIndexForName(columnName));
    }

    public boolean getBooleanOrDefault(int index) {
        if (index != COLUMN_NOT_FOUND && !cursor.isColumnNull(index)) {
            return getBoolean(index);
        } else {
            return false;
        }
    }

    public boolean getBooleanOrDefault(String columnName, boolean defValue) {
        return getBooleanOrDefault(cursor.getColumnIndexForName(columnName), defValue);
    }


    public boolean getBoolean(int index) {
        return cursor.getInt(index) == 1;
    }

    public static FlowCursor from(ResultSet cursor) {
        return new FlowCursor(cursor);
    }
}

