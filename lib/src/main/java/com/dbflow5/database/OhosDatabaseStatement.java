package com.dbflow5.database;

import ohos.data.rdb.RdbStore;
import ohos.data.rdb.Statement;

/**
 * Description:
 */
public class OhosDatabaseStatement extends BaseDatabaseStatement implements DatabaseStatement {

    Statement statement;
    private final RdbStore database;
    private final String sql;

    public OhosDatabaseStatement(String sql, Statement statement, RdbStore database){
        this.sql = sql;
        this.statement = statement;
        this.database = database;
    }

    @Override
    public long executeUpdateDelete() {
        return statement.executeAndGetChanges();
    }

    @Override
    public void execute() {
        statement.execute();
        System.out.println("execute SQL:"+sql);
    }

    @Override
    public void close() {
        statement.close();
    }

    @Override
    public long simpleQueryForLong() {
        return OhosDatabase.rethrowDBFlowException(unused -> statement.executeAndGetLong());
    }

    @Override
    public String simpleQueryForString() {
        return OhosDatabase.rethrowDBFlowException(unused -> statement.executeAndGetString());
    }

    @Override
    public long executeInsert() {
        return OhosDatabase.rethrowDBFlowException(unused -> statement.executeAndGetLastInsertRowId());
    }

    @Override
    public void bindString(int index, String s) {
        statement.setString(index, s);
    }

    @Override
    public void bindNull(int index) {
        statement.setNull(index);
    }

    @Override
    public void bindLong(int index, Long aLong) {
        statement.setLong(index, aLong);
    }

    @Override
    public void bindDouble(int index, Double aDouble) {
        statement.setDouble(index, aDouble);
    }

    @Override
    public void bindBlob(int index, byte[] bytes) {
        statement.setBlob(index, bytes);
    }

    @Override
    public void bindAllArgsAsStrings(String[] selectionArgs) {
        statement.setStrings(selectionArgs);
    }

    public static OhosDatabaseStatement from(String sql, Statement sqLiteStatement, RdbStore database) {
        return new OhosDatabaseStatement(sql, sqLiteStatement, database);
    }
}
