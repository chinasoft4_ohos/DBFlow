package com.dbflow5.database;

import java.util.List;
import java.util.function.Function;

/**
 * Description:
 */
public interface MigrationFileHelper {

    List<String> getListFiles(String dbMigrationPath);

    void executeMigration(String fileName, Function<String, Void> dbFunction);
}