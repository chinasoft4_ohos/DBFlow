package com.dbflow5.database;

import com.dbflow5.query.BaseQueriable;
import com.dbflow5.runtime.NotifyDistributor;

/**
 * Description: Delegates all of its calls to the contained [DatabaseStatement], while
 * providing notification methods for when operations occur.
 */
public class DatabaseStatementWrapper<T> extends BaseDatabaseStatement {

    private final DatabaseStatement databaseStatement;
    private final BaseQueriable<T> modelQueriable;

    public DatabaseStatementWrapper(DatabaseStatement databaseStatement, BaseQueriable<T> modelQueriable) {
        this.databaseStatement = databaseStatement;
        this.modelQueriable = modelQueriable;
    }

    @Override
    public long executeUpdateDelete() {
        long affected = databaseStatement.executeUpdateDelete();
        if (affected > 0) {
            NotifyDistributor.get().notifyTableChanged(modelQueriable.table, modelQueriable.primaryAction());
        }
        return affected;
    }

    @Override
    public long executeInsert() {
        long affected = databaseStatement.executeInsert();
        if (affected > 0) {
            NotifyDistributor.get().notifyTableChanged(modelQueriable.table, modelQueriable.primaryAction());
        }
        return affected;
    }

    @Override
    public void execute() {
        databaseStatement.execute();
    }

    @Override
    public void close() {
        databaseStatement.close();
    }

    @Override
    public long simpleQueryForLong() {
        return databaseStatement.simpleQueryForLong();
    }

    @Override
    public String simpleQueryForString() {
        return databaseStatement.simpleQueryForString();
    }

    @Override
    public void bindString(int index, String s) {
        databaseStatement.bindString(index, s);
    }

    @Override
    public void bindNull(int index) {
        databaseStatement.bindNull(index);
    }

    @Override
    public void bindLong(int index, Long aLong) {
        databaseStatement.bindLong(index, aLong);
    }

    @Override
    public void bindDouble(int index, Double aDouble) {
        databaseStatement.bindDouble(index, aDouble);
    }

    @Override
    public void bindBlob(int index, byte[] bytes) {
        databaseStatement.bindBlob(index, bytes);
    }

    @Override
    public void bindAllArgsAsStrings(String[] selectionArgs) {
        databaseStatement.bindAllArgsAsStrings(selectionArgs);
    }
}
