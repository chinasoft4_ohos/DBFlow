package com.dbflow5.database;

/**
 * Description: Abstracts out the [DatabaseHelperDelegate] into the one used in this library.
 */
public interface OpenHelper extends OpenHelperDelegate {

    void setWriteAheadLoggingEnabled(boolean enabled);

    void setDatabaseListener(DatabaseCallback callback);

    void closeDB();

    void deleteDB();
}
