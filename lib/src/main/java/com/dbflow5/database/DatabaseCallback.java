package com.dbflow5.database;

/**
 * Description: Provides callbacks for [OpenHelper] methods
 */
public interface DatabaseCallback {

    /**
     * Called when the DB is opened
     *
     * @param database The database that is opened
     */
    void onOpen(DatabaseWrapper database);

    /**
     * Called when the DB is created
     *
     * @param database The database that is created
     */
    void onCreate(DatabaseWrapper database);

    /**
     * Called when the DB is upgraded.
     *
     * @param database   The database that is upgraded
     * @param oldVersion The previous DB version
     * @param newVersion The new DB version
     */
    void onUpgrade(DatabaseWrapper database, int oldVersion, int newVersion);

    /**
     * Called when DB is downgraded. Note that this may not be supported by all implementations of the DB.
     *
     * @param databaseWrapper The database downgraded.
     * @param oldVersion      The old. higher version.
     * @param newVersion      The new lower version.
     */
    void onDowngrade(DatabaseWrapper databaseWrapper, int oldVersion, int newVersion);

    /**
     * Called when DB connection is being configured. Useful for checking foreign key support or enabling
     * write-ahead-logging.
     *
     * @param db DB
     */
    void onConfigure(DatabaseWrapper db);
}
