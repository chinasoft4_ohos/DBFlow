package com.dbflow5.database;

public interface OpenHelperDelegate {
    DatabaseWrapper database();

    LocalDatabaseHelperDelegate delegate();

    boolean isDatabaseIntegrityOk();

    void performRestoreFromBackup();

    void backupDB();
}
