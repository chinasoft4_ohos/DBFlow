package com.dbflow5.database;

/**
 * Description: Default implementation for some [DatabaseStatement] methods.
 */
public abstract class BaseDatabaseStatement implements DatabaseStatement {
    @Override
    public void bindStringOrNull(int index, String s) {
        if (s != null) {
            bindString(index, s);
        } else {
            bindNull(index);
        }
    }

    @Override
    public void bindNumber(int index, Number number) {
        bindNumberOrNull(index, number);
    }

    @Override
    public void bindNumberOrNull(int index, Number number) {
        if (number != null) {
            bindLong(index, number.longValue());
        } else {
            bindNull(index);
        }
    }

    @Override
    public void bindDoubleOrNull(int index, Double aDouble) {
        if (aDouble != null) {
            bindDouble(index, aDouble);
        } else {
            bindNull(index);
        }
    }

    @Override
    public void bindFloatOrNull(int index, Float aFloat) {
        if (aFloat != null) {
            bindDouble(index, aFloat.doubleValue());
        } else {
            bindNull(index);
        }
    }

    @Override
    public void bindBlobOrNull(int index, byte[] bytes) {
        if (bytes != null) {
            bindBlob(index, bytes);
        } else {
            bindNull(index);
        }
    }

}
