package com.dbflow5.database;

import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;

/**
 * Description:
 */
public interface OhosDatabaseWrapper extends DatabaseWrapper {


    long updateWithOnConflict(String tableName,
                              ValuesBucket contentValues,
                              String whereClause,
                              String[] whereValues, RdbStore.ConflictResolution conflictAlgorithm);

    long insertWithOnConflict(String tableName,
                              String nullColumnHack,
                              ValuesBucket values,
                              RdbStore.ConflictResolution sqLiteDatabaseAlgorithmInt);

}