package com.dbflow5.database;

import java.io.Closeable;

/**
 * Description: Abstracts out an Ohos SQLiteStatement.
 */
public interface DatabaseStatement extends Closeable {

    long executeUpdateDelete();

    void execute();

    @Override
    void close();

    long simpleQueryForLong();

    String simpleQueryForString();

    long executeInsert();

    void bindString(int index, String s);

    void bindStringOrNull(int index, String s);

    void bindNull(int index);

    void bindLong(int index, Long aLong);

    void bindNumber(int index, Number number);

    void bindNumberOrNull(int index, Number number);

    void bindDouble(int index, Double aDouble);

    void bindDoubleOrNull(int index, Double aDouble);

    void bindFloatOrNull(int index, Float aFloat);

    void bindBlob(int index, byte[] bytes);

    void bindBlobOrNull(int index, byte[] bytes);

    void bindAllArgsAsStrings(String[] selectionArgs);
}
