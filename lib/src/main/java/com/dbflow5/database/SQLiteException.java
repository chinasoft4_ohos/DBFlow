package com.dbflow5.database;

/**
 * Description: DBFlow mirror to an SQLiteException.
 */
public class SQLiteException extends RuntimeException {
    public SQLiteException() {
    }

    public SQLiteException(String message) {
        super(message);
    }

    public SQLiteException(String message, Throwable cause) {
        super(message, cause);
    }
}

