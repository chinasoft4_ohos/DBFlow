package com.dbflow5.paging;

import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Retention;
import java.util.Collections;
import java.util.List;

class PageResult<T> {
    @SuppressWarnings("unchecked")
    private static final PageResult EMPTY_RESULT =
            new PageResult(Collections.emptyList(), 0);

    @SuppressWarnings("unchecked")
    private static final PageResult INVALID_RESULT =
            new PageResult(Collections.emptyList(), 0);

    @SuppressWarnings("unchecked")
    static <T> PageResult<T> getEmptyResult() {
        return EMPTY_RESULT;
    }

    @SuppressWarnings("unchecked")
    static <T> PageResult<T> getInvalidResult() {
        return INVALID_RESULT;
    }

    @Retention(SOURCE)
    @IntDef({INIT, APPEND, PREPEND, TILE})
    @interface ResultType {
    }

    static final int INIT = 0;

    // contiguous results
    static final int APPEND = 1;
    static final int PREPEND = 2;

    // non-contiguous, tile result
    static final int TILE = 3;

    @NonNull
    public final List<T> page;
    @SuppressWarnings("WeakerAccess")
    public final int leadingNulls;
    @SuppressWarnings("WeakerAccess")
    public final int trailingNulls;
    @SuppressWarnings("WeakerAccess")
    public final int positionOffset;

    PageResult(@NonNull List<T> list, int leadingNulls, int trailingNulls, int positionOffset) {
        this.page = list;
        this.leadingNulls = leadingNulls;
        this.trailingNulls = trailingNulls;
        this.positionOffset = positionOffset;
    }

    PageResult(@NonNull List<T> list, int positionOffset) {
        this.page = list;
        this.leadingNulls = 0;
        this.trailingNulls = 0;
        this.positionOffset = positionOffset;
    }

    @Override
    public String toString() {
        return "Result " + leadingNulls
                + ", " + page
                + ", " + trailingNulls
                + ", offset " + positionOffset;
    }

    public boolean isInvalid() {
        return this == INVALID_RESULT;
    }

    abstract static class Receiver<T> {
        @MainThread
        public abstract void onPageResult(@ResultType int type, @NonNull PageResult<T> pageResult);
    }
}