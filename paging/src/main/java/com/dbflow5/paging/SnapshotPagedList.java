package com.dbflow5.paging;

class SnapshotPagedList<T> extends PagedList<T> {
    private final boolean mContiguous;
    private final Object mLastKey;
    private final DataSource<?, T> mDataSource;

    SnapshotPagedList(@NonNull PagedList<T> pagedList) {
        super(pagedList.mStorage.snapshot(),
                pagedList.mMainThreadExecutor,
                pagedList.mBackgroundThreadExecutor,
                null,
                pagedList.mConfig);
        mDataSource = pagedList.getDataSource();
        mContiguous = pagedList.isContiguous();
        mLastLoad = pagedList.mLastLoad;
        mLastKey = pagedList.getLastKey();
    }

    @Override
    public boolean isImmutable() {
        return true;
    }

    @Override
    public boolean isDetached() {
        return true;
    }

    @Override
    boolean isContiguous() {
        return mContiguous;
    }

    @Nullable
    @Override
    public Object getLastKey() {
        return mLastKey;
    }

    @NonNull
    @Override
    public DataSource<?, T> getDataSource() {
        return mDataSource;
    }

    @Override
    void dispatchUpdatesSinceSnapshot(@NonNull PagedList<T> storageSnapshot,
                                      @NonNull Callback callback) {
    }

    @Override
    void loadAroundInternal(int index) {
    }
}

