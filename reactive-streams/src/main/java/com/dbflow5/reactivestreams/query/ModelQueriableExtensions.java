package com.dbflow5.reactivestreams.query;

import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.query.ModelQueriable;
import io.reactivex.rxjava3.core.Flowable;

public class ModelQueriableExtensions {
    public static <T> Flowable<T> queryStreamResults(ModelQueriable<T> modelQueriable, DBFlowDatabase dbFlowDatabase) {
        return new CursorListFlowable<>(modelQueriable, dbFlowDatabase);
    }
}

