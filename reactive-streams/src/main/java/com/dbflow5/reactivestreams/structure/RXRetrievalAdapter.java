package com.dbflow5.reactivestreams.structure;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.FlowManager;
import com.dbflow5.database.DatabaseWrapper;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

public class RXRetrievalAdapter<T>{
    private final RetrievalAdapter<T> retrievalAdapter;

    public RXRetrievalAdapter(RetrievalAdapter<T> retrievalAdapter) {
        this.retrievalAdapter = retrievalAdapter;
    }

    public RXRetrievalAdapter(Class<T> table) {
        this(FlowManager.getRetrievalAdapter(table));
    }

    public Completable load(T model, DatabaseWrapper databaseWrapper) {
        return Completable.fromCallable(() -> {
            retrievalAdapter.load(model, databaseWrapper);
            return null;
        });
    }

    public Single<Boolean> exists(T model, DatabaseWrapper wrapper) {
        return Single.fromCallable(() -> retrievalAdapter.exists(model, wrapper));
    }

    public static <T> RXRetrievalAdapter<T> from(RetrievalAdapter<T> modelAdapter) {
        return new RXRetrievalAdapter<>(modelAdapter);
    }


    public static <T> RXRetrievalAdapter<T> from(Class<T> table) {
        return new RXRetrievalAdapter<>(table);
    }
}
