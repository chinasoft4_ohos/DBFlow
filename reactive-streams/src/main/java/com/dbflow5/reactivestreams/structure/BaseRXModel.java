package com.dbflow5.reactivestreams.structure;

import com.dbflow5.database.DatabaseWrapper;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

/**
 * Description: Similar to [BaseModel] with RX constructs. Extend this for convenience methods.
 */
public class BaseRXModel {
    private RXModelAdapter<BaseRXModel> rxModelAdapter() {
        return new RXModelAdapter<>(BaseRXModel.class);   
    }

    public Single<Boolean> save(DatabaseWrapper databaseWrapper) {
        return rxModelAdapter().save(this, databaseWrapper);
    }

    public Completable load(DatabaseWrapper databaseWrapper) {
        return rxModelAdapter().load(this, databaseWrapper);
    }

    public Single<Boolean> delete(DatabaseWrapper databaseWrapper) {
        return rxModelAdapter().delete(this, databaseWrapper);
    }

    public Single<Boolean> update(DatabaseWrapper databaseWrapper) {
        return rxModelAdapter().update(this, databaseWrapper);
    }

    public Single<Long> insert(DatabaseWrapper databaseWrapper) {
        return rxModelAdapter().insert(this, databaseWrapper);
    }

    public Single<Boolean> exists(DatabaseWrapper databaseWrapper) {
        return rxModelAdapter().exists(this, databaseWrapper);
    }

    public static <T> Single<Boolean> rxSave(T model, DatabaseWrapper databaseWrapper) {
        return new RXModelAdapter<>((Class<T>) model.getClass()).save(model, databaseWrapper);
    }

    public static <T> Completable rxLoad(T model, DatabaseWrapper databaseWrapper) {
        return new RXModelAdapter<>((Class<T>) model.getClass()).load(model, databaseWrapper);
    }

    public static <T> Single<Boolean> rxDelete(T model, DatabaseWrapper databaseWrapper) {
        return new RXModelAdapter<>((Class<T>) model.getClass()).delete(model, databaseWrapper);
    }

    public static <T> Single<Boolean> rxUpdate(T model, DatabaseWrapper databaseWrapper) {
        return new RXModelAdapter<>((Class<T>) model.getClass()).update(model, databaseWrapper);
    }

    public static <T> Single<Long> rxInsert(T model, DatabaseWrapper databaseWrapper) {
        return new RXModelAdapter<>((Class<T>) model.getClass()).insert(model, databaseWrapper);
    }

    public static <T> Single<Boolean> rxExists(T model, DatabaseWrapper databaseWrapper) {
        return new RXModelAdapter<>((Class<T>) model.getClass()).exists(model, databaseWrapper);
    }
}

