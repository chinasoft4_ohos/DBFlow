#  DBFlow

#### 项目介绍
- 项目名称：DBFlow
- 所属系列：openharmony的第三方组件适配移植
- 功能：DBFlow是一个快速、高效、功能丰富数据库组件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta2
- 基线版本：Releases 4.2.4

#### 安装教程

1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:DBFlow_lib:1.0.0')
    implementation('com.gitee.chinasoft_ohos:DBFlow_sqlcipher:1.0.0')
    implementation('com.gitee.chinasoft_ohos:DBFlow_reactive-streams:1.0.0')
    implementation('com.gitee.chinasoft_ohos:DBFlow_contentprovider:1.0.0')
    implementation('com.gitee.chinasoft_ohos:DBFlow_paging:1.0.0')
    implementation('com.gitee.chinasoft_ohos:DBFlow_livedata:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta2下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
* 初始化：
 调用FlowManager.init()方法，传入对应的参数，方法如下：
```
    FlowManager.init(DemoApp.context, builder -> {
                        DatabaseConfig.Builder builder1 = DatabaseConfig.builder(PrepackagedDB.class, OhosSQLiteOpenHelper.createHelperCreator(DemoApp.context));
                        builder1.transactionManagerCreator(ImmediateTransactionManager::new);
                        dbConfigBlock.apply(FlowConfig.builder(DemoApp.context)
                            .database(builder1.build()));
                        return null;
                    });
```
* 创建数据库以及数据库表，方法如下：
```
    @Database(version = 1)
    public abstract class TestDatabase extends DBFlowDatabase {
        @Migration(version = 1, database = TestDatabase.class, priority = 5)
        public static class TestMigration extends UpdateTableMigration<SimpleTestModels.SimpleModel> {
            public void onPreMigrate() {
                super.onPreMigrate();
                set(SimpleModel_Table.name.eq("Test")).where(SimpleModel_Table.name.eq("Test1"));
            }
    
            public TestMigration() {
                super(SimpleTestModels.SimpleModel.class);
            }
    
        }
        @Migration(version = 1, database = TestDatabase.class, priority = 1)
        public static class SecondMigration extends BaseMigration {
            public void migrate(DatabaseWrapper database) {
    
            }
        }
    }
```
* CRUD基本操作：
```
TestDatabase db= FlowManager.getDatabase(TestDatabase.class);
        for (int i = 0; i < 9; i++) {
            SimpleTestModels.SimpleModel simpleModel=new SimpleTestModels.SimpleModel(""+i);

            Model.save(SimpleTestModels.SimpleModel.class, simpleModel, db);

            Model.insert(SimpleTestModels.SimpleModel.class, simpleModel, db);

            Model.delete(SimpleTestModels.SimpleModel.class, simpleModel, db);

            Model.update(SimpleTestModels.SimpleModel.class, simpleModel, db);
        }
```
* 事务操作、批量操作：
```
FlowManager.databaseForTable(SimpleModel.class, dbFlowDatabase -> {
            dbFlowDatabase.executeTransaction((ITransaction<Object>) databaseWrapper -> {
                SQLite.insert(SimpleModel.class, SimpleModel_Table.name).values("test").executeInsert(dbFlowDatabase);
                SQLite.insert(SimpleModel.class, SimpleModel_Table.name).values("test1").executeInsert(dbFlowDatabase);
                SQLite.insert(SimpleModel.class, SimpleModel_Table.name).values("test2").executeInsert(dbFlowDatabase);
                return null;
            });
            return null;
        });
```
```
DBFlowDatabase db = FlowManager.getDatabaseForTable(SimpleModel.class);

        db.beginTransactionAsync((Function<DatabaseWrapper, Object>) databaseWrapper -> {
            for (int i = 0; i < 10; i++) {
                FlowManager.getModelAdapter(SimpleModel.class).save(new SimpleModel(String.valueOf(i)), db);
            }
            return null;
        }).success((objectTransaction, o) -> {
            FlowCursorIterator<SimpleModel> iterator = SQLite.select().from(SimpleModel.class).cursorList(db).iterator(2, 7);
            final int[] count = {0};

            iterator.forEachRemaining(simpleModel -> {
                assertEquals(String.valueOf(count[0] + 2), simpleModel.name);
                count[0]++;
            });
            assertEquals(7, count[0]);
            return null;
        }).execute(null, null, null, null);
```
```
FlowManager.database(TestDatabase.class, testDatabase -> null)
                    .beginTransactionAsync((Function<DatabaseWrapper, Object>) db -> {
                        for(int i=0; i<=2; i++) {
                            LiveDataModel model = new LiveDataModel(String.valueOf(i), i);
                            Model.insert(LiveDataModel.class, model, db);
                        }
                        return null;
                    }).execute(null, null, null, null);
```
* 释放资源：
```
   FlowManager.destroy();
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
Copyright (c) 2014 Raizlabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
