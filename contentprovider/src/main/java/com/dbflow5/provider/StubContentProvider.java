package com.dbflow5.provider;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.Context;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;


public class StubContentProvider {
    private DataAbilityHelper mDataAbilityHelper;

    public StubContentProvider(Context context) {
        mDataAbilityHelper = DataAbilityHelper.creator(context);
    }

    public int insert(Uri uri, ValuesBucket value) {
        return -1;
    }

    public int delete(Uri uri, DataAbilityPredicates predicates) {
        return 0;
    }

    public int update(Uri uri, ValuesBucket value, DataAbilityPredicates predicates) {
        return 0;
    }

    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        return null;
    }

    public String getType(Uri uri) {
        return null;
    }
}
