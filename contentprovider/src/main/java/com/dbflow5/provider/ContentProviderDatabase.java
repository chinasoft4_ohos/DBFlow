package com.dbflow5.provider;

import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.OhosDatabaseWrapper;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;

/**
 * Description: Base class providing [ContentValues] for a [DBFlowDatabase].
 */
public abstract class ContentProviderDatabase extends DBFlowDatabase implements OhosDatabaseWrapper {

    public ContentProviderDatabase() {
        super();
    }

    private OhosDatabaseWrapper database() {
        if(getWritableDatabase() instanceof OhosDatabaseWrapper) {
            return (OhosDatabaseWrapper)getWritableDatabase();
        }

        throw new IllegalStateException("Invalid DB type used. It must be a type of "+OhosDatabaseWrapper.class+". ");
    }

    @Override
    public long updateWithOnConflict(String tableName, ValuesBucket contentValues, String whereClause, String[] whereValues, RdbStore.ConflictResolution conflictAlgorithm) {
        return database().updateWithOnConflict(tableName, contentValues, whereClause, whereValues, conflictAlgorithm);
    }

    @Override
    public long insertWithOnConflict(String tableName, String nullColumnHack, ValuesBucket values, RdbStore.ConflictResolution sqLiteDatabaseAlgorithmInt) {
        return database().insertWithOnConflict(tableName, nullColumnHack, values, sqLiteDatabaseAlgorithmInt);
    }
}