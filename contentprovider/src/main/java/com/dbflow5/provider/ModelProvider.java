package com.dbflow5.provider;

import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.query.Operator;
import com.dbflow5.query.OperatorGroup;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.utils.net.Uri;

/**
 * Description: A base interface for Models that are connected to providers.
 */
public interface ModelProvider {

    /**
     * The [Uri] that passes to a [ContentProvider] to delete a Model.
     *
     * @return The [Uri] that passes to a [ContentProvider] to delete a Model.
     */
    Uri deleteUri();

    /**
     * The [Uri] that passes to a [ContentProvider] to insert a Model.
     *
     * @return The [Uri] that passes to a [ContentProvider] to insert a Model.
     */
    Uri insertUri();

    /**
     * The [Uri] that passes to a [ContentProvider] to update a Model.
     *
     * @return The [Uri] that passes to a [ContentProvider] to update a Model.
     */
    Uri updateUri();

    /**
     * The [Uri] that passes to a [ContentProvider] to query a Model.
     *
     * @return The [Uri] that passes to a [ContentProvider] to query a Model.
     */
    Uri queryUri();

    /**
     * Queries the [ContentResolver] of the app based on the passed parameters and
     * populates this object with the first row from the returned data.
     *
     * @param whereOperatorGroup The set of [Operator] to filter the query by.
     * @param orderBy            The order by without the ORDER BY
     * @param wrapper            wrapper
     * @param columns            The list of columns to select. Leave blank for *
     * @param <T>
     * @return T
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    <T> T load(OperatorGroup whereOperatorGroup, String orderBy, DatabaseWrapper wrapper, String... columns) throws DataAbilityRemoteException;

    /**
     * Queries the [ContentResolver] of the app based on the primary keys of the object and returns a
     * new object with the first row from the returned data.
     *
     * @param wrapper DatabaseWrapper
     * @throws  DataAbilityRemoteException DataAbilityRemoteException
     * @return T
     */
    <T> T load(DatabaseWrapper wrapper) throws DataAbilityRemoteException;
}
