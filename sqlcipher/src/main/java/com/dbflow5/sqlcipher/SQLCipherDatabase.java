package com.dbflow5.sqlcipher;

import com.dbflow5.database.OhosDatabaseWrapper;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.FlowCursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;

import java.util.function.Function;

/**
 * Description: Implements the code necessary to use a [SQLiteDatabase] in dbflow.
 */
public class SQLCipherDatabase implements OhosDatabaseWrapper {
    public SQLiteDatabase database;

    public SQLCipherDatabase(SQLiteDatabase database) {
        this.database = database;
    }

    @Override
    public long updateWithOnConflict(String tableName, ValuesBucket contentValues, String whereColumn, String[] whereValues, RdbStore.ConflictResolution conflictAlgorithm) {
        return 0;
    }

    @Override
    public long insertWithOnConflict(String tableName, String nullColumnHack, ValuesBucket values, RdbStore.ConflictResolution sqLiteDatabaseAlgorithmInt) {
        return 0;
    }

    @Override
    public boolean isInTransaction() {
        //return database.inTransaction();
        return false;
    }

    @Override
    public int getVersion() {
        return database.getVersion();
    }

    @Override
    public void execSQL(String query) {
        database.execSQL(query);
    }

    @Override
    public void beginTransaction() {
        database.beginTransaction();
    }

    @Override
    public void setTransactionSuccessful() {
        database.setTransactionSuccessful();
    }

    @Override
    public void endTransaction() {
        database.endTransaction();
    }

    @Override
    public DatabaseStatement compileStatement(String rawQuery) {
        return SQLCipherStatement.from(database.compileStatement(rawQuery));
    }

    @Override
    public FlowCursor rawQuery(String query, String[] selectionArgs) {
        return FlowCursor.from(database.rawQuery(query, selectionArgs));
    }

    @Override
    public FlowCursor query(String tableName, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return FlowCursor.from(database.query(tableName, columns, selection, selectionArgs, groupBy, having, orderBy));
    }

    @Override
    public int delete(String tableName, String whereClause, String[] whereArgs) {
        return database.delete(tableName, whereClause, whereArgs);
    }

    public static SQLCipherDatabase from(SQLiteDatabase database) {
        return new SQLCipherDatabase(database);
    }

    public static com.dbflow5.database.SQLiteException toDBFlowSQLiteException(SQLiteException exception) {
        return new com.dbflow5.database.SQLiteException("A Database Error Occurred", exception);
    }

    public static <T> T rethrowDBFlowException(Function<Void, T> fn) {
        try {
            return fn.apply(null);
        } catch (SQLiteException e) {
            throw toDBFlowSQLiteException(e);
        }
    }
}

