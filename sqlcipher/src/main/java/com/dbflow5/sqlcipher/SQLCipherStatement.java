package com.dbflow5.sqlcipher;

import com.dbflow5.database.BaseDatabaseStatement;

import net.sqlcipher.database.SQLiteStatement;

/**
 * Description: Implements the methods necessary for [DatabaseStatement]. Delegates calls to
 * the contained [SQLiteStatement].
 */
public class SQLCipherStatement extends BaseDatabaseStatement {
    public SQLiteStatement statement;

    public SQLCipherStatement(SQLiteStatement statement) {
        this.statement = statement;
    }

    @Override
    public long executeUpdateDelete() {
        return SQLCipherDatabase.rethrowDBFlowException(unused -> (long)statement.executeUpdateDelete());
    }

    @Override
    public void execute() {
        statement.execute();
    }

    @Override
    public void close() {
        statement.close();
    }

    @Override
    public long simpleQueryForLong() {
        return SQLCipherDatabase.rethrowDBFlowException(unused -> statement.simpleQueryForLong());
    }

    @Override
    public String simpleQueryForString() {
        return SQLCipherDatabase.rethrowDBFlowException(unused -> statement.simpleQueryForString());
    }

    @Override
    public long executeInsert() {
        return SQLCipherDatabase.rethrowDBFlowException(unused -> statement.executeInsert());
    }

    @Override
    public void bindString(int index, String s) {
        statement.bindString(index, s);
    }

    @Override
    public void bindNull(int index) {
        statement.bindNull(index);
    }

    @Override
    public void bindLong(int index, Long aLong) {
        statement.bindLong(index, aLong);
    }

    @Override
    public void bindDouble(int index, Double aDouble) {
        statement.bindDouble(index, aDouble);
    }

    @Override
    public void bindBlob(int index, byte[] bytes) {
        statement.bindBlob(index, bytes);
    }

    @Override
    public void bindAllArgsAsStrings(String[] selectionArgs) {
        if (selectionArgs != null) {
            for (int i = selectionArgs.length; i > 0; i--) {
                bindString(i, selectionArgs[i - 1]);
            }
        }
    }

    public static SQLCipherStatement from(SQLiteStatement statement) {
        return new SQLCipherStatement(statement);
    }
}
