package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.config.FlowManager;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.structure.Model;
import org.junit.Test;

public class TempModelTest extends BaseUnitTest {
    @Table(database = TestDatabase.class, temporary = true, createWithDatabase = false)
    public static class TempModel{
        @PrimaryKey
        public int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public TempModel(int id) {
            this.id = id;
        }
    }

    @Test
    public void  createTempTable() {
        FlowManager.database(TestDatabase.class, db -> {
            FlowManager.modelAdapter(TempModel.class).createIfNotExists(db);

            Model.save(TempModel.class, new TempModel(5), db);

            FlowManager.modelAdapter(TempModel.class).drop(db, false);

            return null;
        });
    }
}
