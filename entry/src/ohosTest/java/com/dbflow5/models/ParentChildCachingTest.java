package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.config.FlowManager;
import com.dbflow5.query.SQLite;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.structure.Model;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParentChildCachingTest extends BaseUnitTest {
    @Test
    public void testCanLoadChildFromCache() {
        FlowManager.database(TestDatabase.class, db -> {
            SimpleTestModels.TestModelChild child = new SimpleTestModels.TestModelChild(1L, "Test child");
            Model.save(SimpleTestModels.TestModelChild.class, child, db);

            SimpleTestModels.TestModelParent parent = new SimpleTestModels.TestModelParent(1, "Test parent", child);
            Model.save(SimpleTestModels.TestModelParent.class, parent, db);

            parent = SQLite.select().from(SimpleTestModels.TestModelParent.class).requireSingle(db);
            SimpleTestModels.TestModelChild parentChild = parent.child;
            parentChild = Model.load(SimpleTestModels.TestModelChild.class, parentChild, db);

            assertEquals(1, parentChild.id);
            assertEquals("Test child", parentChild.name);
            return null;
        });
    }
}
