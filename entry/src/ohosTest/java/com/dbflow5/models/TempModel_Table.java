package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.TempModelTest.TempModel;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class TempModel_Table extends ModelAdapter<TempModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(TempModel.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id};

  public TempModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TempModel> table() {
    return TempModel.class;
  }

  @Override
  public final String getName() {
    return "TempModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final boolean createWithDatabase() {
    return false;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, TempModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, TempModel model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindLong(2, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, TempModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO TempModel(id) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO TempModel(id) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE TempModel SET id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM TempModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TEMP TABLE IF NOT EXISTS TempModel(id INTEGER, PRIMARY KEY(id))";
  }

  @Override
  public final TempModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    TempModel model = new TempModel(0);
    model.setId(cursor.getIntOrDefault("id"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(TempModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
