package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.Fts3Model;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class Fts3Model_Table extends ModelAdapter<Fts3Model> {
  public static final Property<String> name = new Property<String>(Fts3Model.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name};

  public Fts3Model_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Fts3Model> table() {
    return Fts3Model.class;
  }

  @Override
  public final String getName() {
    return "Fts3Model";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Fts3Model model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Fts3Model model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Fts3Model model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Fts3Model(name) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Fts3Model(name) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Fts3Model SET name=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Fts3Model WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE VIRTUAL TABLE IF NOT EXISTS Fts3Model USING FTS3(name)";
  }

  @Override
  public final Fts3Model loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Fts3Model model = new Fts3Model("");
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Fts3Model model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
