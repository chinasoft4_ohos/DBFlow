package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.ForeignKey;
import com.dbflow5.annotation.MultiCacheField;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.Table;
import com.dbflow5.query.cache.MultiKeyCacheConverter;

public class CachingModels {

    @Table(database = TestDatabase.class, cachingEnabled = true)
    public static class SimpleCacheObject{

        @PrimaryKey
        public String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public SimpleCacheObject(String id) {
            this.id = id;
        }
    }

    @Table(database = TestDatabase.class, cachingEnabled = true)
    public static class Coordinate{
        @PrimaryKey
        public Double latitude;
        @PrimaryKey
        public Double longitude;
        @ForeignKey
        public Path path;

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Path getPath() {
            return path;
        }

        public void setPath(Path path) {
            this.path = path;
        }

        public static MultiKeyCacheConverter<?> getCacheConverter() {
            return cacheConverter;
        }

        public Coordinate(Double latitude, Double longitude, Path path) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.path = path;
        }

        @MultiCacheField
        public static final MultiKeyCacheConverter<?> cacheConverter = (MultiKeyCacheConverter<Object>) values -> values[0] + "," + values[1];
    }

    @Table(database = TestDatabase.class)
    public static class Path{
        @PrimaryKey
        public String id;
        @Column
        public String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Path(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
