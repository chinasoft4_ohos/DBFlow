package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.Fts4Model;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class Fts4Model_Table extends ModelAdapter<Fts4Model> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> id = new Property<Integer>(Fts4Model.class, "id");

  public static final Property<String> name = new Property<String>(Fts4Model.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name};

  public Fts4Model_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Fts4Model> table() {
    return Fts4Model.class;
  }

  @Override
  public final String getName() {
    return "Fts4Model";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(Fts4Model model, Number id) {
    model.setId(id.intValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Fts4Model model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Fts4Model model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    statement.bindLong(3, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Fts4Model model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Fts4Model(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Fts4Model(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Fts4Model SET id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Fts4Model WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Fts4Model(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)";
  }

  @Override
  public final Fts4Model loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Fts4Model model = new Fts4Model(0,"");
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final boolean exists(Fts4Model model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(Fts4Model.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Fts4Model model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
