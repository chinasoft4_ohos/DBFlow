package com.dbflow5.models;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.TestExtensions;
import com.dbflow5.models.java.JavaModelView;
import org.junit.Test;

public class ModelViewTest extends BaseUnitTest {
    @Test
    public void validateModelViewQuery() {
        TestExtensions.assertEquals("SELECT id AS authorId, first_name || ' ' || last_name AS authorName FROM Author", ModelViews.AuthorView.getQuery());
    }

    @Test
    public void validateJavaModelViewQuery() {
        TestExtensions.assertEquals("SELECT first_name AS firstName, id AS id", JavaModelView.getQuery());
    }
}
