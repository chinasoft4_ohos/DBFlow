package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.BlogDeferred;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class BlogDeferred_Table extends ModelAdapter<BlogDeferred> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> id = new Property<Integer>(BlogDeferred.class, "id");

  public static final Property<String> name = new Property<String>(BlogDeferred.class, "name");

  /**
   * Foreign Key */
  public static final Property<Integer> author_id = new Property<Integer>(BlogDeferred.class, "author_id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name,author_id};

  public BlogDeferred_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<BlogDeferred> table() {
    return BlogDeferred.class;
  }

  @Override
  public final String getName() {
    return "BlogDeferred";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      case "author_id": {
        return author_id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(BlogDeferred model, Number id) {
    model.setId(id.intValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, BlogDeferred model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getAuthor() != null) {
      statement.bindLong(3, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, BlogDeferred model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getAuthor() != null) {
      statement.bindLong(3, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(3);
    }
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, BlogDeferred model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO BlogDeferred(id,name,author_id) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO BlogDeferred(id,name,author_id) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE BlogDeferred SET id=?,name=?,author_id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM BlogDeferred WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS BlogDeferred(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, author_id INTEGER, FOREIGN KEY(author_id) REFERENCES Author (id) ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED)";
  }

  @Override
  public final BlogDeferred loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    BlogDeferred model = new BlogDeferred(0,"",null);
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name", ""));
    int index_author_id_Author_Table = cursor.getColumnIndexForName("author_id");
    if (index_author_id_Author_Table != -1 && !cursor.isColumnNull(index_author_id_Author_Table)) {
      model.setAuthor(SQLite.select().from(ForeignKeyModels.Author.class).where()
          .and(Author_Table.id.eq(cursor.getInt(index_author_id_Author_Table)))
          .querySingle(wrapper));
    } else {
      model.setAuthor(null);
    }
    return model;
  }

  @Override
  public final boolean exists(BlogDeferred model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(BlogDeferred.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(BlogDeferred model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
