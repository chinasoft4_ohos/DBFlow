package com.dbflow5.models.java;

import com.dbflow5.adapter.ModelViewAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class JavaModelView_ViewTable extends ModelViewAdapter<JavaModelView> {
  public static final String VIEW_NAME = "JavaModelView";

  public static final Property<String> id = new Property<String>(JavaModelView.class, "id");

  public static final Property<Integer> firstName = new Property<Integer>(JavaModelView.class, "firstName");

  public JavaModelView_ViewTable(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<JavaModelView> table() {
    return JavaModelView.class;
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE VIEW IF NOT EXISTS JavaModelView AS " + JavaModelView.getQuery().getQuery();
  }

  @Override
  public final String getName() {
    return "JavaModelView";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.View;
  }

  @Override
  public final JavaModelView loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    JavaModelView model = new JavaModelView();
    model.id = cursor.getStringOrDefault("id");
    model.firstName = cursor.getIntOrDefault("firstName", 0);
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(JavaModelView model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.id));
    clause.and(firstName.eq(model.firstName));
    return clause;
  }
}
