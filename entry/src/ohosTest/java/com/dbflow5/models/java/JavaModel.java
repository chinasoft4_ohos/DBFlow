package com.dbflow5.models.java;

import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.TestDatabase;

@Table(database = TestDatabase.class)
public class JavaModel {

    @PrimaryKey
    String id;
}

