package com.dbflow5.models.java;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class JavaModel_Table extends ModelAdapter<JavaModel> {
  /**
   * Primary Key */
  public static final Property<String> id = new Property<String>(JavaModel.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id};

  public JavaModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<JavaModel> table() {
    return JavaModel.class;
  }

  @Override
  public final String getName() {
    return "JavaModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, JavaModel model) {
    statement.bindStringOrNull(1, model.id);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, JavaModel model) {
    statement.bindStringOrNull(1, model.id);
    statement.bindStringOrNull(2, model.id);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, JavaModel model) {
    statement.bindStringOrNull(1, model.id);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO JavaModel(id) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO JavaModel(id) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE JavaModel SET id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM JavaModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS JavaModel(id TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final JavaModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    JavaModel model = new JavaModel();
    model.id = cursor.getStringOrDefault("id");
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(JavaModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.id));
    return clause;
  }
}
