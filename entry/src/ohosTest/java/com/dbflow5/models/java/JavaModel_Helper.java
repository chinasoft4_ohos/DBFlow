package com.dbflow5.models.java;

import java.lang.String;

public final class JavaModel_Helper {
  public static final String getId(JavaModel model) {
    return model.id;
  }

  public static final void setId(JavaModel model, String var) {
    model.id = var;
  }
}
