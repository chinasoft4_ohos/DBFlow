package com.dbflow5.models.java.otherpackage;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.models.java.JavaModel;
import com.dbflow5.models.java.JavaModel_Table;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class ExampleModel_Table extends ModelAdapter<ExampleModel> {
  public static final Property<String> name = new Property<String>(ExampleModel.class, "name");

  /**
   * Foreign Key */
  public static final Property<String> model_id = new Property<String>(ExampleModel.class, "model_id");

  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(ExampleModel.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name,model_id,id};

  public ExampleModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<ExampleModel> table() {
    return ExampleModel.class;
  }

  @Override
  public final String getName() {
    return "ExampleModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      case "model_id": {
        return model_id;
      }
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, ExampleModel model) {
    statement.bindStringOrNull(1, model.name);
    if (model.model != null) {
      statement.bindStringOrNull(2, com.dbflow5.models.java.JavaModel_Helper.getId(model.model));
    } else {
      statement.bindNull(2);
    }
    statement.bindNumberOrNull(3, model.getId());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, ExampleModel model) {
    statement.bindStringOrNull(1, model.name);
    if (model.model != null) {
      statement.bindStringOrNull(2, com.dbflow5.models.java.JavaModel_Helper.getId(model.model));
    } else {
      statement.bindNull(2);
    }
    statement.bindNumberOrNull(3, model.getId());
    statement.bindNumberOrNull(4, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, ExampleModel model) {
    statement.bindNumberOrNull(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO ExampleModel(name,model_id,id) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO ExampleModel(name,model_id,id) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE ExampleModel SET name=?,model_id=?,id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM ExampleModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS ExampleModel(name TEXT, model_id TEXT, id INTEGER, PRIMARY KEY(id), FOREIGN KEY(model_id) REFERENCES JavaModel (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final ExampleModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    ExampleModel model = new ExampleModel();
    model.name = cursor.getStringOrDefault("name");
    int index_model_id_JavaModel_Table = cursor.getColumnIndexForName("model_id");
    if (index_model_id_JavaModel_Table != -1 && !cursor.isColumnNull(index_model_id_JavaModel_Table)) {
      model.model = SQLite.select().from(JavaModel.class).where()
          .and(JavaModel_Table.id.eq(cursor.getString(index_model_id_JavaModel_Table)))
          .querySingle(wrapper);
    } else {
      model.model = null;
    }
    model.setId(cursor.getIntOrDefault("id", 0));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(ExampleModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
