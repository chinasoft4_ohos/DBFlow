package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.ForeignKey;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.structure.BaseModel;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
@Table(
    database = TestDatabase.class
)
public final class Refund extends BaseModel {
  @PrimaryKey(
      autoincrement = true
  )
  long _id;

  @ForeignKey(
      saveForeignKeyModel = true
  )
  SimpleTestModels.Transfer refund_in;

  @ForeignKey(
      saveForeignKeyModel = true
  )
  SimpleTestModels.Transfer refund_out;

  public final long getId() {
    return _id;
  }

  public final SimpleTestModels.Transfer getRefund_in() {
    return refund_in;
  }

  public final void setRefund_in(SimpleTestModels.Transfer param) {
    refund_in = param;
  }

  public final SimpleTestModels.Transfer getRefund_out() {
    return refund_out;
  }

  public final void setRefund_out(SimpleTestModels.Transfer param) {
    refund_out = param;
  }
}
