package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.BooleanConverter;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.SimpleTestModels.DontAssignDefaultModel;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class DontAssignDefaultModel_Table extends ModelAdapter<DontAssignDefaultModel> {
  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(DontAssignDefaultModel.class, "name");

  public static final TypeConvertedProperty<Integer, Boolean> nullableBool = new TypeConvertedProperty<Integer, Boolean>(DontAssignDefaultModel.class, "nullableBool", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          DontAssignDefaultModel_Table adapter = (DontAssignDefaultModel_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterBooleanConverter;
                      }
                      });

  public static final Property<Integer> index = new Property<Integer>(DontAssignDefaultModel.class, "_index");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name,nullableBool,index};

  private final BooleanConverter global_typeConverterBooleanConverter;

  public DontAssignDefaultModel_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterBooleanConverter = (BooleanConverter) holder.getTypeConverterForClass(Boolean.class);
  }

  @Override
  public final Class<DontAssignDefaultModel> table() {
    return DontAssignDefaultModel.class;
  }

  @Override
  public final String getName() {
    return "DontAssignDefaultModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      case "nullableBool":  {
        return nullableBool;
      }
      case "_index":  {
        return index;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      DontAssignDefaultModel model) {
    statement.bindStringOrNull(1, model.getName());
    Integer refnullableBool = global_typeConverterBooleanConverter.getDBValue(model.nullableBool);
    statement.bindNumberOrNull(2, refnullableBool);
    statement.bindLong(3, (long)model.getIndex());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      DontAssignDefaultModel model) {
    statement.bindStringOrNull(1, model.getName());
    Integer refnullableBool = global_typeConverterBooleanConverter.getDBValue(model.nullableBool);
    statement.bindNumberOrNull(2, refnullableBool);
    statement.bindLong(3, (long)model.getIndex());
    statement.bindStringOrNull(4, model.getName());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      DontAssignDefaultModel model) {
    statement.bindStringOrNull(1, model.getName());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO DontAssignDefaultModel(name,nullableBool,_index) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO DontAssignDefaultModel(name,nullableBool,_index) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE DontAssignDefaultModel SET name=?,nullableBool=?,_index=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM DontAssignDefaultModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS DontAssignDefaultModel(name TEXT, nullableBool INTEGER, _index INTEGER, PRIMARY KEY(name))";
  }

  @Override
  public final DontAssignDefaultModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    DontAssignDefaultModel model = new DontAssignDefaultModel(null, false, 0);
    model.setName(cursor.getStringOrDefault("name"));
    int index_nullableBool = cursor.getColumnIndexForName("nullableBool");
    if (index_nullableBool != -1 && !cursor.isColumnNull(index_nullableBool)) {
      model.setNullableBool(global_typeConverterBooleanConverter.getModelValue(cursor.getInt(index_nullableBool)));
    }
    model.setIndex(cursor.getIntOrDefault("_index"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(DontAssignDefaultModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
