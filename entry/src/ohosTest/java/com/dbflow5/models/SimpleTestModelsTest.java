package com.dbflow5.models;

import com.dbflow5.config.FlowManager;

import com.dbflow5.BaseUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleTestModelsTest extends BaseUnitTest {
    @Test
    public void validateCreationQuery() {
        assertEquals("CREATE TABLE IF NOT EXISTS TypeConverterModel(" +
            "id INTEGER, " +
            "opaqueData BLOB, " +
            "blob BLOB, " +
            "customType INTEGER, " +
            "PRIMARY KEY(id, customType))", FlowManager.getModelAdapter(SimpleTestModels.TypeConverterModel.class).getCreationQuery());
    }
}
