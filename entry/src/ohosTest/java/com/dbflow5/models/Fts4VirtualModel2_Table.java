package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.Fts4VirtualModel2;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class Fts4VirtualModel2_Table extends ModelAdapter<Fts4VirtualModel2> {
  public static final Property<String> name = new Property<String>(Fts4VirtualModel2.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name};

  public Fts4VirtualModel2_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Fts4VirtualModel2> table() {
    return Fts4VirtualModel2.class;
  }

  @Override
  public final String getName() {
    return "Fts4VirtualModel2";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Fts4VirtualModel2 model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Fts4VirtualModel2 model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Fts4VirtualModel2 model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Fts4VirtualModel2(name) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Fts4VirtualModel2(name) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Fts4VirtualModel2 SET name=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Fts4VirtualModel2 WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE VIRTUAL TABLE IF NOT EXISTS Fts4VirtualModel2 USING FTS4(name, content=Fts4Model)";
  }

  @Override
  public final Fts4VirtualModel2 loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Fts4VirtualModel2 model = new Fts4VirtualModel2("");
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Fts4VirtualModel2 model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
