package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.DefaultModel;
import java.lang.Double;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class DefaultModel_Table extends ModelAdapter<DefaultModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(DefaultModel.class, "id");

  public static final Property<Double> location = new Property<Double>(DefaultModel.class, "location");

  public static final Property<String> name = new Property<String>(DefaultModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,location,name};

  public DefaultModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<DefaultModel> table() {
    return DefaultModel.class;
  }

  @Override
  public final String getName() {
    return "DefaultModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "location":  {
        return location;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, DefaultModel model) {
    if (model.getId() >= 0) {
      statement.bindNumber(1, model.getId());
    } else {
      statement.bindLong(1, 5L);
    }
    if (model.getLocation() != null) {
      statement.bindDouble(2, model.getLocation());
    } else {
      statement.bindDouble(2, 5.0);
    }
    if (model.getName() != null) {
      statement.bindString(3, model.getName());
    } else {
      statement.bindString(3, "String");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, DefaultModel model) {
    if (model.getId() >= 0) {
      statement.bindNumber(1, model.getId());
    } else {
      statement.bindLong(1, 5L);
    }
    if (model.getLocation() != null) {
      statement.bindDouble(2, model.getLocation());
    } else {
      statement.bindDouble(2, 5.0);
    }
    if (model.getName() != null) {
      statement.bindString(3, model.getName());
    } else {
      statement.bindString(3, "String");
    }
    if (model.getId() >= 0) {
      statement.bindNumber(4, model.getId());
    } else {
      statement.bindLong(4, 5L);
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, DefaultModel model) {
    if (model.getId() >= 0) {
      statement.bindNumber(1, model.getId());
    } else {
      statement.bindLong(1, 5L);
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO DefaultModel(id,location,name) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO DefaultModel(id,location,name) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE DefaultModel SET id=?,location=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM DefaultModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS DefaultModel(id INTEGER, location REAL, name TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final DefaultModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    DefaultModel model = new DefaultModel(0, 0.0, "");
    model.setId(cursor.getIntOrDefault("id", 5));
    model.setLocation(cursor.getDoubleOrDefault("location", 5.0));
    model.setName(cursor.getStringOrDefault("name", "String"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(DefaultModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
