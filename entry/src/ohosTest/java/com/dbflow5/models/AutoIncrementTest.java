package com.dbflow5.models;

import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.structure.Model;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AutoIncrementTest extends BaseUnitTest {
    @Test
    public void testCanInsertAutoIncrement() {
        DBFlowDatabase db = FlowManager.getDatabaseForTable(AutoIncrementingModel.class);
        AutoIncrementingModel model = new AutoIncrementingModel(0);
        Model.insert(AutoIncrementingModel.class, model, db);
        assertEquals(1L, model.id);
    }

    @Test
    public void testCanInsertExistingIdAutoIncrement() {
        DBFlowDatabase db = FlowManager.getDatabaseForTable(AutoIncrementingModel.class);
        AutoIncrementingModel model = new AutoIncrementingModel(3);
        Model.insert(AutoIncrementingModel.class, model, db);
        assertEquals(3L, model.id);
    }
}

