package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.Position;
import com.dbflow5.models.ForeignKeyModels.DoubleToDouble;
import com.dbflow5.models.ForeignKeyModels.DoubleConverter;
import java.lang.Double;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class Position_Table extends ModelAdapter<Position> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(Position.class, "id");

  /**
   * Column Mapped Field */
  public static final Property<DoubleToDouble> latitude = new Property<DoubleToDouble>(Position.class, "latitude");

  /**
   * Column Mapped Field */
  public static final Property<DoubleToDouble> longitude = new Property<DoubleToDouble>(Position.class, "longitude");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,latitude,longitude};

  private final DoubleConverter global_typeConverterDoubleConverter;

  public Position_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterDoubleConverter = (DoubleConverter) holder.getTypeConverterForClass(DoubleToDouble.class);
  }

  @Override
  public final Class<Position> table() {
    return Position.class;
  }

  @Override
  public final String getName() {
    return "Position";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "latitude": {
        return latitude;
      }
      case "longitude": {
        return longitude;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Position model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getLocation() != null) {
      Double locationreflatitude = global_typeConverterDoubleConverter.getDBValue(model.getLocation().getLatitude());
      statement.bindDoubleOrNull(2, locationreflatitude);
      Double locationreflongitude = global_typeConverterDoubleConverter.getDBValue(model.getLocation().getLongitude());
      statement.bindDoubleOrNull(3, locationreflongitude);
    } else {
      statement.bindNull(2);
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Position model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getLocation() != null) {
      Double location_reflatitude = global_typeConverterDoubleConverter.getDBValue(model.getLocation().getLatitude());
      statement.bindDoubleOrNull(2, location_reflatitude);
      Double location_reflongitude = global_typeConverterDoubleConverter.getDBValue(model.getLocation().getLongitude());
      statement.bindDoubleOrNull(3, location_reflongitude);
    } else {
      statement.bindNull(2);
      statement.bindNull(3);
    }
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Position model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Position(id,latitude,longitude) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Position(id,latitude,longitude) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Position SET id=?,latitude=?,longitude=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Position WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Position(id INTEGER, latitude REAL ,longitude REAL, PRIMARY KEY(id))";
  }

  @Override
  public final Position loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Position model = new Position(0,null);
    model.setId(cursor.getIntOrDefault("id"));
    int index_latitude_Location_QueryTable = cursor.getColumnIndexForName("latitude");
    int index_longitude_Location_QueryTable = cursor.getColumnIndexForName("longitude");
    if (index_latitude_Location_QueryTable != -1 && !cursor.isColumnNull(index_latitude_Location_QueryTable) && index_longitude_Location_QueryTable != -1 && !cursor.isColumnNull(index_longitude_Location_QueryTable)) {
      model.setLocation(new ForeignKeyModels.Location(new DoubleToDouble(0.0), new DoubleToDouble(0.0)));
      model.getLocation().setLatitude(global_typeConverterDoubleConverter.getModelValue(cursor.getDouble(index_latitude_Location_QueryTable)));
      model.getLocation().setLongitude(global_typeConverterDoubleConverter.getModelValue(cursor.getDouble(index_longitude_Location_QueryTable)));
    } else {
      model.setLocation(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Position model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
