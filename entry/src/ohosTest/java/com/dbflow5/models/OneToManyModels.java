package com.dbflow5.models;

import com.dbflow5.annotation.OneToManyMethod;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
//import com.dbflow5.models.TwoColumnModel_Table.id;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.query.ModelQueriable;
import com.dbflow5.query.SQLite;
import com.dbflow5.structure.BaseModel;
import com.dbflow5.TestDatabase;
import com.dbflow5.structure.OneToMany;

import java.util.List;
import java.util.function.Function;

public class OneToManyModels {

    @Table(database = TestDatabase.class)
    public static class OneToManyModel {
        @PrimaryKey
        public String name;

        public List<SimpleTestModels.TwoColumnModel> orders = null;
        public List<OneToManyBaseModel> models = null;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public OneToManyModel(String name) {
            this.name = name;
        }

        @com.dbflow5.annotation.OneToMany(oneToManyMethods = {OneToManyMethod.ALL})
        public List<OneToManyBaseModel> simpleModels = new OneToMany((Function<Void, ModelQueriable>) unused -> SQLite.select().from(OneToManyBaseModel.class)).getValue(OneToManyBaseModel.class);

        @com.dbflow5.annotation.OneToMany(oneToManyMethods = {OneToManyMethod.ALL})
        public List<OneToManyBaseModel> setBaseModels = new OneToMany((Function<Void, ModelQueriable>) unused -> SQLite.select().from(OneToManyBaseModel.class)).getValue(OneToManyBaseModel.class);

        @com.dbflow5.annotation.OneToMany(oneToManyMethods = {OneToManyMethod.ALL}, variableName = "orders", efficientMethods = false)
        public List<SimpleTestModels.TwoColumnModel> getRelatedOrders(DatabaseWrapper wrapper) {
            List<SimpleTestModels.TwoColumnModel> localOrders = orders;
            if (localOrders == null) {
                localOrders = SQLite.select().from(SimpleTestModels.TwoColumnModel.class).where(TwoColumnModel_Table.id.greaterThan(3)).queryList(wrapper);
            }
            orders = localOrders;
            return localOrders;
        }

        @com.dbflow5.annotation.OneToMany(oneToManyMethods = {OneToManyMethod.DELETE}, variableName = "models")
        public List<OneToManyBaseModel> getRelatedModels(DatabaseWrapper wrapper) {
            List<OneToManyBaseModel> localModels = models;
            if (localModels == null) {
                localModels = SQLite.select().from(OneToManyBaseModel.class).queryList(wrapper);
            }
            models = localModels;
            return localModels;
        }
    }

    @Table(database = TestDatabase.class)
    public static class OneToManyBaseModel extends BaseModel {
        @PrimaryKey
        public int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public OneToManyBaseModel(int id) {
            this.id = id;
        }
    }
}
