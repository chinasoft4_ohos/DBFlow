package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.converter.TypeConverters.UUIDConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.SimpleTestModels.Transfer;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import java.util.UUID;

public final class Transfer_Table extends ModelAdapter<Transfer> {
  /**
   * Primary Key */
  public static final TypeConvertedProperty<String, UUID> transfer_id = new TypeConvertedProperty<String, UUID>(Transfer.class, "transfer_id", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          Transfer_Table adapter = (Transfer_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterUUIDConverter;
                      }
                      });

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{transfer_id};

  private final UUIDConverter global_typeConverterUUIDConverter;

  public Transfer_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterUUIDConverter = (UUIDConverter) holder.getTypeConverterForClass(UUID.class);
  }

  @Override
  public final Class<Transfer> table() {
    return Transfer.class;
  }

  @Override
  public final String getName() {
    return "Transfer";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "transfer_id":  {
        return transfer_id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Transfer model) {
    String reftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.getTransfer_id());
    statement.bindStringOrNull(1, reftransfer_id);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Transfer model) {
    String reftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.getTransfer_id());
    statement.bindStringOrNull(1, reftransfer_id);
    statement.bindStringOrNull(2, reftransfer_id);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Transfer model) {
    String reftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.getTransfer_id());
    statement.bindStringOrNull(1, reftransfer_id);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Transfer(transfer_id) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Transfer(transfer_id) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Transfer SET transfer_id=? WHERE transfer_id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Transfer WHERE transfer_id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Transfer(transfer_id TEXT, PRIMARY KEY(transfer_id))";
  }

  @Override
  public final Transfer loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Transfer model = new Transfer(UUID.randomUUID());
    int index_transfer_id = cursor.getColumnIndexForName("transfer_id");
    if (index_transfer_id != -1 && !cursor.isColumnNull(index_transfer_id)) {
      model.setTransfer_id(global_typeConverterUUIDConverter.getModelValue(cursor.getString(index_transfer_id)));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Transfer model) {
    OperatorGroup clause = OperatorGroup.clause();
    String reftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.getTransfer_id());
    clause.and(transfer_id.invertProperty().eq(reftransfer_id));
    return clause;
  }
}
