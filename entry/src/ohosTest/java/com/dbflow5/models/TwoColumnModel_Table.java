package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.TwoColumnModel;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class TwoColumnModel_Table extends ModelAdapter<TwoColumnModel> {
  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(TwoColumnModel.class, "name");

  public static final Property<Integer> id = new Property<Integer>(TwoColumnModel.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name,id};

  public TwoColumnModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TwoColumnModel> table() {
    return TwoColumnModel.class;
  }

  @Override
  public final String getName() {
    return "TwoColumnModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, TwoColumnModel model) {
    statement.bindStringOrNull(1, model.getName());
    statement.bindLong(2, (long)model.getId());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, TwoColumnModel model) {
    statement.bindStringOrNull(1, model.getName());
    statement.bindLong(2, (long)model.getId());
    statement.bindStringOrNull(3, model.getName());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, TwoColumnModel model) {
    statement.bindStringOrNull(1, model.getName());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO TwoColumnModel(name,id) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO TwoColumnModel(name,id) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE TwoColumnModel SET name=?,id=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM TwoColumnModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS TwoColumnModel(name TEXT, id INTEGER, PRIMARY KEY(name))";
  }

  @Override
  public final TwoColumnModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    TwoColumnModel model = new TwoColumnModel("",0);
    model.setName(cursor.getStringOrDefault("name"));
    model.setId(cursor.getIntOrDefault("id", (int) 0));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(TwoColumnModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
