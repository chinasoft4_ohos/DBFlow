package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.BlogPrimary;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class BlogPrimary_Table extends ModelAdapter<BlogPrimary> {
  /**
   * Foreign Key / Primary Key */
  public static final Property<Integer> author_id = new Property<Integer>(BlogPrimary.class, "author_id");

  public static final Property<Integer> id = new Property<Integer>(BlogPrimary.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{author_id,id};

  public BlogPrimary_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<BlogPrimary> table() {
    return BlogPrimary.class;
  }

  @Override
  public final String getName() {
    return "BlogPrimary";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "author_id": {
        return author_id;
      }
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, BlogPrimary model) {
    if (model.getAuthor() != null) {
      statement.bindLong(1, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(1);
    }
    statement.bindLong(2, (long)model.getId());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, BlogPrimary model) {
    if (model.getAuthor() != null) {
      statement.bindLong(1, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(1);
    }
    statement.bindLong(2, (long)model.getId());
    if (model.getAuthor() != null) {
      statement.bindLong(3, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, BlogPrimary model) {
    if (model.getAuthor() != null) {
      statement.bindLong(1, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(1);
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO BlogPrimary(author_id,id) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO BlogPrimary(author_id,id) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE BlogPrimary SET author_id=?,id=? WHERE author_id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM BlogPrimary WHERE author_id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS BlogPrimary(author_id INTEGER, id INTEGER, PRIMARY KEY(author_id), FOREIGN KEY(author_id) REFERENCES Author (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final BlogPrimary loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    BlogPrimary model = new BlogPrimary(null,0);
    int index_author_id_Author_Table = cursor.getColumnIndexForName("author_id");
    if (index_author_id_Author_Table != -1 && !cursor.isColumnNull(index_author_id_Author_Table)) {
      model.setAuthor(com.dbflow5.query.SQLite.select().from(ForeignKeyModels.Author.class).where()
          .and(Author_Table.id.eq(cursor.getInt(index_author_id_Author_Table)))
          .querySingle(wrapper));
    } else {
      model.setAuthor(null);
    }
    model.setId(cursor.getIntOrDefault("id"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(BlogPrimary model) {
    OperatorGroup clause = OperatorGroup.clause();
    if (model.getAuthor() != null) {
      clause.and(author_id.eq(model.getAuthor().getId()));
    } else {
      clause.and(author_id.eq((com.dbflow5.query.IConditional) null));
    }
    return clause;
  }
}
