package com.dbflow5.models;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.QueryModels.AllFieldsQueryModel;
import java.lang.Override;
import java.lang.String;

public final class AllFieldsQueryModel_QueryTable extends RetrievalAdapter<AllFieldsQueryModel> {
  public static final Property<String> fieldModel = new Property<String>(AllFieldsQueryModel.class, "fieldModel");

  public AllFieldsQueryModel_QueryTable(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<AllFieldsQueryModel> table() {
    return AllFieldsQueryModel.class;
  }

  @Override
  public final AllFieldsQueryModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    AllFieldsQueryModel model = new AllFieldsQueryModel(null);
    model.setFieldModel(cursor.getStringOrDefault("fieldModel"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(AllFieldsQueryModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(fieldModel.eq(model.getFieldModel()));
    return clause;
  }
}
