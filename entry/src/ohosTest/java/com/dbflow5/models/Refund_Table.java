package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.UUIDConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;
import java.util.UUID;

public final class Refund_Table extends ModelAdapter<Refund> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> _id = new Property<Long>(Refund.class, "_id");

  /**
   * Foreign Key */
  public static final Property<UUID> refund_in_transfer_id = new Property<UUID>(Refund.class, "refund_in_transfer_id");

  /**
   * Foreign Key */
  public static final Property<UUID> refund_out_transfer_id = new Property<UUID>(Refund.class, "refund_out_transfer_id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{_id,refund_in_transfer_id,refund_out_transfer_id};

  private final UUIDConverter global_typeConverterUUIDConverter;

  public Refund_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterUUIDConverter = (UUIDConverter) holder.getTypeConverterForClass(UUID.class);
  }

  @Override
  public final Class<Refund> table() {
    return Refund.class;
  }

  @Override
  public final String getName() {
    return "Refund";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "_id":  {
        return _id;
      }
      case "refund_in_transfer_id": {
        return refund_in_transfer_id;
      }
      case "refund_out_transfer_id": {
        return refund_out_transfer_id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(Refund model, Number id) {
    model._id = id.longValue();
  }

  @Override
  public final void saveForeignKeys(Refund model, DatabaseWrapper wrapper) {
    if (model.refund_in != null) {
      FlowManager.getModelAdapter(SimpleTestModels.Transfer.class).save(model.refund_in, wrapper);
    }
    if (model.refund_out != null) {
      FlowManager.getModelAdapter(SimpleTestModels.Transfer.class).save(model.refund_out, wrapper);
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Refund model) {
    statement.bindLong(1, model._id);
    if (model.refund_in != null) {
      String refund_inreftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.refund_in.getTransfer_id());
      statement.bindStringOrNull(2, refund_inreftransfer_id);
    } else {
      statement.bindNull(2);
    }
    if (model.refund_out != null) {
      String refund_outreftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.refund_out.getTransfer_id());
      statement.bindStringOrNull(3, refund_outreftransfer_id);
    } else {
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Refund model) {
    statement.bindLong(1, model._id);
    if (model.refund_in != null) {
      String refund_in_reftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.refund_in.getTransfer_id());
      statement.bindStringOrNull(2, refund_in_reftransfer_id);
    } else {
      statement.bindNull(2);
    }
    if (model.refund_out != null) {
      String refund_out_reftransfer_id = global_typeConverterUUIDConverter.getDBValue(model.refund_out.getTransfer_id());
      statement.bindStringOrNull(3, refund_out_reftransfer_id);
    } else {
      statement.bindNull(3);
    }
    statement.bindLong(4, model._id);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Refund model) {
    statement.bindLong(1, model._id);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Refund(_id,refund_in_transfer_id,refund_out_transfer_id) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Refund(_id,refund_in_transfer_id,refund_out_transfer_id) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Refund SET _id=?,refund_in_transfer_id=?,refund_out_transfer_id=? WHERE _id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Refund WHERE _id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Refund(_id INTEGER PRIMARY KEY AUTOINCREMENT, refund_in_transfer_id TEXT, refund_out_transfer_id TEXT, FOREIGN KEY(refund_in_transfer_id) REFERENCES Transfer (transfer_id) ON UPDATE NO ACTION ON DELETE NO ACTION, FOREIGN KEY(refund_out_transfer_id) REFERENCES Transfer (transfer_id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final Refund loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Refund model = new Refund();
    model._id = cursor.getLongOrDefault("_id");
    int index_refund_in_transfer_id_Transfer_Table = cursor.getColumnIndexForName("refund_in_transfer_id");
    if (index_refund_in_transfer_id_Transfer_Table != -1 && !cursor.isColumnNull(index_refund_in_transfer_id_Transfer_Table)) {
      model.refund_in = SQLite.select().from(SimpleTestModels.Transfer.class).where()
          .and(Transfer_Table.transfer_id.eq(global_typeConverterUUIDConverter.getModelValue(cursor.getString(index_refund_in_transfer_id_Transfer_Table))))
          .querySingle(wrapper);
    } else {
      model.refund_in = null;
    }
    int index_refund_out_transfer_id_Transfer_Table = cursor.getColumnIndexForName("refund_out_transfer_id");
    if (index_refund_out_transfer_id_Transfer_Table != -1 && !cursor.isColumnNull(index_refund_out_transfer_id_Transfer_Table)) {
      model.refund_out = SQLite.select().from(SimpleTestModels.Transfer.class).where()
          .and(Transfer_Table.transfer_id.eq(global_typeConverterUUIDConverter.getModelValue(cursor.getString(index_refund_out_transfer_id_Transfer_Table))))
          .querySingle(wrapper);
    } else {
      model.refund_out = null;
    }
    return model;
  }

  @Override
  public final boolean exists(Refund model, DatabaseWrapper wrapper) {
    return model._id > 0
    && SQLite.selectCountOf()
    .from(Refund.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Refund model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(_id.eq(model._id));
    return clause;
  }
}
