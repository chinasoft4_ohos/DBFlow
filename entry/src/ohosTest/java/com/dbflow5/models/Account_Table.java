package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.converter.TypeConverters.UUIDConverter;
import com.dbflow5.models.SimpleTestModels.Account;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import java.util.UUID;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class Account_Table extends ModelAdapter<Account> {
  /**
   * Primary Key */
  public static final TypeConvertedProperty<String, UUID> id = new TypeConvertedProperty<String, UUID>(Account.class, "id", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          Account_Table adapter = (Account_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterUUIDConverter;
                      }
                      });

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id};

  private final UUIDConverter global_typeConverterUUIDConverter;

  public Account_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterUUIDConverter = (UUIDConverter) holder.getTypeConverterForClass(UUID.class);
  }

  @Override
  public final Class<Account> table() {
    return Account.class;
  }

  @Override
  public final String getName() {
    return "Account";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Account model) {
    String refid = global_typeConverterUUIDConverter.getDBValue(model.id);
    statement.bindStringOrNull(1, refid);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Account model) {
    String refid = global_typeConverterUUIDConverter.getDBValue(model.id);
    statement.bindStringOrNull(1, refid);
    statement.bindStringOrNull(2, refid);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Account model) {
    String refid = global_typeConverterUUIDConverter.getDBValue(model.id);
    statement.bindStringOrNull(1, refid);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Account(id) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Account(id) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Account SET id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Account WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Account(id TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final Account loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Account model = new Account(UUID.randomUUID());
    int index_id = cursor.getColumnIndexForName("id");
    if (index_id != -1 && !cursor.isColumnNull(index_id)) {
      model.id = global_typeConverterUUIDConverter.getModelValue(cursor.getString(index_id));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Account model) {
    OperatorGroup clause = OperatorGroup.clause();
    String refid = global_typeConverterUUIDConverter.getDBValue(model.id);
    clause.and(id.invertProperty().eq(refid));
    return clause;
  }
}
