package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.data.Blob;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.query.property.WrapperProperty;
import com.dbflow5.models.SimpleTestModels.EnumTypeConverterModel;
import com.dbflow5.models.SimpleTestModels.Difficulty;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;


public final class EnumTypeConverterModel_Table extends ModelAdapter<EnumTypeConverterModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(EnumTypeConverterModel.class, "id");

  public static final WrapperProperty<byte[], Blob> blob = new WrapperProperty<byte[], Blob>(EnumTypeConverterModel.class, "blob");

  public static final Property<byte[]> byteArray = new Property<byte[]>(EnumTypeConverterModel.class, "byteArray");

  public static final TypeConvertedProperty<String, Difficulty> difficulty = new TypeConvertedProperty<String, Difficulty>(EnumTypeConverterModel.class, "difficulty", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          EnumTypeConverterModel_Table adapter = (EnumTypeConverterModel_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.typeConverterCustomEnumTypeConverter;
                      }
                      });

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,blob,byteArray,difficulty};

  private final SimpleTestModels.CustomEnumTypeConverter typeConverterCustomEnumTypeConverter = new SimpleTestModels.CustomEnumTypeConverter();

  public EnumTypeConverterModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<EnumTypeConverterModel> table() {
    return EnumTypeConverterModel.class;
  }

  @Override
  public final String getName() {
    return "EnumTypeConverterModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "blob":  {
        return blob;
      }
      case "byteArray":  {
        return byteArray;
      }
      case "difficulty":  {
        return difficulty;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      EnumTypeConverterModel model) {
    statement.bindLong(1, (long)model.getId());
    byte[] refblob = model.getBlob() != null ? model.getBlob().getBlob() : null;
    statement.bindBlobOrNull(2, refblob);
    statement.bindBlobOrNull(3, model.getByteArray());
    String refdifficulty = typeConverterCustomEnumTypeConverter.getDBValue(model.getDifficulty());
    statement.bindStringOrNull(4, refdifficulty);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      EnumTypeConverterModel model) {
    statement.bindLong(1, (long)model.getId());
    byte[] refblob = model.getBlob() != null ? model.getBlob().getBlob() : null;
    statement.bindBlobOrNull(2, refblob);
    statement.bindBlobOrNull(3, model.getByteArray());
    String refdifficulty = typeConverterCustomEnumTypeConverter.getDBValue(model.getDifficulty());
    statement.bindStringOrNull(4, refdifficulty);
    statement.bindLong(5, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      EnumTypeConverterModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO EnumTypeConverterModel(id,blob,byteArray,difficulty) VALUES (?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO EnumTypeConverterModel(id,blob,byteArray,difficulty) VALUES (?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE EnumTypeConverterModel SET id=?,blob=?,byteArray=?,difficulty=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM EnumTypeConverterModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS EnumTypeConverterModel(id INTEGER, blob BLOB, byteArray BLOB, difficulty TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final EnumTypeConverterModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    EnumTypeConverterModel model = new EnumTypeConverterModel(0,null,null,Difficulty.EASY);
    model.setId(cursor.getIntOrDefault("id"));
    int index_blob = cursor.getColumnIndexForName("blob");
    if (index_blob != -1 && !cursor.isColumnNull(index_blob)) {
      model.setBlob(new Blob(cursor.getBlob(index_blob)));
    } else {
      model.setBlob(null);
    }
    model.setByteArray(cursor.getBlobOrDefault("byteArray"));
    int index_difficulty = cursor.getColumnIndexForName("difficulty");
    if (index_difficulty != -1 && !cursor.isColumnNull(index_difficulty)) {
      model.setDifficulty(typeConverterCustomEnumTypeConverter.getModelValue(cursor.getString(index_difficulty)));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(EnumTypeConverterModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
