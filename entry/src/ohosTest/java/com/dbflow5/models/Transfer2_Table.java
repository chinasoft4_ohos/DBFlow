package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.converter.TypeConverters.UUIDConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.SimpleTestModels.Transfer2;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import java.util.UUID;

public final class Transfer2_Table extends ModelAdapter<Transfer2> {
  /**
   * Primary Key */
  public static final TypeConvertedProperty<String, UUID> id = new TypeConvertedProperty<String, UUID>(Transfer2.class, "id", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          Transfer2_Table adapter = (Transfer2_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterUUIDConverter;
                      }
                      });

  /**
   * Foreign Key */
  public static final Property<UUID> origin_id = new Property<UUID>(Transfer2.class, "origin_id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,origin_id};

  private final UUIDConverter global_typeConverterUUIDConverter;

  public Transfer2_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterUUIDConverter = (UUIDConverter) holder.getTypeConverterForClass(UUID.class);
  }

  @Override
  public final Class<Transfer2> table() {
    return Transfer2.class;
  }

  @Override
  public final String getName() {
    return "Transfer2";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "origin_id": {
        return origin_id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Transfer2 model) {
    String refid = global_typeConverterUUIDConverter.getDBValue(model.getId());
    statement.bindStringOrNull(1, refid);
    if (model.getOrigin() != null) {
      String originrefid = global_typeConverterUUIDConverter.getDBValue(model.getOrigin().getId());
      statement.bindStringOrNull(2, originrefid);
    } else {
      statement.bindNull(2);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Transfer2 model) {
    String refid = global_typeConverterUUIDConverter.getDBValue(model.getId());
    statement.bindStringOrNull(1, refid);
    if (model.getOrigin() != null) {
      String origin_refid = global_typeConverterUUIDConverter.getDBValue(model.getOrigin().getId());
      statement.bindStringOrNull(2, origin_refid);
    } else {
      statement.bindNull(2);
    }
    statement.bindStringOrNull(3, refid);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Transfer2 model) {
    String refid = global_typeConverterUUIDConverter.getDBValue(model.getId());
    statement.bindStringOrNull(1, refid);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Transfer2(id,origin_id) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Transfer2(id,origin_id) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Transfer2 SET id=?,origin_id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Transfer2 WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Transfer2(id TEXT, origin_id TEXT, PRIMARY KEY(id), FOREIGN KEY(origin_id) REFERENCES Account (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final Transfer2 loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Transfer2 model = new Transfer2(UUID.randomUUID(), null);
    int index_id = cursor.getColumnIndexForName("id");
    if (index_id != -1 && !cursor.isColumnNull(index_id)) {
      model.setId(global_typeConverterUUIDConverter.getModelValue(cursor.getString(index_id)));
    }
    int index_origin_id_Account_Table = cursor.getColumnIndexForName("origin_id");
    if (index_origin_id_Account_Table != -1 && !cursor.isColumnNull(index_origin_id_Account_Table)) {
      model.setOrigin(new SimpleTestModels.Account(UUID.randomUUID()));
      model.getOrigin().setId(global_typeConverterUUIDConverter.getModelValue(cursor.getString(index_origin_id_Account_Table)));
    } else {
      model.setOrigin(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Transfer2 model) {
    OperatorGroup clause = OperatorGroup.clause();
    String refid = global_typeConverterUUIDConverter.getDBValue(model.getId());
    clause.and(id.invertProperty().eq(refid));
    return clause;
  }
}
