package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.FeedEntry;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class FeedEntry_Table extends ModelAdapter<FeedEntry> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(FeedEntry.class, "id");

  public static final Property<String> title = new Property<String>(FeedEntry.class, "title");

  public static final Property<String> subtitle = new Property<String>(FeedEntry.class, "subtitle");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,title,subtitle};

  public FeedEntry_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<FeedEntry> table() {
    return FeedEntry.class;
  }

  @Override
  public final String getName() {
    return "FeedEntry";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "title":  {
        return title;
      }
      case "subtitle":  {
        return subtitle;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, FeedEntry model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getTitle());
    statement.bindStringOrNull(3, model.getSubtitle());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, FeedEntry model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getTitle());
    statement.bindStringOrNull(3, model.getSubtitle());
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, FeedEntry model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO FeedEntry(id,title,subtitle) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO FeedEntry(id,title,subtitle) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE FeedEntry SET id=?,title=?,subtitle=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM FeedEntry WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS FeedEntry(id INTEGER, title TEXT, subtitle TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final FeedEntry loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    FeedEntry model = new FeedEntry(0,null,null);
    model.setId(cursor.getIntOrDefault("id"));
    model.setTitle(cursor.getStringOrDefault("title"));
    model.setSubtitle(cursor.getStringOrDefault("subtitle"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(FeedEntry model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
