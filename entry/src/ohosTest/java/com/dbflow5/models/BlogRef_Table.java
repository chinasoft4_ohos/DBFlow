package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.BlogRef;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class BlogRef_Table extends ModelAdapter<BlogRef> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(BlogRef.class, "id");

  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(BlogRef.class, "name");

  /**
   * Foreign Key */
  public static final Property<Integer> authorId = new Property<Integer>(BlogRef.class, "authorId");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name,authorId};

  public BlogRef_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<BlogRef> table() {
    return BlogRef.class;
  }

  @Override
  public final String getName() {
    return "BlogRef";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      case "authorId": {
        return authorId;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, BlogRef model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getAuthor() != null) {
      statement.bindLong(3, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, BlogRef model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getAuthor() != null) {
      statement.bindLong(3, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(3);
    }
    statement.bindLong(4, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(5, model.getName());
    } else {
      statement.bindString(5, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, BlogRef model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO BlogRef(id,name,authorId) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO BlogRef(id,name,authorId) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE BlogRef SET id=?,name=?,authorId=? WHERE id=? AND name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM BlogRef WHERE id=? AND name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS BlogRef(id INTEGER, name TEXT, authorId INTEGER, PRIMARY KEY(id, name), FOREIGN KEY(authorId) REFERENCES Author (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final BlogRef loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    BlogRef model = new BlogRef(0,"",null);
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name", ""));
    int index_authorId_Author_Table = cursor.getColumnIndexForName("authorId");
    if (index_authorId_Author_Table != -1 && !cursor.isColumnNull(index_authorId_Author_Table)) {
      model.setAuthor(com.dbflow5.query.SQLite.select().from(ForeignKeyModels.Author.class).where()
          .and(Author_Table.id.eq(cursor.getInt(index_authorId_Author_Table)))
          .querySingle(wrapper));
    } else {
      model.setAuthor(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(BlogRef model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
