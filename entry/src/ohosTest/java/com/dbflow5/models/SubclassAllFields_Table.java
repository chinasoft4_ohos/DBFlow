package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Boolean;
import com.dbflow5.models.SimpleTestModels.SubclassAllFields;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class SubclassAllFields_Table extends ModelAdapter<SubclassAllFields> {
  public static final Property<Integer> order = new Property<Integer>(SubclassAllFields.class, "_order");

  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(SubclassAllFields.class, "name");

  public static final Property<Integer> count = new Property<Integer>(SubclassAllFields.class, "count");

  public static final Property<Boolean> truth = new Property<Boolean>(SubclassAllFields.class, "truth");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{order,name,count,truth};

  public SubclassAllFields_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<SubclassAllFields> table() {
    return SubclassAllFields.class;
  }

  @Override
  public final String getName() {
    return "SubclassAllFields";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "_order":  {
        return order;
      }
      case "name":  {
        return name;
      }
      case "count":  {
        return count;
      }
      case "truth":  {
        return truth;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, SubclassAllFields model) {
    statement.bindLong(1, (long)model.getOrder());
    statement.bindStringOrNull(2, model.getName());
    statement.bindNumberOrNull(3, model.getCount());
    statement.bindLong(4, model.isTruth() ? 1L : 0L);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, SubclassAllFields model) {
    statement.bindLong(1, (long)model.getOrder());
    statement.bindStringOrNull(2, model.getName());
    statement.bindNumberOrNull(3, model.getCount());
    statement.bindLong(4, model.isTruth() ? 1L : 0L);
    statement.bindStringOrNull(5, model.getName());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, SubclassAllFields model) {
    statement.bindStringOrNull(1, model.getName());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO SubclassAllFields(_order,name,count,truth) VALUES (?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO SubclassAllFields(_order,name,count,truth) VALUES (?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE SubclassAllFields SET _order=?,name=?,count=?,truth=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM SubclassAllFields WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS SubclassAllFields(_order INTEGER, name TEXT, count INTEGER, truth INTEGER, PRIMARY KEY(name))";
  }

  @Override
  public final SubclassAllFields loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    SubclassAllFields model = new SubclassAllFields(0);
    model.setOrder(cursor.getIntOrDefault("_order"));
    model.setName(cursor.getStringOrDefault("name"));
    model.setCount(cursor.getIntOrDefault("count", 0));
    int index_truth = cursor.getColumnIndexForName("truth");
    if (index_truth != -1 && !cursor.isColumnNull(index_truth)) {
      model.setTruth(cursor.getBoolean(index_truth));
    } else {
      model.setTruth(false);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(SubclassAllFields model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
