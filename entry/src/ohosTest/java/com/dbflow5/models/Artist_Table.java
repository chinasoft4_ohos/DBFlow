package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ManyToMany.Artist;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class Artist_Table extends ModelAdapter<Artist> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> id = new Property<Integer>(Artist.class, "id");

  public static final Property<String> name = new Property<String>(Artist.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name};

  public Artist_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Artist> table() {
    return Artist.class;
  }

  @Override
  public final String getName() {
    return "Artist";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(Artist model, Number id) {
    model.setId(id.intValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Artist model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Artist model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    statement.bindLong(3, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Artist model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Artist(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Artist(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Artist SET id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Artist WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Artist(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)";
  }

  @Override
  public final Artist loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Artist model = new Artist(0, "");
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final boolean exists(Artist model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(Artist.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Artist model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
