package com.dbflow5.models;

import com.dbflow5.config.FlowManager;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.structure.Model;
import org.junit.Assert;
import org.junit.Test;

public class ManyToManyTest extends BaseUnitTest {
    @Test
    public void testCanCreateManyToMany() {
        FlowManager.databaseForTable(ManyToMany.Artist.class, db -> {
            ManyToMany.Artist artist = new ManyToMany.Artist(0, "Andrew Grosner");
            ManyToMany.Song song = new ManyToMany.Song(0, "Livin' on A Prayer");

            Model.save(ManyToMany.Artist.class, artist, db);
            Model.save(ManyToMany.Song.class, song, db);

            Artist_Song artistSong = new Artist_Song();
            artistSong.artist = artist;
            artistSong.song = song;
            Assert.assertTrue(Model.save(Artist_Song.class, artistSong, db));
            return null;
        });
    }
}
