package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.BigDecimalConverter;
import com.dbflow5.converter.TypeConverters.BigIntegerConverter;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.SimpleTestModels.NullableNumbers;
import java.lang.Class;
import java.lang.Double;
import java.lang.Float;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class NullableNumbers_Table extends ModelAdapter<NullableNumbers> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(NullableNumbers.class, "id");

  public static final Property<Float> f = new Property<Float>(NullableNumbers.class, "f");

  public static final Property<Double> d = new Property<Double>(NullableNumbers.class, "d");

  public static final Property<Long> l = new Property<Long>(NullableNumbers.class, "l");

  public static final Property<Integer> i = new Property<Integer>(NullableNumbers.class, "i");

  public static final TypeConvertedProperty<String, BigDecimal> bigDecimal = new TypeConvertedProperty<String, BigDecimal>(NullableNumbers.class, "bigDecimal", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          NullableNumbers_Table adapter = (NullableNumbers_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterBigDecimalConverter;
                      }
                      });

  public static final TypeConvertedProperty<String, BigInteger> bigInteger = new TypeConvertedProperty<String, BigInteger>(NullableNumbers.class, "bigInteger", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          NullableNumbers_Table adapter = (NullableNumbers_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterBigIntegerConverter;
                      }
                      });

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,f,d,l,i,bigDecimal,bigInteger};

  private final BigDecimalConverter global_typeConverterBigDecimalConverter;

  private final BigIntegerConverter global_typeConverterBigIntegerConverter;

  public NullableNumbers_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterBigDecimalConverter = (BigDecimalConverter) holder.getTypeConverterForClass(BigDecimal.class);
    global_typeConverterBigIntegerConverter = (BigIntegerConverter) holder.getTypeConverterForClass(BigInteger.class);
  }

  @Override
  public final Class<NullableNumbers> table() {
    return NullableNumbers.class;
  }

  @Override
  public final String getName() {
    return "NullableNumbers";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "f":  {
        return f;
      }
      case "d":  {
        return d;
      }
      case "l":  {
        return l;
      }
      case "i":  {
        return i;
      }
      case "bigDecimal":  {
        return bigDecimal;
      }
      case "bigInteger":  {
        return bigInteger;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, NullableNumbers model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindFloatOrNull(2, model.getF());
    statement.bindDoubleOrNull(3, model.getD());
    statement.bindNumberOrNull(4, model.getL());
    statement.bindNumberOrNull(5, model.getI());
    String refbigDecimal = global_typeConverterBigDecimalConverter.getDBValue(model.getBigDecimal());
    statement.bindStringOrNull(6, refbigDecimal);
    String refbigInteger = global_typeConverterBigIntegerConverter.getDBValue(model.getBigInteger());
    statement.bindStringOrNull(7, refbigInteger);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, NullableNumbers model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindFloatOrNull(2, model.getF());
    statement.bindDoubleOrNull(3, model.getD());
    statement.bindNumberOrNull(4, model.getL());
    statement.bindNumberOrNull(5, model.getI());
    String refbigDecimal = global_typeConverterBigDecimalConverter.getDBValue(model.getBigDecimal());
    statement.bindStringOrNull(6, refbigDecimal);
    String refbigInteger = global_typeConverterBigIntegerConverter.getDBValue(model.getBigInteger());
    statement.bindStringOrNull(7, refbigInteger);
    statement.bindLong(8, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, NullableNumbers model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO NullableNumbers(id,f,d,l,i,bigDecimal,bigInteger) VALUES (?,?,?,?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO NullableNumbers(id,f,d,l,i,bigDecimal,bigInteger) VALUES (?,?,?,?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE NullableNumbers SET id=?,f=?,d=?,l=?,i=?,bigDecimal=?,bigInteger=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM NullableNumbers WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS NullableNumbers(id INTEGER, f REAL, d REAL, l INTEGER, i INTEGER, bigDecimal TEXT, bigInteger TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final NullableNumbers loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    NullableNumbers model = new NullableNumbers(0, null, null, 0, 0, null, null);
    model.setId(cursor.getIntOrDefault("id"));
    model.setF(cursor.getFloatOrDefault("f", 0));
    model.setD(cursor.getDoubleOrDefault("d", 0));
    model.setL(cursor.getLongOrDefault("l", 0));
    model.setI(cursor.getIntOrDefault("i", 0));
    int index_bigDecimal = cursor.getColumnIndexForName("bigDecimal");
    if (index_bigDecimal != -1 && !cursor.isColumnNull(index_bigDecimal)) {
      model.setBigDecimal(global_typeConverterBigDecimalConverter.getModelValue(cursor.getString(index_bigDecimal)));
    } else {
      model.setBigDecimal(global_typeConverterBigDecimalConverter.getModelValue(null));
    }
    int index_bigInteger = cursor.getColumnIndexForName("bigInteger");
    if (index_bigInteger != -1 && !cursor.isColumnNull(index_bigInteger)) {
      model.setBigInteger(global_typeConverterBigIntegerConverter.getModelValue(cursor.getString(index_bigInteger)));
    } else {
      model.setBigInteger(global_typeConverterBigIntegerConverter.getModelValue(null));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(NullableNumbers model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
