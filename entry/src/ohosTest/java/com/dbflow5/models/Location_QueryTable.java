package com.dbflow5.models;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.ForeignKeyModels.Location;
import com.dbflow5.models.ForeignKeyModels.DoubleToDouble;
import java.lang.Class;
import java.lang.Double;
import java.lang.Override;

public final class Location_QueryTable extends RetrievalAdapter<Location> {
  public static final TypeConvertedProperty<Double, DoubleToDouble> latitude = new TypeConvertedProperty<Double, DoubleToDouble>(Location.class, "latitude", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          Location_QueryTable adapter = (Location_QueryTable) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterDoubleConverter;
                      }
                      });

  public static final TypeConvertedProperty<Double, DoubleToDouble> longitude = new TypeConvertedProperty<Double, DoubleToDouble>(Location.class, "longitude", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          Location_QueryTable adapter = (Location_QueryTable) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterDoubleConverter;
                      }
                      });

  private final ForeignKeyModels.DoubleConverter global_typeConverterDoubleConverter;

  public Location_QueryTable(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterDoubleConverter = (ForeignKeyModels.DoubleConverter) holder.getTypeConverterForClass(DoubleToDouble.class);
  }

  @Override
  public final Class<Location> table() {
    return Location.class;
  }

  @Override
  public final Location loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Location model = new Location(new DoubleToDouble(0.0), new DoubleToDouble(0.0));
    int index_latitude = cursor.getColumnIndexForName("latitude");
    if (index_latitude != -1 && !cursor.isColumnNull(index_latitude)) {
      model.setLatitude(global_typeConverterDoubleConverter.getModelValue(cursor.getDouble(index_latitude)));
    } else {
      model.setLatitude(global_typeConverterDoubleConverter.getModelValue(null));
    }
    int index_longitude = cursor.getColumnIndexForName("longitude");
    if (index_longitude != -1 && !cursor.isColumnNull(index_longitude)) {
      model.setLongitude(global_typeConverterDoubleConverter.getModelValue(cursor.getDouble(index_longitude)));
    } else {
      model.setLongitude(global_typeConverterDoubleConverter.getModelValue(null));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Location model) {
    OperatorGroup clause = OperatorGroup.clause();
    Double reflatitude = global_typeConverterDoubleConverter.getDBValue(model.getLatitude());
    clause.and(latitude.invertProperty().eq(reflatitude));
    Double reflongitude = global_typeConverterDoubleConverter.getDBValue(model.getLongitude());
    clause.and(longitude.invertProperty().eq(reflongitude));
    return clause;
  }
}
