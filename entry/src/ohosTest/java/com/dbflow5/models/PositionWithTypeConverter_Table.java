package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.PositionWithTypeConverter;
import com.dbflow5.models.ForeignKeyModels.DoubleToDouble;
import com.dbflow5.models.ForeignKeyModels.DoubleConverter;
import java.lang.Double;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class PositionWithTypeConverter_Table extends ModelAdapter<PositionWithTypeConverter> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(PositionWithTypeConverter.class, "id");

  /**
   * Column Mapped Field */
  public static final Property<DoubleToDouble> latitude = new Property<DoubleToDouble>(PositionWithTypeConverter.class, "latitude");

  /**
   * Column Mapped Field */
  public static final Property<Double> longitude = new Property<Double>(PositionWithTypeConverter.class, "longitude");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,latitude,longitude};

  private final DoubleConverter typeConverterDoubleConverter = new DoubleConverter();

  public PositionWithTypeConverter_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<PositionWithTypeConverter> table() {
    return PositionWithTypeConverter.class;
  }

  @Override
  public final String getName() {
    return "PositionWithTypeConverter";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "latitude": {
        return latitude;
      }
      case "longitude": {
        return longitude;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      PositionWithTypeConverter model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getLocation() != null) {
      Double locationreflatitude = typeConverterDoubleConverter.getDBValue(model.getLocation().getLatitude());
      statement.bindDoubleOrNull(2, locationreflatitude);
      statement.bindDoubleOrNull(3, model.getLocation().getLongitude());
    } else {
      statement.bindNull(2);
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      PositionWithTypeConverter model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getLocation() != null) {
      Double location_reflatitude = typeConverterDoubleConverter.getDBValue(model.getLocation().getLatitude());
      statement.bindDoubleOrNull(2, location_reflatitude);
      statement.bindDoubleOrNull(3, model.getLocation().getLongitude());
    } else {
      statement.bindNull(2);
      statement.bindNull(3);
    }
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      PositionWithTypeConverter model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO PositionWithTypeConverter(id,latitude,longitude) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO PositionWithTypeConverter(id,latitude,longitude) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE PositionWithTypeConverter SET id=?,latitude=?,longitude=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM PositionWithTypeConverter WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS PositionWithTypeConverter(id INTEGER, latitude REAL ,longitude REAL, PRIMARY KEY(id))";
  }

  @Override
  public final PositionWithTypeConverter loadFromCursor(FlowCursor cursor,
      DatabaseWrapper wrapper) {
    PositionWithTypeConverter model = new PositionWithTypeConverter(0, null);
    model.setId(cursor.getIntOrDefault("id"));
    int index_latitude_Location2_QueryTable = cursor.getColumnIndexForName("latitude");
    int index_longitude_Location2_QueryTable = cursor.getColumnIndexForName("longitude");
    if (index_latitude_Location2_QueryTable != -1 && !cursor.isColumnNull(index_latitude_Location2_QueryTable) && index_longitude_Location2_QueryTable != -1 && !cursor.isColumnNull(index_longitude_Location2_QueryTable)) {
      model.setLocation(new ForeignKeyModels.Location2(new DoubleToDouble(0.0), 0.0));
      model.getLocation().setLatitude(typeConverterDoubleConverter.getModelValue(cursor.getDouble(index_latitude_Location2_QueryTable)));
      model.getLocation().setLongitude(cursor.getDouble(index_longitude_Location2_QueryTable));
    } else {
      model.setLocation(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(PositionWithTypeConverter model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
