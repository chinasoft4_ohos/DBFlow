package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.OneToManyModels.OneToManyBaseModel;
import com.dbflow5.models.OneToManyModels.OneToManyModel;
import com.dbflow5.models.SimpleTestModels.TwoColumnModel;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;
import javax.annotation.Generated;

public final class OneToManyModel_Table extends ModelAdapter<OneToManyModel> {
  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(OneToManyModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name};

  public OneToManyModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<OneToManyModel> table() {
    return OneToManyModel.class;
  }

  @Override
  public final String getName() {
    return "OneToManyModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, OneToManyModel model) {
    statement.bindStringOrNull(1, model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, OneToManyModel model) {
    statement.bindStringOrNull(1, model.getName());
    statement.bindStringOrNull(2, model.getName());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, OneToManyModel model) {
    statement.bindStringOrNull(1, model.getName());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO OneToManyModel(name) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO OneToManyModel(name) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE OneToManyModel SET name=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM OneToManyModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS OneToManyModel(name TEXT, PRIMARY KEY(name))";
  }

  @Override
  public final OneToManyModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    OneToManyModel model = new OneToManyModel(null);
    model.setName(cursor.getStringOrDefault("name"));
//    model.getSimpleModels();
//    model.getSetBaseModels();
    model.getRelatedOrders(wrapper);
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(OneToManyModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }

  @Override
  public final boolean delete(OneToManyModel model, DatabaseWrapper wrapper) {
    boolean successful = super.delete(model, wrapper);
    if (model.simpleModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.deleteAll(model.simpleModels, wrapper);
    }
    model.setBaseModels = null;
    if (model.setBaseModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.deleteAll(model.setBaseModels, wrapper);
    }
    model.setBaseModels = null;
    if (model.getRelatedOrders(wrapper) != null) {
      ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
      for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
        adapter.delete(value, wrapper);
      }
    }
    model.orders = null;
    if (model.getRelatedModels(wrapper) != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.deleteAll(model.getRelatedModels(wrapper), wrapper);
    }
    model.models = null;
    return successful;
  }

  @Override
  public final long deleteAll(Collection<OneToManyModel> models, DatabaseWrapper wrapper) {
    long successful = super.deleteAll(models, wrapper);
    for (OneToManyModel model: models) {
      if (model.simpleModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.deleteAll(model.simpleModels, wrapper);
      }
      model.simpleModels = null;
      if (model.setBaseModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.deleteAll(model.setBaseModels, wrapper);
      }
      model.setBaseModels = null;
      if (model.getRelatedOrders(wrapper) != null) {
        ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
        for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
          adapter.delete(value, wrapper);
        }
      }
      model.orders = null;
      if (model.getRelatedModels(wrapper) != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.deleteAll(model.getRelatedModels(wrapper), wrapper);
      }
      model.models = null;
    }
    return successful;
  }

  @Override
  public final boolean save(OneToManyModel model, DatabaseWrapper wrapper) {
    boolean successful = super.save(model, wrapper);
    if (model.simpleModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.saveAll(model.simpleModels, wrapper);
    }
    if (model.setBaseModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.saveAll(model.setBaseModels, wrapper);
    }
    if (model.getRelatedOrders(wrapper) != null) {
      ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
      for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
        adapter.save(value, wrapper);
      }
    }
    return successful;
  }

  @Override
  public final long saveAll(Collection<OneToManyModel> models, DatabaseWrapper wrapper) {
    long count = super.saveAll(models, wrapper);
    for (OneToManyModel model: models) {
      if (model.simpleModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.saveAll(model.simpleModels, wrapper);
      }
      if (model.setBaseModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.saveAll(model.setBaseModels, wrapper);
      }
      if (model.getRelatedOrders(wrapper) != null) {
        ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
        for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
          adapter.save(value, wrapper);
        }
      }
    }
    return count;
  }

  @Override
  public final long insert(OneToManyModel model, DatabaseWrapper wrapper) {
    long rowId = super.insert(model, wrapper);
    if (model.simpleModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.insertAll(model.simpleModels, wrapper);
    }
    if (model.setBaseModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.insertAll(model.setBaseModels, wrapper);
    }
    if (model.getRelatedOrders(wrapper) != null) {
      ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
      for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
        adapter.insert(value, wrapper);
      }
    }
    return rowId;
  }

  @Override
  public final long insertAll(Collection<OneToManyModel> models,
      DatabaseWrapper wrapper) {
    long count = super.insertAll(models, wrapper);
    for (OneToManyModel model: models) {
      if (model.simpleModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.insertAll(model.simpleModels, wrapper);
      }
      if (model.setBaseModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.insertAll(model.setBaseModels, wrapper);
      }
      if (model.getRelatedOrders(wrapper) != null) {
        ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
        for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
          adapter.insert(value, wrapper);
        }
      }
    }
    return count;
  }

  @Override
  public final boolean update(OneToManyModel model, DatabaseWrapper wrapper) {
    boolean successful = super.update(model, wrapper);
    if (model.simpleModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.updateAll(model.simpleModels, wrapper);
    }
    if (model.setBaseModels != null) {
      ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
      adapter.updateAll(model.setBaseModels, wrapper);
    }
    if (model.getRelatedOrders(wrapper) != null) {
      ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
      for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
        adapter.update(value, wrapper);
      }
    }
    return successful;
  }

  @Override
  public final long updateAll(Collection<OneToManyModel> models,
      DatabaseWrapper wrapper) {
    long count = super.updateAll(models, wrapper);
    for (OneToManyModel model: models) {
      if (model.simpleModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.updateAll(model.simpleModels, wrapper);
      }
      if (model.setBaseModels != null) {
        ModelAdapter<OneToManyBaseModel> adapter = FlowManager.getModelAdapter(OneToManyBaseModel.class);
        adapter.updateAll(model.setBaseModels, wrapper);
      }
      if (model.getRelatedOrders(wrapper) != null) {
        ModelAdapter<TwoColumnModel> adapter = FlowManager.getModelAdapter(TwoColumnModel.class);
        for (TwoColumnModel value: model.getRelatedOrders(wrapper)) {
          adapter.update(value, wrapper);
        }
      }
    }
    return count;
  }
}
