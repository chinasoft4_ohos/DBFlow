package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.OrderCursorModel;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class OrderCursorModel_Table extends ModelAdapter<OrderCursorModel> {
  public static final Property<Integer> age = new Property<Integer>(OrderCursorModel.class, "age");

  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(OrderCursorModel.class, "id");

  public static final Property<String> name = new Property<String>(OrderCursorModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{age,id,name};

  public OrderCursorModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<OrderCursorModel> table() {
    return OrderCursorModel.class;
  }

  @Override
  public final String getName() {
    return "OrderCursorModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "age":  {
        return age;
      }
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, OrderCursorModel model) {
    statement.bindLong(1, (long)model.getAge());
    statement.bindLong(2, (long)model.getId());
    statement.bindStringOrNull(3, model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, OrderCursorModel model) {
    statement.bindLong(1, (long)model.getAge());
    statement.bindLong(2, (long)model.getId());
    statement.bindStringOrNull(3, model.getName());
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, OrderCursorModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO OrderCursorModel(age,id,name) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO OrderCursorModel(age,id,name) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE OrderCursorModel SET age=?,id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM OrderCursorModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS OrderCursorModel(age INTEGER, id INTEGER, name TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final OrderCursorModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    OrderCursorModel model = new OrderCursorModel(0,0,"");
    model.setAge(cursor.getIntOrDefault(0));
    model.setId(cursor.getIntOrDefault(1));
    model.setName(cursor.getStringOrDefault(2));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(OrderCursorModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
