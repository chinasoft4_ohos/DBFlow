package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.BlogRefNoModel;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class BlogRefNoModel_Table extends ModelAdapter<BlogRefNoModel> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> id = new Property<Integer>(BlogRefNoModel.class, "id");

  public static final Property<String> name = new Property<String>(BlogRefNoModel.class, "name");

  /**
   * Foreign Key */
  public static final Property<Integer> authorId = new Property<Integer>(BlogRefNoModel.class, "authorId");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name,authorId};

  public BlogRefNoModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<BlogRefNoModel> table() {
    return BlogRefNoModel.class;
  }

  @Override
  public final String getName() {
    return "BlogRefNoModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      case "authorId": {
        return authorId;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(BlogRefNoModel model, Number id) {
    model.setId(id.intValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, BlogRefNoModel model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    statement.bindStringOrNull(3, model.getAuthorId());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, BlogRefNoModel model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    statement.bindStringOrNull(3, model.getAuthorId());
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, BlogRefNoModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO BlogRefNoModel(id,name,authorId) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO BlogRefNoModel(id,name,authorId) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE BlogRefNoModel SET id=?,name=?,authorId=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM BlogRefNoModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS BlogRefNoModel(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, authorId INTEGER NOT NULL ON CONFLICT FAIL, FOREIGN KEY(authorId) REFERENCES Author (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final BlogRefNoModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    BlogRefNoModel model = new BlogRefNoModel(0,"",null);
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name", ""));
    model.setAuthorId(cursor.getStringOrDefault("authorId"));
    return model;
  }

  @Override
  public final boolean exists(BlogRefNoModel model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(BlogRefNoModel.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(BlogRefNoModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
