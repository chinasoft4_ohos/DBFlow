package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.InternalClass;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;

public final class InternalClass_Table extends ModelAdapter<InternalClass> {
  /**
   * Primary Key */
  public static final Property<String> id = new Property<String>(InternalClass.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id};

  public InternalClass_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<InternalClass> table() {
    return InternalClass.class;
  }

  @Override
  public final String getName() {
    return "InternalClass";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, InternalClass model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, InternalClass model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    if (model.getId() != null) {
      statement.bindString(2, model.getId());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, InternalClass model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO InternalClass(id) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO InternalClass(id) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE InternalClass SET id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM InternalClass WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS InternalClass(id TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final InternalClass loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    InternalClass model = new InternalClass("");
    model.setId(cursor.getStringOrDefault("id", ""));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(InternalClass model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
