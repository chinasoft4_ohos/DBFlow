package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.Currency;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class Currency_Table extends ModelAdapter<Currency> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> id = new Property<Long>(Currency.class, "id");

  public static final Property<String> symbol = new Property<String>(Currency.class, "symbol");

  public static final Property<String> shortName = new Property<String>(Currency.class, "shortName");

  public static final Property<String> name = new Property<String>(Currency.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,symbol,shortName,name};

  public Currency_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Currency> table() {
    return Currency.class;
  }

  @Override
  public final String getName() {
    return "Currency";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "symbol":  {
        return symbol;
      }
      case "shortName":  {
        return shortName;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(Currency model, Number id) {
    model.setId(id.longValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Currency model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getSymbol());
    statement.bindStringOrNull(3, model.getShortName());
    if (model.getName() != null) {
      statement.bindString(4, model.getName());
    } else {
      statement.bindString(4, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Currency model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getSymbol());
    statement.bindStringOrNull(3, model.getShortName());
    if (model.getName() != null) {
      statement.bindString(4, model.getName());
    } else {
      statement.bindString(4, "");
    }
    statement.bindLong(5, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Currency model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Currency(id,symbol,shortName,name) VALUES (nullif(?, 0),?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Currency(id,symbol,shortName,name) VALUES (nullif(?, 0),?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Currency SET id=?,symbol=?,shortName=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Currency WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Currency(id INTEGER PRIMARY KEY AUTOINCREMENT, symbol TEXT UNIQUE ON CONFLICT FAIL, shortName TEXT, name TEXT UNIQUE ON CONFLICT FAIL)";
  }

  @Override
  public final Currency loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Currency model = new Currency(0,null,null,"");
    model.setId(cursor.getLongOrDefault("id"));
    model.setSymbol(cursor.getStringOrDefault("symbol"));
    model.setShortName(cursor.getStringOrDefault("shortName"));
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final boolean exists(Currency model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(Currency.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Currency model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
