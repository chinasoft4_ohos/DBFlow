package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.ColumnMap;
import com.dbflow5.annotation.ModelView;
import com.dbflow5.annotation.ModelViewQuery;
import com.dbflow5.query.From;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.PropertyFactory;

public class ModelViews {

   public static class AuthorName{
       public String name;
       public int age;

       public String getName() {
           return name;
       }

       public void setName(String name) {
           this.name = name;
       }

       public int getAge() {
           return age;
       }

       public void setAge(int age) {
           this.age = age;
       }

       public AuthorName(String name, int age) {
           this.name = name;
           this.age = age;
       }
   }

    @ModelView(database = TestDatabase.class)
    public static class AuthorView{
       @Column
       public int authorId;
       @Column
       public String authorName;
       @ColumnMap
       public AuthorName author;

        public int getAuthorId() {
            return authorId;
        }

        public void setAuthorId(int authorId) {
            this.authorId = authorId;
        }

        public String getAuthorName() {
            return authorName;
        }

        public void setAuthorName(String authorName) {
            this.authorName = authorName;
        }

        public AuthorName getAuthor() {
            return author;
        }

        public void setAuthor(AuthorName author) {
            this.author = author;
        }

        public AuthorView(int authorId, String authorName, AuthorName author) {
            this.authorId = authorId;
            this.authorName = authorName;
            this.author = author;
        }

        @ModelViewQuery
        public static From<ForeignKeyModels.Author> getQuery() {
            return SQLite.select(Author_Table.id.as("authorId"), Author_Table.first_name.concatenate((IProperty)PropertyFactory.property(" "))
                    .concatenate((IProperty)Author_Table.last_name).as("authorName")).from(ForeignKeyModels.Author.class);
        }
    }

    @ModelView(database = TestDatabase.class, priority = 2, allFields = true)
    public static class PriorityView {
        public String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public PriorityView(String name) {
            this.name = name;
        }

        public static From<ForeignKeyModels.Author> getQuery() {
            return SQLite.select(Author_Table.first_name.plus(Author_Table.last_name).as("name")).from(ForeignKeyModels.Author.class);
        }
    }
}


