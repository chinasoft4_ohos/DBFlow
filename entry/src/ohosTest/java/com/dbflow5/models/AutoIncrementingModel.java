package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;

@Table(database = TestDatabase.class)
public class AutoIncrementingModel {
    @PrimaryKey(autoincrement = true)
    public long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AutoIncrementingModel(long id) {
        this.id = id;
    }
}
