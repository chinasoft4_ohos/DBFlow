package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.IndexProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.ProspectQuizs.ProspectQuiz;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.util.Set;

public final class ProspectQuiz_Table extends ModelAdapter<ProspectQuiz> {
  /**
   * Primary Key */
  public static final Property<String> ID = new Property<String>(ProspectQuiz.class, "ID");

  public static final Property<String> question = new Property<String>(ProspectQuiz.class, "question");

  public static final Property<Integer> pendingResponseCount = new Property<Integer>(ProspectQuiz.class, "pendingResponseCount");

  public static final Property<Integer> resolvedResponseCount = new Property<Integer>(ProspectQuiz.class, "resolvedResponseCount");

  public static final Property<Integer> newResponseCount = new Property<Integer>(ProspectQuiz.class, "newResponseCount");

  public static final TypeConvertedProperty<String, Set<String>> pendingUnanswered = new TypeConvertedProperty<String, Set<String>>(ProspectQuiz.class, "pendingUnanswered", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          ProspectQuiz_Table adapter = (ProspectQuiz_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.typeConverterMutableSetTypeConverter;
                      }
                      });

  public static final Property<Long> modifiedDate = new Property<Long>(ProspectQuiz.class, "modifiedDate");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{ID,question,pendingResponseCount,resolvedResponseCount,newResponseCount,pendingUnanswered,modifiedDate};

  public static final IndexProperty<ProspectQuiz> index_modified = new IndexProperty<>("modified", false, ProspectQuiz.class,modifiedDate);

  private final ProspectQuizs.MutableSetTypeConverter typeConverterMutableSetTypeConverter = new ProspectQuizs.MutableSetTypeConverter();

  public ProspectQuiz_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<ProspectQuiz> table() {
    return ProspectQuiz.class;
  }

  @Override
  public final String getName() {
    return "ProspectQuiz";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "ID":  {
        return ID;
      }
      case "question":  {
        return question;
      }
      case "pendingResponseCount":  {
        return pendingResponseCount;
      }
      case "resolvedResponseCount":  {
        return resolvedResponseCount;
      }
      case "newResponseCount":  {
        return newResponseCount;
      }
      case "pendingUnanswered":  {
        return pendingUnanswered;
      }
      case "modifiedDate":  {
        return modifiedDate;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, ProspectQuiz model) {
    statement.bindStringOrNull(1, model.ID);
    if (model.getQuestion() != null) {
      statement.bindString(2, model.getQuestion());
    } else {
      statement.bindString(2, "");
    }
    statement.bindLong(3, (long)model.getPendingResponseCount());
    statement.bindLong(4, (long)model.getResolvedResponseCount());
    statement.bindLong(5, (long)model.getNewResponseCount());
    String refpendingUnanswered = typeConverterMutableSetTypeConverter.getDBValue(model.getPendingUnanswered());
    statement.bindStringOrNull(6, refpendingUnanswered);
    if (model.getModifiedDate() >= 0) {
      statement.bindNumber(7, model.getModifiedDate());
    } else {
      statement.bindLong(7, 1L);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, ProspectQuiz model) {
    statement.bindStringOrNull(1, model.ID);
    if (model.getQuestion() != null) {
      statement.bindString(2, model.getQuestion());
    } else {
      statement.bindString(2, "");
    }
    statement.bindLong(3, (long)model.getPendingResponseCount());
    statement.bindLong(4, (long)model.getResolvedResponseCount());
    statement.bindLong(5, (long)model.getNewResponseCount());
    String refpendingUnanswered = typeConverterMutableSetTypeConverter.getDBValue(model.getPendingUnanswered());
    statement.bindStringOrNull(6, refpendingUnanswered);
    if (model.getModifiedDate() >= 0) {
      statement.bindNumber(7, model.getModifiedDate());
    } else {
      statement.bindLong(7, 1L);
    }
    statement.bindStringOrNull(8, model.ID);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, ProspectQuiz model) {
    statement.bindStringOrNull(1, model.ID);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO ProspectQuiz(ID,question,pendingResponseCount,resolvedResponseCount,newResponseCount,pendingUnanswered,modifiedDate) VALUES (?,?,?,?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO ProspectQuiz(ID,question,pendingResponseCount,resolvedResponseCount,newResponseCount,pendingUnanswered,modifiedDate) VALUES (?,?,?,?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE ProspectQuiz SET ID=?,question=?,pendingResponseCount=?,resolvedResponseCount=?,newResponseCount=?,pendingUnanswered=?,modifiedDate=? WHERE ID=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM ProspectQuiz WHERE ID=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS ProspectQuiz(ID TEXT NOT NULL ON CONFLICT FAIL, question TEXT NOT NULL ON CONFLICT FAIL, pendingResponseCount INTEGER NOT NULL ON CONFLICT FAIL, resolvedResponseCount INTEGER NOT NULL ON CONFLICT FAIL, newResponseCount INTEGER NOT NULL ON CONFLICT FAIL, pendingUnanswered TEXT NOT NULL ON CONFLICT FAIL, modifiedDate INTEGER, PRIMARY KEY(ID))";
  }

  @Override
  public final ProspectQuiz loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    ProspectQuiz model = new ProspectQuiz();
    model.ID = cursor.getStringOrDefault("ID");
    model.setQuestion(cursor.getStringOrDefault("question", ""));
    model.setPendingResponseCount(cursor.getIntOrDefault("pendingResponseCount"));
    model.setResolvedResponseCount(cursor.getIntOrDefault("resolvedResponseCount"));
    model.setNewResponseCount(cursor.getIntOrDefault("newResponseCount"));
    int index_pendingUnanswered = cursor.getColumnIndexForName("pendingUnanswered");
    if (index_pendingUnanswered != -1 && !cursor.isColumnNull(index_pendingUnanswered)) {
      model.setPendingUnanswered(typeConverterMutableSetTypeConverter.getModelValue(cursor.getString(index_pendingUnanswered)));
    }
    model.setModifiedDate(cursor.getLongOrDefault("modifiedDate", 1L));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(ProspectQuiz model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(ID.eq(model.ID));
    return clause;
  }
}
