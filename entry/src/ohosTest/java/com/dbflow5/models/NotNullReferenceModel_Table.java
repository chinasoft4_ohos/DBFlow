package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.NotNullReferenceModel;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;

public final class NotNullReferenceModel_Table extends ModelAdapter<NotNullReferenceModel> {
  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(NotNullReferenceModel.class, "name");

  /**
   * Foreign Key */
  public static final Property<String> model_name = new Property<String>(NotNullReferenceModel.class, "model_name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name,model_name};

  public NotNullReferenceModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<NotNullReferenceModel> table() {
    return NotNullReferenceModel.class;
  }

  @Override
  public final String getName() {
    return "NotNullReferenceModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      case "model_name": {
        return model_name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      NotNullReferenceModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
    if (model.getModel() != null) {
      statement.bindStringOrNull(2, model.getModel().getName());
    } else {
      statement.bindNull(2);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      NotNullReferenceModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
    if (model.getModel() != null) {
      statement.bindStringOrNull(2, model.getModel().getName());
    } else {
      statement.bindNull(2);
    }
    if (model.getName() != null) {
      statement.bindString(3, model.getName());
    } else {
      statement.bindString(3, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      NotNullReferenceModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO NotNullReferenceModel(name,model_name) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO NotNullReferenceModel(name,model_name) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE NotNullReferenceModel SET name=?,model_name=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM NotNullReferenceModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS NotNullReferenceModel(name TEXT, model_name TEXT NOT NULL ON CONFLICT FAIL, PRIMARY KEY(name), FOREIGN KEY(model_name) REFERENCES SimpleModel (name) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final NotNullReferenceModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    NotNullReferenceModel model = new NotNullReferenceModel("", null);
    model.setName(cursor.getStringOrDefault("name", ""));
    int index_model_name_SimpleModel_Table = cursor.getColumnIndexForName("model_name");
    if (index_model_name_SimpleModel_Table != -1 && !cursor.isColumnNull(index_model_name_SimpleModel_Table)) {
      model.setModel(com.dbflow5.query.SQLite.select().from(SimpleTestModels.SimpleModel.class).where()
          .and(SimpleModel_Table.name.eq(cursor.getString(index_model_name_SimpleModel_Table)))
          .querySingle(wrapper));
    } else {
      model.setModel(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(NotNullReferenceModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
