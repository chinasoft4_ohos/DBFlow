package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.Dog;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class Dog_Table extends ModelAdapter<Dog> {
  /**
   * Foreign Key */
  public static final Property<Integer> owner_id = new Property<Integer>(Dog.class, "owner_id");

  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> id = new Property<Integer>(Dog.class, "id");

  public static final Property<String> name = new Property<String>(Dog.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{owner_id,id,name};

  public Dog_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Dog> table() {
    return Dog.class;
  }

  @Override
  public final String getName() {
    return "Dog";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "owner_id": {
        return owner_id;
      }
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(Dog model, Number id) {
    model.setId(id.intValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Dog model) {
    if (model.getOwner() != null) {
      statement.bindLong(1, (long)model.getOwner().getId());
    } else {
      statement.bindNull(1);
    }
    statement.bindLong(2, (long)model.getId());
    statement.bindStringOrNull(3, model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Dog model) {
    if (model.getOwner() != null) {
      statement.bindLong(1, (long)model.getOwner().getId());
    } else {
      statement.bindNull(1);
    }
    statement.bindLong(2, (long)model.getId());
    statement.bindStringOrNull(3, model.getName());
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Dog model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Dog(owner_id,id,name) VALUES (?,nullif(?, 0),?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Dog(owner_id,id,name) VALUES (?,nullif(?, 0),?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Dog SET owner_id=?,id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Dog WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Dog(owner_id INTEGER, id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, FOREIGN KEY(owner_id) REFERENCES Owner (id) ON UPDATE NO ACTION ON DELETE CASCADE)";
  }

  @Override
  public final Dog loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Dog model = new Dog(null,0,null);
    int index_owner_id_Owner_Table = cursor.getColumnIndexForName("owner_id");
    if (index_owner_id_Owner_Table != -1 && !cursor.isColumnNull(index_owner_id_Owner_Table)) {
      model.setOwner(new SimpleTestModels.Owner(0,null));
      model.getOwner().setId(cursor.getInt(index_owner_id_Owner_Table));
    } else {
      model.setOwner(null);
    }
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name"));
    return model;
  }

  @Override
  public final boolean exists(Dog model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(Dog.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Dog model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
