package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.UserInfo;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;

public final class UserInfo_Table extends ModelAdapter<UserInfo> {
  /**
   * Primary Key */
  public static final Property<String> email = new Property<String>(UserInfo.class, "email");

  public static final Property<String> password = new Property<String>(UserInfo.class, "password");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{email,password};

  public UserInfo_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<UserInfo> table() {
    return UserInfo.class;
  }

  @Override
  public final String getName() {
    return "UserInfo";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "email":  {
        return email;
      }
      case "password":  {
        return password;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, UserInfo model) {
    if (model.getEmail() != null) {
      statement.bindString(1, model.getEmail().value);
    } else {
      statement.bindString(1, "");
    }
    if (model.getPassword() != null) {
      statement.bindString(2, model.getPassword().value);
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, UserInfo model) {
    if (model.getEmail() != null) {
      statement.bindString(1, model.getEmail().value);
    } else {
      statement.bindString(1, "");
    }
    if (model.getPassword() != null) {
      statement.bindString(2, model.getPassword().value);
    } else {
      statement.bindString(2, "");
    }
    if (model.getEmail() != null) {
      statement.bindString(3, model.getEmail().value);
    } else {
      statement.bindString(3, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, UserInfo model) {
    if (model.getEmail() != null) {
      statement.bindString(1, model.getEmail().value);
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO UserInfo(email,password) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO UserInfo(email,password) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE UserInfo SET email=?,password=? WHERE email=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM UserInfo WHERE email=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS UserInfo(email TEXT, password TEXT, PRIMARY KEY(email))";
  }

  @Override
  public final UserInfo loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    UserInfo model = new UserInfo();
    model.setEmail(new SimpleTestModels.Email(cursor.getStringOrDefault("email", "")));
    model.setPassword(new SimpleTestModels.Password(cursor.getStringOrDefault("password", "")));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(UserInfo model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(email.eq(model.getEmail().value));
    return clause;
  }
}
