package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.IndexProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.WrapperProperty;
import com.dbflow5.models.ProspectQuizs.ProspectQuizEntry;
import com.dbflow5.models.ProspectQuizs.QuizParticipantStatus;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;

public final class ProspectQuizEntry_Table extends ModelAdapter<ProspectQuizEntry> {
  /**
   * Primary Key */
  public static final Property<String> profileID = new Property<String>(ProspectQuizEntry.class, "profileID");

  /**
   * Foreign Key / Primary Key */
  public static final Property<String> quizID_ID = new Property<String>(ProspectQuizEntry.class, "quizID_ID");

  public static final Property<String> text = new Property<String>(ProspectQuizEntry.class, "text");

  public static final WrapperProperty<String, QuizParticipantStatus> participantStatus = new WrapperProperty<String, QuizParticipantStatus>(ProspectQuizEntry.class, "participantStatus");

  public static final Property<String> name = new Property<String>(ProspectQuizEntry.class, "name");

  public static final Property<Long> answerEpoch = new Property<Long>(ProspectQuizEntry.class, "answerEpoch");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{profileID,quizID_ID,text,participantStatus,name,answerEpoch};

  public static final IndexProperty<ProspectQuizEntry> index_quizid_answerts = new IndexProperty<>("quizid_answerts", false, ProspectQuizEntry.class,quizID_ID, answerEpoch);

  public ProspectQuizEntry_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<ProspectQuizEntry> table() {
    return ProspectQuizEntry.class;
  }

  @Override
  public final String getName() {
    return "ProspectQuizEntry";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "profileID":  {
        return profileID;
      }
      case "quizID_ID": {
        return quizID_ID;
      }
      case "text":  {
        return text;
      }
      case "participantStatus":  {
        return participantStatus;
      }
      case "name":  {
        return name;
      }
      case "answerEpoch":  {
        return answerEpoch;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, ProspectQuizEntry model) {
    statement.bindStringOrNull(1, model.profileID);
    statement.bindStringOrNull(2, model.quizID);
    statement.bindStringOrNull(3, model.getText());
    String refparticipantStatus = model.getParticipantStatus() != null ? model.getParticipantStatus().name() : null;
    statement.bindStringOrNull(4, refparticipantStatus);
    if (model.getName() != null) {
      statement.bindString(5, model.getName());
    } else {
      statement.bindString(5, "");
    }
    statement.bindLong(6, model.getAnswerEpoch());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, ProspectQuizEntry model) {
    statement.bindStringOrNull(1, model.profileID);
    statement.bindStringOrNull(2, model.quizID);
    statement.bindStringOrNull(3, model.getText());
    String refparticipantStatus = model.getParticipantStatus() != null ? model.getParticipantStatus().name() : null;
    statement.bindStringOrNull(4, refparticipantStatus);
    if (model.getName() != null) {
      statement.bindString(5, model.getName());
    } else {
      statement.bindString(5, "");
    }
    statement.bindLong(6, model.getAnswerEpoch());
    statement.bindStringOrNull(7, model.profileID);
    statement.bindStringOrNull(8, model.quizID);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, ProspectQuizEntry model) {
    statement.bindStringOrNull(1, model.profileID);
    statement.bindStringOrNull(2, model.quizID);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO ProspectQuizEntry(profileID,quizID_ID,text,participantStatus,name,answerEpoch) VALUES (?,?,?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO ProspectQuizEntry(profileID,quizID_ID,text,participantStatus,name,answerEpoch) VALUES (?,?,?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE ProspectQuizEntry SET profileID=?,quizID_ID=?,text=?,participantStatus=?,name=?,answerEpoch=? WHERE profileID=? AND quizID_ID=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM ProspectQuizEntry WHERE profileID=? AND quizID_ID=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS ProspectQuizEntry(profileID TEXT NOT NULL ON CONFLICT FAIL, quizID_ID TEXT NOT NULL ON CONFLICT FAIL, text TEXT, participantStatus TEXT, name TEXT NOT NULL ON CONFLICT FAIL, answerEpoch INTEGER NOT NULL ON CONFLICT FAIL, PRIMARY KEY(profileID, quizID_ID), FOREIGN KEY(quizID_ID) REFERENCES ProspectQuiz (ID) ON UPDATE NO ACTION ON DELETE CASCADE)";
  }

  @Override
  public final ProspectQuizEntry loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    ProspectQuizEntry model = new ProspectQuizEntry();
    model.profileID = cursor.getStringOrDefault("profileID");
    model.quizID = cursor.getStringOrDefault("quizID_ID");
    model.setText(cursor.getStringOrDefault("text"));
    int index_participantStatus = cursor.getColumnIndexForName("participantStatus");
    if (index_participantStatus != -1 && !cursor.isColumnNull(index_participantStatus)) {
      try {
        model.setParticipantStatus(QuizParticipantStatus.valueOf(cursor.getString(index_participantStatus)));
      } catch (IllegalArgumentException e) {
        model.setParticipantStatus(null);
      }
    } else {
      model.setParticipantStatus(null);
    }
    model.setName(cursor.getStringOrDefault("name", ""));
    model.setAnswerEpoch(cursor.getLongOrDefault("answerEpoch"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(ProspectQuizEntry model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(profileID.eq(model.profileID));
    clause.and(quizID_ID.eq(model.quizID));
    return clause;
  }
}
