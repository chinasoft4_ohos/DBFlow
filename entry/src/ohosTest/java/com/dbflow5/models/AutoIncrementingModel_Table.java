package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class AutoIncrementingModel_Table extends ModelAdapter<AutoIncrementingModel> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> id = new Property<Long>(AutoIncrementingModel.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id};

  public AutoIncrementingModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<AutoIncrementingModel> table() {
    return AutoIncrementingModel.class;
  }

  @Override
  public final String getName() {
    return "AutoIncrementingModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(AutoIncrementingModel model, Number id) {
    model.setId(id.longValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      AutoIncrementingModel model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      AutoIncrementingModel model) {
    statement.bindLong(1, model.getId());
    statement.bindLong(2, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      AutoIncrementingModel model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO AutoIncrementingModel(id) VALUES (nullif(?, 0))";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO AutoIncrementingModel(id) VALUES (nullif(?, 0))";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE AutoIncrementingModel SET id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM AutoIncrementingModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS AutoIncrementingModel(id INTEGER PRIMARY KEY AUTOINCREMENT)";
  }

  @Override
  public final AutoIncrementingModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    AutoIncrementingModel model = new AutoIncrementingModel(0);
    model.setId(cursor.getLongOrDefault("id"));
    return model;
  }

  @Override
  public final boolean exists(AutoIncrementingModel model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(AutoIncrementingModel.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(AutoIncrementingModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
