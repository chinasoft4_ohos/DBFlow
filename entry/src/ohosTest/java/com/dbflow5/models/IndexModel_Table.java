package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.DateConverter;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.IndexProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.util.Date;

public final class IndexModel_Table extends ModelAdapter<IndexModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(IndexModel.class, "id");

  public static final Property<String> first_name = new Property<String>(IndexModel.class, "first_name");

  public static final Property<String> last_name = new Property<String>(IndexModel.class, "last_name");

  public static final TypeConvertedProperty<Long, Date> created_date = new TypeConvertedProperty<Long, Date>(IndexModel.class, "created_date", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          IndexModel_Table adapter = (IndexModel_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterDateConverter;
                      }
                      });

  public static final Property<Boolean> isPro = new Property<Boolean>(IndexModel.class, "isPro");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,first_name,last_name,created_date,isPro};

  public static final IndexProperty<IndexModel> index_firstIndex = new IndexProperty<>("firstIndex", false, IndexModel.class,id, first_name);

  public static final IndexProperty<IndexModel> index_secondIndex = new IndexProperty<>("secondIndex", false, IndexModel.class,id, last_name, isPro);

  public static final IndexProperty<IndexModel> index_thirdIndex = new IndexProperty<>("thirdIndex", false, IndexModel.class,id, created_date, isPro);

  private final DateConverter global_typeConverterDateConverter;

  public IndexModel_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterDateConverter = (DateConverter) holder.getTypeConverterForClass(Date.class);
  }

  @Override
  public final Class<IndexModel> table() {
    return IndexModel.class;
  }

  @Override
  public final String getName() {
    return "IndexModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "first_name":  {
        return first_name;
      }
      case "last_name":  {
        return last_name;
      }
      case "created_date":  {
        return created_date;
      }
      case "isPro":  {
        return isPro;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, IndexModel model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getFirst_name());
    statement.bindStringOrNull(3, model.getLast_name());
    Long refcreated_date = global_typeConverterDateConverter.getDBValue(model.getCreated_date());
    statement.bindNumberOrNull(4, refcreated_date);
    statement.bindLong(5, model.isPro() ? 1L : 0L);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, IndexModel model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getFirst_name());
    statement.bindStringOrNull(3, model.getLast_name());
    Long refcreated_date = global_typeConverterDateConverter.getDBValue(model.getCreated_date());
    statement.bindNumberOrNull(4, refcreated_date);
    statement.bindLong(5, model.isPro() ? 1L : 0L);
    statement.bindLong(6, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, IndexModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO IndexModel(id,first_name,last_name,created_date,isPro) VALUES (?,?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO IndexModel(id,first_name,last_name,created_date,isPro) VALUES (?,?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE IndexModel SET id=?,first_name=?,last_name=?,created_date=?,isPro=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM IndexModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS IndexModel(id INTEGER, first_name TEXT, last_name TEXT, created_date INTEGER, isPro INTEGER, PRIMARY KEY(id))";
  }

  @Override
  public final IndexModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    IndexModel model = new IndexModel(0,null,null,null,false);
    model.setId(cursor.getIntOrDefault("id"));
    model.setFirst_name(cursor.getStringOrDefault("first_name"));
    model.setLast_name(cursor.getStringOrDefault("last_name"));
    int index_created_date = cursor.getColumnIndexForName("created_date");
    if (index_created_date != -1 && !cursor.isColumnNull(index_created_date)) {
      model.setCreated_date(global_typeConverterDateConverter.getModelValue(cursor.getLong(index_created_date)));
    } else {
      model.setCreated_date(global_typeConverterDateConverter.getModelValue(null));
    }
    int index_isPro = cursor.getColumnIndexForName("isPro");
    if (index_isPro != -1 && !cursor.isColumnNull(index_isPro)) {
      model.setPro(cursor.getBoolean(index_isPro));
    } else {
      model.setPro(false);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(IndexModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
