package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.Author;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class Author_Table extends ModelAdapter<Author> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> id = new Property<>(Author.class, "id");

  public static final Property<String> first_name = new Property<>(Author.class, "first_name");

  public static final Property<String> last_name = new Property<>(Author.class, "last_name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,first_name,last_name};

  public Author_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Author> table() {
    return Author.class;
  }

  @Override
  public final String getName() {
    return "Author";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "first_name":  {
        return first_name;
      }
      case "last_name":  {
        return last_name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(Author model, Number id) {
    model.setId(id.intValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Author model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getFirstName() != null) {
      statement.bindString(2, model.getFirstName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getLastName() != null) {
      statement.bindString(3, model.getLastName());
    } else {
      statement.bindString(3, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Author model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getFirstName() != null) {
      statement.bindString(2, model.getFirstName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getLastName() != null) {
      statement.bindString(3, model.getLastName());
    } else {
      statement.bindString(3, "");
    }
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Author model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Author(id,first_name,last_name) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Author(id,first_name,last_name) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE `Author` SET id=?,first_name=?,last_name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Author WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Author(id INTEGER PRIMARY KEY AUTOINCREMENT, first_name TEXT, last_name TEXT)";
  }

  @Override
  public final Author loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Author model = new Author(0, "", "");
    model.setId(cursor.getIntOrDefault("id"));
    model.setFirstName(cursor.getStringOrDefault("first_name", ""));
    model.setLastName(cursor.getStringOrDefault("last_name", ""));
    return model;
  }

  @Override
  public final boolean exists(Author model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(Author.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Author model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
