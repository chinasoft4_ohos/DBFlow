package com.dbflow5.models;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.data.Blob;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.QueryModels.CustomBlobModel;
import java.lang.Class;
import java.lang.Override;

public final class CustomBlobModel_QueryTable extends RetrievalAdapter<CustomBlobModel> {
  public static final TypeConvertedProperty<Blob, CustomBlobModel.MyBlob> myBlob = new TypeConvertedProperty<Blob, CustomBlobModel.MyBlob>(CustomBlobModel.class, "myBlob", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          CustomBlobModel_QueryTable adapter = (CustomBlobModel_QueryTable) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterMyTypeConverter;
                      }
                      });

  private final CustomBlobModel.MyTypeConverter global_typeConverterMyTypeConverter;

  public CustomBlobModel_QueryTable(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterMyTypeConverter = (CustomBlobModel.MyTypeConverter) holder.getTypeConverterForClass(CustomBlobModel.MyBlob.class);
  }

  @Override
  public final Class<CustomBlobModel> table() {
    return CustomBlobModel.class;
  }

  @Override
  public final CustomBlobModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    CustomBlobModel model = new CustomBlobModel(null);
    int index_myBlob = cursor.getColumnIndexForName("myBlob");
    if (index_myBlob != -1 && !cursor.isColumnNull(index_myBlob)) {
      model.setMyBlob(global_typeConverterMyTypeConverter.getModelValue(new Blob(cursor.getBlob(index_myBlob))));
    } else {
      model.setMyBlob(global_typeConverterMyTypeConverter.getModelValue(null));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(CustomBlobModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    Blob refmyBlob = global_typeConverterMyTypeConverter.getDBValue(model.getMyBlob());
    clause.and(myBlob.invertProperty().eq(refmyBlob));
    return clause;
  }
}
