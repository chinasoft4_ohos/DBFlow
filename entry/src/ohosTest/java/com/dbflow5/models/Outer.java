package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;

public class Outer {
    @Table(database = TestDatabase.class)
    public static class Inner{
        @PrimaryKey
        public int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Inner(int id) {
            this.id = id;
        }
    }
}
