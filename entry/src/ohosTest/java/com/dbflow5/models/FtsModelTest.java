package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.query.Method;
import com.dbflow5.query.SQLite;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.query.property.PropertyFactory;
import com.dbflow5.structure.Model;
import org.junit.Test;

import static com.dbflow5.query.property.PropertyFactory.docId;

public class FtsModelTest extends BaseUnitTest {
    @Test
    public void validate_fts4_created() {
        FlowManager.database(TestDatabase.class, testDatabase -> {
            SimpleTestModels.Fts4Model model = new SimpleTestModels.Fts4Model(0, "FTSBABY");
            Model.save(SimpleTestModels.Fts4Model.class, model, testDatabase);

            long rows = SQLite.insert(SimpleTestModels.Fts4VirtualModel2.class, docId, Fts4VirtualModel2_Table.name)
                    .select(SQLite.select(Fts4Model_Table.id, Fts4Model_Table.name)
                            .from(SimpleTestModels.Fts4Model.class))
                    .executeInsert(testDatabase);
            assert (rows > 0);
            return null;
        });
    }

    @Test
    public void match_query() {
        validate_fts4_created();
        FlowManager.database(TestDatabase.class, testDatabase -> {
            SimpleTestModels.Fts4VirtualModel2 model = SQLite.select()
                    .from(SimpleTestModels.Fts4VirtualModel2.class)
                    .where(PropertyFactory.tableName(SimpleTestModels.Fts4VirtualModel2.class).match("FTSBABY")).querySingle(testDatabase);
            assert (model != null);
            return null;
        });
    }

    @Test
    public void offsets_query() {
        validate_fts4_created();
        FlowManager.database(TestDatabase.class, testDatabase -> {
            String value = SQLite.select(Method.offsets(SimpleTestModels.Fts4VirtualModel2.class))
                    .from(SimpleTestModels.Fts4VirtualModel2.class)
                    .where(PropertyFactory.tableName(SimpleTestModels.Fts4VirtualModel2.class)
                            .match("FTSBaby")).stringValue(testDatabase);
            assert (value != null);
            assert (value.equals("0 0 0 7"));
            return null;
        });
    }

    @Test
    public void snippet_query() {
        FlowManager.database(TestDatabase.class, db -> {
            SimpleTestModels.Fts4Model model = new SimpleTestModels.Fts4Model(0, "During 30 Nov-1 Dec, 2-3oC drops. Cool in the upper portion, minimum temperature 14-16oC \n" +
                    "  and cool elsewhere, minimum temperature 17-20oC. Cold to very cold on mountaintops, \n" +
                    "  minimum temperature 6-12oC. Northeasterly winds 15-30 km/hr. After that, temperature \n" +
                    "  increases. Northeasterly winds 15-30 km/hr. ");
            Model.save(SimpleTestModels.Fts4Model.class, model, db);

            long rows = SQLite.insert(SimpleTestModels.Fts4VirtualModel2.class, docId, Fts4VirtualModel2_Table.name)
                    .select(SQLite.select(Fts4Model_Table.id, Fts4Model_Table.name)
                            .from(SimpleTestModels.Fts4Model.class))
                    .executeInsert(db);
            assert (rows > 0);
            String value = SQLite.select(Method.snippet(SimpleTestModels.Fts4VirtualModel2.class, "[", "]", "...", 0, 0))
                    .from(SimpleTestModels.Fts4VirtualModel2.class)
                    .where(PropertyFactory.tableName(SimpleTestModels.Fts4VirtualModel2.class)
                            .match("\"min* tem*\""))
                    .stringValue(db);
            assert (value != null);
            assert (value.equals("...the upper portion, [minimum] [temperature] 14-16oC \n" +
                    "  and cool elsewhere, [minimum] [temperature] 17-20oC. Cold..."));
            return null;
        });
    }
}
