package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.DateConverter;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.SimpleTestModels.NonNullKotlinModel;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import javax.annotation.Generated;

public final class NonNullKotlinModel_Table extends ModelAdapter<NonNullKotlinModel> {
  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(NonNullKotlinModel.class, "name");

  public static final TypeConvertedProperty<Long, Date> date = new TypeConvertedProperty<Long, Date>(NonNullKotlinModel.class, "date", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          NonNullKotlinModel_Table adapter = (NonNullKotlinModel_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterDateConverter;
                      }
                      });

  public static final Property<Integer> numb = new Property<Integer>(NonNullKotlinModel.class, "numb");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name,date,numb};

  private final DateConverter global_typeConverterDateConverter;

  public NonNullKotlinModel_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterDateConverter = (DateConverter) holder.getTypeConverterForClass(Date.class);
  }

  @Override
  public final Class<NonNullKotlinModel> table() {
    return NonNullKotlinModel.class;
  }

  @Override
  public final String getName() {
    return "NonNullKotlinModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      case "date":  {
        return date;
      }
      case "numb":  {
        return numb;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, NonNullKotlinModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
    Long refdate = global_typeConverterDateConverter.getDBValue(model.getDate());
    statement.bindNumberOrNull(2, refdate);
    statement.bindLong(3, (long)model.getNumb());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, NonNullKotlinModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
    Long refdate = global_typeConverterDateConverter.getDBValue(model.getDate());
    statement.bindNumberOrNull(2, refdate);
    statement.bindLong(3, (long)model.getNumb());
    if (model.getName() != null) {
      statement.bindString(4, model.getName());
    } else {
      statement.bindString(4, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, NonNullKotlinModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO NonNullKotlinModel(name,date,numb) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO NonNullKotlinModel(name,date,numb) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE NonNullKotlinModel SET name=?,date=?,numb=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM NonNullKotlinModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS NonNullKotlinModel(name TEXT, date INTEGER, numb INTEGER, PRIMARY KEY(name))";
  }

  @Override
  public final NonNullKotlinModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    NonNullKotlinModel model = new NonNullKotlinModel("", new Date(), 0);
    model.setName(cursor.getStringOrDefault("name", ""));
    int index_date = cursor.getColumnIndexForName("date");
    if (index_date != -1 && !cursor.isColumnNull(index_date)) {
      model.setDate(global_typeConverterDateConverter.getModelValue(cursor.getLong(index_date)));
    }
    model.setNumb(cursor.getIntOrDefault("numb"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(NonNullKotlinModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
