package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.CacheAdapter;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.adapter.queriable.ListModelLoader;
import com.dbflow5.adapter.queriable.SingleKeyCacheableListModelLoader;
import com.dbflow5.adapter.queriable.SingleKeyCacheableModelLoader;
import com.dbflow5.adapter.queriable.SingleModelLoader;
import com.dbflow5.adapter.saveable.CacheableListModelSaver;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.cache.SimpleMapCache;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.TestModelChild;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;

public final class TestModelChild_Table extends ModelAdapter<TestModelChild> {
  /**
   * Primary Key */
  public static final Property<Long> id = new Property<Long>(TestModelChild.class, "id");

  public static final Property<String> name = new Property<String>(TestModelChild.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name};

  public static final CacheAdapter cacheAdapter = new CacheAdapter<TestModelChild>(new SimpleMapCache(25), 1, null) {
    @Override
    public final Object getCachingColumnValueFromModel(TestModelChild model) {
      return model.getId();
    }

    @Override
    public final Object getCachingColumnValueFromCursor(FlowCursor cursor) {
      return cursor.getLong(cursor.getColumnIndexForName("id"));
    }

    @Override
    public final Object getCachingId(TestModelChild model) {
      return getCachingColumnValueFromModel(model);
    }

    @Override
    public Object[] getCachingColumnValuesFromCursor(Object[] inValues, FlowCursor cursor) {
      return new Object[0];
    }

    @Override
    public Object[] getCachingColumnValuesFromModel(Object[] inValues, TestModelChild TModel) {
      return new Object[0];
    }

    @Override
    public void reloadRelationships(TestModelChild model, FlowCursor cursor, DatabaseWrapper databaseWrapper) {
    }
  };

  public TestModelChild_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TestModelChild> table() {
    return TestModelChild.class;
  }

  @Override
  public final String getName() {
    return "TestModelChild";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final SingleModelLoader createSingleModelLoader() {
    return new SingleKeyCacheableModelLoader<>(table(), cacheAdapter);
  }

  @Override
  public final ListModelLoader createListModelLoader() {
    return new SingleKeyCacheableListModelLoader<>(table(), cacheAdapter);
  }

  @Override
  protected CacheableListModelSaver<TestModelChild> createListModelSaver() {
    return new CacheableListModelSaver<>(getModelSaver(), cacheAdapter);
  }

  @Override
  public final boolean cachingEnabled() {
    return true;
  }

  @Override
  public final TestModelChild load(TestModelChild model, DatabaseWrapper wrapper) {
    TestModelChild loaded = super.load(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return loaded;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, TestModelChild model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, TestModelChild model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
    statement.bindLong(3, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, TestModelChild model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO TestModelChild(id,name) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO TestModelChild(id,name) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE TestModelChild SET id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM TestModelChild WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS TestModelChild(id INTEGER, name TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final TestModelChild loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    TestModelChild model = new TestModelChild(0,null);
    model.setId(cursor.getLongOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(TestModelChild model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }

  @Override
  public final boolean delete(TestModelChild model, DatabaseWrapper wrapper) {
    cacheAdapter.removeModelFromCache(model);
    boolean successful = super.delete(model, wrapper);
    return successful;
  }

  @Override
  public final long deleteAll(Collection<TestModelChild> models,
      DatabaseWrapper wrapper) {
    cacheAdapter.removeModelsFromCache(models);
    long successful = super.deleteAll(models, wrapper);
    return successful;
  }

  @Override
  public final boolean save(TestModelChild model, DatabaseWrapper wrapper) {
    boolean successful = super.save(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return successful;
  }

  @Override
  public final long saveAll(Collection<TestModelChild> models, DatabaseWrapper wrapper) {
    long count = super.saveAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }

  @Override
  public final long insert(TestModelChild model, DatabaseWrapper wrapper) {
    long rowId = super.insert(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return rowId;
  }

  @Override
  public final long insertAll(Collection<TestModelChild> models,
      DatabaseWrapper wrapper) {
    long count = super.insertAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }

  @Override
  public final boolean update(TestModelChild model, DatabaseWrapper wrapper) {
    boolean successful = super.update(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return successful;
  }

  @Override
  public final long updateAll(Collection<TestModelChild> models,
      DatabaseWrapper wrapper) {
    long count = super.updateAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }
}
