package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.CharConverter;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.SimpleTestModels.CharModel;
import java.lang.Character;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class CharModel_Table extends ModelAdapter<CharModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(CharModel.class, "id");

  public static final TypeConvertedProperty<String, Character> exampleChar = new TypeConvertedProperty<String, Character>(CharModel.class, "exampleChar", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          CharModel_Table adapter = (CharModel_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterCharConverter;
                      }
                      });

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,exampleChar};

  private final CharConverter global_typeConverterCharConverter;

  public CharModel_Table(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterCharConverter = (CharConverter) holder.getTypeConverterForClass(Character.class);
  }

  @Override
  public final Class<CharModel> table() {
    return CharModel.class;
  }

  @Override
  public final String getName() {
    return "CharModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "exampleChar":  {
        return exampleChar;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, CharModel model) {
    statement.bindLong(1, (long)model.getId());
    String refexampleChar = global_typeConverterCharConverter.getDBValue(model.getExampleChar());
    statement.bindStringOrNull(2, refexampleChar);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, CharModel model) {
    statement.bindLong(1, (long)model.getId());
    String refexampleChar = global_typeConverterCharConverter.getDBValue(model.getExampleChar());
    statement.bindStringOrNull(2, refexampleChar);
    statement.bindLong(3, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, CharModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO CharModel(id,exampleChar) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO CharModel(id,exampleChar) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE CharModel SET id=?,exampleChar=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM CharModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS CharModel(id INTEGER, exampleChar TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final CharModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    CharModel model = new CharModel(0,null);
    model.setId(cursor.getIntOrDefault("id"));
    int index_exampleChar = cursor.getColumnIndexForName("exampleChar");
    if (index_exampleChar != -1 && !cursor.isColumnNull(index_exampleChar)) {
      model.setExampleChar(global_typeConverterCharConverter.getModelValue(cursor.getString(index_exampleChar)));
    } else {
      model.setExampleChar(global_typeConverterCharConverter.getModelValue(null));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(CharModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
