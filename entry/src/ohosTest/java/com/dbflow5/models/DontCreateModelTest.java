package com.dbflow5.models;

import com.dbflow5.TestExtensions;
import com.dbflow5.config.FlowManager;
import com.dbflow5.query.SQLite;

import com.dbflow5.BaseUnitTest;
import ohos.data.rdb.RdbException;
import org.junit.Test;

public class DontCreateModelTest extends BaseUnitTest {
    @Test
    public void testModelNotCreated() {
        FlowManager.databaseForTable(SimpleTestModels.DontCreateModel.class, dbFlowDatabase -> {
            TestExtensions.assertThrowsException(RdbException.class, unused -> {
                SQLite.select().from(SimpleTestModels.DontCreateModel.class).queryList(dbFlowDatabase);
                return null;
            });
            return null;
        });
    }
}
