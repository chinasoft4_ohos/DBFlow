package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.SimpleQuickCheckModel;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class SimpleQuickCheckModel_Table extends ModelAdapter<SimpleQuickCheckModel> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> name = new Property<Integer>(SimpleQuickCheckModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name};

  public SimpleQuickCheckModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<SimpleQuickCheckModel> table() {
    return SimpleQuickCheckModel.class;
  }

  @Override
  public final String getName() {
    return "SimpleQuickCheckModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(SimpleQuickCheckModel model, Number id) {
    model.setName(id.intValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      SimpleQuickCheckModel model) {
    statement.bindLong(1, (long)model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      SimpleQuickCheckModel model) {
    statement.bindLong(1, (long)model.getName());
    statement.bindLong(2, (long)model.getName());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      SimpleQuickCheckModel model) {
    statement.bindLong(1, (long)model.getName());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO SimpleQuickCheckModel(name) VALUES (nullif(?, 0))";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO SimpleQuickCheckModel(name) VALUES (nullif(?, 0))";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE SimpleQuickCheckModel SET name=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM SimpleQuickCheckModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS SimpleQuickCheckModel(name INTEGER PRIMARY KEY AUTOINCREMENT)";
  }

  @Override
  public final SimpleQuickCheckModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    SimpleQuickCheckModel model = new SimpleQuickCheckModel(0);
    model.setName(cursor.getIntOrDefault("name"));
    return model;
  }

  @Override
  public final boolean exists(SimpleQuickCheckModel model, DatabaseWrapper wrapper) {
    return model.getName() > 0;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(SimpleQuickCheckModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
