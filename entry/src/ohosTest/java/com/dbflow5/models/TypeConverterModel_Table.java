package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.data.Blob;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.query.property.WrapperProperty;
import com.dbflow5.models.SimpleTestModels.TypeConverterModel;
import com.dbflow5.models.SimpleTestModels.CustomType;
import com.dbflow5.models.SimpleTestModels.CustomTypeConverter;
import com.dbflow5.models.SimpleTestModels.BlobConverter;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class TypeConverterModel_Table extends ModelAdapter<TypeConverterModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(TypeConverterModel.class, "id");

  public static final TypeConvertedProperty<Blob, byte[]> opaqueData = new TypeConvertedProperty<Blob, byte[]>(TypeConverterModel.class, "opaqueData", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          TypeConverterModel_Table adapter = (TypeConverterModel_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.typeConverterBlobConverter;
                      }
                      });

  public static final WrapperProperty<byte[], Blob> blob = new WrapperProperty<byte[], Blob>(TypeConverterModel.class, "blob");

  /**
   * Primary Key */
  public static final TypeConvertedProperty<Integer, CustomType> customType = new TypeConvertedProperty<Integer, CustomType>(TypeConverterModel.class, "customType", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          TypeConverterModel_Table adapter = (TypeConverterModel_Table) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.typeConverterCustomTypeConverter;
                      }
                      });

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,opaqueData,blob,customType};

  private final CustomTypeConverter typeConverterCustomTypeConverter = new CustomTypeConverter();

  private final BlobConverter typeConverterBlobConverter = new BlobConverter();

  public TypeConverterModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TypeConverterModel> table() {
    return TypeConverterModel.class;
  }

  @Override
  public final String getName() {
    return "TypeConverterModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "opaqueData":  {
        return opaqueData;
      }
      case "blob":  {
        return blob;
      }
      case "customType":  {
        return customType;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, TypeConverterModel model) {
    statement.bindLong(1, (long)model.getId());
    Blob refopaqueData = typeConverterBlobConverter.getDBValue(model.getOpaqueData());
    statement.bindBlobOrNull(2, refopaqueData != null ? refopaqueData.getBlob() : null);
    byte[] refblob = model.getBlob() != null ? model.getBlob().getBlob() : null;
    statement.bindBlobOrNull(3, refblob);
    Integer refcustomType = typeConverterCustomTypeConverter.getDBValue(model.getCustomType());
    statement.bindNumberOrNull(4, refcustomType);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, TypeConverterModel model) {
    statement.bindLong(1, (long)model.getId());
    Blob refopaqueData = typeConverterBlobConverter.getDBValue(model.getOpaqueData());
    statement.bindBlobOrNull(2, refopaqueData != null ? refopaqueData.getBlob() : null);
    byte[] refblob = model.getBlob() != null ? model.getBlob().getBlob() : null;
    statement.bindBlobOrNull(3, refblob);
    Integer refcustomType = typeConverterCustomTypeConverter.getDBValue(model.getCustomType());
    statement.bindNumberOrNull(4, refcustomType);
    statement.bindLong(5, (long)model.getId());
    statement.bindNumberOrNull(6, refcustomType);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, TypeConverterModel model) {
    statement.bindLong(1, (long)model.getId());
    Integer refcustomType = typeConverterCustomTypeConverter.getDBValue(model.getCustomType());
    statement.bindNumberOrNull(2, refcustomType);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO TypeConverterModel(id,opaqueData,blob,customType) VALUES (?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO TypeConverterModel(id,opaqueData,blob,customType) VALUES (?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE TypeConverterModel SET id=?,opaqueData=?,blob=?,customType=? WHERE id=? AND customType=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM TypeConverterModel WHERE id=? AND customType=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS TypeConverterModel(id INTEGER, opaqueData BLOB, blob BLOB, customType INTEGER, PRIMARY KEY(id, customType))";
  }

  @Override
  public final TypeConverterModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    TypeConverterModel model = new TypeConverterModel(0,null,null,null);
    model.setId(cursor.getIntOrDefault("id"));
    int index_opaqueData = cursor.getColumnIndexForName("opaqueData");
    if (index_opaqueData != -1 && !cursor.isColumnNull(index_opaqueData)) {
      model.setOpaqueData(typeConverterBlobConverter.getModelValue(new Blob(cursor.getBlob(index_opaqueData))));
    } else {
      model.setOpaqueData(typeConverterBlobConverter.getModelValue(null));
    }
    int index_blob = cursor.getColumnIndexForName("blob");
    if (index_blob != -1 && !cursor.isColumnNull(index_blob)) {
      model.setBlob(new Blob(cursor.getBlob(index_blob)));
    } else {
      model.setBlob(null);
    }
    int index_customType = cursor.getColumnIndexForName("customType");
    if (index_customType != -1 && !cursor.isColumnNull(index_customType)) {
      model.setCustomType(typeConverterCustomTypeConverter.getModelValue(cursor.getInt(index_customType)));
    } else {
      model.setCustomType(typeConverterCustomTypeConverter.getModelValue(null));
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(TypeConverterModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    Integer refcustomType = typeConverterCustomTypeConverter.getDBValue(model.getCustomType());
    clause.and(customType.invertProperty().eq(refcustomType));
    return clause;
  }
}
