package com.dbflow5.models;

import com.dbflow5.adapter.ModelViewAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ModelViews.PriorityView;
import java.lang.Override;
import java.lang.String;

public final class PriorityView_ViewTable extends ModelViewAdapter<PriorityView> {
  public static final String VIEW_NAME = "PriorityView";

  public static final Property<String> name = new Property<String>(PriorityView.class, "name");

  public PriorityView_ViewTable(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<PriorityView> table() {
    return PriorityView.class;
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE VIEW IF NOT EXISTS PriorityView AS " + PriorityView.getQuery().getQuery();
  }

  @Override
  public final String getName() {
    return "PriorityView";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.View;
  }

  @Override
  public final PriorityView loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    PriorityView model = new PriorityView("");
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(PriorityView model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
