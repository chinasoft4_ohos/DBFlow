package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.CacheAdapter;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.adapter.queriable.CacheableListModelLoader;
import com.dbflow5.adapter.queriable.CacheableModelLoader;
import com.dbflow5.adapter.queriable.ListModelLoader;
import com.dbflow5.adapter.queriable.SingleModelLoader;
import com.dbflow5.adapter.saveable.CacheableListModelSaver;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.cache.SimpleMapCache;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.CachingModels.Coordinate;
import java.lang.Double;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;

public final class Coordinate_Table extends ModelAdapter<Coordinate> {
  /**
   * Primary Key */
  public static final Property<Double> latitude = new Property<Double>(Coordinate.class, "latitude");

  /**
   * Primary Key */
  public static final Property<Double> longitude = new Property<Double>(Coordinate.class, "longitude");

  /**
   * Foreign Key */
  public static final Property<String> path_id = new Property<String>(Coordinate.class, "path_id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{latitude,longitude,path_id};

  public static final CacheAdapter cacheAdapter = new CacheAdapter<Coordinate>(new SimpleMapCache(25), 2, Coordinate.cacheConverter) {
    @Override
    public final Object[] getCachingColumnValuesFromModel(Object[] inValues, Coordinate model) {
      inValues[0] = model.getLatitude();
      inValues[1] = model.getLongitude();
      return inValues;
    }

    @Override
    public final Object[] getCachingColumnValuesFromCursor(Object[] inValues, FlowCursor cursor) {
      inValues[0] = cursor.getDouble(cursor.getColumnIndexForName("latitude"));
      inValues[1] = cursor.getDouble(cursor.getColumnIndexForName("longitude"));
      return inValues;
    }

    @Override
    public final void reloadRelationships(Coordinate model, FlowCursor cursor,
        DatabaseWrapper wrapper) {
      int index_path_id_Path_Table = cursor.getColumnIndexForName("path_id");
      if (index_path_id_Path_Table != -1 && !cursor.isColumnNull(index_path_id_Path_Table)) {
        model.setPath(com.dbflow5.query.SQLite.select().from(CachingModels.Path.class).where()
            .and(Path_Table.id.eq(cursor.getString(index_path_id_Path_Table)))
            .querySingle(wrapper));
      } else {
        model.setPath(null);
      }
    }

    @Override
    public Object getCachingColumnValueFromCursor(FlowCursor cursor) {
      return null;
    }

    @Override
    public Object getCachingColumnValueFromModel(Coordinate model) {
      return null;
    }
  };

  public Coordinate_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Coordinate> table() {
    return Coordinate.class;
  }

  @Override
  public final String getName() {
    return "Coordinate";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "latitude":  {
        return latitude;
      }
      case "longitude":  {
        return longitude;
      }
      case "path_id": {
        return path_id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final SingleModelLoader createSingleModelLoader() {
    return new CacheableModelLoader<>(table(), cacheAdapter);
  }

  @Override
  public final ListModelLoader createListModelLoader() {
    return new CacheableListModelLoader<>(table(), cacheAdapter);
  }

  @Override
  protected CacheableListModelSaver<Coordinate> createListModelSaver() {
    return new CacheableListModelSaver<>(getModelSaver(), cacheAdapter);
  }

  @Override
  public final boolean cachingEnabled() {
    return true;
  }

  @Override
  public final Coordinate load(Coordinate model, DatabaseWrapper wrapper) {
    Coordinate loaded = super.load(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return loaded;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Coordinate model) {
    statement.bindDouble(1, model.getLatitude());
    statement.bindDouble(2, model.getLongitude());
    if (model.getPath() != null) {
      if (model.getPath().getId() != null) {
        statement.bindString(3, model.getPath().getId());
      } else {
        statement.bindString(3, "");
      }
    } else {
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Coordinate model) {
    statement.bindDouble(1, model.getLatitude());
    statement.bindDouble(2, model.getLongitude());
    if (model.getPath() != null) {
      if (model.getPath().getId() != null) {
        statement.bindString(3, model.getPath().getId());
      } else {
        statement.bindString(3, "");
      }
    } else {
      statement.bindNull(3);
    }
    statement.bindDouble(4, model.getLatitude());
    statement.bindDouble(5, model.getLongitude());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Coordinate model) {
    statement.bindDouble(1, model.getLatitude());
    statement.bindDouble(2, model.getLongitude());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Coordinate(latitude,longitude,path_id) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Coordinate(latitude,longitude,path_id) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Coordinate SET latitude=?,longitude=?,path_id=? WHERE latitude=? AND longitude=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Coordinate WHERE latitude=? AND longitude=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Coordinate(latitude REAL, longitude REAL, path_id TEXT, PRIMARY KEY(latitude, longitude), FOREIGN KEY(path_id) REFERENCES Path (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final Coordinate loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Coordinate model = new Coordinate(0.0,0.0,null);
    model.setLatitude(cursor.getDoubleOrDefault("latitude"));
    model.setLongitude(cursor.getDoubleOrDefault("longitude"));
    int index_path_id_Path_Table = cursor.getColumnIndexForName("path_id");
    if (index_path_id_Path_Table != -1 && !cursor.isColumnNull(index_path_id_Path_Table)) {
      model.setPath(com.dbflow5.query.SQLite.select().from(CachingModels.Path.class).where()
          .and(Path_Table.id.eq(cursor.getString(index_path_id_Path_Table)))
          .querySingle(wrapper));
    } else {
      model.setPath(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Coordinate model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(latitude.eq(model.getLatitude()));
    clause.and(longitude.eq(model.getLongitude()));
    return clause;
  }

  @Override
  public final boolean delete(Coordinate model, DatabaseWrapper wrapper) {
    cacheAdapter.removeModelFromCache(model);
    boolean successful = super.delete(model, wrapper);
    return successful;
  }

  @Override
  public final long deleteAll(Collection<Coordinate> models, DatabaseWrapper wrapper) {
    cacheAdapter.removeModelsFromCache(models);
    long successful = super.deleteAll(models, wrapper);
    return successful;
  }

  @Override
  public final boolean save(Coordinate model, DatabaseWrapper wrapper) {
    boolean successful = super.save(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return successful;
  }

  @Override
  public final long saveAll(Collection<Coordinate> models, DatabaseWrapper wrapper) {
    long count = super.saveAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }

  @Override
  public final long insert(Coordinate model, DatabaseWrapper wrapper) {
    long rowId = super.insert(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return rowId;
  }

  @Override
  public final long insertAll(Collection<Coordinate> models, DatabaseWrapper wrapper) {
    long count = super.insertAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }

  @Override
  public final boolean update(Coordinate model, DatabaseWrapper wrapper) {
    boolean successful = super.update(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return successful;
  }

  @Override
  public final long updateAll(Collection<Coordinate> models, DatabaseWrapper wrapper) {
    long count = super.updateAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }
}
