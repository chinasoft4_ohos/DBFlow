package com.dbflow5.models;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.SimpleCustomModel;
import java.lang.Override;
import java.lang.String;

public final class SimpleCustomModel_QueryTable extends RetrievalAdapter<SimpleCustomModel> {
  public static final Property<String> name = new Property<String>(SimpleCustomModel.class, "name");

  public SimpleCustomModel_QueryTable(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<SimpleCustomModel> table() {
    return SimpleCustomModel.class;
  }

  @Override
  public final SimpleCustomModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    SimpleCustomModel model = new SimpleCustomModel("");
    model.setName(cursor.getStringOrDefault("name"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(SimpleCustomModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
