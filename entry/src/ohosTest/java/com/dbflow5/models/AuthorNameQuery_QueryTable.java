package com.dbflow5.models;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.QueryModels.AuthorNameQuery;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class AuthorNameQuery_QueryTable extends RetrievalAdapter<AuthorNameQuery> {
  public static final Property<String> blogName = new Property<String>(AuthorNameQuery.class, "blogName");

  public static final Property<Integer> authorId = new Property<Integer>(AuthorNameQuery.class, "authorId");

  public static final Property<Integer> blogId = new Property<Integer>(AuthorNameQuery.class, "blogId");

  public AuthorNameQuery_QueryTable(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<AuthorNameQuery> table() {
    return AuthorNameQuery.class;
  }

  @Override
  public final AuthorNameQuery loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    AuthorNameQuery model = new AuthorNameQuery("",0,0);
    model.setBlogName(cursor.getStringOrDefault("blogName", ""));
    model.setAuthorId(cursor.getIntOrDefault("authorId"));
    model.setBlogId(cursor.getIntOrDefault("blogId"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(AuthorNameQuery model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(blogName.eq(model.getBlogName()));
    clause.and(authorId.eq(model.getAuthorId()));
    clause.and(blogId.eq(model.getBlogId()));
    return clause;
  }
}
