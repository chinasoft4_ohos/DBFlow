package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.UniqueModel;
import com.dbflow5.models.SimpleTestModels.CustomType;
import com.dbflow5.models.SimpleTestModels.CustomTypeConverter;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class UniqueModel_Table extends ModelAdapter<UniqueModel> {
  /**
   * Primary Key */
  public static final Property<String> id = new Property<String>(UniqueModel.class, "id");

  public static final Property<String> name = new Property<String>(UniqueModel.class, "name");

  /**
   * Foreign Key */
  public static final Property<Integer> model_id = new Property<Integer>(UniqueModel.class, "model_id");

  /**
   * Foreign Key */
  public static final Property<CustomType> model_customType = new Property<CustomType>(UniqueModel.class, "model_customType");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name,model_id,model_customType};

  private final CustomTypeConverter typeConverterCustomTypeConverter = new CustomTypeConverter();

  public UniqueModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<UniqueModel> table() {
    return UniqueModel.class;
  }

  @Override
  public final String getName() {
    return "UniqueModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      case "model_id": {
        return model_id;
      }
      case "model_customType": {
        return model_customType;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, UniqueModel model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getModel() != null) {
      statement.bindLong(3, (long)model.getModel().getId());
      Integer modelrefcustomType = typeConverterCustomTypeConverter.getDBValue(model.getModel().getCustomType());
      statement.bindNumberOrNull(4, modelrefcustomType);
    } else {
      statement.bindNull(3);
      statement.bindNull(4);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, UniqueModel model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getModel() != null) {
      statement.bindLong(3, (long)model.getModel().getId());
      Integer model_refcustomType = typeConverterCustomTypeConverter.getDBValue(model.getModel().getCustomType());
      statement.bindNumberOrNull(4, model_refcustomType);
    } else {
      statement.bindNull(3);
      statement.bindNull(4);
    }
    if (model.getId() != null) {
      statement.bindString(5, model.getId());
    } else {
      statement.bindString(5, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, UniqueModel model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO UniqueModel(id,name,model_id,model_customType) VALUES (?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO UniqueModel(id,name,model_id,model_customType) VALUES (?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE UniqueModel SET id=?,name=?,model_id=?,model_customType=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM UniqueModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS UniqueModel(id TEXT, name TEXT UNIQUE ON CONFLICT FAIL, model_id INTEGER ,model_customType INTEGER, UNIQUE(name, model_id, model_customType) ON CONFLICT FAIL, PRIMARY KEY(id), FOREIGN KEY(model_id, model_customType) REFERENCES TypeConverterModel (id, customType) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final UniqueModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    UniqueModel model = new UniqueModel("","",null);
    model.setId(cursor.getStringOrDefault("id", ""));
    model.setName(cursor.getStringOrDefault("name", ""));
    int index_model_id_TypeConverterModel_Table = cursor.getColumnIndexForName("model_id");
    int index_model_customType_TypeConverterModel_Table = cursor.getColumnIndexForName("model_customType");
    if (index_model_id_TypeConverterModel_Table != -1 && !cursor.isColumnNull(index_model_id_TypeConverterModel_Table) && index_model_customType_TypeConverterModel_Table != -1 && !cursor.isColumnNull(index_model_customType_TypeConverterModel_Table)) {
      model.setModel(com.dbflow5.query.SQLite.select().from(SimpleTestModels.TypeConverterModel.class).where()
          .and(TypeConverterModel_Table.id.eq(cursor.getInt(index_model_id_TypeConverterModel_Table)))
          .and(TypeConverterModel_Table.customType.eq(typeConverterCustomTypeConverter.getModelValue(cursor.getInt(index_model_customType_TypeConverterModel_Table))))
          .querySingle(wrapper));
    } else {
      model.setModel(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(UniqueModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
