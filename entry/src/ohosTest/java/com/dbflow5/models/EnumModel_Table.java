package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.WrapperProperty;
import com.dbflow5.models.SimpleTestModels.EnumModel;
import com.dbflow5.models.SimpleTestModels.Difficulty;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class EnumModel_Table extends ModelAdapter<EnumModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(EnumModel.class, "id");

  public static final WrapperProperty<String, Difficulty> difficulty = new WrapperProperty<String, Difficulty>(EnumModel.class, "difficulty");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,difficulty};

  public EnumModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<EnumModel> table() {
    return EnumModel.class;
  }

  @Override
  public final String getName() {
    return "EnumModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "difficulty":  {
        return difficulty;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, EnumModel model) {
    statement.bindLong(1, (long)model.getId());
    String refdifficulty = model.getDifficulty() != null ? model.getDifficulty().name() : null;
    statement.bindStringOrNull(2, refdifficulty);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, EnumModel model) {
    statement.bindLong(1, (long)model.getId());
    String refdifficulty = model.getDifficulty() != null ? model.getDifficulty().name() : null;
    statement.bindStringOrNull(2, refdifficulty);
    statement.bindLong(3, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, EnumModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO EnumModel(id,difficulty) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO EnumModel(id,difficulty) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE EnumModel SET id=?,difficulty=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM EnumModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS EnumModel(id INTEGER, difficulty TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final EnumModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    EnumModel model = new EnumModel(0, Difficulty.EASY);
    model.setId(cursor.getIntOrDefault("id"));
    int index_difficulty = cursor.getColumnIndexForName("difficulty");
    if (index_difficulty != -1 && !cursor.isColumnNull(index_difficulty)) {
      try {
        model.setDifficulty(Difficulty.valueOf(cursor.getString(index_difficulty)));
      } catch (IllegalArgumentException e) {
        model.setDifficulty(null);
      }
    } else {
      model.setDifficulty(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(EnumModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
