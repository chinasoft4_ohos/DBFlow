package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.SqlListenerModel;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class SqlListenerModel_Table extends ModelAdapter<SqlListenerModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(SqlListenerModel.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id};

  public SqlListenerModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<SqlListenerModel> table() {
    return SqlListenerModel.class;
  }

  @Override
  public final String getName() {
    return "SqlListenerModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, SqlListenerModel model) {
    statement.bindLong(1, (long)model.getId());
    model.onBindToInsertStatement(statement);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, SqlListenerModel model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindLong(2, (long)model.getId());
    model.onBindToUpdateStatement(statement);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, SqlListenerModel model) {
    statement.bindLong(1, (long)model.getId());
    model.onBindToDeleteStatement(statement);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO SqlListenerModel(id) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO SqlListenerModel(id) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE SqlListenerModel SET id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM SqlListenerModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS SqlListenerModel(id INTEGER, PRIMARY KEY(id))";
  }

  @Override
  public final SqlListenerModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    SqlListenerModel model = new SqlListenerModel(0);
    model.setId(cursor.getIntOrDefault("id"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(SqlListenerModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
