package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.ForeignKey;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.structure.BaseModel;

@Table(database = TestDatabase.class)
public final class Artist_Song extends BaseModel {
  @PrimaryKey(autoincrement = true)
  long _id;

  @ForeignKey(saveForeignKeyModel = false)
  ManyToMany.Song song;

  @ForeignKey(saveForeignKeyModel = false)
  ManyToMany.Artist artist;

  public final long getId() {
    return _id;
  }

  public final ManyToMany.Song getSong() {
    return song;
  }

  public final void setSong(ManyToMany.Song param) {
    song = param;
  }

  public final ManyToMany.Artist getArtist() {
    return artist;
  }

  public final void setArtist(ManyToMany.Artist param) {
    artist = param;
  }
}
