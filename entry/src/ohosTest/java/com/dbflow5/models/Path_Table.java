package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.CachingModels.Path;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;

public final class Path_Table extends ModelAdapter<Path> {
  /**
   * Primary Key */
  public static final Property<String> id = new Property<String>(Path.class, "id");

  public static final Property<String> name = new Property<String>(Path.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name};

  public Path_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Path> table() {
    return Path.class;
  }

  @Override
  public final String getName() {
    return "Path";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Path model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Path model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getId() != null) {
      statement.bindString(3, model.getId());
    } else {
      statement.bindString(3, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Path model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Path(id,name) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Path(id,name) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Path SET id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Path WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Path(id TEXT, name TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final Path loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Path model = new Path("","");
    model.setId(cursor.getStringOrDefault("id", ""));
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Path model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
