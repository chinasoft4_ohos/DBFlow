package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.TestModelParent;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;

public final class TestModelParent_Table extends ModelAdapter<TestModelParent> {
  /**
   * Primary Key */
  public static final Property<Long> id = new Property<Long>(TestModelParent.class, "id");

  public static final Property<String> name = new Property<String>(TestModelParent.class, "name");

  /**
   * Foreign Key */
  public static final Property<Long> child_id = new Property<Long>(TestModelParent.class, "child_id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name,child_id};

  public TestModelParent_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TestModelParent> table() {
    return TestModelParent.class;
  }

  @Override
  public final String getName() {
    return "TestModelParent";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      case "child_id": {
        return child_id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, TestModelParent model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
    if (model.getChild() != null) {
      statement.bindLong(3, model.getChild().getId());
    } else {
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, TestModelParent model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
    if (model.getChild() != null) {
      statement.bindLong(3, model.getChild().getId());
    } else {
      statement.bindNull(3);
    }
    statement.bindLong(4, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, TestModelParent model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO TestModelParent(id,name,child_id) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO TestModelParent(id,name,child_id) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE TestModelParent SET id=?,name=?,child_id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM TestModelParent WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS TestModelParent(id INTEGER, name TEXT, child_id INTEGER, PRIMARY KEY(id), FOREIGN KEY(child_id) REFERENCES TestModelChild (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final TestModelParent loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    TestModelParent model = new TestModelParent(0,null,null);
    model.setId(cursor.getLongOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name"));
    int index_child_id_TestModelChild_Table = cursor.getColumnIndexForName("child_id");
    if (index_child_id_TestModelChild_Table != -1 && !cursor.isColumnNull(index_child_id_TestModelChild_Table)) {
      model.setChild(new SimpleTestModels.TestModelChild(0,null));
      model.getChild().setId(cursor.getLong(index_child_id_TestModelChild_Table));
    } else {
      model.setChild(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(TestModelParent model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
