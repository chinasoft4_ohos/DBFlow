package com.dbflow5.models.NonTypical;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;

@Table(database = TestDatabase.class)
public class nonTypicalClassName {
    @PrimaryKey
    public int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public nonTypicalClassName(int id) {
        this.id = id;
    }
}
