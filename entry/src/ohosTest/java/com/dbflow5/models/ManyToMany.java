package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;

public class ManyToMany {

    @com.dbflow5.annotation.ManyToMany(referencedTable = Song.class)
    @Table(database = TestDatabase.class)
    public static class Artist{
        @PrimaryKey(autoincrement = true)
        public int id;
        @Column
        public String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Artist(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    @Table(database = TestDatabase.class)
    public static class Song{
        @PrimaryKey(autoincrement = true)
        public int id;
        @Column
        public String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Song(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
