package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class Artist_Song_Table extends ModelAdapter<Artist_Song> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> _id = new Property<Long>(Artist_Song.class, "_id");

  /**
   * Foreign Key */
  public static final Property<Integer> song_id = new Property<Integer>(Artist_Song.class, "song_id");

  /**
   * Foreign Key */
  public static final Property<Integer> artist_id = new Property<Integer>(Artist_Song.class, "artist_id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{_id,song_id,artist_id};

  public Artist_Song_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Artist_Song> table() {
    return Artist_Song.class;
  }

  @Override
  public final String getName() {
    return "Artist_Song";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "_id":  {
        return _id;
      }
      case "song_id": {
        return song_id;
      }
      case "artist_id": {
        return artist_id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(Artist_Song model, Number id) {
    model._id = id.longValue();
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Artist_Song model) {
    statement.bindLong(1, model._id);
    if (model.song != null) {
      statement.bindLong(2, (long)model.song.getId());
    } else {
      statement.bindNull(2);
    }
    if (model.artist != null) {
      statement.bindLong(3, (long)model.artist.getId());
    } else {
      statement.bindNull(3);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Artist_Song model) {
    statement.bindLong(1, model._id);
    if (model.song != null) {
      statement.bindLong(2, (long)model.song.getId());
    } else {
      statement.bindNull(2);
    }
    if (model.artist != null) {
      statement.bindLong(3, (long)model.artist.getId());
    } else {
      statement.bindNull(3);
    }
    statement.bindLong(4, model._id);
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Artist_Song model) {
    statement.bindLong(1, model._id);
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Artist_Song(_id,song_id,artist_id) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Artist_Song(_id,song_id,artist_id) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Artist_Song SET _id=?,song_id=?,artist_id=? WHERE _id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Artist_Song WHERE _id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Artist_Song(_id INTEGER PRIMARY KEY AUTOINCREMENT, song_id INTEGER, artist_id INTEGER, FOREIGN KEY(song_id) REFERENCES Song (id) ON UPDATE NO ACTION ON DELETE NO ACTION, FOREIGN KEY(artist_id) REFERENCES Artist (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final Artist_Song loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Artist_Song model = new Artist_Song();
    model._id = cursor.getLongOrDefault("_id");
    int index_song_id_Song_Table = cursor.getColumnIndexForName("song_id");
    if (index_song_id_Song_Table != -1 && !cursor.isColumnNull(index_song_id_Song_Table)) {
      model.song = SQLite.select().from(ManyToMany.Song.class).where()
          .and(Song_Table.id.eq(cursor.getInt(index_song_id_Song_Table)))
          .querySingle(wrapper);
    } else {
      model.song = null;
    }
    int index_artist_id_Artist_Table = cursor.getColumnIndexForName("artist_id");
    if (index_artist_id_Artist_Table != -1 && !cursor.isColumnNull(index_artist_id_Artist_Table)) {
      model.artist = SQLite.select().from(ManyToMany.Artist.class).where()
          .and(Artist_Table.id.eq(cursor.getInt(index_artist_id_Artist_Table)))
          .querySingle(wrapper);
    } else {
      model.artist = null;
    }
    return model;
  }

  @Override
  public final boolean exists(Artist_Song model, DatabaseWrapper wrapper) {
    return model._id > 0
    && SQLite.selectCountOf()
    .from(Artist_Song.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Artist_Song model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(_id.eq(model._id));
    return clause;
  }
}
