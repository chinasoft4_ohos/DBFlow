package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.QueryModel;
import com.dbflow5.converter.TypeConverters;
import com.dbflow5.data.Blob;

public class QueryModels{

    @QueryModel(database = TestDatabase.class, allFields = true)
    public static class AuthorNameQuery{
        public String blogName;
        public int authorId;
        public int blogId;

        public String getBlogName() {
            return blogName;
        }

        public void setBlogName(String blogName) {
            this.blogName = blogName;
        }

        public int getAuthorId() {
            return authorId;
        }

        public void setAuthorId(int authorId) {
            this.authorId = authorId;
        }

        public int getBlogId() {
            return blogId;
        }

        public void setBlogId(int blogId) {
            this.blogId = blogId;
        }

        public AuthorNameQuery(String blogName, int authorId, int blogId) {
            this.blogName = blogName;
            this.authorId = authorId;
            this.blogId = blogId;
        }
    }

    @QueryModel(database = TestDatabase.class)
    public static class CustomBlobModel{
        @Column
        public MyBlob myBlob;

        public MyBlob getMyBlob() {
            return myBlob;
        }

        public void setMyBlob(MyBlob myBlog) {
            this.myBlob = myBlog;
        }

        public CustomBlobModel(MyBlob myBlob) {
            this.myBlob = myBlob;
        }

        public static class MyBlob{
            public byte[] blob;

            public MyBlob(byte[] blob) {
                this.blob = blob;
            }
        }

        @com.dbflow5.annotation.TypeConverter
        public static class MyTypeConverter extends TypeConverters.TypeConverter<Blob, MyBlob> {

            public MyTypeConverter() {
                super();
            }

            @Override
            public Blob getDBValue(MyBlob model) {
                if(model != null) {
                    return new Blob(model.blob);
                }
                return null;
            }

            @Override
            public MyBlob getModelValue(Blob data) {
                if(data != null) {
                    return new MyBlob(data.getBlob());
                }
                return null;
            }
        }
    }

    @QueryModel(database = TestDatabase.class, allFields = true)
    public static class AllFieldsQueryModel{
        public String fieldModel;

        public String getFieldModel() {
            return fieldModel;
        }

        public void setFieldModel(String fieldModel) {
            this.fieldModel = fieldModel;
        }

        public AllFieldsQueryModel(String fieldModel) {
            this.fieldModel = fieldModel;
        }
    }
}

