package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.SimpleTestModels.AllFieldsModel;
import java.lang.Boolean;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class AllFieldsModel_Table extends ModelAdapter<AllFieldsModel> {
  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(AllFieldsModel.class, "name");

  public static final Property<Integer> count = new Property<Integer>(AllFieldsModel.class, "count");

  public static final Property<Boolean> truth = new Property<Boolean>(AllFieldsModel.class, "truth");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name,count,truth};

  public AllFieldsModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<AllFieldsModel> table() {
    return AllFieldsModel.class;
  }

  @Override
  public final String getName() {
    return "AllFieldsModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      case "count":  {
        return count;
      }
      case "truth":  {
        return truth;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, AllFieldsModel model) {
    statement.bindStringOrNull(1, model.getName());
    statement.bindNumberOrNull(2, model.getCount());
    statement.bindLong(3, model.isTruth() ? 1L : 0L);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, AllFieldsModel model) {
    statement.bindStringOrNull(1, model.getName());
    statement.bindNumberOrNull(2, model.getCount());
    statement.bindLong(3, model.isTruth() ? 1L : 0L);
    statement.bindStringOrNull(4, model.getName());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, AllFieldsModel model) {
    statement.bindStringOrNull(1, model.getName());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO AllFieldsModel(name,count,truth) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO AllFieldsModel(name,count,truth) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE AllFieldsModel SET name=?,count=?,truth=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM AllFieldsModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS AllFieldsModel(name TEXT, count INTEGER, truth INTEGER, PRIMARY KEY(name))";
  }

  @Override
  public final AllFieldsModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    AllFieldsModel model = new AllFieldsModel(null, 0, false, "", 0);
    model.setName(cursor.getStringOrDefault("name"));
    model.setCount(cursor.getIntOrDefault("count", 0));
    int index_truth = cursor.getColumnIndexForName("truth");
    if (index_truth != -1 && !cursor.isColumnNull(index_truth)) {
      model.setTruth(cursor.getBoolean(index_truth));
    } else {
      model.setTruth(false);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(AllFieldsModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
