package com.dbflow5.models;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.converter.TypeConverters.TypeConverter;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;
import com.dbflow5.query.property.TypeConvertedProperty.TypeConverterGetter;
import com.dbflow5.models.ForeignKeyModels.Location2;
import com.dbflow5.models.ForeignKeyModels.DoubleToDouble;
import java.lang.Class;
import java.lang.Double;
import java.lang.Override;

public final class Location2_QueryTable extends RetrievalAdapter<Location2> {
  public static final TypeConvertedProperty<Double, DoubleToDouble> latitude = new TypeConvertedProperty<Double, DoubleToDouble>(Location2.class, "latitude", true,
                      new TypeConverterGetter() {
                      @Override
                      public TypeConverter getTypeConverter(Class<?> modelClass) {
                          Location2_QueryTable adapter = (Location2_QueryTable) FlowManager.getRetrievalAdapter(modelClass);
                          return adapter.global_typeConverterDoubleConverter;
                      }
                      });

  public static final Property<Double> longitude = new Property<Double>(Location2.class, "longitude");

  private final ForeignKeyModels.DoubleConverter global_typeConverterDoubleConverter;

  public Location2_QueryTable(DatabaseHolder holder, DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
    global_typeConverterDoubleConverter = (ForeignKeyModels.DoubleConverter) holder.getTypeConverterForClass(DoubleToDouble.class);
  }

  @Override
  public final Class<Location2> table() {
    return Location2.class;
  }

  @Override
  public final Location2 loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Location2 model = new Location2(new DoubleToDouble(0.0), 0.0);
    int index_latitude = cursor.getColumnIndexForName("latitude");
    if (index_latitude != -1 && !cursor.isColumnNull(index_latitude)) {
      model.setLatitude(global_typeConverterDoubleConverter.getModelValue(cursor.getDouble(index_latitude)));
    } else {
      model.setLatitude(global_typeConverterDoubleConverter.getModelValue(null));
    }
    model.setLongitude(cursor.getDoubleOrDefault("longitude", 0.0));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Location2 model) {
    OperatorGroup clause = OperatorGroup.clause();
    Double reflatitude = global_typeConverterDoubleConverter.getDBValue(model.getLatitude());
    clause.and(latitude.invertProperty().eq(reflatitude));
    clause.and(longitude.eq(model.getLongitude()));
    return clause;
  }
}
