package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.CacheAdapter;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.adapter.queriable.ListModelLoader;
import com.dbflow5.adapter.queriable.SingleKeyCacheableListModelLoader;
import com.dbflow5.adapter.queriable.SingleKeyCacheableModelLoader;
import com.dbflow5.adapter.queriable.SingleModelLoader;
import com.dbflow5.adapter.saveable.CacheableListModelSaver;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.cache.SimpleMapCache;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.CachingModels.SimpleCacheObject;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;

public final class SimpleCacheObject_Table extends ModelAdapter<SimpleCacheObject> {
  /**
   * Primary Key */
  public static final Property<String> id = new Property<String>(SimpleCacheObject.class, "id");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id};

  public static final CacheAdapter cacheAdapter = new CacheAdapter<SimpleCacheObject>(new SimpleMapCache(25), 1, null) {
    @Override
    public final Object getCachingColumnValueFromModel(SimpleCacheObject model) {
      return model.getId();
    }

    @Override
    public final Object getCachingColumnValueFromCursor(FlowCursor cursor) {
      return cursor.getString(cursor.getColumnIndexForName("id"));
    }

    @Override
    public final Object getCachingId(SimpleCacheObject model) {
      return getCachingColumnValueFromModel(model);
    }

    @Override
    public Object[] getCachingColumnValuesFromCursor(Object[] inValues, FlowCursor cursor) {
      return new Object[0];
    }

    @Override
    public Object[] getCachingColumnValuesFromModel(Object[] inValues, SimpleCacheObject TModel) {
      return new Object[0];
    }

    @Override
    public void reloadRelationships(SimpleCacheObject model, FlowCursor cursor, DatabaseWrapper databaseWrapper) {
    }
  };

  public SimpleCacheObject_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<SimpleCacheObject> table() {
    return SimpleCacheObject.class;
  }

  @Override
  public final String getName() {
    return "SimpleCacheObject";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final SingleModelLoader createSingleModelLoader() {
    return new SingleKeyCacheableModelLoader(table(), cacheAdapter);
  }

  @Override
  public final ListModelLoader createListModelLoader() {
    return new SingleKeyCacheableListModelLoader<>(table(), cacheAdapter);
  }

  @Override
  protected CacheableListModelSaver<SimpleCacheObject> createListModelSaver() {
    return new CacheableListModelSaver<>(getModelSaver(), cacheAdapter);
  }

  @Override
  public final boolean cachingEnabled() {
    return true;
  }

  @Override
  public final SimpleCacheObject load(SimpleCacheObject model, DatabaseWrapper wrapper) {
    SimpleCacheObject loaded = super.load(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return loaded;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, SimpleCacheObject model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, SimpleCacheObject model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    if (model.getId() != null) {
      statement.bindString(2, model.getId());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, SimpleCacheObject model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO SimpleCacheObject(id) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO SimpleCacheObject(id) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE SimpleCacheObject SET id=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM SimpleCacheObject WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS SimpleCacheObject(id TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final SimpleCacheObject loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    SimpleCacheObject model = new SimpleCacheObject("");
    model.setId(cursor.getStringOrDefault("id", ""));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(SimpleCacheObject model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }

  @Override
  public final boolean delete(SimpleCacheObject model, DatabaseWrapper wrapper) {
    cacheAdapter.removeModelFromCache(model);
    boolean successful = super.delete(model, wrapper);
    return successful;
  }

  @Override
  public final long deleteAll(Collection<SimpleCacheObject> models,
      DatabaseWrapper wrapper) {
    cacheAdapter.removeModelsFromCache(models);
    long successful = super.deleteAll(models, wrapper);
    return successful;
  }

  @Override
  public final boolean save(SimpleCacheObject model, DatabaseWrapper wrapper) {
    boolean successful = super.save(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return successful;
  }

  @Override
  public final long saveAll(Collection<SimpleCacheObject> models,
      DatabaseWrapper wrapper) {
    long count = super.saveAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }

  @Override
  public final long insert(SimpleCacheObject model, DatabaseWrapper wrapper) {
    long rowId = super.insert(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return rowId;
  }

  @Override
  public final long insertAll(Collection<SimpleCacheObject> models,
      DatabaseWrapper wrapper) {
    long count = super.insertAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }

  @Override
  public final boolean update(SimpleCacheObject model, DatabaseWrapper wrapper) {
    boolean successful = super.update(model, wrapper);
    cacheAdapter.storeModelInCache(model);
    return successful;
  }

  @Override
  public final long updateAll(Collection<SimpleCacheObject> models,
      DatabaseWrapper wrapper) {
    long count = super.updateAll(models, wrapper);
    cacheAdapter.storeModelsInCache(models);
    return count;
  }
}
