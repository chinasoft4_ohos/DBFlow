package com.dbflow5.models;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.Index;
import com.dbflow5.annotation.IndexGroup;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import java.util.*;

/**
 * Description:
 */
@Table(database = TestDatabase.class,
    indexGroups = {@IndexGroup(number = 1, name = "firstIndex"),
            @IndexGroup(number = 2, name = "secondIndex"),
            @IndexGroup(number = 3, name = "thirdIndex")}
    )
public class IndexModel {
    @Index(indexGroups = {1, 2, 3})
    @PrimaryKey
    public int id;

    @Index(indexGroups = {1})
    @Column
    public String first_name;

    @Index(indexGroups = {2})
    @Column
    public String last_name;

    @Index(indexGroups = {3})
    @Column
    public Date created_date;

    @Index(indexGroups = {2, 3})
    @Column
    public boolean isPro;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public boolean isPro() {
        return isPro;
    }

    public void setPro(boolean pro) {
        isPro = pro;
    }

    public IndexModel(int id, String first_name, String last_name, Date created_date, boolean isPro) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.created_date = created_date;
        this.isPro = isPro;
    }
}