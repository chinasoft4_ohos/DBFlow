package com.dbflow5.models;

import com.dbflow5.adapter.RetrievalAdapter;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ModelViews.AuthorName;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class AuthorName_QueryTable extends RetrievalAdapter<AuthorName> {
  public static final Property<String> name = new Property<String>(AuthorName.class, "name");

  public static final Property<Integer> age = new Property<Integer>(AuthorName.class, "age");

  public AuthorName_QueryTable(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<AuthorName> table() {
    return AuthorName.class;
  }

  @Override
  public final AuthorName loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    AuthorName model = new AuthorName("", 0);
    model.setName(cursor.getStringOrDefault("name", ""));
    model.setAge(cursor.getIntOrDefault("age"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(AuthorName model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    clause.and(age.eq(model.getAge()));
    return clause;
  }
}
