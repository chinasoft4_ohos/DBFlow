package com.dbflow5.models;

import com.dbflow5.adapter.ModelViewAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ModelViews.AuthorView;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class AuthorView_ViewTable extends ModelViewAdapter<AuthorView> {
  public static final String VIEW_NAME = "AuthorView";

  public static final Property<Integer> authorId = new Property<Integer>(AuthorView.class, "authorId");

  public static final Property<String> authorName = new Property<String>(AuthorView.class, "authorName");

  /**
   * Column Mapped Field */
  public static final Property<String> name = new Property<String>(AuthorView.class, "name");

  /**
   * Column Mapped Field */
  public static final Property<Integer> age = new Property<Integer>(AuthorView.class, "age");

  public AuthorView_ViewTable(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<AuthorView> table() {
    return AuthorView.class;
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE VIEW IF NOT EXISTS AuthorView AS " + AuthorView.getQuery().getQuery();
  }

  @Override
  public final String getName() {
    return "AuthorView";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.View;
  }

  @Override
  public final AuthorView loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    AuthorView model = new AuthorView(0,"",null);
    model.setAuthorId(cursor.getIntOrDefault("authorId"));
    model.setAuthorName(cursor.getStringOrDefault("authorName", ""));
    int index_name_AuthorName_QueryTable = cursor.getColumnIndexForName("name");
    int index_age_AuthorName_QueryTable = cursor.getColumnIndexForName("age");
    if (index_name_AuthorName_QueryTable != -1 && !cursor.isColumnNull(index_name_AuthorName_QueryTable) && index_age_AuthorName_QueryTable != -1 && !cursor.isColumnNull(index_age_AuthorName_QueryTable)) {
      model.setAuthor(new ModelViews.AuthorName("", 0));
      model.getAuthor().setName(cursor.getString(index_name_AuthorName_QueryTable));
      model.getAuthor().setAge(cursor.getInt(index_age_AuthorName_QueryTable));
    } else {
      model.setAuthor(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(AuthorView model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(authorId.eq(model.getAuthorId()));
    clause.and(authorName.eq(model.getAuthorName()));
    if (model.getAuthor() != null) {
      clause.and(name.eq(model.getAuthor().getName()));
      clause.and(age.eq(model.getAuthor().getAge()));
    } else {
      clause.and(name.eq((com.dbflow5.query.IConditional) null));
      clause.and(age.eq((com.dbflow5.query.IConditional) null));
    }
    return clause;
  }
}
