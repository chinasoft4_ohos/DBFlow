package com.dbflow5.models;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.models.ForeignKeyModels.BlogStubbed;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class BlogStubbed_Table extends ModelAdapter<BlogStubbed> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Integer> id = new Property<Integer>(BlogStubbed.class, "id");

  public static final Property<String> name = new Property<String>(BlogStubbed.class, "name");

  /**
   * Foreign Key */
  public static final Property<Integer> author_id = new Property<Integer>(BlogStubbed.class, "author_id");

  public static final Property<String> setter = new Property<String>(BlogStubbed.class, "setter");

  public static final Property<String> getter = new Property<String>(BlogStubbed.class, "getter");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name,author_id,setter,getter};

  public BlogStubbed_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<BlogStubbed> table() {
    return BlogStubbed.class;
  }

  @Override
  public final String getName() {
    return "BlogStubbed";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      case "author_id": {
        return author_id;
      }
      case "setter":  {
        return setter;
      }
      case "getter":  {
        return getter;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(BlogStubbed model, Number id) {
    model.setId(id.intValue());
  }

  @Override
  public final void saveForeignKeys(BlogStubbed model, DatabaseWrapper wrapper) {
    if (model.getAuthor() != null) {
      FlowManager.getModelAdapter(ForeignKeyModels.Author.class).save(model.getAuthor(), wrapper);
    }
  }

  @Override
  public final void deleteForeignKeys(BlogStubbed model, DatabaseWrapper wrapper) {
    if (model.getAuthor() != null) {
      FlowManager.getModelAdapter(ForeignKeyModels.Author.class).delete(model.getAuthor(), wrapper);
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, BlogStubbed model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getAuthor() != null) {
      statement.bindLong(3, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(3);
    }
    if (model.getSetter() != null) {
      statement.bindString(4, model.getSetter());
    } else {
      statement.bindString(4, "");
    }
    if (model.getGetter() != null) {
      statement.bindString(5, model.getGetter());
    } else {
      statement.bindString(5, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, BlogStubbed model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    if (model.getAuthor() != null) {
      statement.bindLong(3, (long)model.getAuthor().getId());
    } else {
      statement.bindNull(3);
    }
    if (model.getSetter() != null) {
      statement.bindString(4, model.getSetter());
    } else {
      statement.bindString(4, "");
    }
    if (model.getGetter() != null) {
      statement.bindString(5, model.getGetter());
    } else {
      statement.bindString(5, "");
    }
    statement.bindLong(6, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, BlogStubbed model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO BlogStubbed(id,name,author_id,setter,getter) VALUES (nullif(?, 0),?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO BlogStubbed(id,name,author_id,setter,getter) VALUES (nullif(?, 0),?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE BlogStubbed SET id=?,name=?,author_id=?,setter=?,getter=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM BlogStubbed WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS BlogStubbed(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, author_id INTEGER, setter TEXT, getter TEXT, FOREIGN KEY(author_id) REFERENCES Author (id) ON UPDATE RESTRICT ON DELETE CASCADE)";
  }

  @Override
  public final BlogStubbed loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    BlogStubbed model = new BlogStubbed(0,"",null,"","");
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name", ""));
    int index_author_id_Author_Table = cursor.getColumnIndexForName("author_id");
    if (index_author_id_Author_Table != -1 && !cursor.isColumnNull(index_author_id_Author_Table)) {
      model.setAuthor(new ForeignKeyModels.Author(0,"",""));
      model.getAuthor().setId(cursor.getInt(index_author_id_Author_Table));
    } else {
      model.setAuthor(null);
    }
    model.setSetter(cursor.getStringOrDefault("setter", ""));
    model.setGetter(cursor.getStringOrDefault("getter", ""));
    model.onLoadFromCursor(cursor);
    return model;
  }

  @Override
  public final boolean exists(BlogStubbed model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(BlogStubbed.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(BlogStubbed model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
