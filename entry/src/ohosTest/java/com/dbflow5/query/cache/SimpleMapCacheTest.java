package com.dbflow5.query.cache;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels.SimpleModel;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleMapCacheTest extends BaseUnitTest {
    @Test
    public void validateCacheAddRemove() {
        SimpleMapCache<SimpleModel> cache = new SimpleMapCache<>(10);
        cache.addModel("1", new SimpleModel("1"));

        assertEquals("1", cache.get("1").name);
        assertEquals(1, cache.cache.size());

        cache.removeModel("1");

        Assert.assertTrue(cache.cache.isEmpty());
    }

    @Test
    public void validateCacheClear() {
        SimpleMapCache<SimpleModel> cache = new SimpleMapCache<>(10);
        cache.addModel("1", new SimpleModel("1"));
        cache.addModel("2", new SimpleModel("2"));

        assertEquals(2, cache.cache.size());

        cache.clear();

        Assert.assertTrue(cache.cache.isEmpty());
    }
}
