package com.dbflow5.query.cache;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels.NumberModel;

import org.junit.Assert;
import org.junit.Test;

public class ModelLruCacheTest extends BaseUnitTest {
    @Test
    public void validateCacheAddRemove() {
        SimpleMapCache<NumberModel> cache = new SimpleMapCache<>(10);
        cache.addModel(1, new NumberModel(1));

        Assert.assertEquals(1, cache.get(1).id);
        Assert.assertEquals(1, cache.cache.size());

        cache.removeModel(1);

        Assert.assertTrue(cache.cache.isEmpty());

    }

    @Test
    public void validateCacheClear() {
        SimpleMapCache<NumberModel> cache = new SimpleMapCache<>(10);
        cache.addModel(1, new NumberModel(1));
        cache.addModel(2, new NumberModel(2));

        Assert.assertEquals(2, cache.cache.size());

        cache.clear();

        Assert.assertTrue(cache.cache.isEmpty());
    }
}
