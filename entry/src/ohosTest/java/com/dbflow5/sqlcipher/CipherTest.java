package com.dbflow5.sqlcipher;

import com.dbflow5.DBFlowInstrumentedTestRule;
import com.dbflow5.DemoApp;
import com.dbflow5.config.FlowManager;
import com.dbflow5.query.SQLite;
import com.dbflow5.structure.Model;
import ohos.aafwk.ability.DataAbilityRemoteException;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CipherTest {
    @Rule
    public DBFlowInstrumentedTestRule dblflowTestRule;
    public CipherTest() {
        dblflowTestRule = DBFlowInstrumentedTestRule.create(builder -> {
            builder.database(CipherDatabase.class, builder12 -> null, SQLCipherOpenHelper.createHelperCreator(DemoApp.context, "dbflow-rules"));
            return null;
        });
    }

    @Test
    public void testCipherModel() {
        FlowManager.database(CipherDatabase.class, cipherDatabase -> {
            SQLite.delete().from(CipherTestObjects.CipherModel.class).execute(cipherDatabase);
            CipherTestObjects.CipherModel model = new CipherTestObjects.CipherModel(0L, "name");
            Model.save(CipherTestObjects.CipherModel.class, model, cipherDatabase);
            try {
                assertTrue(model.exists(cipherDatabase));
            } catch (DataAbilityRemoteException e) {
                e.printStackTrace();
            }

            CipherTestObjects.CipherModel retrieval = SQLite.select().from(CipherTestObjects.CipherModel.class).where(CipherModel_Table.name.eq("name")).querySingle(cipherDatabase);
            assertEquals(retrieval.id, model.id);
            SQLite.delete().from(CipherTestObjects.CipherModel.class).execute(cipherDatabase);
            return null;
        });
    }
}
