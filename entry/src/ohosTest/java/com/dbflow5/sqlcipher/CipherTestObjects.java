package com.dbflow5.sqlcipher;

import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseCallback;
import com.dbflow5.structure.BaseModel;
import ohos.app.Context;

public class CipherTestObjects {

    public static class SQLCipherOpenHelperImpl extends SQLCipherOpenHelper {
        public Context context;
        public DBFlowDatabase databaseDefinition;
        public DatabaseCallback callback;

        public SQLCipherOpenHelperImpl(Context context, DBFlowDatabase databaseDefinition, DatabaseCallback listener, Context context1, DBFlowDatabase databaseDefinition1, DatabaseCallback callback) {
            super(context, databaseDefinition, listener);
            this.context = context1;
            this.databaseDefinition = databaseDefinition1;
            this.callback = callback;
        }

        @Override
        protected String cipherSecret() {
            return "dbflow-rules";
        }
    }

    @Table(database = CipherDatabase.class)
    public static class CipherModel extends BaseModel {
        @PrimaryKey
        public long id = 0;
        @Column
        public String name;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public CipherModel(long id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
