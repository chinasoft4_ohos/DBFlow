package com.dbflow5.sqlcipher;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.sqlcipher.CipherTestObjects.CipherModel;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class CipherModel_Table extends ModelAdapter<CipherModel> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> id = new Property<Long>(CipherModel.class, "id");

  public static final Property<String> name = new Property<String>(CipherModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name};

  public CipherModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<CipherModel> table() {
    return CipherModel.class;
  }

  @Override
  public final String getName() {
    return "CipherModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(CipherModel model, Number id) {
    model.setId(id.longValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, CipherModel model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, CipherModel model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
    statement.bindLong(3, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, CipherModel model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO CipherModel(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO CipherModel(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE CipherModel SET id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM CipherModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS CipherModel(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)";
  }

  @Override
  public final CipherModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    CipherModel model = new CipherModel(0, null);
    model.setId(cursor.getLongOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name"));
    return model;
  }

  @Override
  public final boolean exists(CipherModel model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(CipherModel.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(CipherModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
