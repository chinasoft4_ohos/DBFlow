package com.dbflow5;

import com.dbflow5.annotation.Database;
import com.dbflow5.annotation.ForeignKey;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.config.DBFlowDatabase;

@Database(version = 1, foreignKeyConstraintsEnforced = true)
public abstract class TestForeignKeyDatabase extends DBFlowDatabase {
    @Table(database = TestForeignKeyDatabase.class)
    public static class SimpleModel{
        @PrimaryKey
        public String name = "";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public SimpleModel(String name) {
            this.name = name;
        }
    }

    @Table(database = TestForeignKeyDatabase.class)
    public static class SimpleForeignModel{
        @PrimaryKey
        public int id;
        @ForeignKey
        public SimpleModel model;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public SimpleModel getModel() {
            return model;
        }

        public void setModel(SimpleModel model) {
            this.model = model;
        }

        public SimpleForeignModel(int id, SimpleModel model) {
            this.id = id;
            this.model = model;
        }
    }
}
