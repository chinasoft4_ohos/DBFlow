package com.dbflow5;

import com.dbflow5.annotation.Database;
import com.dbflow5.annotation.Migration;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.migration.BaseMigration;
import com.dbflow5.migration.UpdateTableMigration;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels;

@Database(version = 1)
public abstract class TestDatabase extends DBFlowDatabase {
    @Migration(version = 1, database = TestDatabase.class, priority = 5)
    public static class TestMigration extends UpdateTableMigration<SimpleTestModels.SimpleModel> {
        public void onPreMigrate() {
            super.onPreMigrate();
            set(SimpleModel_Table.name.eq("Test")).where(SimpleModel_Table.name.eq("Test1"));
        }

        public TestMigration() {
            super(SimpleTestModels.SimpleModel.class);
        }

    }

    @Migration(version = 1, database = TestDatabase.class, priority = 1)
    public static class SecondMigration extends BaseMigration {
        public void migrate(DatabaseWrapper database) {

        }
    }
}
