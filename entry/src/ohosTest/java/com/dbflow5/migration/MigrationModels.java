package com.dbflow5.migration;

import com.dbflow5.TestDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.annotation.Migration;
import com.dbflow5.migration.BaseMigration;

public class MigrationModels{
    @Migration(database = TestDatabase.class, priority = 1, version = 1)
    public static class FirstMigration extends BaseMigration {
        @Override
        public void migrate(DatabaseWrapper database) {

        }
    }

    @Migration(database = TestDatabase.class, priority = 2, version = 1)
    public static class SecondMigration extends BaseMigration{
        @Override
        public void migrate(DatabaseWrapper database) {

        }
    }
}
