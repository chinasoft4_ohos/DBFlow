package com.dbflow5.migration;

import com.dbflow5.config.FlowManager;
import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels;
import org.junit.Test;

public class UpdateTableMigrationTest extends BaseUnitTest {
   @Test
   public void testUpdateMigrationQuery(){
       UpdateTableMigration<SimpleTestModels.SimpleModel> update=new UpdateTableMigration<>(SimpleTestModels.SimpleModel.class);
       update.set(SimpleModel_Table.name.eq("yes"));
       update.migrate(FlowManager.databaseForTable(SimpleTestModels.SimpleModel.class, dbFlowDatabase -> null));
   }
}
