package com.dbflow5;

import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class SimpleForeignModel_Table extends ModelAdapter<TestForeignKeyDatabase.SimpleForeignModel> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(TestForeignKeyDatabase.SimpleForeignModel.class, "id");

  /**
   * Foreign Key */
  public static final Property<String> model_name = new Property<String>(TestForeignKeyDatabase.SimpleForeignModel.class, "model_name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,model_name};

  public SimpleForeignModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TestForeignKeyDatabase.SimpleForeignModel> table() {
    return TestForeignKeyDatabase.SimpleForeignModel.class;
  }

  @Override
  public final String getName() {
    return "SimpleForeignModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "model_name": {
        return model_name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      TestForeignKeyDatabase.SimpleForeignModel model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getModel() != null) {
      if (model.getModel().getName() != null) {
        statement.bindString(2, model.getModel().getName());
      } else {
        statement.bindString(2, "");
      }
    } else {
      statement.bindNull(2);
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      TestForeignKeyDatabase.SimpleForeignModel model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getModel() != null) {
      if (model.getModel().getName() != null) {
        statement.bindString(2, model.getModel().getName());
      } else {
        statement.bindString(2, "");
      }
    } else {
      statement.bindNull(2);
    }
    statement.bindLong(3, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      TestForeignKeyDatabase.SimpleForeignModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO SimpleForeignModel(id,model_name) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO SimpleForeignModel(id,model_name) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE SimpleForeignModel SET id=?,model_name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM SimpleForeignModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS SimpleForeignModel(id INTEGER, model_name TEXT, PRIMARY KEY(id), FOREIGN KEY(model_name) REFERENCES SimpleModel (name) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final TestForeignKeyDatabase.SimpleForeignModel loadFromCursor(FlowCursor cursor,
      DatabaseWrapper wrapper) {
    TestForeignKeyDatabase.SimpleForeignModel model = new TestForeignKeyDatabase.SimpleForeignModel(0, null);
    model.setId(cursor.getIntOrDefault("id"));
    int index_model_name_SimpleModel_Table = cursor.getColumnIndexForName("model_name");
    if (index_model_name_SimpleModel_Table != -1 && !cursor.isColumnNull(index_model_name_SimpleModel_Table)) {
      model.setModel(com.dbflow5.query.SQLite.select().from(TestForeignKeyDatabase.SimpleModel.class).where()
          .and(SimpleModel_Table.name.eq(cursor.getString(index_model_name_SimpleModel_Table)))
          .querySingle(wrapper));
    } else {
      model.setModel(null);
    }
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(
      TestForeignKeyDatabase.SimpleForeignModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
