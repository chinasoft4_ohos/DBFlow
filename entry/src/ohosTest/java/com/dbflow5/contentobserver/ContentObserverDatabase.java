package com.dbflow5.contentobserver;

import com.dbflow5.annotation.Database;
import com.dbflow5.config.DBFlowDatabase;


@Database(version = 1)
public abstract class ContentObserverDatabase extends DBFlowDatabase {
    public ContentObserverDatabase() {
        super();
    }
}
