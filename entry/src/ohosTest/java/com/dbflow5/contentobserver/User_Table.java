package com.dbflow5.contentobserver;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class User_Table extends ModelAdapter<User> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(User.class, "id");

  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(User.class, "name");

  public static final Property<Integer> age = new Property<Integer>(User.class, "age");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name,age};

  public User_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<User> table() {
    return User.class;
  }

  @Override
  public final String getName() {
    return "User";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      case "age":  {
        return age;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, User model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    statement.bindLong(3, (long)model.getAge());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, User model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
    statement.bindLong(3, (long)model.getAge());
    statement.bindLong(4, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(5, model.getName());
    } else {
      statement.bindString(5, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, User model) {
    statement.bindLong(1, (long)model.getId());
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO User(id,name,age) VALUES (?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO User(id,name,age) VALUES (?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE User SET id=?,name=?,age=? WHERE id=? AND name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM User WHERE id=? AND name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS User(id INTEGER, name TEXT, age INTEGER, PRIMARY KEY(id, name))";
  }

  @Override
  public final User loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    User model = new User(0, "", 0);
    model.setId(cursor.getIntOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name", ""));
    model.setAge(cursor.getIntOrDefault("age"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(User model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
