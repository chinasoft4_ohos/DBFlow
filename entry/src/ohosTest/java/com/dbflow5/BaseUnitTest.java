package com.dbflow5;

import decc.testkit.runner.HarmonyJUnitClassRunner;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Rule;
import org.junit.runner.RunWith;

@RunWith(HarmonyJUnitClassRunner.class)
public abstract class BaseUnitTest {
    @Rule
    public DBFlowInstrumentedTestRule dblflowTestRule;

    public BaseUnitTest() {
        dblflowTestRule = DBFlowInstrumentedTestRule.create(builder -> null);
    }

    public final Context getContext() {
        return AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
    }

}
