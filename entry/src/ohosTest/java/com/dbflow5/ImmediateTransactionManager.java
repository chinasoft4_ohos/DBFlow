package com.dbflow5;

import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.transaction.BaseTransactionManager;
import com.dbflow5.transaction.ITransactionQueue;
import com.dbflow5.transaction.Transaction;

public class ImmediateTransactionManager extends BaseTransactionManager {
    public ImmediateTransactionManager(DBFlowDatabase databaseDefinition) {
        super(new ImmediateTransactionQueue(), databaseDefinition);
    }

    static class ImmediateTransactionQueue implements ITransactionQueue{
        public void add(Transaction<?> transaction) {
            transaction.newBuilder().runCallbacksOnSameThread(true).build().executeSync();
        }

        public void cancel(Transaction<?> transaction) {
        }

        public void startIfNotAlive() {
        }

        public void cancel(String name) {
        }

        public void quit() {
        }
    }
}

