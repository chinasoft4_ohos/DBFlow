package com.dbflow5;

import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;

public final class SimpleModel_Table extends ModelAdapter<TestForeignKeyDatabase.SimpleModel> {
  /**
   * Primary Key */
  public static final Property<String> name = new Property<String>(TestForeignKeyDatabase.SimpleModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{name};

  public SimpleModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TestForeignKeyDatabase.SimpleModel> table() {
    return TestForeignKeyDatabase.SimpleModel.class;
  }

  @Override
  public final String getName() {
    return "SimpleModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement,
      TestForeignKeyDatabase.SimpleModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement,
      TestForeignKeyDatabase.SimpleModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
    if (model.getName() != null) {
      statement.bindString(2, model.getName());
    } else {
      statement.bindString(2, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement,
      TestForeignKeyDatabase.SimpleModel model) {
    if (model.getName() != null) {
      statement.bindString(1, model.getName());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO SimpleModel(name) VALUES (?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO SimpleModel(name) VALUES (?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE SimpleModel SET name=? WHERE name=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM SimpleModel WHERE name=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS SimpleModel(name TEXT, PRIMARY KEY(name))";
  }

  @Override
  public final TestForeignKeyDatabase.SimpleModel loadFromCursor(FlowCursor cursor,
      DatabaseWrapper wrapper) {
    TestForeignKeyDatabase.SimpleModel model = new TestForeignKeyDatabase.SimpleModel("");
    model.setName(cursor.getStringOrDefault("name", ""));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(TestForeignKeyDatabase.SimpleModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(name.eq(model.getName()));
    return clause;
  }
}
