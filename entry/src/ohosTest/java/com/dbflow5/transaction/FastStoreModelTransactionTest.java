package com.dbflow5.transaction;

import com.dbflow5.TestDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.query.SQLite;
import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FastStoreModelTransactionTest extends BaseUnitTest {
    @Test
    public void testSaveBuilder() {
        TaskDispatcher globalTaskDispatcher = getContext().getGlobalTaskDispatcher(TaskPriority.DEFAULT);
        globalTaskDispatcher.syncDispatch(() -> FlowManager.database(TestDatabase.class, db -> {
            List<SimpleTestModels.SimpleModel> models = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                SimpleTestModels.SimpleModel simpleModel = new SimpleTestModels.SimpleModel(""+i);
                models.add(simpleModel);
            }
            Transaction<Long> transaction = db.beginTransactionAsync(FastStoreModelTransaction.fastSave(SimpleTestModels.SimpleModel.class, models).build()).success((longTransaction, aLong) -> {
                assertEquals(10L, aLong.longValue());
                return null;
            }).error((longTransaction, throwable) -> null).build();
            transaction.execute();

            List<SimpleTestModels.SimpleModel> list = SQLite.select().from(SimpleTestModels.SimpleModel.class).queryList(db);
            assertEquals(10, list.size());
            return null;
        }));
    }

    @Test
    public void testInsertBuilder() {
        TaskDispatcher globalTaskDispatcher = getContext().getGlobalTaskDispatcher(TaskPriority.DEFAULT);
        globalTaskDispatcher.syncDispatch(() -> FlowManager.database(TestDatabase.class, db -> {
            List<SimpleTestModels.SimpleModel> models = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                SimpleTestModels.SimpleModel simpleModel = new SimpleTestModels.SimpleModel(""+i);
                models.add(simpleModel);
            }
            Transaction<Long> transaction = db.beginTransactionAsync(FastStoreModelTransaction.fastInsert(SimpleTestModels.SimpleModel.class, models).build()).success((longTransaction, aLong) -> {
                assertEquals(10L, aLong.longValue());
                return null;
            }).error((longTransaction, throwable) -> null).build();
            transaction.execute();

            List<SimpleTestModels.SimpleModel> list = SQLite.select().from(SimpleTestModels.SimpleModel.class).queryList(db);
            assertEquals(10, list.size());
            return null;
        }));
    }

    @Test
    public void testUpdateBuilder() {
        TaskDispatcher globalTaskDispatcher = getContext().getGlobalTaskDispatcher(TaskPriority.DEFAULT);
        globalTaskDispatcher.syncDispatch(() -> FlowManager.database(TestDatabase.class, db -> {
            List<SimpleTestModels.TwoColumnModel> oldList = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                SimpleTestModels.TwoColumnModel twoColumnModel=new SimpleTestModels.TwoColumnModel(""+i, new Random().nextInt());
                oldList.add(twoColumnModel);
            }
            Transaction<Long> transaction = db.beginTransactionAsync(FastStoreModelTransaction.fastInsert(SimpleTestModels.TwoColumnModel.class, oldList).build()).build();
            transaction.execute();

            List<SimpleTestModels.TwoColumnModel> models = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                SimpleTestModels.TwoColumnModel twoColumnModel=new SimpleTestModels.TwoColumnModel(""+i, new Random().nextInt());
                models.add(twoColumnModel);
            }
            Transaction<Long> transaction2 = db.beginTransactionAsync(FastStoreModelTransaction.fastUpdate(SimpleTestModels.TwoColumnModel.class, models).build()).build();
            transaction2.execute();

            List<SimpleTestModels.TwoColumnModel> list = SQLite.select().from(SimpleTestModels.TwoColumnModel.class).queryList(db);
            assertEquals(10, list.size());

            for(int index =0;index<list.size();index++) {
                SimpleTestModels.TwoColumnModel model = list.get(index);
                assertNotEquals(model, oldList.get(index));
            }
            return null;
        }));
    }
}
