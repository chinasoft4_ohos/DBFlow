package com.dbflow5;

import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class User_Table extends ModelAdapter<User> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(User.class, "id");

  public static final Property<String> firstName = new Property<String>(User.class, "firstName");

  public static final Property<String> lastName = new Property<String>(User.class, "lastName");

  public static final Property<String> email = new Property<String>(User.class, "email");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,firstName,lastName,email};

  public User_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<User> table() {
    return User.class;
  }

  @Override
  public final String getName() {
    return "User2";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "firstName":  {
        return firstName;
      }
      case "lastName":  {
        return lastName;
      }
      case "email":  {
        return email;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, User model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getFirstName());
    statement.bindStringOrNull(3, model.getLastName());
    statement.bindStringOrNull(4, model.getEmail());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, User model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getFirstName());
    statement.bindStringOrNull(3, model.getLastName());
    statement.bindStringOrNull(4, model.getEmail());
    statement.bindLong(5, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, User model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO User2(id,firstName,lastName,email) VALUES (?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO User2(id,firstName,lastName,email) VALUES (?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE User2 SET id=?,firstName=?,lastName=?,email=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM User2 WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS User2(id INTEGER, firstName TEXT, lastName TEXT, email TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final User loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    User model = new User(0, null, null, null);
    model.setId(cursor.getIntOrDefault("id"));
    model.setFirstName(cursor.getStringOrDefault("firstName"));
    model.setLastName(cursor.getStringOrDefault("lastName"));
    model.setEmail(cursor.getStringOrDefault("email"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(User model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
