package com.dbflow5.prepackaged;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.prepackaged.PrepackagedDB.Dog2;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;

public final class Dog2_Table extends ModelAdapter<Dog2> {
  /**
   * Primary Key */
  public static final Property<Integer> id = new Property<Integer>(Dog2.class, "id");

  public static final Property<String> breed = new Property<String>(Dog2.class, "breed");

  public static final Property<String> color = new Property<String>(Dog2.class, "color");

  public static final Property<String> newField = new Property<String>(Dog2.class, "newField");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,breed,color,newField};

  public Dog2_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<Dog2> table() {
    return Dog2.class;
  }

  @Override
  public final String getName() {
    return "Dog";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "breed":  {
        return breed;
      }
      case "color":  {
        return color;
      }
      case "newField":  {
        return newField;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, Dog2 model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getBreed());
    statement.bindStringOrNull(3, model.getColor());
    statement.bindStringOrNull(4, model.getNewField());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, Dog2 model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getBreed());
    statement.bindStringOrNull(3, model.getColor());
    statement.bindStringOrNull(4, model.getNewField());
    statement.bindLong(5, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, Dog2 model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO Dog(id,breed,color,newField) VALUES (?,?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO Dog(id,breed,color,newField) VALUES (?,?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE Dog SET id=?,breed=?,color=?,newField=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM Dog WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS Dog(id INTEGER, breed TEXT, color TEXT, newField TEXT, PRIMARY KEY(id))";
  }

  @Override
  public final Dog2 loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    Dog2 model = new Dog2(0,null,null,null);
    model.setId(cursor.getIntOrDefault("id"));
    model.setBreed(cursor.getStringOrDefault("breed"));
    model.setColor(cursor.getStringOrDefault("color"));
    model.setNewField(cursor.getStringOrDefault("newField"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(Dog2 model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
