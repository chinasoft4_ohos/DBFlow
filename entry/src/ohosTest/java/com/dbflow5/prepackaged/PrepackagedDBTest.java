package com.dbflow5.prepackaged;

import com.dbflow5.DBFlowInstrumentedTestRule;
import com.dbflow5.DemoApp;
import com.dbflow5.config.FlowManager;
import com.dbflow5.database.OhosSQLiteOpenHelper;
import com.dbflow5.query.SQLite;

import org.junit.Rule;
import org.junit.Test;

import com.dbflow5.prepackaged.PrepackagedDB.Dog2;

import java.util.List;

import static org.junit.Assert.*;

public class PrepackagedDBTest {
    @Rule
    public DBFlowInstrumentedTestRule dblflowTestRule;

    public PrepackagedDBTest() {
        this.dblflowTestRule = DBFlowInstrumentedTestRule.create(builder -> {
            builder.database(PrepackagedDB.class, builder1 -> {
                builder1.databaseName("prepackaged");
                return null;
            }, OhosSQLiteOpenHelper.createHelperCreator(DemoApp.context));

            builder.database(PrepackagedDB.MigratedPrepackagedDB.class, builder12 -> {
                builder12.databaseName("prepackaged_2");
                return null;
            }, OhosSQLiteOpenHelper.createHelperCreator(DemoApp.context));
            return null;
        });
    }

    @Test
    public void assertWeCanLoadFromDB() {
        FlowManager.database(PrepackagedDB.class, db -> {
            List<PrepackagedDB.Dog> list = SQLite.select().from(PrepackagedDB.Dog.class).queryList(db);
            assertFalse(list.isEmpty());
            return null;
        });
    }

    @Test
    public void assertWeCanLoadFromDBPostMigrate(){
        FlowManager.database(PrepackagedDB.MigratedPrepackagedDB.class, db -> {
            List<Dog2> list = SQLite.select().from(Dog2.class).queryList(db);
            assertFalse(list.isEmpty());

            Dog2 newData = SQLite.select().from(Dog2.class).
                    where(Dog2_Table.breed.eq("NewBreed")).
                    and(Dog2_Table.newField.eq("New Field Data")).
                    querySingle(db);
            assertNotNull(newData);
            return null;
        });
    }
}
