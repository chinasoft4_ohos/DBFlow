package com.dbflow5.prepackaged;

import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.Database;
import com.dbflow5.annotation.Migration;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.migration.AlterTableMigration;
import com.dbflow5.migration.BaseMigration;
import com.dbflow5.sql.SQLiteType;
import com.dbflow5.structure.BaseModel;

import ohos.aafwk.ability.DataAbilityRemoteException;

@Database(version = 1)
public abstract class PrepackagedDB extends DBFlowDatabase{

    @Database(version = 2)
    public abstract static class MigratedPrepackagedDB extends DBFlowDatabase{
        @Migration(version = 2, database = MigratedPrepackagedDB.class, priority = 1)
        public static class AddNewFieldMigration extends AlterTableMigration<Dog2>{
            public AddNewFieldMigration() {
                super(Dog2.class);
            }
            @Override
            public void onPreMigrate() {
                addColumn(SQLiteType.TEXT, "newField", null);
            }
        }
        @Migration(version = 2, database = MigratedPrepackagedDB.class, priority = 2)
        static
        public class AddSomeDataMigration extends BaseMigration{
            @Override
            public void migrate(DatabaseWrapper database) throws DataAbilityRemoteException {
                new Dog2("NewBreed","New Field Data").insert(database);
            }
        }
    }

    @Table(database = PrepackagedDB.class, allFields = true)
    public static class Dog extends BaseModel {
        @PrimaryKey
        public int id;
        @Column
        public String breed;
        @Column
        public String color;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public Dog(int id, String breed, String color) {
            this.id = id;
            this.breed = breed;
            this.color = color;
        }

        public Dog(String breed, String color) {
            this.breed = breed;
            this.color = color;
        }
    }

    @Table(database = MigratedPrepackagedDB.class, allFields = true, name = "Dog")
    static class Dog2 extends BaseModel {
        @PrimaryKey
        public int id;
        @Column
        public String breed;
        @Column
        public String color;
        @Column
        public String newField;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getNewField() {
            return newField;
        }

        public void setNewField(String newField) {
            this.newField = newField;
        }

        public Dog2(int id, String breed, String color, String newField) {
            this.id = id;
            this.breed = breed;
            this.color = color;
            this.newField = newField;
        }

        public Dog2(String breed, String newField) {
            this.breed = breed;
            this.newField = newField;
        }

        @Override
        public String toString() {
            return "Dog2{" +
                    "id=" + id +
                    ", breed='" + breed + '\'' +
                    ", color='" + color + '\'' +
                    ", newField='" + newField + '\'' +
                    '}';
        }
    }

}
