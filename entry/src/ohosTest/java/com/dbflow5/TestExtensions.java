package com.dbflow5;

import com.dbflow5.sql.Query;

import org.junit.Assert;

import java.util.function.Function;

import static org.junit.Assert.fail;

public class TestExtensions {
    public static void assertEquals(String string, Query query) {
        Assert.assertEquals(string, query.getQuery().trim());
    }

    public static void assertEquals(Query query, Query actual) {
        Assert.assertEquals(query, actual.getQuery().trim());
    }

    public static void assertThrowsException(Class<? extends Exception> expectedException, Function<Void, Void> function) {
        try {
            if (function != null) {
                function.apply(null);
                fail("Expected call to fail. Unexpectedly passed");
            }
        } catch (Exception e) {
            if (e.getClass() != expectedException) {
                e.printStackTrace();
                fail("Expected "+expectedException+" but got " + e.getClass());
            }
        }
    }
}
