package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.models.TwoColumnModel_Table;
import com.dbflow5.query.SQLite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetTest extends BaseUnitTest {
    @Test
    public void validateSetWithConditions() {
        assertEquals("UPDATE SimpleModel SET name='name'", SQLite.update(SimpleTestModels.SimpleModel.class).set(SimpleModel_Table.name.is("name")).getQuery().trim());
    }

    @Test
    public void validateMultipleConditions() {
        assertEquals("UPDATE SimpleModel SET name='name', id=0", SQLite.update(SimpleTestModels.SimpleModel.class).set(SimpleModel_Table.name.eq("name")).and(TwoColumnModel_Table.id.eq(0)).getQuery().trim());
    }
}
