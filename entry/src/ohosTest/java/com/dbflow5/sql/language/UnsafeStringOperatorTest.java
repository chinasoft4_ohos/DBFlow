package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.UnSafeStringOperator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UnsafeStringOperatorTest extends BaseUnitTest {
    @Test
    public void testCanIncludeInQuery() {
        UnSafeStringOperator op=  new UnSafeStringOperator("name = ?, id = ?, test = ?","'name'", "0", "'test'");
        assertEquals("name = 'name', id = 0, test = 'test'", op.getQuery().trim());
        assertEquals("SELECT * FROM SimpleModel WHERE name = 'name', id = 0, test = 'test'", SQLite.select().from(SimpleTestModels.SimpleModel.class).where(op).getQuery().trim());
    }
}
