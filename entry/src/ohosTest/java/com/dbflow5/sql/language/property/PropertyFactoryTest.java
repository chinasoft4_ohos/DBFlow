package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.PropertyFactory;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PropertyFactoryTest extends BaseUnitTest {
    @Test
    public void testPrimitives() {
        assertEquals("'c'", PropertyFactory.from("c").getQuery());
        assertEquals("5", PropertyFactory.from(5).getQuery());
        assertEquals("5.0", PropertyFactory.from(5.0).getQuery());
        assertEquals("5.0", PropertyFactory.from(5f).getQuery());
        assertEquals("5", PropertyFactory.from(5L).getQuery());
        assertEquals("5", PropertyFactory.from((short) 5).getQuery());
        assertEquals("5", PropertyFactory.from((byte) 5).getQuery());
        Object nullable = null;
        assertEquals("NULL", PropertyFactory.from(nullable).getQuery());
        assertEquals("(SELECT * FROM SimpleModel)", PropertyFactory.from((SQLite.select().from(SimpleTestModels.SimpleModel.class))).getQuery());
        assertEquals("SomethingCool", PropertyFactory.propertyString(String.class, "SomethingCool").getQuery());
    }
}
