package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels.SimpleModel;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.models.SimpleTestModels.TwoColumnModel;
import com.dbflow5.models.TwoColumnModel_Table;
import com.dbflow5.query.*;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.PropertyFactory;
import com.dbflow5.sql.SQLiteType;

import com.dbflow5.structure.Model;
import ohos.utils.Pair;
import org.junit.Test;

import java.util.function.Function;

import static com.dbflow5.query.SQLite.createTempTrigger;
import static com.dbflow5.query.SQLite.createTrigger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TriggerTest extends BaseUnitTest {
    @Test
    public void validateBasicTrigger() {
        assertEquals("CREATE TRIGGER IF NOT EXISTS MyTrigger AFTER INSERT ON SimpleModel " +
                "\nBEGIN" +
                "\nINSERT INTO TwoColumnModel(name) VALUES(new.name);" +
                "\nEND", createTrigger("MyTrigger").after().insertOn(SimpleTestModels.SimpleModel.class).begin(
                SQLite.insert(SimpleTestModels.TwoColumnModel.class).columnValues(new Pair<>(SimpleModel_Table.name, NameAlias.ofTable("new", "name")))
            ).getQuery().trim()
        );
    }

    @Test
    public void validateUpdateTriggerMultiline() {
        assertEquals("CREATE TEMP TRIGGER IF NOT EXISTS MyTrigger BEFORE UPDATE ON SimpleModel " +
                "\nBEGIN" +
                "\nINSERT INTO TwoColumnModel(name) VALUES(new.name);" +
                "\nINSERT INTO TwoColumnModel(id) VALUES(CAST(new.name AS INTEGER));" +
                "\nEND", createTempTrigger("MyTrigger").before().updateOn(SimpleModel.class).begin(
            SQLite.insert(TwoColumnModel.class, SimpleModel_Table.name).values(NameAlias.ofTable("new", "name"))
            ).and(SQLite.insert(TwoColumnModel.class, TwoColumnModel_Table.id).values(
            Method.cast(
                PropertyFactory.from(
                    NameAlias.ofTable("new", "name")
                )
            ).as(SQLiteType.INTEGER)
            )).getQuery()
        );
    }

    @Test
    public void validateTriggerWorks() {
        FlowManager.databaseForTable(SimpleTestModels.SimpleModel.class, db -> {
            CompletedTrigger trigger = createTrigger("MyTrigger").after().insertOn(SimpleModel.class).begin(
                    SQLite.insert(TwoColumnModel.class).columnValues(new Pair<>(SimpleModel_Table.name, NameAlias.ofTable("new", "name")))
            );

            trigger.enable(db);
            Model.insert(SimpleModel.class, new SimpleModel("Test"), db);
            Where<TwoColumnModel> result = SQLite.select().from(TwoColumnModel.class).where(SimpleModel_Table.name.eq("Test"));
            assertNotNull(result);
            return null;
        });
    }
}