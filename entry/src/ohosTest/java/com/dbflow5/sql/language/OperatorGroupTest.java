package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLOperator;

import static com.dbflow5.models.TwoColumnModel_Table.id;
import static com.dbflow5.models.TwoColumnModel_Table.name;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class OperatorGroupTest extends BaseUnitTest {
    @Test
    public void validateCommaSeparated() {
        assertEquals("(name='name', id=0)", OperatorGroup.clause().setAllCommaSeparated(true).andAll(name.eq("name"), id.eq(0)).getQuery().trim());
    }

    @Test
    public void validateParanthesis() {
        assertEquals("name='name'", OperatorGroup.nonGroupingClause(name.eq("name")).setUseParenthesis(false).getQuery().trim());
    }

    @Test
    public void validateOr() {
        assertEquals("(name='name' OR id=0)", name.eq("name").or(id.eq(0)).getQuery().trim());
    }

    @Test
    public void validateOrAll() {
        assertEquals("(name='name' OR id=0 OR name='test')", name.eq("name").orAll(Arrays.asList(id.eq(0), name.eq("test"))).getQuery().trim());
    }

    @Test
    public void validateAnd() {
        assertEquals("(name='name' AND id=0)", name.eq("name").and(id.eq(0)).getQuery().trim());
    }

    @Test
    public void validateAndAll() {
        assertEquals("(name='name' AND id=0 AND name='test')", name.eq("name").andAll(new ArrayList<SQLOperator>() {{
            add(id.eq(0));
            add(name.eq("test"));
        }}).getQuery().trim());
    }
}
