package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.query.NameAlias;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class NameAliasTest extends BaseUnitTest {
    @Test
    public void testSimpleCase() {
        assertEquals("name", NameAlias.of("name").getQuery());
    }

    @Test
    public void testAlias() {
        assertEquals("name AS alias", NameAlias.as("name", "alias").fullQuery());
    }

    @Test
    public void validateBuilder() {
        NameAlias nameAlias = NameAlias.builder("name")
            .keyword("DISTINCT")
            .as("Alias")
            .withTable("MyTable")
            .shouldAddIdentifierToAliasName(false)
            .shouldAddIdentifierToName(false)
            .shouldStripAliasName(false)
            .shouldStripIdentifier(false).build();
        assertEquals("DISTINCT", nameAlias.keyword);
        assertEquals("Alias", nameAlias.aliasName());
        assertEquals("Alias", nameAlias.aliasNameRaw());
        assertEquals("MyTable", nameAlias.tableName);
        assertFalse(nameAlias.shouldStripAliasName);
        assertFalse(nameAlias.shouldStripIdentifier);
        assertEquals("Alias", nameAlias.nameAsKey());
        assertEquals("DISTINCT MyTable.name AS Alias", nameAlias.fullQuery());
    }
}
