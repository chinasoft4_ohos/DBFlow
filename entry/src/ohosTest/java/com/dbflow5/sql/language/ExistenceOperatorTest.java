package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels.SimpleModel;
import com.dbflow5.query.ExistenceOperator;
import com.dbflow5.query.Operator;
import com.dbflow5.query.SQLite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExistenceOperatorTest extends BaseUnitTest {
    @Test
    public void validateQuery(){
        assertEquals("EXISTS (SELECT * FROM SimpleModel WHERE name='name')",
            new ExistenceOperator(
                (SQLite.select().from(SimpleModel.class).where(SimpleModel_Table.name.eq("name"))))
                .getQuery().trim());
    }
}
