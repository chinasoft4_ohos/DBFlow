package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.SimpleModel_Table;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.config.FlowManager;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.property.IndexProperty;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IndexPropertyTest extends BaseUnitTest {
    @Test
    public void validateIndexProperty() {
        FlowManager.databaseForTable(SimpleTestModels.SimpleModel.class, db -> {
            IndexProperty<SimpleTestModels.SimpleModel> prop = new IndexProperty<>("_Index", true, SimpleTestModels.SimpleModel.class, SimpleModel_Table.name);
            prop.createIfNotExists(db);
            prop.drop(db);
            assertEquals("_Index", prop.indexName);
            return null;
        });
    }
}
