package com.dbflow5.sql.language;

import static com.dbflow5.models.TwoColumnModel_Table.id;
import static com.dbflow5.models.TwoColumnModel_Table.name;
import com.dbflow5.BaseUnitTest;
import com.dbflow5.query.Method;
import com.dbflow5.sql.SQLiteType;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MethodTest extends BaseUnitTest {
    @Test
    public void testMainMethods() {
        assertEquals("AVG(name, id)", Method.avg(name, id).getQuery());
        assertEquals("COUNT(name, id)", Method.count(name, id).getQuery());
        assertEquals("GROUP_CONCAT(name, id)", Method.groupConcat(name, id).getQuery());
        assertEquals("MAX(name, id)", Method.max(name, id).getQuery());
        assertEquals("MIN(name, id)", Method.min(name, id).getQuery());
        assertEquals("SUM(name, id)", Method.sum(name, id).getQuery());
        assertEquals("TOTAL(name, id)", Method.total(name, id).getQuery());
        assertEquals("CAST(name AS INTEGER)", Method.cast(name).as(SQLiteType.INTEGER).getQuery());
        assertEquals("REPLACE(name, 'Andrew', 'Grosner')", Method.replace(name, "Andrew", "Grosner").getQuery());
    }

    @Test
    public void test_strftime() {
        assertEquals("strftime('%s', 'now')", Method.strftime("%s", "now").getQuery());
    }

    @Test
    public void test_dateMethod() {
        assertEquals("date('now', 'start of month', '+1 month')",
            Method.date("now", "start of month", "+1 month").getQuery());
    }

    @Test
    public void test_datetimeMethod() {
        assertEquals("datetime(1092941466, 'unix epoch')",
            Method.datetime(1092941466, "unix epoch").getQuery());
    }

    @Test
    public void testIfNull() {
        assertEquals("IFNULL(name, id)", Method.ifNull(name, id).getQuery());
    }

    @Test
    public void testNulllIf() {
        assertEquals("NULLIF(name, id)", Method.nullIf(name, id).getQuery());
    }

    @Test
    public void random_generates_correct_query() {
        assertEquals("RANDOM()", Method.random().getQuery());
    }

    @Test
    public void testOpMethods() {
        assertEquals("AVG(name + id)", Method.avg(name.plus(id)).getQuery());
        assertEquals("AVG(name + id)", (Method.avg(name).plus(id)).getQuery());
        assertEquals("AVG(name - id)", Method.avg(name.minus(id)).getQuery());
        assertEquals("AVG(name - id)", (Method.avg(name).minus(id)).getQuery());
        assertEquals("AVG(name / id)", Method.avg(name.div(id)).getQuery());
        assertEquals("AVG(name * id)", (Method.avg(name).times(id)).getQuery());
        assertEquals("AVG(name % id)", Method.avg(name.rem(id)).getQuery());
        assertEquals("AVG(name % id)", (Method.avg(name).rem(id)).getQuery());
    }
}
