package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.property.Property;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BytePropertyTest extends BaseUnitTest {
    @Test
    public void testOperators() {
        Property<Byte> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop");
        assertEquals("Prop=5", prop.is((byte) 5).getQuery().trim());
        assertEquals("Prop=5", prop.eq((byte) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.notEq((byte) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.isNot((byte) 5).getQuery().trim());
        assertEquals("Prop>5", prop.greaterThan((byte) 5).getQuery().trim());
        assertEquals("Prop>=5", prop.greaterThanOrEq((byte) 5).getQuery().trim());
        assertEquals("Prop<5", prop.lessThan((byte) 5).getQuery().trim());
        assertEquals("Prop<=5", prop.lessThanOrEq((byte) 5).getQuery().trim());
        assertEquals("Prop BETWEEN 5 AND 6", prop.between((byte) 5).and((byte) 6).getQuery().trim());
        assertEquals("Prop IN (5,6,7,8)", prop.in((byte) 5, (byte) 6, (byte) 7, (byte) 8).getQuery().trim());
        assertEquals("Prop NOT IN (5,6,7,8)", prop.notIn((byte) 5, (byte) 6, (byte) 7, (byte) 8).getQuery().trim());
        assertEquals("Prop=Prop + 5", prop.concatenate((byte) 5).getQuery().trim());
    }

    @Test
    public void testAlias() {
        Property<Byte> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop", "Alias");
        assertEquals("Prop AS Alias", prop.toString().trim());
        Property<Byte> prop2 = new Property<>(SimpleTestModels.SimpleModel.class,
            NameAlias.builder("Prop").shouldAddIdentifierToName(false)
                .as("Alias")
                .shouldAddIdentifierToAliasName(false).build()
        );
        assertEquals("Prop AS Alias", prop2.toString().trim());
    }
}
