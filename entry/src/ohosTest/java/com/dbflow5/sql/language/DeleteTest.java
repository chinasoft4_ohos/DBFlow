package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.config.FlowManager;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels.SimpleModel;
import com.dbflow5.query.Operator;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.Where;
import com.dbflow5.structure.Model;

import org.junit.Test;

import static com.dbflow5.query.SQLite.delete;
import static com.dbflow5.query.SQLite.select;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class DeleteTest extends BaseUnitTest {
    @Test
    public void validateQuery(){
        assertEquals("DELETE ", delete().getQuery());
    }

    @Test
    public void validateDeletion(){
        FlowManager.databaseForTable(SimpleModel.class, dbFlowDatabase -> {
            Model.save(SimpleModel.class, new SimpleModel("name"), dbFlowDatabase);
            SQLite.delete(SimpleModel.class).execute(dbFlowDatabase);
            assertFalse((select().from(SimpleModel.class)).hasData(dbFlowDatabase));
            return null;
        });
    }

    @Test
    public void validateDeletionWithQuery(){
        FlowManager.databaseForTable(SimpleModel.class,dbFlowDatabase -> {
            Model.save(SimpleModel.class, new SimpleModel("name"), dbFlowDatabase);
            Model.save(SimpleModel.class, new SimpleModel("another name"), dbFlowDatabase);
            Where<SimpleModel> where=delete(SimpleModel.class).where(SimpleModel_Table.name.is("name"));
            assertEquals("DELETE FROM SimpleModel WHERE name='name'", where.getQuery().trim());
            where.execute(dbFlowDatabase);
            assertEquals(1, (select().from(SimpleModel.class)).queryList(dbFlowDatabase).size());
            return null;
        });
    }
}
