package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.property.Property;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShortPropertyTest extends BaseUnitTest {
    @Test
    public void testOperators() {
        Property<Short> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop");
        assertEquals("Prop=5", prop.is((short) 5).getQuery().trim());
        assertEquals("Prop=5", prop.eq((short) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.notEq((short) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.isNot((short) 5).getQuery().trim());
        assertEquals("Prop>5", prop.greaterThan((short) 5).getQuery().trim());
        assertEquals("Prop>=5", prop.greaterThanOrEq((short) 5).getQuery().trim());
        assertEquals("Prop<5", prop.lessThan((short) 5).getQuery().trim());
        assertEquals("Prop<=5", prop.lessThanOrEq((short) 5).getQuery().trim());
        assertEquals("Prop BETWEEN 5 AND 6", prop.between((short) 5).and((short) 6).getQuery().trim());
        assertEquals("Prop IN (5,6,7,8)", prop.in((short) 5, (short) 6, (short) 7, (short) 8).getQuery().trim());
        assertEquals("Prop NOT IN (5,6,7,8)", prop.notIn((short) 5, (short) 6, (short) 7, (short) 8).getQuery().trim());
        assertEquals("Prop=Prop + 5", prop.concatenate((short) 5).getQuery().trim());
    }

    @Test
    public void testAlias() {
        Property<Short> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop", "Alias");
        assertEquals("Prop AS Alias", prop.toString().trim());
        Property<Short> prop2 = new Property<>(SimpleTestModels.SimpleModel.class,
            NameAlias.builder("Prop").shouldAddIdentifierToName(false)
                .as("Alias")
                .shouldAddIdentifierToAliasName(false).build()
        );
        assertEquals("Prop AS Alias", prop2.toString().trim());
    }
}
