package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.Index;
import com.dbflow5.query.NameAlias;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IndexTest extends BaseUnitTest {
    @Test
    public void validateBasicIndex() {
        assertEquals("CREATE INDEX IF NOT EXISTS index ON SimpleModel(name)",
            Index.indexOn(SimpleTestModels.SimpleModel.class, "index", SimpleModel_Table.name).getQuery());
    }

    @Test
    public void validateUniqueIndex() {
        assertEquals("CREATE UNIQUE INDEX IF NOT EXISTS index ON SimpleModel(name, test)",
            Index.indexOn(SimpleTestModels.SimpleModel.class, "index").unique(true).and(SimpleModel_Table.name)
                .and(NameAlias.of("test")).getQuery());
    }

    @Test
    public void validateBasicIndexNameAlias() {
        assertEquals("CREATE INDEX IF NOT EXISTS index ON SimpleModel(name, test)",
            Index.indexOn(SimpleTestModels.SimpleModel.class, "index", NameAlias.of("name"), NameAlias.of("test")).getQuery());
    }
}
