package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.property.Property;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PropertyTest extends BaseUnitTest {
    @Test
    public void testOperators() {
        Property<String> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop");
        assertEquals("Prop='5'", prop.is("5").getQuery());
        assertEquals("Prop='5'", prop.eq("5").getQuery());
        assertEquals("Prop!='5'", prop.notEq("5").getQuery());
        assertEquals("Prop!='5'", prop.isNot("5").getQuery());
        assertEquals("Prop LIKE '5'", prop.like("5").getQuery());
        assertEquals("Prop NOT LIKE '5'", prop.notLike("5").getQuery());
        assertEquals("Prop GLOB '5'", prop.glob("5").getQuery());
        assertEquals("Prop>'5'", prop.greaterThan("5").getQuery());
        assertEquals("Prop>='5'", prop.greaterThanOrEq("5").getQuery());
        assertEquals("Prop<'5'", prop.lessThan("5").getQuery());
        assertEquals("Prop<='5'", prop.lessThanOrEq("5").getQuery());
        assertEquals("Prop BETWEEN '5' AND '6' ", prop.between("5").and("6").getQuery());
        assertEquals("Prop IN ('5','6','7','8')", prop.in("5","6","7","8").getQuery());
        assertEquals("Prop NOT IN ('5','6','7','8')", prop.notIn("5","6","7","8").getQuery());
        assertEquals("Prop=Prop || '5'", prop.concatenate("5").getQuery());
        assertEquals("Prop MATCH 'age'", prop.match("age").getQuery());
    }

    @Test
    public void testAlias() {
        Property<String> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop", "Alias");
        assertEquals("Prop AS Alias", prop.toString().trim());
        Property<String> prop2 = new Property<>(SimpleTestModels.SimpleModel.class,
            NameAlias.builder("Prop").shouldAddIdentifierToName(false)
                .as("Alias")
                .shouldAddIdentifierToAliasName(false).build()
        );
        assertEquals("Prop AS Alias", prop2.toString().trim());
    }
}
