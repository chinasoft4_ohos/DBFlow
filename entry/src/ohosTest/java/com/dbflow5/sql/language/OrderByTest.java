package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.SimpleModel_Table;
import com.dbflow5.annotation.Collate;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.OrderBy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OrderByTest extends BaseUnitTest {
    @Test
    public void validateBasicOrderBy() {
        assertEquals("name ASC", OrderBy.fromProperty(SimpleModel_Table.name, true).ascending().getQuery().trim());
    }

    @Test
    public void validateDescendingOrderBy() {
        assertEquals("name DESC", OrderBy.fromNameAlias(NameAlias.of("name"), true).descending().getQuery().trim());
    }

    @Test
    public void validateCollate() {
        assertEquals("name COLLATE RTRIM ASC", OrderBy.fromProperty(SimpleModel_Table.name, true).ascending().collate(Collate.RTRIM).getQuery().trim());
    }

    @Test
    public void validateCustomOrdrBy() {
        assertEquals("name ASC This is custom", OrderBy.fromString("name ASC This is custom").getQuery().trim());
    }
}
