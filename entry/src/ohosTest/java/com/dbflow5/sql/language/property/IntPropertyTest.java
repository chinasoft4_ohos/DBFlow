package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.property.Property;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntPropertyTest extends BaseUnitTest {
    @Test
    public void testOperators() {
        Property<Integer> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop");
        assertEquals("Prop=5", prop.is((int) 5).getQuery().trim());
        assertEquals("Prop=5", prop.eq((int) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.notEq((int) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.isNot((int) 5).getQuery().trim());
        assertEquals("Prop>5", prop.greaterThan((int) 5).getQuery().trim());
        assertEquals("Prop>=5", prop.greaterThanOrEq((int) 5).getQuery().trim());
        assertEquals("Prop<5", prop.lessThan((int) 5).getQuery().trim());
        assertEquals("Prop<=5", prop.lessThanOrEq((int) 5).getQuery().trim());
        assertEquals("Prop BETWEEN 5 AND 6", prop.between((int) 5).and((int) 6).getQuery().trim());
        assertEquals("Prop IN (5,6,7,8)", prop.in((int) 5, (int) 6, (int) 7, (int) 8).getQuery().trim());
        assertEquals("Prop NOT IN (5,6,7,8)", prop.notIn((int) 5, (int) 6, (int) 7, (int) 8).getQuery().trim());
        assertEquals("Prop=Prop + 5", prop.concatenate((int) 5).getQuery().trim());
    }

    @Test
    public void testAlias() {
        Property<Integer> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop", "Alias");
        assertEquals("Prop AS Alias", prop.toString().trim());
        Property<Integer> prop2 = new Property<>(SimpleTestModels.SimpleModel.class,
            NameAlias.builder("Prop").shouldAddIdentifierToName(false)
                .as("Alias")
                .shouldAddIdentifierToAliasName(false).build()
        );
        assertEquals("Prop AS Alias", prop2.toString().trim());
    }
}
