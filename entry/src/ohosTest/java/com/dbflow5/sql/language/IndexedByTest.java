package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.IndexedBy;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IndexProperty;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IndexedByTest extends BaseUnitTest {
    @Test
    public void validateQuery() {
        IndexedBy<SimpleTestModels.SimpleModel> indexed = (SQLite.select().from(SimpleTestModels.SimpleModel.class))
            .indexedBy(new IndexProperty<>("Index", false, SimpleTestModels.SimpleModel.class, SimpleModel_Table.name));
        assertEquals("SELECT * FROM SimpleModel INDEXED BY Index", indexed.getQuery().trim());
    }
}
