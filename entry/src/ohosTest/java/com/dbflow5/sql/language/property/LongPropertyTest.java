package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.property.Property;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LongPropertyTest extends BaseUnitTest {
    @Test
    public void testOperators() {
        Property<Long> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop");
        assertEquals("Prop=5", prop.is((long) 5).getQuery().trim());
        assertEquals("Prop=5", prop.eq((long) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.notEq((long) 5).getQuery().trim());
        assertEquals("Prop!=5", prop.isNot((long) 5).getQuery().trim());
        assertEquals("Prop>5", prop.greaterThan((long) 5).getQuery().trim());
        assertEquals("Prop>=5", prop.greaterThanOrEq((long) 5).getQuery().trim());
        assertEquals("Prop<5", prop.lessThan((long) 5).getQuery().trim());
        assertEquals("Prop<=5", prop.lessThanOrEq((long) 5).getQuery().trim());
        assertEquals("Prop BETWEEN 5 AND 6", prop.between((long) 5).and((long) 6).getQuery().trim());
        assertEquals("Prop IN (5,6,7,8)", prop.in((long) 5, (long) 6, (long) 7, (long) 8).getQuery().trim());
        assertEquals("Prop NOT IN (5,6,7,8)", prop.notIn((long) 5, (long) 6, (long) 7, (long) 8).getQuery().trim());
        assertEquals("Prop=Prop + 5", prop.concatenate((long) 5).getQuery().trim());
    }

    @Test
    public void testAlias() {
        Property<Long> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop", "Alias");
        assertEquals("Prop AS Alias", prop.toString().trim());
        Property<Long> prop2 = new Property<>(SimpleTestModels.SimpleModel.class,
            NameAlias.builder("Prop").shouldAddIdentifierToName(false)
                .as("Alias")
                .shouldAddIdentifierToAliasName(false).build()
        );
        assertEquals("Prop AS Alias", prop2.toString().trim());
    }
}
