package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.property.Property;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FloatPropertyTest extends BaseUnitTest {
    @Test
    public void testOperators() {
        Property<Float> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop");
        assertEquals("Prop=5.0", prop.is((Float) 5f).getQuery().trim());
        assertEquals("Prop=5.0", prop.eq((Float) 5f).getQuery().trim());
        assertEquals("Prop!=5.0", prop.notEq((Float) 5f).getQuery().trim());
        assertEquals("Prop!=5.0", prop.isNot((Float) 5f).getQuery().trim());
        assertEquals("Prop>5.0", prop.greaterThan((Float) 5f).getQuery().trim());
        assertEquals("Prop>=5.0", prop.greaterThanOrEq((Float) 5f).getQuery().trim());
        assertEquals("Prop<5.0", prop.lessThan((Float) 5f).getQuery().trim());
        assertEquals("Prop<=5.0", prop.lessThanOrEq((Float) 5f).getQuery().trim());
        assertEquals("Prop BETWEEN 5.0 AND 6.0", prop.between((Float) 5f).and((Float) 6f).getQuery().trim());
        assertEquals("Prop IN (5.0,6.0,7.0,8.0)", prop.in((Float) 5f, (Float) 6f, (Float) 7f, (Float) 8f).getQuery().trim());
        assertEquals("Prop NOT IN (5.0,6.0,7.0,8.0)", prop.notIn((Float) 5f, (Float) 6f, (Float) 7f, (Float) 8f).getQuery().trim());
        assertEquals("Prop=Prop + 5.0", prop.concatenate((Float) 5f).getQuery().trim());
    }

    @Test
    public void testAlias() {
        Property<Float> prop = new Property<>(SimpleTestModels.SimpleModel.class, "Prop", "Alias");
        assertEquals("Prop AS Alias", prop.toString().trim());
        Property<Float> prop2 = new Property<>(SimpleTestModels.SimpleModel.class,
            NameAlias.builder("Prop").shouldAddIdentifierToName(false)
                .as("Alias")
                .shouldAddIdentifierToAliasName(false).build()
        );
        assertEquals("Prop AS Alias", prop2.toString().trim());
    }
}
