package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.annotation.ConflictAction;
import com.dbflow5.models.NumberModel_Table;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.models.SimpleTestModels.SimpleModel;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.Property;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UpdateTest extends BaseUnitTest {
    @Test
    public void validateUpdateRollback() {
        assertEquals("UPDATE OR ROLLBACK SimpleModel", SQLite.update(SimpleModel.class).orRollback().getQuery().trim());
    }

    @Test
    public void validateUpdateFail() {
        assertEquals("UPDATE OR FAIL SimpleModel", SQLite.update(SimpleModel.class).orFail().getQuery().trim());
    }

    @Test
    public void validateUpdateIgnore() {
        assertEquals("UPDATE OR IGNORE SimpleModel", SQLite.update(SimpleModel.class).orIgnore().getQuery().trim());
    }

    @Test
    public void validateUpdateReplace() {
        assertEquals("UPDATE OR REPLACE SimpleModel", SQLite.update(SimpleModel.class).orReplace().getQuery().trim());
    }

    @Test
    public void validateUpdateAbort() {
        assertEquals("UPDATE OR ABORT SimpleModel", SQLite.update(SimpleModel.class).orAbort().getQuery().trim());
    }

    @Test
    public void validateSetQuery() {
        assertEquals("UPDATE SimpleModel SET name='name'", SQLite.update(SimpleModel.class).set(SimpleModel_Table.name.eq("name")).getQuery().trim());
    }

    @Test
    public void validateWildcardQuery() {
        assertEquals("UPDATE OR FAIL NumberModel SET id=? WHERE id=?", SQLite.update(SimpleTestModels.NumberModel.class).or(ConflictAction.FAIL)
            .set(NumberModel_Table.id.eq(Property.WILDCARD)).where(NumberModel_Table.id.eq(Property.WILDCARD)).getQuery().trim());
    }
}
