package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.annotation.Collate;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels.SimpleModel;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.models.TwoColumnModel_Table;
import com.dbflow5.query.Operator;
import com.dbflow5.query.SQLite;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class OperatorTest extends BaseUnitTest {
    @Test
    public void testEquals() {
        assertEquals("name='name'", Operator.op("name").eq("name").getQuery());
        assertEquals("name='name'", Operator.op("name").is("name").getQuery());
    }

    @Test
    public void testNotEquals() {

        assertEquals("name!='name'", Operator.op("name").notEq("name").getQuery());
        assertEquals("name!='name'", Operator.op("name").isNot("name").getQuery());
    }

    @Test
    public void testLike() {
        assertEquals("name LIKE 'name'", Operator.op("name").like("name").getQuery());
        assertEquals("name NOT LIKE 'name'", Operator.op("name").notLike("name").getQuery());
        assertEquals("name GLOB 'name'", Operator.op("name").glob("name").getQuery());
    }

    @Test
    public void testMath() {
        assertEquals("name>'name'", Operator.op("name").greaterThan("name").getQuery());
        assertEquals("name>='name'", Operator.op("name").greaterThanOrEq("name").getQuery());
        assertEquals("name<'name'", Operator.op("name").lessThan("name").getQuery());
        assertEquals("name<='name'", Operator.op("name").lessThanOrEq("name").getQuery());
        assertEquals("name+'name'", Operator.op("name").plus("name").getQuery());
        assertEquals("name/'name'", Operator.op("name").div("name").getQuery());
        assertEquals("name*'name'", Operator.op("name").times("name").getQuery());
        assertEquals("name%'name'", Operator.op("name").rem("name").getQuery());
    }

    @Test
    public void testCollate() {
        assertEquals("name COLLATE NOCASE", Operator.op("name").collate(Collate.NOCASE).getQuery());
        assertEquals("name COLLATE NOCASE", Operator.op("name").collate("NOCASE").getQuery());
    }

    @Test
    public void testBetween() {
        assertEquals("id BETWEEN 6 AND 7", TwoColumnModel_Table.id.between(6).and(7).getQuery().trim());
    }

    @Test
    public void testIn() {
        assertEquals("id IN (5,6,7,8,9)", TwoColumnModel_Table.id.in(Arrays.asList(5, 6, 7, 8)).and(9).getQuery());
        assertEquals("id NOT IN (SELECT * FROM SimpleModel)", TwoColumnModel_Table.id.notIn(SQLite.select().from(SimpleTestModels.SimpleModel.class)).getQuery());
    }

    @Test
    public void matchOperator() {
        assertEquals("name MATCH 'age'", Operator.op("name").match("age").getQuery());
    }
}
