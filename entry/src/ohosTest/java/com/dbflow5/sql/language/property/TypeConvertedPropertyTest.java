package com.dbflow5.sql.language.property;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.converter.TypeConverters;
import com.dbflow5.models.EnumTypeConverterModel_Table;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.query.NameAlias;
import com.dbflow5.query.property.Property;
import com.dbflow5.query.property.TypeConvertedProperty;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TypeConvertedPropertyTest extends BaseUnitTest {
    @Test
    public void testTypeConverter() {
        TypeConvertedProperty<Long, Date> property = new TypeConvertedProperty<>(SimpleTestModels.SimpleModel.class, "Prop", true, new TypeConvertedProperty.TypeConverterGetter() {
            @Override
            public TypeConverters.TypeConverter<?, ?> getTypeConverter(Class<?> modelClass) {

                return new TypeConverters.TypeConverter<Long, Date>() {
                    @Override
                    public Long getDBValue(Date model) {
                        return model.getTime();
                    }

                    @Override
                    public Date getModelValue(Long data) {
                        return new Date(data);
                    }
                };
            }
        });
        assertEquals("Prop", property.toString());
        Date date = new Date();
        assertEquals("Prop=" + date.getTime(), property.eq(date).getQuery());
        assertEquals("SimpleModel.Prop=" + date.getTime(), property.withTable().eq(date).getQuery());

        Property<Long> inverted = property.invertProperty();
        assertEquals("Prop=5050505", inverted.eq((long) 5050505).getQuery());
    }

    @Test
    public void testCustomEnumTypeConverter(){
        assertEquals("difficulty='H'", EnumTypeConverterModel_Table.difficulty.eq(SimpleTestModels.Difficulty.HARD).getQuery());
        assertEquals("EnumTypeConverterModel.difficulty='H'", EnumTypeConverterModel_Table.difficulty.withTable().eq(SimpleTestModels.Difficulty.HARD).getQuery());
        assertEquals("et.difficulty='H'", EnumTypeConverterModel_Table.difficulty.withTable(NameAlias.tableNameBuilder("et").build()).eq(SimpleTestModels.Difficulty.HARD).getQuery());
    }
}
