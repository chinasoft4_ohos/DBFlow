package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.models.SimpleTestModels.SimpleModel;
import com.dbflow5.models.SimpleTestModels;
import com.dbflow5.models.SimpleTestModels.TwoColumnModel;
import com.dbflow5.models.TwoColumnModel_Table;
import com.dbflow5.query.OrderBy;
import com.dbflow5.query.SQLite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SelectTest extends BaseUnitTest {
    @Test
    public void validateSelect() {
        assertEquals("SELECT name, id FROM TwoColumnModel", SQLite.select(TwoColumnModel_Table.name, TwoColumnModel_Table.id).from(TwoColumnModel.class).getQuery().trim());
    }

    @Test
    public void validateSelectDistinct() {
        assertEquals("SELECT DISTINCT name FROM SimpleModel", SQLite.select(TwoColumnModel_Table.name).distinct().from(SimpleTestModels.SimpleModel.class).getQuery().trim());
    }
}
