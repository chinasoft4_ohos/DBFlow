package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
//import com.dbflow5.models.SimpleModel_Table.name;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleTestModels;
//import com.dbflow5.models.TwoColumnModel_Table;
//import com.dbflow5.models.TwoColumnModel_Table.id;
import com.dbflow5.models.TwoColumnModel_Table;
import com.dbflow5.query.From;
import com.dbflow5.query.Operator;
import com.dbflow5.query.SQLite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class FromTest extends BaseUnitTest {
    @Test
    public void validateSimpleFrom(){
        assertEquals("SELECT * FROM SimpleModel", (SQLite.select().from(SimpleTestModels.SimpleModel.class).getQuery().trim()));
    }

    @Test
    public void validateProjectionFrom(){
        assertEquals("SELECT name FROM SimpleModel", (SQLite.select(SimpleModel_Table.name).from(SimpleTestModels.SimpleModel.class).getQuery()).trim());
    }

    @Test
    public void validateMultipleProjection(){
        assertEquals("SELECT name, name, id FROM SimpleModel",
            (SQLite.select(SimpleModel_Table.name, TwoColumnModel_Table.name, TwoColumnModel_Table.id).from(SimpleTestModels.SimpleModel.class)).getQuery().trim());
    }

    @Test
    public void validateAlias() {
        assertEquals("SELECT * FROM SimpleModel AS Simple", (SQLite.select().from(SimpleTestModels.SimpleModel.class).as("Simple")).getQuery().trim());
    }

    @Test
    public void validateJoins() {
        From<SimpleTestModels.SimpleModel> from = SQLite.select().from(SimpleTestModels.SimpleModel.class).innerJoin(SimpleTestModels.TwoColumnModel.class).on(SimpleModel_Table.name.eq(TwoColumnModel_Table.name.withTable()));

        assertEquals("SELECT * FROM SimpleModel INNER JOIN TwoColumnModel ON name=TwoColumnModel.name",
            from.getQuery().trim());
        assertFalse(from.associatedTables().isEmpty());
    }
}
