package com.dbflow5.sql.language;

import com.dbflow5.BaseUnitTest;
import com.dbflow5.query.Case;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.PropertyFactory;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CaseTest extends BaseUnitTest {
    @Test
    public void simpleCaseTest(){
        Case<String> aCase = SQLite._case(PropertyFactory.propertyString(String.class, "country"))
            .whenever("USA").then("Domestic")._else("Foreign");

        assertEquals("CASE country WHEN 'USA' THEN 'Domestic' ELSE 'Foreign' END Country",
            aCase.end("Country").getQuery().trim());
        assertTrue(aCase.isEfficientCase);
    }
}
