package com.dbflow5.rx2.query;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.reactivestreams.structure.BaseRXModel;

@Table(database = TestDatabase.class, allFields = true)
public class SimpleRXModel extends BaseRXModel {
    @PrimaryKey
    public String id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SimpleRXModel(String id) {
        this.id = id;
    }
}
