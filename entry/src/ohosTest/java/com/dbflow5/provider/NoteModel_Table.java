package com.dbflow5.provider;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Boolean;
import com.dbflow5.provider.ContentProviderObjects.NoteModel;
import ohos.data.rdb.ValuesBucket;

import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class NoteModel_Table extends ModelAdapter<NoteModel> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> id = new Property<Long>(NoteModel.class, "id");

  /**
   * Foreign Key */
  public static final Property<Long> providerModel = new Property<Long>(NoteModel.class, "providerModel");

  public static final Property<String> note = new Property<String>(NoteModel.class, "note");

  public static final Property<Boolean> isOpen = new Property<Boolean>(NoteModel.class, "isOpen");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,providerModel,note,isOpen};

  public NoteModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<NoteModel> table() {
    return NoteModel.class;
  }

  @Override
  public final String getName() {
    return "NoteModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "providerModel": {
        return providerModel;
      }
      case "note":  {
        return note;
      }
      case "isOpen":  {
        return isOpen;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(NoteModel model, Number id) {
    model.setId(id.longValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, NoteModel model) {
    statement.bindLong(1, model.getId());
    if (model.getContentProviderModel() != null) {
      statement.bindLong(2, (long)model.getContentProviderModel().getId());
    } else {
      statement.bindNull(2);
    }
    statement.bindStringOrNull(3, model.getNote());
    statement.bindLong(4, model.isOpen() ? 1L : 0L);
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, NoteModel model) {
    statement.bindLong(1, model.getId());
    if (model.getContentProviderModel() != null) {
      statement.bindLong(2, (long)model.getContentProviderModel().getId());
    } else {
      statement.bindNull(2);
    }
    statement.bindStringOrNull(3, model.getNote());
    statement.bindLong(4, model.isOpen() ? 1L : 0L);
    statement.bindLong(5, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, NoteModel model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO NoteModel(id,providerModel,note,isOpen) VALUES (nullif(?, 0),?,?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO NoteModel(id,providerModel,note,isOpen) VALUES (nullif(?, 0),?,?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE NoteModel SET id=?,providerModel=?,note=?,isOpen=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM NoteModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS NoteModel(id INTEGER PRIMARY KEY AUTOINCREMENT, providerModel INTEGER, note TEXT, isOpen INTEGER, FOREIGN KEY(providerModel) REFERENCES ContentProviderModel (id) ON UPDATE NO ACTION ON DELETE NO ACTION)";
  }

  @Override
  public final NoteModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    NoteModel model = new NoteModel();
    model.setId(cursor.getLongOrDefault("id"));
    int index_providerModel_ContentProviderModel_Table = cursor.getColumnIndexForName("providerModel");
    if (index_providerModel_ContentProviderModel_Table != -1 && !cursor.isColumnNull(index_providerModel_ContentProviderModel_Table)) {
      model.setContentProviderModel(SQLite.select().from(ContentProviderObjects.ContentProviderModel.class).where()
          .and(ContentProviderModel_Table.id.eq(cursor.getLong(index_providerModel_ContentProviderModel_Table)))
          .querySingle(wrapper));
    } else {
      model.setContentProviderModel(null);
    }
    model.setNote(cursor.getStringOrDefault("note"));
    int index_isOpen = cursor.getColumnIndexForName("isOpen");
    if (index_isOpen != -1 && !cursor.isColumnNull(index_isOpen)) {
      model.setOpen(cursor.getBoolean(index_isOpen));
    } else {
      model.setOpen(false);
    }
    return model;
  }

  @Override
  public final boolean exists(NoteModel model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(NoteModel.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(NoteModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }

  @Override
  public final void bindToInsertValues(ValuesBucket values, NoteModel model) {
    if (model.getContentProviderModel() != null) {
      values.putInteger("providerModel", model.getContentProviderModel().getId());
    } else {
      values.putNull("providerModel");
    }
    values.putString("note", model.getNote());
    values.putInteger("isOpen", model.isOpen() ? 1 : 0);
  }

  @Override
  public final void bindToContentValues(ValuesBucket values, NoteModel model) {
    values.putLong("id", model.getId());
    bindToInsertValues(values, model);
  }
}
