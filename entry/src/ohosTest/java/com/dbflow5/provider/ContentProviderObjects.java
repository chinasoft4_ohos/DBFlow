package com.dbflow5.provider;

import com.dbflow5.annotation.Column;
import com.dbflow5.annotation.Database;
import com.dbflow5.annotation.ForeignKey;
import com.dbflow5.annotation.ForeignKeyReference;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;
import com.dbflow5.contentprovider.annotation.ContentProvider;
import com.dbflow5.contentprovider.annotation.TableEndpoint;

import ohos.utils.net.Uri;

import java.util.ArrayList;

public class ContentProviderObjects {
    @ContentProvider(authority = ContentDatabase.AUTHORITY, database = ContentDatabase.class,
            baseContentUri = ContentDatabase.BASE_CONTENT_URI)
    @Database(version = ContentDatabase.VERSION)
    public abstract static class ContentDatabase extends ContentProviderDatabase {
        public static final String BASE_CONTENT_URI = "content://";
        public static final String AUTHORITY = "com.grosner.content.test.ContentDatabase";
        public static final int VERSION = 1;
    }

    @TableEndpoint(name = ContentProviderModel.NAME, contentProvider = ContentDatabase.class)
    public static class ContentProviderModel extends BaseProviderModel {
        @PrimaryKey(autoincrement = true)
        public int id;
        @Column
        public String notes;
        @Column
        public String title;

        public static final String NAME = "ContentProviderModel";

        private Uri CONTENT_URI;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public ContentProviderModel() {
        }

        public ContentProviderModel(int id, String notes, String title) {
            this.id = id;
            this.notes = notes;
            this.title = title;
            this.CONTENT_URI = ContentUtils.buildUriWithAuthority(ContentDatabase.AUTHORITY);
        }

        @Override
        public Uri deleteUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }

        @Override
        public Uri insertUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }

        @Override
        public Uri updateUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }

        @Override
        public Uri queryUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }
    }

    @Table(database = ContentDatabase.class, generateContentValues = true)
    public static class NoteModel extends BaseProviderModel {
        @PrimaryKey(autoincrement = true)
        public Long id;
        @ForeignKey(references = {@ForeignKeyReference(columnName = "providerModel", foreignKeyColumnName = "id")})
        public ContentProviderModel contentProviderModel;
        @Column
        public String note;
        @Column
        public boolean isOpen;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public ContentProviderModel getContentProviderModel() {
            return contentProviderModel;
        }

        public void setContentProviderModel(ContentProviderModel contentProviderModel) {
            this.contentProviderModel = contentProviderModel;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public boolean isOpen() {
            return isOpen;
        }

        public void setOpen(boolean open) {
            isOpen = open;
        }

        public NoteModel(Long id, ContentProviderModel contentProviderModel, String note, boolean isOpen) {
            this.id = id;
            this.contentProviderModel = contentProviderModel;
            this.note = note;
            this.isOpen = isOpen;
        }

        public NoteModel() {
        }

        @Override
        public Uri deleteUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }

        @Override
        public Uri insertUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }

        @Override
        public Uri updateUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }

        @Override
        public Uri queryUri() {
            return TestContentProvider.ContentProviderModel.getCONTENT_URI();
        }
    }

    @Table(database = ContentDatabase.class, generateContentValues = true)
    public static class TestSyncableModel extends BaseSyncableProviderModel {
        @PrimaryKey(autoincrement = true)
        public Long id;
        @Column
        public String name;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public TestSyncableModel(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public TestSyncableModel() {
        }

        @Override
        public Uri deleteUri() {
            return TestContentProvider.TestSyncableModel.getCONTENT_URI();
        }

        @Override
        public Uri insertUri() {
            return TestContentProvider.TestSyncableModel.getCONTENT_URI();
        }

        @Override
        public Uri updateUri() {
            return TestContentProvider.TestSyncableModel.getCONTENT_URI();
        }

        @Override
        public Uri queryUri() {
            return TestContentProvider.TestSyncableModel.getCONTENT_URI();
        }
    }
}
