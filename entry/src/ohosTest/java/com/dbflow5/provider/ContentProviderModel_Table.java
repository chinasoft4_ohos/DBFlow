package com.dbflow5.provider;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.provider.ContentProviderObjects.ContentProviderModel;
import ohos.data.rdb.ValuesBucket;

import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class ContentProviderModel_Table extends ModelAdapter<ContentProviderModel> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> id = new Property<Long>(ContentProviderModel.class, "id");

  public static final Property<String> notes = new Property<String>(ContentProviderModel.class, "notes");

  public static final Property<String> title = new Property<String>(ContentProviderModel.class, "title");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,notes,title};

  public ContentProviderModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<ContentProviderModel> table() {
    return ContentProviderModel.class;
  }

  @Override
  public final String getName() {
    return "ContentProviderModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "notes":  {
        return notes;
      }
      case "title":  {
        return title;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(ContentProviderModel model, Number id) {
    model.setId((int)id.longValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, ContentProviderModel model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getNotes());
    statement.bindStringOrNull(3, model.getTitle());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, ContentProviderModel model) {
    statement.bindLong(1, (long)model.getId());
    statement.bindStringOrNull(2, model.getNotes());
    statement.bindStringOrNull(3, model.getTitle());
    statement.bindLong(4, (long)model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, ContentProviderModel model) {
    statement.bindLong(1, (long)model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO ContentProviderModel(id,notes,title) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO ContentProviderModel(id,notes,title) VALUES (nullif(?, 0),?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE ContentProviderModel SET id=?,notes=?,title=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM ContentProviderModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS ContentProviderModel(id INTEGER PRIMARY KEY AUTOINCREMENT, notes TEXT, title TEXT)";
  }

  @Override
  public final ContentProviderModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    ContentProviderModel model = new ContentProviderModel();
    model.setId((int)cursor.getLongOrDefault("id", 0));
    model.setNotes(cursor.getStringOrDefault("notes"));
    model.setTitle(cursor.getStringOrDefault("title"));
    return model;
  }

  @Override
  public final boolean exists(ContentProviderModel model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(ContentProviderModel.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(ContentProviderModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq((long)model.getId()));
    return clause;
  }

  @Override
  public final void bindToInsertValues(ValuesBucket values, ContentProviderModel model) {
    values.putString("notes", model.getNotes());
    values.putString("title", model.getTitle());
  }

  @Override
  public final void bindToContentValues(ValuesBucket values, ContentProviderModel model) {
    values.putInteger("id", model.getId());
    bindToInsertValues(values, model);
  }
}
