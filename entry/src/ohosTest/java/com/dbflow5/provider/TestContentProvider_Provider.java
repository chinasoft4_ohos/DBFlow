package com.dbflow5.provider;

import com.dbflow5.ConflictActionUtils;
import com.dbflow5.UriMatcher;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.annotation.ConflictAction;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.sqlcipher.utils.DatabaseUtils;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;

import com.dbflow5.provider.ContentProviderObjects.ContentDatabase;

import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import java.util.Collections;
import java.util.List;

public final class TestContentProvider_Provider extends BaseContentProvider {
    private static final int ContentProviderModel_CONTENT_URI = 0;

    private static final int ContentProviderModel_withId = 1;

    private static final int NoteModel_CONTENT_URI = 2;

    private static final int NoteModel_withId = 3;

    private static final int NoteModel_fromList = 4;

    private static final int NoteModel_withOpenId = 5;

    private static final int TestSyncableModel_CONTENT_URI = 6;

    private final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    public TestContentProvider_Provider(Class<DatabaseHolder> databaseHolderClass) {
        super(databaseHolderClass);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        final String AUTHORITY = "com.dbflow5.dataability";
        MATCHER.addURI(AUTHORITY, "ContentProviderModel", ContentProviderModel_CONTENT_URI);
        MATCHER.addURI(AUTHORITY, "ContentProviderModel/#", ContentProviderModel_withId);
        MATCHER.addURI(AUTHORITY, "NoteModel", NoteModel_CONTENT_URI);
        MATCHER.addURI(AUTHORITY, "NoteModel/#", NoteModel_withId);
        MATCHER.addURI(AUTHORITY, "NoteModel/#/#", NoteModel_fromList);
        MATCHER.addURI(AUTHORITY, "NoteModel/#/#", NoteModel_withOpenId);
        MATCHER.addURI(AUTHORITY, "TestSyncableModel", TestSyncableModel_CONTENT_URI);
    }

    @Override
    public final String getDatabaseName() {
        return FlowManager.getDatabaseName(ContentDatabase.class);
    }

    @Override
    public final String getType(Uri uri) {
        String type = null;
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                type = "vnd.ohos.cursor.dir/ContentProviderModel";
                break;
            }
            case ContentProviderModel_withId: {
                type = "vnd.ohos.cursor.item/ContentProviderModel";
                break;
            }
            case NoteModel_CONTENT_URI: {
                type = "vnd.ohos.cursor.dir/NoteModel";
                break;
            }
            case NoteModel_withId: {
                type = "vnd.ohos.cursor.dir/NoteModel";
                break;
            }
            case NoteModel_fromList: {
                type = "vnd.ohos.cursor.item/ContentProviderModel";
                break;
            }
            case NoteModel_withOpenId: {
                type = "vnd.ohos.cursor.item/ContentProviderModel";
                break;
            }
            case TestSyncableModel_CONTENT_URI: {
                type = "vnd.ohos.cursor.dir/TestSyncableModel";
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
        return type;
    }

    @Override
    public ResultSet query(Uri uri, String[] projection, DataAbilityPredicates predicates) {
        ResultSet cursor = null;
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                cursor = FlowManager.getDatabase(ContentDatabase.class).query("ContentProviderModel", projection, predicates.getWhereClause(),
                        predicates.getWhereArgs().toArray(new String[0]), null, null, predicates.getOrder());
                break;
            }
            case ContentProviderModel_withId: {
                List<String> segments = uri.getDecodedPathList();
                cursor = FlowManager.getDatabase(ContentDatabase.class).query("ContentProviderModel", projection, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1)}), null, null, predicates.getOrder());
                break;
            }
            case NoteModel_CONTENT_URI: {
                cursor = FlowManager.getDatabase(ContentDatabase.class).query("NoteModel", projection, predicates.getWhereClause(),
                        predicates.getWhereArgs().toArray(new String[0]), null, null, predicates.getOrder());
                break;
            }
            case NoteModel_withId: {
                List<String> segments = uri.getDecodedPathList();
                cursor = FlowManager.getDatabase(ContentDatabase.class).query("NoteModel", projection, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1)}), null, null, predicates.getOrder());
                break;
            }
            case NoteModel_fromList: {
                List<String> segments = uri.getDecodedPathList();
                cursor = FlowManager.getDatabase(ContentDatabase.class).query("NoteModel", projection, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(2)}), null, null, predicates.getOrder());
                break;
            }
            case NoteModel_withOpenId: {
                List<String> segments = uri.getDecodedPathList();
                cursor = FlowManager.getDatabase(ContentDatabase.class).query("NoteModel", projection, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ? AND isOpen = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1), segments.get(2)}), null, null, predicates.getOrder());
                break;
            }
            case TestSyncableModel_CONTENT_URI: {
                cursor = FlowManager.getDatabase(ContentDatabase.class).query("TestSyncableModel", projection, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]), null, null, predicates.getOrder());
                break;
            }
        }
        if (cursor != null) {
            cursor.setAffectedByUris(this, Collections.singletonList(uri));
        }
        return cursor;
    }

    @Override
    public final int insert(Uri uri, ValuesBucket values) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return (int) id;
            }
            case ContentProviderModel_withId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                Uri[] notifyUrisonInsert = TestContentProvider.ContentProviderModel.onInsert(values);
                for (Uri notifyUri : notifyUrisonInsert) {
                    DataAbilityHelper.creator(this).notifyChange(notifyUri);
                }
                return (int) id;
            }
            case NoteModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                Uri[] notifyUrisonInsert = TestContentProvider.NoteModel.onInsert(values);
                for (Uri notifyUri : notifyUrisonInsert) {
                    DataAbilityHelper.creator(this).notifyChange(notifyUri);
                }
                Uri notifyUrionInsert2 = TestContentProvider.NoteModel.onInsert2(values);
                DataAbilityHelper.creator(this).notifyChange(notifyUrionInsert2);
                return (int) id;
            }
            case NoteModel_withId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return (int) id;
            }
            case NoteModel_fromList: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return (int) id;
            }
            case NoteModel_withOpenId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return (int) id;
            }
            case TestSyncableModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "TestSyncableModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("TestSyncableModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return (int) id;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }

    @Override
    protected final int bulkInsert(Uri uri, ValuesBucket values) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return id > 0 ? 1 : 0;
            }
            case ContentProviderModel_withId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                Uri[] notifyUrisonInsert = TestContentProvider.ContentProviderModel.onInsert(values);
                for (Uri notifyUri : notifyUrisonInsert) {
                    DataAbilityHelper.creator(this).notifyChange(notifyUri);
                }
                return id > 0 ? 1 : 0;
            }
            case NoteModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                Uri[] notifyUrisonInsert = TestContentProvider.NoteModel.onInsert(values);
                for (Uri notifyUri : notifyUrisonInsert) {
                    DataAbilityHelper.creator(this).notifyChange(notifyUri);
                }
                Uri notifyUrionInsert2 = TestContentProvider.NoteModel.onInsert2(values);
                DataAbilityHelper.creator(this).notifyChange(notifyUrionInsert2);
                return id > 0 ? 1 : 0;
            }
            case NoteModel_withId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return id > 0 ? 1 : 0;
            }
            case NoteModel_fromList: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return id > 0 ? 1 : 0;
            }
            case NoteModel_withOpenId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("NoteModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return id > 0 ? 1 : 0;
            }
            case TestSyncableModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "TestSyncableModel"));
                final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("TestSyncableModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return id > 0 ? 1 : 0;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }

    @Override
    public final int delete(Uri uri, DataAbilityPredicates predicates) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                long count = FlowManager.getDatabase(ContentDatabase.class).delete("ContentProviderModel", predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case ContentProviderModel_withId: {
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).delete("ContentProviderModel", DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1)}));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case NoteModel_CONTENT_URI: {
                long count = FlowManager.getDatabase(ContentDatabase.class).delete("NoteModel", predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case NoteModel_withId: {
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).delete("NoteModel", DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1)}));
                Uri[] notifyUrisonDelete = new Uri[0];
                try {
                    notifyUrisonDelete = TestContentProvider.NoteModel.onDelete(getContext(), uri);
                } catch (DataAbilityRemoteException e) {
                    e.printStackTrace();
                }
                for (Uri notifyUri : notifyUrisonDelete) {
                    DataAbilityHelper.creator(this).notifyChange(notifyUri);
                }
                return (int) count;
            }
            case NoteModel_fromList: {
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).delete("NoteModel", DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(2)}));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case NoteModel_withOpenId: {
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).delete("NoteModel", DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ? AND isOpen = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1), segments.get(2)}));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case TestSyncableModel_CONTENT_URI: {
                long count = FlowManager.getDatabase(ContentDatabase.class).delete("TestSyncableModel", predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }

    @Override
    public int update(Uri uri, ValuesBucket values, DataAbilityPredicates predicates) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
                long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("ContentProviderModel", values, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case ContentProviderModel_withId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("ContentProviderModel", values, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1)}),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case NoteModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("NoteModel", values, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case NoteModel_withId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("NoteModel", values, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1)}),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                Uri[] notifyUrisonUpdate = new Uri[0];
                try {
                    notifyUrisonUpdate = TestContentProvider.NoteModel.onUpdate(getContext(), uri);
                } catch (DataAbilityRemoteException e) {
                    e.printStackTrace();
                }
                for (Uri notifyUri : notifyUrisonUpdate) {
                    DataAbilityHelper.creator(this).notifyChange(notifyUri);
                }
                return (int) count;
            }
            case NoteModel_fromList: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("NoteModel", values, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(2)}),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case NoteModel_withOpenId: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "NoteModel"));
                List<String> segments = uri.getDecodedPathList();
                long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("NoteModel", values, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ? AND isOpen = ?"),
                        DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[]{segments.get(1), segments.get(2)}),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            case TestSyncableModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "TestSyncableModel"));
                long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("TestSyncableModel", values, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }
}
