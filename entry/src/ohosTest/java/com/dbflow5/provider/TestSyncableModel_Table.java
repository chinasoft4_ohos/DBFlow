package com.dbflow5.provider;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.SQLite;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import com.dbflow5.provider.ContentProviderObjects.TestSyncableModel;
import ohos.data.rdb.ValuesBucket;

import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Number;
import java.lang.Override;
import java.lang.String;

public final class TestSyncableModel_Table extends ModelAdapter<TestSyncableModel> {
  /**
   * Primary Key AutoIncrement */
  public static final Property<Long> id = new Property<Long>(TestSyncableModel.class, "id");

  public static final Property<String> name = new Property<String>(TestSyncableModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name};

  public TestSyncableModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<TestSyncableModel> table() {
    return TestSyncableModel.class;
  }

  @Override
  public final String getName() {
    return "TestSyncableModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final void updateAutoIncrement(TestSyncableModel model, Number id) {
    model.setId(id.longValue());
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, TestSyncableModel model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, TestSyncableModel model) {
    statement.bindLong(1, model.getId());
    statement.bindStringOrNull(2, model.getName());
    statement.bindLong(3, model.getId());
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, TestSyncableModel model) {
    statement.bindLong(1, model.getId());
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO TestSyncableModel(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO TestSyncableModel(id,name) VALUES (nullif(?, 0),?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE TestSyncableModel SET id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM TestSyncableModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS TestSyncableModel(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)";
  }

  @Override
  public final TestSyncableModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    TestSyncableModel model = new TestSyncableModel();
    model.setId(cursor.getLongOrDefault("id"));
    model.setName(cursor.getStringOrDefault("name"));
    return model;
  }

  @Override
  public final boolean exists(TestSyncableModel model, DatabaseWrapper wrapper) {
    return model.getId() > 0
    && SQLite.selectCountOf()
    .from(TestSyncableModel.class)
    .where(getPrimaryConditionClause(model))
    .hasData(wrapper);
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(TestSyncableModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }

  @Override
  public final void bindToInsertValues(ValuesBucket values, TestSyncableModel model) {
    values.putString("name", model.getName());
  }

  @Override
  public final void bindToContentValues(ValuesBucket values, TestSyncableModel model) {
    values.putLong("id", model.getId());
    bindToInsertValues(values, model);
  }
}
