package com.dbflow5.provider;

import com.dbflow5.contentprovider.annotation.ContentProvider;
import com.dbflow5.contentprovider.annotation.ContentType;
import com.dbflow5.contentprovider.annotation.ContentUri;
import com.dbflow5.contentprovider.annotation.Notify;
import com.dbflow5.contentprovider.annotation.NotifyMethod;
import com.dbflow5.contentprovider.annotation.PathSegment;
import com.dbflow5.contentprovider.annotation.TableEndpoint;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.app.Context;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;

import static com.dbflow5.SqlUtils.getContentValuesKey;

class TestContentProvider2{
    @TableEndpoint(name = ContentProviderModel.ENDPOINT,
        contentProvider = ContentProviderObjects.ContentDatabase.class)
    static class ContentProviderModel{
        public static final String ENDPOINT = "ContentProviderModel";

        @ContentUri(path = TestContentProvider.ContentProviderModel.ENDPOINT,
            type = ContentType.VND_MULTIPLE + TestContentProvider.ContentProviderModel.ENDPOINT)
        public static Uri CONTENT_URI = TestContentProvider.buildUri(TestContentProvider.ContentProviderModel.ENDPOINT);

        @ContentUri(path = TestContentProvider.ContentProviderModel.ENDPOINT + "/#",
            type = ContentType.VND_SINGLE + TestContentProvider.ContentProviderModel.ENDPOINT,
            segments = {@PathSegment(segment = 1, column = "id")})
        public static Uri withId(long id) {
            return TestContentProvider.buildUri(String.valueOf(id));
        }

        @Notify(notifyMethod = NotifyMethod.INSERT, paths = {TestContentProvider.ContentProviderModel.ENDPOINT + "/#"})
        public static Uri[] onInsert(ValuesBucket contentValues) {
            Long id = contentValues.getLong("id");
            return new Uri[]{withId(id)};
        }
    }
}
@ContentProvider(authority = TestContentProvider.AUTHORITY, database = ContentProviderObjects.ContentDatabase.class,
    baseContentUri = TestContentProvider.BASE_CONTENT_URI)
public class TestContentProvider {

    public static final String AUTHORITY = "com.dbflow5.dataability";
    public static final String BASE_CONTENT_URI = "dataability://";

    public static Uri buildUri(String... paths) {
        Uri.Builder builder = Uri.parse(BASE_CONTENT_URI + AUTHORITY).makeBuilder();
        for (String path : paths) {
            builder.appendDecodedPath(path);
        }
        return builder.build();
    }

    @TableEndpoint(name = ContentProviderModel.ENDPOINT, contentProvider = ContentProviderObjects.ContentDatabase.class)
    static class ContentProviderModel {
        public static final String ENDPOINT = "ContentProviderModel";

        @ContentUri(path = ENDPOINT + "/#",
            type = ContentType.VND_SINGLE + ENDPOINT,
            segments = {@PathSegment(segment = 1, column = "id")})
        public static Uri withId(long id) {
            return buildUri(String.valueOf(id));
        }

        @Notify(notifyMethod = NotifyMethod.INSERT, paths = {ENDPOINT + "/#"})
        public static Uri[] onInsert(ValuesBucket contentValues) {
            Long id = contentValues.getLong("id");
            return new Uri[]{withId(id)};
        }

        @ContentUri(path = ENDPOINT,
            type = ContentType.VND_MULTIPLE + ENDPOINT)
        public static Uri getCONTENT_URI() {
            return buildUri(ENDPOINT);
        }
    }

    @TableEndpoint(name = NoteModel.ENDPOINT, contentProvider = ContentProviderObjects.ContentDatabase.class)
    static class NoteModel {
        public static final String ENDPOINT = "NoteModel";

        @ContentUri(path = ENDPOINT, type = ContentType.VND_MULTIPLE + ENDPOINT)
        public static Uri getCONTENT_URI() {
            return buildUri(ENDPOINT);
        }
        @ContentUri(path = ENDPOINT + "/#",
            type = ContentType.VND_MULTIPLE + ENDPOINT,
            segments = {@PathSegment(segment = 1, column = "id")})
        public static Uri withId(long id) {
            return buildUri(ENDPOINT, String.valueOf(id));
        }

        @ContentUri(path = ENDPOINT + "/#/#",
            type = ContentType.VND_SINGLE + ContentProviderModel.ENDPOINT,
            segments = {@PathSegment(segment = 2, column = "id"), @PathSegment(segment = 2, column = "isOpen")})
        public static Uri fromList(long id) {
            return buildUri(ENDPOINT, "fromList", String.valueOf(id));
        }

        @ContentUri(path = ENDPOINT + "/#/#",
            type = ContentType.VND_SINGLE + ContentProviderModel.ENDPOINT,
            segments = {@PathSegment(segment = 1, column = "id"), @PathSegment(segment = 2, column = "isOpen")})
        public static Uri withOpenId(long id, boolean isOpen) {
            return buildUri(ENDPOINT, String.valueOf(id), String.valueOf(isOpen));
        }


        @Notify(notifyMethod = NotifyMethod.INSERT, paths = {ENDPOINT})
        public static Uri[] onInsert(ValuesBucket contentValues) {
            Long listId = contentValues.getLong(getContentValuesKey(contentValues, "providerModel"));
            return new Uri[]{ContentProviderModel.withId(listId), fromList(listId)};
        }

        @Notify(notifyMethod = NotifyMethod.INSERT, paths = {ENDPOINT})
        public static Uri onInsert2(ValuesBucket contentValues) {
            Long listId = contentValues.getLong(getContentValuesKey(contentValues, "providerModel"));
            return fromList(listId);
        }

        @Notify(notifyMethod = NotifyMethod.UPDATE, paths = {ENDPOINT + "/#"})
        public static Uri[] onUpdate(Context context, Uri uri) throws DataAbilityRemoteException {

            long noteId = Long.parseLong(uri.getDecodedPathList().get(1));
            ResultSet c = DataAbilityHelper.creator(context).query(uri, new String[]{"noteModel"}, null);
            assert c != null;
            c.goToFirstRow();

            long listId = c.getLong(c.getColumnIndexForName("providerModel"));
            c.close();
            return new Uri[]{withId(noteId), fromList(listId), ContentProviderModel.withId(listId)};
        }

        @Notify(notifyMethod = NotifyMethod.DELETE, paths = {ENDPOINT + "/#"})
        public static Uri[] onDelete(Context context, Uri uri) throws DataAbilityRemoteException {

            long noteId = Long.parseLong(uri.getDecodedPathList().get(1));
            ResultSet c = DataAbilityHelper.creator(context).query(uri, new String[]{"noteModel"}, null);
            assert c != null;
            c.goToFirstRow();

            long listId = c.getLong(c.getColumnIndexForName("providerModel"));
            c.close();
            return new Uri[]{withId(noteId), fromList(listId), ContentProviderModel.withId(listId)};
        }
    }

    @TableEndpoint(name = TestSyncableModel.ENDPOINT, contentProvider = ContentProviderObjects.ContentDatabase.class)
    static class TestSyncableModel {
        public static final String ENDPOINT = "TestSyncableModel";

        @ContentUri(path = ENDPOINT, type = ContentType.VND_MULTIPLE + ENDPOINT)
        public static Uri getCONTENT_URI() {
            return buildUri(ENDPOINT);
        }
    }
}
