package com.dbflow5.provider;

import com.dbflow5.ConflictActionUtils;
import com.dbflow5.UriMatcher;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.annotation.ConflictAction;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityOperation;
import ohos.aafwk.ability.DataAbilityResult;
import ohos.aafwk.content.Intent;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;

import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;

import java.util.ArrayList;
import java.util.Collections;

public final class ContentDatabase_Provider extends BaseContentProvider {
    private static final int ContentProviderModel_CONTENT_URI = 0;

    private final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    public ContentDatabase_Provider(Class<DatabaseHolder> databaseHolderClass) {
        super(databaseHolderClass);
    }

    @Override
    protected void onStart(Intent intent) {
        final String AUTHORITY = "com.grosner.content.test.ContentDatabase";
        MATCHER.addURI(AUTHORITY, "ContentProviderModel", ContentProviderModel_CONTENT_URI);
        super.onStart(intent);
    }

    @Override
    public final String getDatabaseName() {
        return FlowManager.getDatabaseName(ContentProviderObjects.ContentDatabase.class);
    }

    @Override
    public final String getType(Uri uri) {
        String type = null;
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                type = "vnd.ohos.cursor.dir/ContentProviderModel";
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
        return type;
    }

    @Override
    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        ResultSet resultSet = null;
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                resultSet = FlowManager.getDatabase(ContentProviderObjects.ContentDatabase.class).query("ContentProviderModel", columns, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]),
                        null, null, predicates.getOrder());
                break;
            }
        }
        if (resultSet != null) {
            resultSet.setAffectedByUris(getContext(), Collections.singletonList(uri));
        }
        return resultSet;
    }

    @Override
    public int insert(Uri uri, ValuesBucket value) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentProviderObjects.ContentDatabase.class, "ContentProviderModel"));
                long id = FlowManager.getDatabase(ContentProviderObjects.ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, value,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return (int)id;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }

    @Override
    protected final int bulkInsert(Uri uri, ValuesBucket values) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentProviderObjects.ContentDatabase.class, "ContentProviderModel"));
                final long id = FlowManager.getDatabase(ContentProviderObjects.ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
                DataAbilityHelper.creator(this).notifyChange(uri);
                return id > 0 ? 1 : 0;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }

    @Override
    public final int delete(Uri uri, DataAbilityPredicates predicates) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                long count = FlowManager.getDatabase(ContentProviderObjects.ContentDatabase.class).delete("ContentProviderModel", predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }

    @Override
    public int update(Uri uri, ValuesBucket values, DataAbilityPredicates predicates) {
        switch (MATCHER.match(uri)) {
            case ContentProviderModel_CONTENT_URI: {
                ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentProviderObjects.ContentDatabase.class, "ContentProviderModel"));
                long count = FlowManager.getDatabase(ContentProviderObjects.ContentDatabase.class).updateWithOnConflict("ContentProviderModel", values, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]),
                        ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
                if (count > 0) {
                    DataAbilityHelper.creator(this).notifyChange(uri);
                }
                return (int) count;
            }
            default: {
                throw new IllegalArgumentException("Unknown URI" + uri);
            }
        }
    }

    @Override
    public DataAbilityResult[] executeBatch(ArrayList<DataAbilityOperation> operations) {
        return super.executeBatch(operations);
    }
}
