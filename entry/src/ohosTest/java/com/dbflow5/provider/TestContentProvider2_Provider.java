package com.dbflow5.provider;

import com.dbflow5.ConflictActionUtils;
import com.dbflow5.UriMatcher;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.annotation.ConflictAction;
import com.dbflow5.config.DatabaseHolder;
import com.dbflow5.config.FlowManager;
import com.dbflow5.sqlcipher.utils.DatabaseUtils;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;

import com.dbflow5.provider.ContentProviderObjects.ContentDatabase;

import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import java.util.Collections;
import java.util.List;

public final class TestContentProvider2_Provider extends BaseContentProvider {
  private static final int ContentProviderModel_CONTENT_URI = 0;

  private static final int ContentProviderModel_withId = 1;

  private final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

  public TestContentProvider2_Provider(Class<DatabaseHolder> databaseHolderClass) {
    super(databaseHolderClass);
  }

  @Override
  protected void onStart(Intent intent) {
    super.onStart(intent);
    final String AUTHORITY = "com.dbflow5.test.provider";
    MATCHER.addURI(AUTHORITY, "ContentProviderModel", ContentProviderModel_CONTENT_URI);
    MATCHER.addURI(AUTHORITY, "ContentProviderModel/#", ContentProviderModel_withId);
  }

  @Override
  public final String getDatabaseName() {
    return FlowManager.getDatabaseName(ContentProviderObjects.ContentDatabase.class);
  }

  @Override
  public final String getType(Uri uri) {
    String type = null;
    switch(MATCHER.match(uri)) {
      case ContentProviderModel_CONTENT_URI: {
        type = "vnd.ohos.cursor.dir/ContentProviderModel";
        break;
      }
      case ContentProviderModel_withId: {
        type = "vnd.ohos.cursor.item/ContentProviderModel";
        break;
      }
      default: {
        throw new IllegalArgumentException("Unknown URI" + uri);
      }
    }
    return type;
  }

  @Override
  public ResultSet query(Uri uri, String[] projection, DataAbilityPredicates predicates) {
    ResultSet cursor = null;
    switch(MATCHER.match(uri)) {
      case ContentProviderModel_CONTENT_URI: {
        cursor = FlowManager.getDatabase(ContentProviderObjects.ContentDatabase.class).query("ContentProviderModel",
                projection, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]), null, null, predicates.getOrder());
        break;
      }
      case ContentProviderModel_withId: {
        List<String> segments = uri.getDecodedPathList();
        cursor = FlowManager.getDatabase(ContentProviderObjects.ContentDatabase.class).query("ContentProviderModel", projection, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[] {segments.get(1)}), null, null, predicates.getOrder());
        break;
      }
    }
    if (cursor != null) {
      cursor.setAffectedByUris(getContext(), Collections.singletonList(uri));
    }
    return cursor;
  }

  @Override
  public final int insert(Uri uri, ValuesBucket values) {
    switch(MATCHER.match(uri)) {
      case ContentProviderModel_CONTENT_URI: {
        ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
        final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
        DataAbilityHelper.creator(this).notifyChange(uri);
        return (int)id;
      }
      case ContentProviderModel_withId: {
        ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
        final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
        Uri[] notifyUrisonInsert = TestContentProvider2.ContentProviderModel.onInsert(values);
        for (Uri notifyUri: notifyUrisonInsert) {
          DataAbilityHelper.creator(this).notifyChange(notifyUri);
        }
        return (int)id;
      }
      default: {
        throw new IllegalArgumentException("Unknown URI" + uri);
      }
    }
  }

  @Override
  protected final int bulkInsert(Uri uri, ValuesBucket values) {
    switch(MATCHER.match(uri)) {
      case ContentProviderModel_CONTENT_URI: {
        ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
        final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
        DataAbilityHelper.creator(this).notifyChange(uri);
        return id > 0 ? 1 : 0;
      }
      case ContentProviderModel_withId: {
        ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
        final long id = FlowManager.getDatabase(ContentDatabase.class).insertWithOnConflict("ContentProviderModel", null, values,
                ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getInsertOnConflictAction())));
        Uri[] notifyUrisonInsert = TestContentProvider2.ContentProviderModel.onInsert(values);
        for (Uri notifyUri: notifyUrisonInsert) {
          DataAbilityHelper.creator(this).notifyChange(notifyUri);
        }
        return id > 0 ? 1 : 0;
      }
      default: {
        throw new IllegalArgumentException("Unknown URI" + uri);
      }
    }
  }

  @Override
  public final int delete(Uri uri, DataAbilityPredicates predicates) {
    switch(MATCHER.match(uri)) {
      case ContentProviderModel_CONTENT_URI: {
        long count = FlowManager.getDatabase(ContentDatabase.class).delete("ContentProviderModel", predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]));
        if (count > 0) {
          DataAbilityHelper.creator(this).notifyChange(uri);
        }
        return (int) count;
      }
      case ContentProviderModel_withId: {
        List<String> segments = uri.getDecodedPathList();
        long count = FlowManager.getDatabase(ContentDatabase.class).delete("ContentProviderModel", DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"), DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[] {segments.get(1)}));
        if (count > 0) {
          DataAbilityHelper.creator(this).notifyChange(uri);
        }
        return (int) count;
      }
      default: {
        throw new IllegalArgumentException("Unknown URI" + uri);
      }
    }
  }

  @Override
  public int update(Uri uri, ValuesBucket values, DataAbilityPredicates predicates) {
    switch(MATCHER.match(uri)) {
      case ContentProviderModel_CONTENT_URI: {
        ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
        long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("ContentProviderModel", values, predicates.getWhereClause(), predicates.getWhereArgs().toArray(new String[0]),
                ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
        if (count > 0) {
          DataAbilityHelper.creator(this).notifyChange(uri);
        }
        return (int) count;
      }
      case ContentProviderModel_withId: {
        ModelAdapter adapter = FlowManager.getModelAdapter(FlowManager.getTableClassForName(ContentDatabase.class, "ContentProviderModel"));
        List<String> segments = uri.getDecodedPathList();
        long count = FlowManager.getDatabase(ContentDatabase.class).updateWithOnConflict("ContentProviderModel", values, DatabaseUtils.concatenateWhere(predicates.getWhereClause(), "id = ?"),
                DatabaseUtils.appendSelectionArgs(predicates.getWhereArgs().toArray(new String[0]), new String[] {segments.get(1)}),
                ConflictActionUtils.getConflictResolution(ConflictAction.getSQLiteDatabaseAlgorithmInt(adapter.getUpdateOnConflictAction())));
        if (count > 0) {
          DataAbilityHelper.creator(this).notifyChange(uri);
        }
        return (int) count;
      }
      default: {
        throw new IllegalArgumentException("Unknown URI" + uri);
      }
    }
  }
}
