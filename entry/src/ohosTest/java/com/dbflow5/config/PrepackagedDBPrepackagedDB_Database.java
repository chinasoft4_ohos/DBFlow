package com.dbflow5.config;

import com.dbflow5.prepackaged.Dog_Table;
import com.dbflow5.prepackaged.PrepackagedDB;
import java.lang.Class;
import java.lang.Override;
import javax.annotation.Generated;

/**
 * This is generated code. Please do not modify */
@Generated("com.dbflow5.processor.DBFlowProcessor")
public final class PrepackagedDBPrepackagedDB_Database extends PrepackagedDB {
  public PrepackagedDBPrepackagedDB_Database(DatabaseHolder holder) {
    addModelAdapter(new Dog_Table(this), holder);
  }

  @Override
  public final Class<?> associatedDatabaseClassFile() {
    return PrepackagedDB.class;
  }

  @Override
  public final boolean isForeignKeysSupported() {
    return false;
  }

  @Override
  public final boolean backupEnabled() {
    return false;
  }

  @Override
  public final boolean areConsistencyChecksEnabled() {
    return false;
  }

  @Override
  public final int databaseVersion() {
    return 1;
  }
}
