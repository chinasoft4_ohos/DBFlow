package com.dbflow5.config;

import com.dbflow5.provider.ContentProviderModel_Table;
import com.dbflow5.provider.ContentProviderObjects;
import com.dbflow5.provider.NoteModel_Table;
import com.dbflow5.provider.TestSyncableModel_Table;
import java.lang.Class;
import java.lang.Override;

public final class ContentDatabaseContentDatabase_Database extends ContentProviderObjects.ContentDatabase {
  public ContentDatabaseContentDatabase_Database(DatabaseHolder holder) {
    addModelAdapter(new ContentProviderModel_Table(this), holder);
    addModelAdapter(new NoteModel_Table(this), holder);
    addModelAdapter(new TestSyncableModel_Table(this), holder);
  }

  @Override
  public final Class<?> associatedDatabaseClassFile() {
    return ContentProviderObjects.ContentDatabase.class;
  }

  @Override
  public final boolean isForeignKeysSupported() {
    return false;
  }

  @Override
  public final boolean backupEnabled() {
    return false;
  }

  @Override
  public final boolean areConsistencyChecksEnabled() {
    return false;
  }

  @Override
  public final int databaseVersion() {
    return 1;
  }
}
