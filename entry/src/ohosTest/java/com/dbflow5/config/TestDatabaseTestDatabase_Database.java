package com.dbflow5.config;

import com.dbflow5.TestDatabase;
import com.dbflow5.livedata.LiveDataModel_Table;
import com.dbflow5.migration.MigrationModels;
import com.dbflow5.models.Account_Table;
import com.dbflow5.models.AllFieldsModel_Table;
import com.dbflow5.models.AllFieldsQueryModel_QueryTable;
import com.dbflow5.models.Artist_Song_Table;
import com.dbflow5.models.Artist_Table;
import com.dbflow5.models.AuthorNameQuery_QueryTable;
import com.dbflow5.models.AuthorName_QueryTable;
import com.dbflow5.models.AuthorView_ViewTable;
import com.dbflow5.models.Author_Table;
import com.dbflow5.models.AutoIncrementingModel_Table;
import com.dbflow5.models.BlogDeferred_Table;
import com.dbflow5.models.BlogPrimary_Table;
import com.dbflow5.models.BlogRefNoModel_Table;
import com.dbflow5.models.BlogRef_Table;
import com.dbflow5.models.BlogStubbed_Table;
import com.dbflow5.models.Blog_Table;
import com.dbflow5.models.CharModel_Table;
import com.dbflow5.models.Coordinate_Table;
import com.dbflow5.models.Currency_Table;
import com.dbflow5.models.CustomBlobModel_QueryTable;
import com.dbflow5.models.DefaultModel_Table;
import com.dbflow5.models.Dog_Table;
import com.dbflow5.models.DontAssignDefaultModel_Table;
import com.dbflow5.models.DontCreateModel_Table;
import com.dbflow5.models.EnumModel_Table;
import com.dbflow5.models.EnumTypeConverterModel_Table;
import com.dbflow5.models.FeedEntry_Table;
import com.dbflow5.models.Fts3Model_Table;
import com.dbflow5.models.Fts4Model_Table;
import com.dbflow5.models.Fts4VirtualModel2_Table;
import com.dbflow5.models.IndexModel_Table;
import com.dbflow5.models.Inner_Table;
import com.dbflow5.models.InternalClass_Table;
import com.dbflow5.models.Location2_QueryTable;
import com.dbflow5.models.Location_QueryTable;
import com.dbflow5.models.NonNullKotlinModel_Table;
import com.dbflow5.models.NonTypical.nonTypicalClassName_Table;
import com.dbflow5.models.NotNullReferenceModel_Table;
import com.dbflow5.models.NullableNumbers_Table;
import com.dbflow5.models.NumberModel_Table;
import com.dbflow5.models.OneToManyBaseModel_Table;
import com.dbflow5.models.OneToManyModel_Table;
import com.dbflow5.models.OrderCursorModel_Table;
import com.dbflow5.models.Owner_Table;
import com.dbflow5.models.Path_Table;
import com.dbflow5.models.Position2_Table;
import com.dbflow5.models.PositionWithTypeConverter_Table;
import com.dbflow5.models.Position_Table;
import com.dbflow5.models.PriorityView_ViewTable;
import com.dbflow5.models.ProspectQuizEntry_Table;
import com.dbflow5.models.ProspectQuiz_Table;
import com.dbflow5.models.Refund_Table;
import com.dbflow5.models.SimpleCacheObject_Table;
import com.dbflow5.models.SimpleCustomModel_QueryTable;
import com.dbflow5.models.SimpleModel_Table;
import com.dbflow5.models.SimpleQuickCheckModel_Table;
import com.dbflow5.models.Song_Table;
import com.dbflow5.models.SqlListenerModel_Table;
import com.dbflow5.models.SubclassAllFields_Table;
import com.dbflow5.models.TempModel_Table;
import com.dbflow5.models.TestModelChild_Table;
import com.dbflow5.models.TestModelParent_Table;
import com.dbflow5.models.Transfer2_Table;
import com.dbflow5.models.Transfer_Table;
import com.dbflow5.models.TwoColumnModel_Table;
import com.dbflow5.models.TypeConverterModel_Table;
import com.dbflow5.models.UniqueModel_Table;
import com.dbflow5.models.UserInfo_Table;
import com.dbflow5.models.java.JavaModelView_ViewTable;
import com.dbflow5.models.java.JavaModel_Table;
import com.dbflow5.models.java.otherpackage.ExampleModel_Table;
import com.dbflow5.rx2.query.SimpleRXModel_Table;
import java.lang.Class;
import java.lang.Override;

public final class TestDatabaseTestDatabase_Database extends TestDatabase {
  public TestDatabaseTestDatabase_Database(DatabaseHolder holder) {
    addModelAdapter(new Account_Table(holder, this), holder);
    addModelAdapter(new AllFieldsModel_Table(this), holder);
    addModelAdapter(new Artist_Song_Table(this), holder);
    addModelAdapter(new Artist_Table(this), holder);
    addModelAdapter(new Author_Table(this), holder);
    addModelAdapter(new AutoIncrementingModel_Table(this), holder);
    addModelAdapter(new BlogDeferred_Table(this), holder);
    addModelAdapter(new BlogPrimary_Table(this), holder);
    addModelAdapter(new BlogRefNoModel_Table(this), holder);
    addModelAdapter(new BlogRef_Table(this), holder);
    addModelAdapter(new BlogStubbed_Table(this), holder);
    addModelAdapter(new Blog_Table(this), holder);
    addModelAdapter(new CharModel_Table(holder, this), holder);
    addModelAdapter(new Coordinate_Table(this), holder);
    addModelAdapter(new Currency_Table(this), holder);
    addModelAdapter(new DefaultModel_Table(this), holder);
    addModelAdapter(new Dog_Table(this), holder);
    addModelAdapter(new DontAssignDefaultModel_Table(holder, this), holder);
    addModelAdapter(new DontCreateModel_Table(this), holder);
    addModelAdapter(new EnumModel_Table(this), holder);
    addModelAdapter(new EnumTypeConverterModel_Table(this), holder);
    addModelAdapter(new ExampleModel_Table(this), holder);
    addModelAdapter(new FeedEntry_Table(this), holder);
    addModelAdapter(new Fts3Model_Table(this), holder);
    addModelAdapter(new Fts4Model_Table(this), holder);
    addModelAdapter(new Fts4VirtualModel2_Table(this), holder);
    addModelAdapter(new IndexModel_Table(holder, this), holder);
    addModelAdapter(new Inner_Table(this), holder);
    addModelAdapter(new InternalClass_Table(this), holder);
    addModelAdapter(new JavaModel_Table(this), holder);
    addModelAdapter(new LiveDataModel_Table(this), holder);
    addModelAdapter(new NonNullKotlinModel_Table(holder, this), holder);
    addModelAdapter(new NotNullReferenceModel_Table(this), holder);
    addModelAdapter(new NullableNumbers_Table(holder, this), holder);
    addModelAdapter(new NumberModel_Table(this), holder);
    addModelAdapter(new OneToManyBaseModel_Table(this), holder);
    addModelAdapter(new OneToManyModel_Table(this), holder);
    addModelAdapter(new OrderCursorModel_Table(this), holder);
    addModelAdapter(new Owner_Table(this), holder);
    addModelAdapter(new Path_Table(this), holder);
    addModelAdapter(new Position2_Table(holder, this), holder);
    addModelAdapter(new PositionWithTypeConverter_Table(this), holder);
    addModelAdapter(new Position_Table(holder, this), holder);
    addModelAdapter(new ProspectQuizEntry_Table(this), holder);
    addModelAdapter(new ProspectQuiz_Table(this), holder);
    addModelAdapter(new Refund_Table(holder, this), holder);
    addModelAdapter(new SimpleCacheObject_Table(this), holder);
    addModelAdapter(new SimpleModel_Table(this), holder);
    addModelAdapter(new SimpleQuickCheckModel_Table(this), holder);
    addModelAdapter(new SimpleRXModel_Table(this), holder);
    addModelAdapter(new Song_Table(this), holder);
    addModelAdapter(new SqlListenerModel_Table(this), holder);
    addModelAdapter(new SubclassAllFields_Table(this), holder);
    addModelAdapter(new TempModel_Table(this), holder);
    addModelAdapter(new TestModelChild_Table(this), holder);
    addModelAdapter(new TestModelParent_Table(this), holder);
    addModelAdapter(new Transfer2_Table(holder, this), holder);
    addModelAdapter(new Transfer_Table(holder, this), holder);
    addModelAdapter(new TwoColumnModel_Table(this), holder);
    addModelAdapter(new TypeConverterModel_Table(this), holder);
    addModelAdapter(new UniqueModel_Table(this), holder);
    addModelAdapter(new UserInfo_Table(this), holder);
    addModelAdapter(new nonTypicalClassName_Table(this), holder);
    addModelViewAdapter(new PriorityView_ViewTable(this), holder);
    addModelViewAdapter(new AuthorView_ViewTable(this), holder);
    addModelViewAdapter(new JavaModelView_ViewTable(this), holder);
    addRetrievalAdapter(new AllFieldsQueryModel_QueryTable(this), holder);
    addRetrievalAdapter(new AuthorNameQuery_QueryTable(this), holder);
    addRetrievalAdapter(new AuthorName_QueryTable(this), holder);
    addRetrievalAdapter(new CustomBlobModel_QueryTable(holder, this), holder);
    addRetrievalAdapter(new Location2_QueryTable(holder, this), holder);
    addRetrievalAdapter(new Location_QueryTable(holder, this), holder);
    addRetrievalAdapter(new SimpleCustomModel_QueryTable(this), holder);
    addMigration(1, new MigrationModels.FirstMigration());
    addMigration(1, new SecondMigration());
    addMigration(1, new SecondMigration());
    addMigration(1, new TestMigration());
  }

  @Override
  public final Class<?> associatedDatabaseClassFile() {
    return TestDatabase.class;
  }

  @Override
  public final boolean isForeignKeysSupported() {
    return false;
  }

  @Override
  public final boolean backupEnabled() {
    return false;
  }

  @Override
  public final boolean areConsistencyChecksEnabled() {
    return false;
  }

  @Override
  public final int databaseVersion() {
    return 1;
  }
}
