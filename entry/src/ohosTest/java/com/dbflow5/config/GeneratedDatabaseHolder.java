package com.dbflow5.config;

import com.dbflow5.converter.TypeConverters.BigDecimalConverter;
import com.dbflow5.converter.TypeConverters.BigIntegerConverter;
import com.dbflow5.converter.TypeConverters.BooleanConverter;
import com.dbflow5.converter.TypeConverters.CalendarConverter;
import com.dbflow5.converter.TypeConverters.CharConverter;
import com.dbflow5.converter.TypeConverters.DateConverter;
import com.dbflow5.converter.TypeConverters.SqlDateConverter;
import com.dbflow5.converter.TypeConverters.UUIDConverter;
import com.dbflow5.models.ForeignKeyModels;
import com.dbflow5.models.ProspectQuizs;
import com.dbflow5.models.QueryModels;
import com.dbflow5.models.SimpleTestModels;

import java.lang.Boolean;
import java.lang.Character;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.UUID;

public final class GeneratedDatabaseHolder extends DatabaseHolder {
  public GeneratedDatabaseHolder() {
    typeConverters.put(byte[].class, new SimpleTestModels.BlobConverter());
    typeConverters.put(QueryModels.CustomBlobModel.MyBlob.class, new QueryModels.CustomBlobModel.MyTypeConverter());
    typeConverters.put(ForeignKeyModels.DoubleToDouble.class, new ForeignKeyModels.DoubleConverter());
    typeConverters.put(Boolean.class, new BooleanConverter());
    typeConverters.put(Character.class, new CharConverter());
    typeConverters.put(BigDecimal.class, new BigDecimalConverter());
    typeConverters.put(BigInteger.class, new BigIntegerConverter());
    typeConverters.put(Date.class, new SqlDateConverter());
    typeConverters.put(Time.class, new SqlDateConverter());
    typeConverters.put(Timestamp.class, new SqlDateConverter());
    typeConverters.put(Calendar.class, new CalendarConverter());
    typeConverters.put(GregorianCalendar.class, new CalendarConverter());
    typeConverters.put(java.util.Date.class, new DateConverter());
    typeConverters.put(Set.class, new ProspectQuizs.MutableSetTypeConverter());
    typeConverters.put(UUID.class, new UUIDConverter());
    new CipherDatabaseCipherDatabase_Database(this);
    new ContentDatabaseContentDatabase_Database(this);
    new ContentObserverDatabaseContentObserverDatabase_Database(this);
    new MigratedPrepackagedDBMigratedPrepackagedDB_Database(this);
    new PrepackagedDBPrepackagedDB_Database(this);
    new TestDatabaseTestDatabase_Database(this);
    new TestForeignKeyDatabaseTestForeignKeyDatabase_Database(this);
  }
}
