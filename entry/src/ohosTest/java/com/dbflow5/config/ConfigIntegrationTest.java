package com.dbflow5.config;

import com.dbflow5.TestDatabase;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.queriable.ListModelLoader;
import com.dbflow5.adapter.queriable.SingleModelLoader;
import com.dbflow5.adapter.saveable.ModelSaver;
import com.dbflow5.BaseUnitTest;
import com.dbflow5.database.OhosSQLiteOpenHelper;
import com.dbflow5.models.SimpleTestModels;
import org.junit.Assert;
import org.junit.Test;

public class ConfigIntegrationTest extends BaseUnitTest {

    public void setup() {
        FlowManager.reset();
        FlowLog.setMinimumLoggingLevel(FlowLog.Level.V);
    }

    @Test
    public void test_flowConfig() {
        FlowConfig config = FlowConfig.flowConfig(getContext(), builder -> {
            builder.openDatabasesOnInit(true);
            return null;
        });
        Assert.assertTrue(config.openDatabasesOnInit);
        Assert.assertTrue(config.databaseConfigMap.isEmpty());
        Assert.assertTrue(config.databaseHolders.isEmpty());
    }

    @Test
    public void test_tableConfig() {
        setup();
        ListModelLoader<SimpleTestModels.SimpleModel> customListModelLoader = new ListModelLoader<>(SimpleTestModels.SimpleModel.class);
        SingleModelLoader<SimpleTestModels.SimpleModel> singleModelLoader = new SingleModelLoader<>(SimpleTestModels.SimpleModel.class);
        ModelSaver<SimpleTestModels.SimpleModel> modelSaver = new ModelSaver<>();

        FlowManager.init(FlowConfig.flowConfig(getContext(), builder -> {
            builder.database(TestDatabase.class, builder1 -> {
                builder1.table(SimpleTestModels.SimpleModel.class, simpleModelBuilder -> {
                    simpleModelBuilder.singleModelLoader(singleModelLoader);
                    simpleModelBuilder.listModelLoader(customListModelLoader);
                    simpleModelBuilder.modelAdapterModelSaver(modelSaver);
                    return null;
                });
                return null;
            }, OhosSQLiteOpenHelper.createHelperCreator(getContext()));
            return null;
        }));

        FlowConfig flowConfig = FlowManager.getConfig();
        Assert.assertNotNull(flowConfig);

        DatabaseConfig databaseConfig = flowConfig.databaseConfigMap.get(TestDatabase.class);
        Assert.assertNotNull(databaseConfig);

        TableConfig<?> config = databaseConfig.tableConfigMap.get(SimpleTestModels.SimpleModel.class);
        Assert.assertNotNull(config);

        Assert.assertEquals(config.listModelLoader, customListModelLoader);
        Assert.assertEquals(config.singleModelLoader, singleModelLoader);

        ModelAdapter<SimpleTestModels.SimpleModel> modelAdapter = FlowManager.modelAdapter(SimpleTestModels.SimpleModel.class);
        Assert.assertEquals(modelAdapter.getListModelLoader(), customListModelLoader);
        Assert.assertEquals(modelAdapter.getSingleModelLoader(), singleModelLoader);
        Assert.assertEquals(modelAdapter.getModelSaver(), modelSaver);
    }
}
