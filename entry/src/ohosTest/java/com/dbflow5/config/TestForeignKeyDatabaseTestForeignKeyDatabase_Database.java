package com.dbflow5.config;

import com.dbflow5.SimpleForeignModel_Table;
import com.dbflow5.SimpleModel_Table;
import com.dbflow5.TestForeignKeyDatabase;
import java.lang.Class;
import java.lang.Override;

public final class TestForeignKeyDatabaseTestForeignKeyDatabase_Database extends TestForeignKeyDatabase {
  public TestForeignKeyDatabaseTestForeignKeyDatabase_Database(DatabaseHolder holder) {
    addModelAdapter(new SimpleForeignModel_Table(this), holder);
    addModelAdapter(new SimpleModel_Table(this), holder);
  }

  @Override
  public final Class<?> associatedDatabaseClassFile() {
    return TestForeignKeyDatabase.class;
  }

  @Override
  public final boolean isForeignKeysSupported() {
    return true;
  }

  @Override
  public final boolean backupEnabled() {
    return false;
  }

  @Override
  public final boolean areConsistencyChecksEnabled() {
    return false;
  }

  @Override
  public final int databaseVersion() {
    return 1;
  }
}
