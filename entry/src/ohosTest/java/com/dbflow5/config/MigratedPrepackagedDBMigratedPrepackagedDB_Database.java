package com.dbflow5.config;

import com.dbflow5.prepackaged.Dog2_Table;
import com.dbflow5.prepackaged.PrepackagedDB;

import java.lang.Class;
import java.lang.Override;

public final class MigratedPrepackagedDBMigratedPrepackagedDB_Database extends PrepackagedDB.MigratedPrepackagedDB {
  public MigratedPrepackagedDBMigratedPrepackagedDB_Database(DatabaseHolder holder) {
    addModelAdapter(new Dog2_Table(this), holder);
    addMigration(2, new PrepackagedDB.MigratedPrepackagedDB.AddNewFieldMigration());
    addMigration(2, new PrepackagedDB.MigratedPrepackagedDB.AddSomeDataMigration());
  }

  @Override
  public final Class<?> associatedDatabaseClassFile() {
    return PrepackagedDB.MigratedPrepackagedDB.class;
  }

  @Override
  public final boolean isForeignKeysSupported() {
    return false;
  }

  @Override
  public final boolean backupEnabled() {
    return false;
  }

  @Override
  public final boolean areConsistencyChecksEnabled() {
    return false;
  }

  @Override
  public final int databaseVersion() {
    return 2;
  }
}
