package com.dbflow5.config;

import com.dbflow5.sqlcipher.CipherDatabase;
import com.dbflow5.sqlcipher.CipherModel_Table;
import java.lang.Class;
import java.lang.Override;

public final class CipherDatabaseCipherDatabase_Database extends CipherDatabase {
  public CipherDatabaseCipherDatabase_Database(DatabaseHolder holder) {
    addModelAdapter(new CipherModel_Table(this), holder);
  }

  @Override
  public final Class<?> associatedDatabaseClassFile() {
    return CipherDatabase.class;
  }

  @Override
  public final boolean isForeignKeysSupported() {
    return false;
  }

  @Override
  public final boolean backupEnabled() {
    return false;
  }

  @Override
  public final boolean areConsistencyChecksEnabled() {
    return false;
  }

  @Override
  public final int databaseVersion() {
    return 1;
  }
}
