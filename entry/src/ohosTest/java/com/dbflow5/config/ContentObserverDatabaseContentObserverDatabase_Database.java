package com.dbflow5.config;

import com.dbflow5.contentobserver.ContentObserverDatabase;
import com.dbflow5.contentobserver.User_Table;
import java.lang.Class;
import java.lang.Override;

public final class ContentObserverDatabaseContentObserverDatabase_Database extends ContentObserverDatabase {
  public ContentObserverDatabaseContentObserverDatabase_Database(DatabaseHolder holder) {
    addModelAdapter(new User_Table(this), holder);
    addModelAdapter(new User_Table(this), holder);
  }

  @Override
  public final Class<?> associatedDatabaseClassFile() {
    return ContentObserverDatabase.class;
  }

  @Override
  public final boolean isForeignKeysSupported() {
    return false;
  }

  @Override
  public final boolean backupEnabled() {
    return false;
  }

  @Override
  public final boolean areConsistencyChecksEnabled() {
    return false;
  }

  @Override
  public final int databaseVersion() {
    return 1;
  }
}
