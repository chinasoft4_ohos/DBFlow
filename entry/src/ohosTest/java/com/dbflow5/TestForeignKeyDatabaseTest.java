package com.dbflow5;

import com.dbflow5.config.FlowManager;
import com.dbflow5.database.DatabaseStatement;
import org.junit.Test;

public class TestForeignKeyDatabaseTest extends BaseUnitTest {
    @Test
    public void verifyDB() {
        TestForeignKeyDatabase db = FlowManager.getDatabase(TestForeignKeyDatabase.class);
        try (DatabaseStatement statement = db.compileStatement("PRAGMA foreign_keys;")) {
            long enabled = statement.simpleQueryForLong();
            assert (enabled == 1L);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
