package com.dbflow5.livedata;

import com.dbflow5.StringUtils;
import com.dbflow5.adapter.ModelAdapter;
import com.dbflow5.adapter.ObjectType;
import com.dbflow5.config.DBFlowDatabase;
import com.dbflow5.database.DatabaseStatement;
import com.dbflow5.database.DatabaseWrapper;
import com.dbflow5.database.FlowCursor;
import com.dbflow5.query.OperatorGroup;
import com.dbflow5.query.property.IProperty;
import com.dbflow5.query.property.Property;
import java.lang.Class;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;

public final class LiveDataModel_Table extends ModelAdapter<LiveDataModel> {
  /**
   * Primary Key */
  public static final Property<String> id = new Property<String>(LiveDataModel.class, "id");

  public static final Property<Integer> name = new Property<Integer>(LiveDataModel.class, "name");

  public static final IProperty[] ALL_COLUMN_PROPERTIES = new IProperty[]{id,name};

  public LiveDataModel_Table(DBFlowDatabase databaseDefinition) {
    super(databaseDefinition);
  }

  @Override
  public final Class<LiveDataModel> table() {
    return LiveDataModel.class;
  }

  @Override
  public final String getName() {
    return "LiveDataModel";
  }

  @Override
  public final ObjectType getType() {
    return ObjectType.Table;
  }

  @Override
  public final Property getProperty(String columnName) {
    String columnName2 = StringUtils.quoteIfNeeded(columnName);
    switch ((columnName2)) {
      case "id":  {
        return id;
      }
      case "name":  {
        return name;
      }
      default: {
        throw new IllegalArgumentException("Invalid column name passed. Ensure you are calling the correct table's column");
      }
    }
  }

  @Override
  public final IProperty[] getAllColumnProperties() {
    return ALL_COLUMN_PROPERTIES;
  }

  @Override
  public final void bindToInsertStatement(DatabaseStatement statement, LiveDataModel model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    statement.bindLong(2, (long)model.getName());
  }

  @Override
  public final void bindToUpdateStatement(DatabaseStatement statement, LiveDataModel model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
    statement.bindLong(2, (long)model.getName());
    if (model.getId() != null) {
      statement.bindString(3, model.getId());
    } else {
      statement.bindString(3, "");
    }
  }

  @Override
  public final void bindToDeleteStatement(DatabaseStatement statement, LiveDataModel model) {
    if (model.getId() != null) {
      statement.bindString(1, model.getId());
    } else {
      statement.bindString(1, "");
    }
  }

  @Override
  public final String getInsertStatementQuery() {
    return "INSERT INTO LiveDataModel(id,name) VALUES (?,?)";
  }

  @Override
  public final String getSaveStatementQuery() {
    return "INSERT OR REPLACE INTO LiveDataModel(id,name) VALUES (?,?)";
  }

  @Override
  public final String getUpdateStatementQuery() {
    return "UPDATE LiveDataModel SET id=?,name=? WHERE id=?";
  }

  @Override
  public final String getDeleteStatementQuery() {
    return "DELETE FROM LiveDataModel WHERE id=?";
  }

  @Override
  public final String getCreationQuery() {
    return "CREATE TABLE IF NOT EXISTS LiveDataModel(id TEXT, name INTEGER, PRIMARY KEY(id))";
  }

  @Override
  public final LiveDataModel loadFromCursor(FlowCursor cursor, DatabaseWrapper wrapper) {
    LiveDataModel model = new LiveDataModel("", 0);
    model.setId(cursor.getStringOrDefault("id", ""));
    model.setName(cursor.getIntOrDefault("name"));
    return model;
  }

  @Override
  public final OperatorGroup getPrimaryConditionClause(LiveDataModel model) {
    OperatorGroup clause = OperatorGroup.clause();
    clause.and(id.eq(model.getId()));
    return clause;
  }
}
