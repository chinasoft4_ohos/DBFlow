package com.dbflow5.livedata;

import com.dbflow5.TestDatabase;
import com.dbflow5.annotation.PrimaryKey;
import com.dbflow5.annotation.Table;

@Table(database = TestDatabase.class)
public class LiveDataModel {
    @PrimaryKey
    public String id;
    @PrimaryKey
    public int name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public LiveDataModel(String id, int name) {
        this.id=id;
        this.name=name;
    }
}
