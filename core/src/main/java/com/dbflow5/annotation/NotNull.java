package com.dbflow5.annotation;

/**
 * Description: Specifies that a [Column] is not null.
 * Note this simply inserts the constraint into SQLite statements and does not enforce the column's
 * model field nullability, which is separate.
 *
 * In the case of a [ForeignKeyReference], this will make all references [NotNull] based on the
 * [ConflictAction] specified. If you need one to support null, then you need to be explicit
 * in the [ForeignKeyReference].
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface NotNull {
    /**
     * Defines how to handle conflicts for not null column
     *
     * @return a [com.dbflow5.annotation.ConflictAction] enum
     */
    ConflictAction onNullConflict() default ConflictAction.FAIL;
}
