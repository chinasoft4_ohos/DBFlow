package com.dbflow5.annotation;

/**
 * Description: Creates an index for a specified [Column]. A single column can belong to multiple
 * indexes within the same table if you wish.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.FIELD)
public @interface Index {
    /**
     * @return The set of index groups that this index belongs to.
     */
    int[] indexGroups() default {};
}
