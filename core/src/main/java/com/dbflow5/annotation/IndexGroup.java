package com.dbflow5.annotation;

/**
 * Description:
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface IndexGroup {
    int INDEX_GENERIC = -1;

    /**
     * @return The number that each contained [Index] points to, so they can be combined into a single index.
     * If [.INDEX_GENERIC], this will assume a generic index that covers the whole table.
     */
    int number() default INDEX_GENERIC;
    /**
     * @return The name of this index. It must be unique from other [IndexGroup].
     */
    String name();
    /**
     * @return If true, this will disallow duplicate values to be inserted into the table.
     */
    boolean unique() default false;
}

