package com.dbflow5.annotation;

/**
 * Description: Marks a Model class as NOT a [Table], but generates code for retrieving data from a
 * generic query
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(value = {ElementType.TYPE, ElementType.PACKAGE})
public @interface QueryModel {
    /**
     * @return Specify the class of the database to use.
     */
    Class<?> database();

    /**
     * @return If true, all accessible, non-static, and non-final fields are treated as valid fields.
     */
    boolean allFields() default true;

    /**
     * @return If true, we throw away checks for column indexing and simply assume that the cursor returns
     * all our columns in order. This may provide a slight performance boost.
     */
    boolean orderedCursorLookUp() default false;

    /**
     * @return When true, we reassign the corresponding Model's fields to default values when loading
     * from cursor. If false, we assign values only if present in Cursor.
     */
    boolean assignDefaultValuesFromCursor() default true;
}

