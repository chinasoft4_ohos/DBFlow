package com.dbflow5.annotation;

/**
 * Description: marks a single field as a ModelCache creator that is used in the corresponding ModelAdapter.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface ModelCacheField{

}
