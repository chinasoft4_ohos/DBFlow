package com.dbflow5.annotation;

/**
 * Description:
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface UniqueGroup {
    /**
     * The number that columns point to to use this group
     */
    int groupNumber() default  0;
    /**
     * The conflict action that this group takes.
     */
    ConflictAction uniqueConflict() default ConflictAction.FAIL;

}
