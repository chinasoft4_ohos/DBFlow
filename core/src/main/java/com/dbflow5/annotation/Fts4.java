package com.dbflow5.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: Creates a class using the SQLITE FTS4 [https://www.sqlite.org/fts3.html]
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface Fts4 {
    /**
     * Optionally points to a content table that fills this FTS4 with content.
     * The content option allows FTS4 to forego storing the text being indexed and
     * results in significant space savings.
     */
    Class<?> contentTable() default Object.class;
}