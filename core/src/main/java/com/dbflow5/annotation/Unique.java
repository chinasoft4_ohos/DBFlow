package com.dbflow5.annotation;

/**
 * Description: Marks the field as unique, meaning its value cannot be repeated. It is, however,
 * NOT a primary key.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.FIELD)
public @interface Unique {
    /**
     * @return if field is unique. If false, we expect [.uniqueGroups] to be specified.`
     */
    boolean unique() default true;
    /**
     * @return Marks a unique field as part of a unique group. For every unique number entered,
     * it will generate a UNIQUE() column statement.
     */
    int[] uniqueGroups() default {};
    /**
     * Defines how to handle conflicts for a unique column
     *
     * @return a [ConflictAction] enum
     */
    ConflictAction onUniqueConflict() default ConflictAction.FAIL;
}
