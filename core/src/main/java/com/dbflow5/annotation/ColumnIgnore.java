package com.dbflow5.annotation;

/**
 * Description: An annotation used to ignore a column in the [Table.allFields] instance.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.FIELD)
public @interface ColumnIgnore{

}
