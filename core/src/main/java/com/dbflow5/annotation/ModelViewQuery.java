package com.dbflow5.annotation;

import com.dbflow5.sql.Query;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: Represents a field that is a [Query]. This is only meant to be used as a query
 * reference in [ModelView]. This is so the annotation com.dbflow5.processor knows how to access the query of
 * the view.
 */
@Target(value = {ElementType.METHOD/*, ElementType.PROPERTY_GETTER*/})
@Retention(RetentionPolicy.SOURCE)
public @interface ModelViewQuery{

}
