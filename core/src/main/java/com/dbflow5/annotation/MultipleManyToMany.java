package com.dbflow5.annotation;

/**
 * Description: Provides ability to add multiple [ManyToMany] annotations at once.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(value = {ElementType.TYPE, ElementType.TYPE_USE})
public @interface MultipleManyToMany{
    ManyToMany[] value();
}
