package com.dbflow5.annotation;

/**
 * Description: Describes a 1-many relationship. It applies to some method that returns a [List] of Model objects.
 * This annotation can handle loading, deleting, and saving when the current data changes. By default it will call the
 * associated method when the containing class operates.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.METHOD, ElementType.FIELD/*,
        ElementType.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER*/})
@Retention(RetentionPolicy.SOURCE)
public @interface OneToMany {
    /**
     * @return The methods you wish to call it from. By default it's loaded out of the DB.
     */
    OneToManyMethod[] oneToManyMethods() default {OneToManyMethod.LOAD};
    /**
     * @return The name of the list variable to use. If is left blank, we will remove the "get" and then decapitalize the remaining name.
     */
    String variableName() default "";
    /**
     * @return If true, the code generated for this relationship done as efficiently as possible.
     * It will not work on nested relationships, caching, and other code that requires overriding of BaseModel or Model operations.
     */
    boolean efficientMethods() default true;
}



