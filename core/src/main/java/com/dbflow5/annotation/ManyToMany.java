package com.dbflow5.annotation;

/**
 * Description: Builds a many-to-many relationship with another [Table]. Only one table needs to specify
 * the annotation and its assumed that they use primary keys only. The generated
 * class will contain an auto-incrementing primary key by default.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(value = {ElementType.TYPE, ElementType.PACKAGE})
public @interface ManyToMany{
        /**
         * @return The other table class by which this will get merged.
         */
        Class<?> referencedTable();
        /**
         * @return A name that we use as the column name for the referenced table in the
         * generated ManyToMany table class.
         */
        String referencedTableColumnName() default "";
        /**
         * @return A name that we use as the column name for this specific table's name.
         */
        String thisTableColumnName() default "";
        /**
         * @return By default, we generate an auto-incrementing [Long] [PrimaryKey].
         * If false, all [PrimaryKey] of the corresponding tables will be placed as [ForeignKey] and [PrimaryKey]
         * of the generated table instead of using an autoincrementing Long [PrimaryKey].
         */
        boolean generateAutoIncrement() default true;
        /**
         * @return by default, we append {selfTable}{generatedClassSeparator}{referencedTable} or "User_Follower",
         * for example. If you want different name, change this.
         */
        String generatedTableClassName() default "";
        /**
         * @return by default the Models referenced here are not saved prior to saving this
         * object for obvious efficiency reasons.
         * @see ForeignKey.saveForeignKeyModel
         */
        boolean saveForeignKeyModels() default false;
}
