package com.dbflow5.annotation;

/**
 * Description: Creates a class using the SQLITE FTS3 [https://www.sqlite.org/fts3.html]
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface Fts3{

}




