package com.dbflow5.annotation;

/**
 * Description: Marks a Migration class to be included in DB construction. The class using this annotation
 * must implement the Migration interface.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(value = {ElementType.TYPE, ElementType.PACKAGE})
public @interface Migration {
    /**
     * @return The version the migration will trigger at.
     */
    int version();
    /**
     * @return Specify the database class that this migration belongs to.
     */
    Class<?> database();
    /**
     * @return If number greater than -1, the migrations are in run in reverse priority,
     * meaning ones from the same [version] get ordered from
     * lowest to highest number.  if they are the same priority,
     * there is no telling which one is executed first. The
     * annotation com.dbflow5.processor will process in order it finds the classes.
     */
    int priority() default -1;
}
