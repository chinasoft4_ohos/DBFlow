package com.dbflow5.annotation;

/**
 * Description: Maps an arbitrary object and its corresponding fields into a set of columns. It is similar
 * to [ForeignKey] except it's not represented in the DB hierarchy.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.FIELD)
public @interface ColumnMap {
    /**
     * Defines explicit references for a composite [ColumnMap] definition.
     *
     * @return override explicit usage of all fields and provide custom references.
     */
    ColumnMapReference[] references() default {};
}
