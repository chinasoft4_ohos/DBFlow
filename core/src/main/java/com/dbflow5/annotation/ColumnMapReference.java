package com.dbflow5.annotation;

import com.dbflow5.converter.TypeConverters;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: Allows a [ColumnMap] to specify a reference override for its fields. Anything not
 * defined here will not be used.
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.FIELD)
public @interface ColumnMapReference {
    /**
     * @return The local column name that will be referenced in the DB
     */
    String columnName();
    /**
     * @return The column name in the referenced table
     */
    String columnMapFieldName();
    /**
     * @return The default value for the reference column. Same as [Column.defaultValue]
     */
    String defaultValue() default "";

    /**
     * @return A custom type converter that's only used for this field. It will be created and used in
     * the Adapter associated with this table.
     */
    Class<? extends TypeConverters.TypeConverter> typeConverter() default TypeConverters.TypeConverter.class;

    /**
     * @return Specify the [NotNull] annotation here and it will get pasted into the reference definition.
     */
    NotNull notNull() default @NotNull(
            onNullConflict = ConflictAction.NONE
    );
}
