package com.dbflow5.annotation;

/**
 * Description: Marks a field as the IMultiKeyCacheModel that we use to convert multiple fields into
 * a single key for caching.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface MultiCacheField{

}
