package com.dbflow5;

import java.util.Map;
import java.util.function.Function;

public class MapUtils {
    /**
     * 主要用来返回一个Map对象的对应的value, 如果没有则返回默认值并放入Map
     *
     * @param map 源集合
     * @param key 键
     * @param defaultValue 默认值
     * @return value
     */
    public static <K, V> V getOrPut(Map<K, V> map, K key, V defaultValue) {
        V value = map.get(key);
        if(value == null){
            map.put(key, defaultValue);
            value = defaultValue;
        }
        return value;
    }

    /**
     * 主要用来返回一个Map对象的对应的value, 如果没有则返回默认值并放入Map
     *
     * @param map 源集合
     * @param key 键
     * @param fn 操作函数
     * @return value
     */
    public static <K, V> V getOrPut(Map<K, V> map, K key, Function<Void, V> fn) {
        V value = map.get(key);
        if(value == null){
            V answer = fn.apply(null);
            map.put(key, answer);
            value = answer;
        }
        return value;
    }
}
