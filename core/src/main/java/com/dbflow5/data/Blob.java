package com.dbflow5.data;

/**
 * Description: Provides a way to support blob format data.
 */
public final class Blob{
    private final byte[] blob;

    public Blob(byte[] blob) {
        if(blob == null) {
            this.blob = null;
        }else {
            this.blob = blob.clone();
        }
    }

    public byte[] getBlob() {
        if(blob == null) {
            return null;
        }
        return blob.clone();
    }

}
