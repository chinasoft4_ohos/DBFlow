package com.dbflow5.sql;

/**
 * Description: The basic interface for something that has a piece of a query.
 */
public interface Query {

    /**
     * The SQL query statement here
     *
     * @return the SQL query statement here
     */
    String getQuery();
}
