package com.dbflow5.sql;

import com.dbflow5.data.Blob;

import java.util.HashMap;
import java.util.Map;

/**
 * Description: Represents a type that SQLite understands.
 */
public enum SQLiteType {

    /**
     * Represents an integer number in the DB.
     */
    INTEGER,

    /**
     * Represents a floating-point, precise number.
     */
    REAL,

    /**
     * Represents text.
     */
    TEXT,

    /**
     * A column defined by [byte[]] data. Usually reserved for images or larger pieces of content.
     */
    BLOB;

    public static final Map<String, SQLiteType> sTypeMap = new HashMap<>();

    static {
        sTypeMap.put(Byte.TYPE.getName(), INTEGER);
        sTypeMap.put(Short.TYPE.getName(), INTEGER);
        sTypeMap.put(Integer.TYPE.getName(), INTEGER);
        sTypeMap.put(Long.TYPE.getName(), INTEGER);
        sTypeMap.put(Float.TYPE.getName(), REAL);
        sTypeMap.put(Double.TYPE.getName(), REAL);
        sTypeMap.put(Boolean.TYPE.getName(), INTEGER);
        sTypeMap.put(Character.TYPE.getName(), TEXT);

        sTypeMap.put(Byte[].class.getName(), BLOB);
        sTypeMap.put(Byte.class.getName(), INTEGER);
        sTypeMap.put(Short.class.getName(), INTEGER);
        sTypeMap.put(Integer.class.getName(), INTEGER);
        sTypeMap.put(Long.class.getName(), INTEGER);
        sTypeMap.put(Float.class.getName(), REAL);
        sTypeMap.put(Double.class.getName(), REAL);
        sTypeMap.put(Boolean.class.getName(), INTEGER);
        sTypeMap.put(Character.class.getName(), TEXT);
        sTypeMap.put(CharSequence.class.getName(), TEXT);
        sTypeMap.put(String.class.getName(), TEXT);
        sTypeMap.put(Byte[].class.getName(), TEXT);
        sTypeMap.put(Blob.class.getName(), TEXT);
    }

    /**
     * Returns the [SQLiteType] for this class
     *
     * @param className The fully qualified class name
     * @return The type from the class name
     */
    public static SQLiteType get(String className){
        return sTypeMap.get(className);
    }

    public static boolean containsClass(String className){
        return sTypeMap.containsKey(className);
    }
}
