package com.dbflow5.sql;

/**
 * Description:
 */
public interface QueryCloneable<T> {
    T cloneSelf();
}