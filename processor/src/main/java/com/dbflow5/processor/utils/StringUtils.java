package com.dbflow5.processor.utils;

/**
 * Description:
 */
public class StringUtils{
    public static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty() || str.equals("null");
    }

    public static String capitalizeFirstLetter(String str) {
        if (str == null || str.trim().isEmpty()) {
            return str != null? str : "";
        }

        return com.dbflow5.StringUtils.capitalize(str);
    }

    public static String lower(String str) {
        if (str == null || str.trim().isEmpty()) {
            return str != null? str : "";
        }

        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }
}
