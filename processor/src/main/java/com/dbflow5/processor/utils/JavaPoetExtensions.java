package com.dbflow5.processor.utils;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class JavaPoetExtensions{
    public static TypeSpec.Builder overrideFun(TypeSpec.Builder builder, TypeName type, String name, Function<MethodSpec.Builder, Void> codeMethod, ParameterSpec.Builder... params) {
        List<ParameterSpec> list = new ArrayList<>();
        for(ParameterSpec.Builder b : params) {
            list.add(b.build());
        }

        MethodSpec.Builder mBuilder = MethodSpec.methodBuilder(name)
                .returns(type)
                .addParameters(list)
                .addAnnotation(Override.class);
        codeMethod.apply(mBuilder);

        return builder.addMethod(mBuilder.build());
    }

    public static TypeSpec.Builder overrideFun(TypeSpec.Builder builder, Class<?> type, String name, Function<MethodSpec.Builder, Void> codeMethod, ParameterSpec.Builder... params) {
        List<ParameterSpec> list = new ArrayList<>();
        for(ParameterSpec.Builder b : params) {
            list.add(b.build());
        }

        MethodSpec.Builder mBuilder = MethodSpec
                .methodBuilder(name)
                .returns(type)
                .addParameters(list)
                .addAnnotation(Override.class);
        codeMethod.apply(mBuilder);
        return builder.addMethod(mBuilder.build());
    }

    public static MethodSpec overrideFun(TypeName type, String name, Function<MethodSpec.Builder, Void> codeMethod, ParameterSpec.Builder... params) {
        List<ParameterSpec> list = new ArrayList<>();
        for(ParameterSpec.Builder b : params) {
            list.add(b.build());
        }

        MethodSpec.Builder mBuilder = MethodSpec
                .methodBuilder(name)
                .returns(type)
                .addParameters(list)
                .addAnnotation(Override.class);
        codeMethod.apply(mBuilder);
        return mBuilder.build();
    }

    public static MethodSpec overrideFun(Class<?> type, String name, Function<MethodSpec.Builder, Void> codeMethod, ParameterSpec.Builder... params) {
        List<ParameterSpec> list = new ArrayList<>();
        for(ParameterSpec.Builder b : params) {
            list.add(b.build());
        }

        MethodSpec.Builder mBuilder = MethodSpec
                .methodBuilder(name)
                .returns(type)
                .addParameters(list)
            .addAnnotation(Override.class);
        codeMethod.apply(mBuilder);
        return mBuilder.build();
    }

}