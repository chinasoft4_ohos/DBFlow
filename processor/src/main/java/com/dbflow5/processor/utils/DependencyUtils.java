package com.dbflow5.processor.utils;

import com.dbflow5.processor.ProcessorManager;

public class DependencyUtils{
    /**
     * Used to check if class exists on class path, if so, we add the annotation to generated class files.
     *
     * @return if class exists on class path
     */
    public static boolean hasJavaX() {
        return ProcessorManager.manager.elements.getTypeElement("javax.annotation.Generated") != null;
    }
}