package com.dbflow5.processor.utils;

import java.util.function.BiFunction;

/**
 * Description: Multi-let execution.
 */
public class LetUtils{
    public static <A, B, R> void safeLet(A a, B b, BiFunction<A, B, R> fn) {
        if (a != null && b != null) fn.apply(a, b);
    }
}