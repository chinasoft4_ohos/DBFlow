package com.dbflow5.processor;

import com.squareup.javapoet.MethodSpec;

public interface MethodDefinition {
    MethodSpec methodSpec();
}
