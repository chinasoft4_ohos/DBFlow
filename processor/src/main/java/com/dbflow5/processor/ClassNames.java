package com.dbflow5.processor;

import com.dbflow5.annotation.ConflictAction;
import com.squareup.javapoet.ClassName;

/**
 * Description: The static FQCN string file to assist in providing class names for imports and more in the Compiler
 */
public class ClassNames {
    public static final String BASE_PACKAGE = "com.dbflow5";
    public static final String FLOW_MANAGER_PACKAGE = BASE_PACKAGE + ".config";
    public static final String DATABASE_HOLDER_STATIC_CLASS_NAME = "GeneratedDatabaseHolder";
    public static final String CONVERTER = BASE_PACKAGE + ".converter";
    public static final String ADAPTER = BASE_PACKAGE + ".adapter";
    public static final String QUERY_PACKAGE = BASE_PACKAGE + ".query";
    public static final String STRUCTURE = BASE_PACKAGE + ".structure";
    public static final String DATABASE = BASE_PACKAGE + ".database";
    public static final String QUERIABLE = ADAPTER + ".queriable";
    public static final String PROPERTY_PACKAGE = QUERY_PACKAGE + ".property";
    public static final String CONFIG = BASE_PACKAGE + ".config";
    public static final String RUNTIME = BASE_PACKAGE + ".runtime";
    public static final String SAVEABLE = ADAPTER + ".saveable";
    public static final String PROVIDER = BASE_PACKAGE + ".provider";

    public static final ClassName DATABASE_HOLDER = ClassName.get(CONFIG, "DatabaseHolder");
    public static final ClassName FLOW_MANAGER = ClassName.get(CONFIG, "FlowManager");
    public static final ClassName BASE_DATABASE_DEFINITION_CLASSNAME = ClassName.get(CONFIG, "DBFlowDatabase");
    public static final ClassName CONTENT_PROVIDER_DATABASE = ClassName.get(PROVIDER, "ContentProviderDatabase");

    public static final ClassName URI = ClassName.get("ohos.net", "Uri");
    public static final ClassName URI_MATCHER = ClassName.get("ohos.content", "UriMatcher");
    public static final ClassName CURSOR = ClassName.get("ohos.database", "Cursor");
    public static final ClassName FLOW_CURSOR = ClassName.get(DATABASE, "FlowCursor");
    public static final ClassName DATABASE_UTILS = ClassName.get("ohos.database", "DatabaseUtils");
    public static final ClassName CONTENT_VALUES = ClassName.get("ohos.content", "ContentValues");
    public static final ClassName CONTENT_URIS = ClassName.get("ohos.content", "ContentUris");

    public static final ClassName MODEL_ADAPTER = ClassName.get(ADAPTER, "ModelAdapter");
    public static final ClassName RETRIEVAL_ADAPTER = ClassName.get(ADAPTER, "RetrievalAdapter");
    public static final ClassName MODEL = ClassName.get(STRUCTURE, "Model");
    public static final ClassName MODEL_VIEW_ADAPTER = ClassName.get(ADAPTER, "ModelViewAdapter");
    public static final ClassName OBJECT_TYPE = ClassName.get(ADAPTER, "ObjectType");

    public static final ClassName DATABASE_STATEMENT = ClassName.get(DATABASE, "DatabaseStatement");

    public static final ClassName QUERY = ClassName.get(QUERY_PACKAGE, "Query");

    public static final ClassName TYPE_CONVERTER = ClassName.get(CONVERTER, "TypeConverter");
    public static final ClassName TYPE_CONVERTER_GETTER = ClassName.get(PROPERTY_PACKAGE,
        "TypeConvertedProperty.TypeConverterGetter");

    public static final ClassName CONFLICT_ACTION = ClassName.get(ConflictAction.class);

    public static final ClassName CONTENT_VALUES_LISTENER = ClassName.get(QUERY_PACKAGE, "ContentValuesListener");
    public static final ClassName LOAD_FROM_CURSOR_LISTENER = ClassName.get(QUERY_PACKAGE, "LoadFromCursorListener");
    public static final ClassName SQLITE_STATEMENT_LISTENER = ClassName.get(QUERY_PACKAGE, "SQLiteStatementListener");


    public static final ClassName PROPERTY = ClassName.get(PROPERTY_PACKAGE, "Property");
    public static final ClassName TYPE_CONVERTED_PROPERTY = ClassName.get(PROPERTY_PACKAGE, "TypeConvertedProperty");
    public static final ClassName WRAPPER_PROPERTY = ClassName.get(PROPERTY_PACKAGE, "WrapperProperty");

    public static final ClassName IPROPERTY = ClassName.get(PROPERTY_PACKAGE, "IProperty");
    public static final ClassName INDEX_PROPERTY = ClassName.get(PROPERTY_PACKAGE, "IndexProperty");
    public static final ClassName OPERATOR_GROUP = ClassName.get(QUERY_PACKAGE, "OperatorGroup");

    public static final ClassName ICONDITIONAL = ClassName.get(QUERY_PACKAGE, "IConditional");

    public static final ClassName BASE_CONTENT_PROVIDER = ClassName.get(PROVIDER, "BaseContentProvider");

    public static final ClassName BASE_MODEL = ClassName.get(STRUCTURE, "BaseModel");
    public static final ClassName MODEL_CACHE = ClassName.get("$QUERY_PACKAGE.cache", "ModelCache");
    public static final ClassName MULTI_KEY_CACHE_CONVERTER = ClassName.get("$QUERY_PACKAGE.cache", "MultiKeyCacheConverter");
    public static final ClassName SIMPLE_MAP_CACHE = ClassName.get("$QUERY_PACKAGE.cache", "SimpleMapCache");

    public static final ClassName CACHEABLE_MODEL_LOADER = ClassName.get(QUERIABLE, "CacheableModelLoader");
    public static final ClassName SINGLE_MODEL_LOADER = ClassName.get(QUERIABLE, "SingleModelLoader");
    public static final ClassName CACHEABLE_LIST_MODEL_LOADER = ClassName.get(QUERIABLE, "CacheableListModelLoader");
    public static final ClassName LIST_MODEL_LOADER = ClassName.get(QUERIABLE, "ListModelLoader");
    public static final ClassName CACHE_ADAPTER = ClassName.get(ADAPTER, "CacheAdapter");

    public static final ClassName DATABASE_WRAPPER = ClassName.get(DATABASE, "DatabaseWrapper");

    public static final ClassName SQLITE = ClassName.get(QUERY_PACKAGE, "SQLite");

    public static final ClassName CACHEABLE_LIST_MODEL_SAVER = ClassName.get(SAVEABLE, "CacheableListModelSaver");
    public static final ClassName SINGLE_MODEL_SAVER = ClassName.get(SAVEABLE, "ModelSaver");

    public static final ClassName SINGLE_KEY_CACHEABLE_MODEL_LOADER = ClassName.get(QUERIABLE, "SingleKeyCacheableModelLoader");
    public static final ClassName SINGLE_KEY_CACHEABLE_LIST_MODEL_LOADER = ClassName.get(QUERIABLE, "SingleKeyCacheableListModelLoader");

    public static final ClassName NON_NULL = ClassName.get("ohos.support.annotation", "NonNull");
    public static final ClassName NON_NULL_X = ClassName.get("ohos.annotation", "NonNull");

    public static final ClassName GENERATED = ClassName.get("javax.annotation", "Generated");

    public static final ClassName STRING_UTILS = ClassName.get(BASE_PACKAGE, "StringUtils");
}
