package com.dbflow5.processor.definition.column;

import com.dbflow5.StringUtils;
import com.dbflow5.processor.SQLiteHelper;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;

/**
 * Description:
 */
public class DefinitionUtils {
    public static CodeBlock.Builder getCreationStatement(TypeName elementTypeName, TypeName wrapperTypeName, String columnName) {
        String statement = null;

        if (SQLiteHelper.containsType(wrapperTypeName != null? wrapperTypeName : elementTypeName)) {
            statement = SQLiteHelper.get(wrapperTypeName != null? wrapperTypeName : elementTypeName).toString();
        }

        return CodeBlock.builder().add("$L $L", StringUtils.quote(columnName), statement);
    }

    public static String getLoadFromCursorMethodString(TypeName elementTypeName, TypeName wrapperTypeName) {
        String method = "";
        if (SQLiteHelper.containsMethod(wrapperTypeName != null? wrapperTypeName : elementTypeName)) {
            method = SQLiteHelper.getMethod(elementTypeName);
        }
        return method;
    }
}
