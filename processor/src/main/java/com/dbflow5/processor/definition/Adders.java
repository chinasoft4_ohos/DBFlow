package com.dbflow5.processor.definition;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeSpec;

public class Adders{
    /**
     * Description:
     *
     * @author Andrew Grosner (fuzz)
     */
    public interface TypeAdder {
        void addToType(TypeSpec.Builder typeBuilder);
    }

    public interface CodeAdder {
        CodeBlock.Builder addCode(CodeBlock.Builder code);
    }
}