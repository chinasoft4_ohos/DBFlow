package com.dbflow5.processor.definition;

import com.squareup.javapoet.TypeSpec;

/**
 * Description: Simple interface for returning a [TypeSpec].
 */
public interface TypeDefinition {
    TypeSpec typeSpec();
}

