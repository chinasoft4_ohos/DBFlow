package com.dbflow5.processor.definition.behavior;

import com.dbflow5.processor.definition.Adders;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import javax.lang.model.element.Modifier;

/**
 * Description:
 */
public class CreationQueryBehavior implements Adders.TypeAdder {

    public final boolean createWithDatabase;

    public CreationQueryBehavior(boolean createWithDatabase){
        this.createWithDatabase = createWithDatabase;
    }

    @Override
    public void addToType(TypeSpec.Builder typeBuilder) {
        if (!createWithDatabase) {
            typeBuilder.addMethod(MethodSpec
                    .methodBuilder("createWithDatabase")
                    .returns(TypeName.BOOLEAN)
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                    .addStatement("return false")
                    .build());
        }
    }
}