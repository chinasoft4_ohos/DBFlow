package com.dbflow5.processor.definition.behavior;

import com.dbflow5.annotation.ForeignKeyAction;
import com.dbflow5.processor.definition.column.ColumnDefinition;

public class ColumnBehaviors {
    /**
     * Defines how Primary Key columns behave. If has autoincrementing column or ROWID, the [associatedColumn] is not null.
     */
    public static class PrimaryKeyColumnBehavior{
        public boolean hasRowID;
        /**
         * Either [hasRowID] or [hasAutoIncrement] or null.
         */
        public ColumnDefinition associatedColumn;
        public boolean hasAutoIncrement;

        public PrimaryKeyColumnBehavior(boolean hasRowID, ColumnDefinition associatedColumn, boolean hasAutoIncrement) {
            this.hasRowID = hasRowID;
            this.associatedColumn = associatedColumn;
            this.hasAutoIncrement = hasAutoIncrement;
        }
    }


    /**
     * Defines how Foreign Key columns behave.
     */
    public static class ForeignKeyColumnBehavior{
        public ForeignKeyAction onDelete;
        public ForeignKeyAction onUpdate;
        public boolean saveForeignKeyModel;
        public boolean deleteForeignKeyModel;
        public boolean deferred;

        public ForeignKeyColumnBehavior(ForeignKeyAction onDelete, ForeignKeyAction onUpdate, boolean saveForeignKeyModel, boolean deleteForeignKeyModel, boolean deferred) {
            this.onDelete = onDelete;
            this.onUpdate = onUpdate;
            this.saveForeignKeyModel = saveForeignKeyModel;
            this.deleteForeignKeyModel = deleteForeignKeyModel;
            this.deferred = deferred;
        }
    }
}