package com.dbflow5.processor.definition.provider;

import com.dbflow5.contentprovider.annotation.ContentUri;
import com.dbflow5.contentprovider.annotation.PathSegment;
import com.dbflow5.processor.ClassNames;
import com.dbflow5.processor.ProcessorManager;
import com.dbflow5.processor.definition.BaseDefinition;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

/**
 * Description:
 */
public class ContentUriDefinition extends BaseDefinition {

    public String name;

    public String path;
    public String type;
    public boolean queryEnabled;
    public boolean insertEnabled;
    public boolean deleteEnabled;
    public boolean updateEnabled;
    public PathSegment[] segments;

    public ContentUriDefinition(ContentUri contentUri, Element typeElement, ProcessorManager processorManager) {
        super(typeElement, processorManager, processorManager.elements.getPackageOf(typeElement) != null &&
                        processorManager.elements.getPackageOf(typeElement).getQualifiedName() != null?
                processorManager.elements.getPackageOf(typeElement).getQualifiedName().toString() : "");
        name = typeElement.getEnclosingElement().getSimpleName() + "_" + typeElement.getSimpleName();
        path = contentUri.path();
        type = contentUri.type();
        queryEnabled = contentUri.queryEnabled();
        insertEnabled = contentUri.insertEnabled();
        deleteEnabled = contentUri.deleteEnabled();
        updateEnabled = contentUri.updateEnabled();
        segments = contentUri.segments();

        if (typeElement instanceof VariableElement) {
            if (ClassNames.URI != elementTypeName) {
                processorManager.logError("Content Uri field returned wrong type. It must return a Uri");
            }
        } else if (typeElement instanceof ExecutableElement) {
            if (ClassNames.URI != elementTypeName) {
                processorManager.logError("ContentUri method returns wrong type. It must return Uri");
            }
        }
    }
}