package com.dbflow5.processor.definition;

import com.dbflow5.processor.definition.provider.ContentProviderDefinition;
import com.squareup.javapoet.TypeName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description: Provides overarching holder for [DatabaseDefinition], [TableDefinition],
 * and more. So we can safely use.
 */
public class DatabaseObjectHolder {

    public Map<TypeName, TableDefinition> tableDefinitionMap = new HashMap<>();
    public Map<String, TableDefinition> tableNameMap = new HashMap<>();

    public Map<TypeName, QueryModelDefinition> queryModelDefinitionMap = new HashMap<>();
    public Map<TypeName, ModelViewDefinition> modelViewDefinitionMap = new HashMap<>();
    public Map<TypeName, List<ManyToManyDefinition>> manyToManyDefinitionMap = new HashMap<>();
    public Map<TypeName, ContentProviderDefinition> providerMap = new HashMap<>();

    public DatabaseDefinition databaseDefinition = null;
    public void set(DatabaseDefinition databaseDefinition) {
        this.databaseDefinition = databaseDefinition;
        this.databaseDefinition.objectHolder = this;
    }

    /**
     * Retrieve what database class they're trying to reference.
     *
     * @return list
     */
    public List<String> getMissingDBRefs() {
        if (databaseDefinition == null) {
            List<String> list = new ArrayList<>();
            List<String> finalList = list;
            tableDefinitionMap.values().forEach(tableDefinition -> finalList.add("Database "+tableDefinition.associationalBehavior().databaseTypeName+" not found for Table "+tableDefinition.associationalBehavior().name));
            queryModelDefinitionMap.values().forEach(queryModelDefinition -> finalList.add("Database "+queryModelDefinition.associationalBehavior().databaseTypeName+" not found for QueryModel " + queryModelDefinition.elementName));
            modelViewDefinitionMap.values().forEach(modelViewDefinition -> finalList.add("Database "+modelViewDefinition.associationalBehavior().databaseTypeName+" not found for ModelView "+modelViewDefinition.elementName));
            providerMap.values().forEach(contentProviderDefinition -> finalList.add("Database "+contentProviderDefinition.databaseTypeName+" not found for ContentProvider "+contentProviderDefinition.elementName));

            return finalList;

        } else {
            return new ArrayList<>();
        }
    }
}
